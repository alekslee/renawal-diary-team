import { connect } from 'react-redux';
import authorizedApp from './authorizedApp';

import { selectors, actions } from 'core/category';

const mapStateToProps = state => ({
  firstCategory: selectors.getFirstCategory(state),
  category: selectors.getCategory(state),
});

export default connect(mapStateToProps, null)(authorizedApp);
