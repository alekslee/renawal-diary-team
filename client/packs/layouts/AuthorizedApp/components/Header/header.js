import React, { Component } from 'react';
import { defaultMessages } from 'locales/default';
import { intlShape } from 'react-intl';
import { toastr } from 'lib/helpers';
import { object, shape, string, number, func } from 'prop-types';

import SharedHeader from 'components/Header';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { SettingsModal, DeleteConfirmModal } from './components';
import { paths } from 'layouts/constants';
import LogoutIcon from 'images/icons/LogoutIcon';

const propTypes = {
  intl: intlShape.isRequired,
  currentUser: shape({
    avatar: shape({
      thumbUrl: string.isRequired
    }).isRequired,
    displayName: string.isRequired,
    bonusPoints: number.isRequired,
  }).isRequired,
  history: object.isRequired,
  countryCode: string.isRequired,
  logOutDispatch: func.isRequired
};

class Header extends Component {
  state = {
    isDropdownOpen: false,
    isOpenSettingsModal: false,
    isOpenDeleteConfirmModal: false,
  };

  logOutHandler = () => {
    const { logOutDispatch, history, countryCode, intl: { formatMessage } } = this.props;

    const callback = () => {
      toastr.success(formatMessage(defaultMessages.deviseSessionsSignedOut));
      history.push(paths.ROOT.replace(':country', countryCode));
    };
    logOutDispatch({ callback });
  };

  toggle = key => this.setState({ [key]: !this.state[key] });

  render() {
    const {
      logOutHandler,
      toggle,
      state: { isDropdownOpen, isOpenSettingsModal, isOpenDeleteConfirmModal },
      props: { currentUser, intl: { formatMessage } }
    } = this;

    return (
      <SharedHeader>
        <li className='nav-item dropdown login-user'>
          <Dropdown isOpen={ isDropdownOpen } toggle={ () => toggle('isDropdownOpen') } id="user-dropdown-toggle">
            <DropdownToggle className='nav-link dropdown-toggle'>
              <div className='user-avatar' >
                <img src={ currentUser.avatar.thumbUrl } alt='Avatar' />
              </div>
              <div className='user-name' >
                { currentUser.displayName }
              </div>
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem className='dropdown-item'>
                { formatMessage(defaultMessages.points) }
                <span className='points' >
                  + { currentUser.bonusPoints }
                </span>
              </DropdownItem>
              <DropdownItem onClick={ () => toggle('isOpenSettingsModal') } >
                { formatMessage(defaultMessages.setting) }
              </DropdownItem>
              <DropdownItem className='dropdown-logout' onClick={ logOutHandler } >
                { formatMessage(defaultMessages.logOut) }
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </li>

        <li className='nav-item logout'>
          <span className='logout-link' onClick={ logOutHandler }>
            <LogoutIcon />
            <span>{ formatMessage(defaultMessages.logOut) }</span>
          </span>
        </li>

        <SettingsModal isOpen={ isOpenSettingsModal } toggle={toggle} />
        <DeleteConfirmModal isOpen={ isOpenDeleteConfirmModal } toggle={toggle} />
      </SharedHeader>
    );
  }
};

Header.propTypes = propTypes;

export default Header;
