import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { defaultMessages } from 'locales/default';
import { toastr } from 'lib/helpers';
import { bool, func, object } from 'prop-types';

import I18n from 'components/I18n';
import Loader from 'components/Loader';
import { Form } from './components';

const propTypes = {
  isOpen: bool.isRequired,
  toggle: func.isRequired,
  currentUser: object.isRequired,
  updateCurrentUserDispatch: func.isRequired,
  category: object.isRequired,
};

class SettingsModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialValues: {
        displayName: props.currentUser.displayName
      }
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (this.props.currentUser.displayName !== nextProps.currentUser.displayName)
      this.setState({ initialValues: { displayName: nextProps.currentUser.displayName } });
    if (!this.props.isOpen && nextProps.isOpen)
      this.setState({ initialValues: { displayName: this.props.currentUser.displayName } });
  };

  submitHandler = ({ avatar, displayName }) => {
    const { toggle, updateCurrentUserDispatch } = this.props;
    const callback = () => {
      toastr.success('User successfully updated');
      toggle('isOpenSettingsModal');
    };

    updateCurrentUserDispatch({ avatar, displayName, callback });
  };

  render() {
    const {
      submitHandler,
      state: { initialValues },
      props: { isOpen, toggle, category }
    } = this;

    const categoryName = category && category.root && category.root.enName

    return (
      <Modal
        { ...{ isOpen } }
        toggle={ () => toggle('isOpenSettingsModal') }
        className={
          `settings-modal ${categoryName}`
        }
      >
        <ModalHeader
          toggle={ () => toggle('isOpenSettingsModal') }
          charCode='x'
        >
          <I18n text={ defaultMessages.profileSettings } className='modal-title' />
        </ModalHeader>
        <ModalBody>
          <Form onSubmit={ submitHandler } { ...{ initialValues, toggle } } enableReinitialize />
        </ModalBody>
      </Modal>
    );
  }
};
SettingsModal.propTypes = propTypes;

export default SettingsModal;
