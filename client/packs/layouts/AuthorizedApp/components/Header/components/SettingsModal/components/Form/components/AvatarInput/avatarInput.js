import React from 'react';
import { shape, func } from 'prop-types';

const propTypes = {
  input: shape({
    onChange: func.isRequired,
    onBlur: func.isRequired
  })
};

const AvatarInput = ({
  input: {
    onChange,
    onBlur,
    ...inputProps,
  },
  ...props,
}) => {
  const adaptFileEventToValue = delegate => (
    e => delegate(e.target.files[0])
  );

  return (
    <input
      type='file'
      { ...inputProps }
      { ...{ ...props, value: '' } }
      onChange={ adaptFileEventToValue(onChange) }
      onBlur={ adaptFileEventToValue(onBlur) }
    />
  );
};
AvatarInput.propTypes = propTypes;

export default AvatarInput;
