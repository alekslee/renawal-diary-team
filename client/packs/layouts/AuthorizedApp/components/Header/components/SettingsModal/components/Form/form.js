import React, { Component } from 'react';
import { Field } from 'redux-form';
import { func, shape, string } from 'prop-types';
import { defaultMessages } from 'locales/default';

import I18n from 'components/I18n';
import { AvatarInput } from './components';

const propTypes = {
  handleSubmit: func.isRequired,
  toggle: func.isRequired,
  currentUser: shape({
    avatar: shape({
      thumbUrl: string.isRequired
    }).isRequired,
    displayName: string.isRequired
  }).isRequired,
  clearDisplayNameDispatch: func.isRequired
};

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      avatarUrl: props.currentUser.avatar.thumbUrl
    };
  }

  changeAvatarPreview = file => {
    if(file) {
      let reader = new FileReader();

      reader.onload = ({ target: { result } }) => this.setState({ avatarUrl: result });

      reader.readAsDataURL(file);
    }
  };

  render() {
    const {
      changeAvatarPreview,
      state: { avatarUrl },
      props: { handleSubmit, toggle, currentUser, clearDisplayNameDispatch }
    } = this;

    return (
      <form encType='multipart/form-data' onSubmit={ handleSubmit } >
        <div className='avatar'>
          <img src={ avatarUrl } alt='avatar' />
          <Field
            component={ AvatarInput }
            name='avatar'
            id='avatar-input'
            className='avatar-input'
            onChange={ changeAvatarPreview }
          />
          <label htmlFor='avatar-input' >+
            <I18n text={ defaultMessages.addPhoto } />
          </label>
        </div>
        <div className="user-id">
          Your ID number:  <span>{currentUser.uid}</span>
        </div>
        <div className='input-field'>
          <label>
            <I18n text={ defaultMessages.displayName } />
          </label>
          <Field
            type='text'
            component='input'
            name='displayName'
          />
          <div className='reset-input' onClick={ clearDisplayNameDispatch } />
        </div>
        <button type='submit' className='signup-modal-btn'>
          <I18n text={ defaultMessages.update } />
        </button>

        <a href="javascript:;"
          onClick={() => { toggle('isOpenDeleteConfirmModal') }}
          className="delete-account"
        >
          Delete Account
        </a>
      </form>
    );
  }
}

Form.propTypes = propTypes;

export default Form;
