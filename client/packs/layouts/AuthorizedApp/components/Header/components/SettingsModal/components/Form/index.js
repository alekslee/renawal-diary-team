import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, change } from 'redux-form';
import Form from './form';

import { selectors } from 'core/currentUser';

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state)
});

const validate = ({ displayName }) => {
  const errors = {};

  if (!displayName) errors.displayName = 'Required';
  return errors;
};

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    clearDisplayNameDispatch: () => change('userSettingsUpdate', 'displayName', '')
  }, dispatsh)
);

export default reduxForm({ form: 'userSettingsUpdate', validate })(connect(mapStateToProps, mapDispatchToProps)(Form));
