import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DeleteConfirmModal from './deleteConfirmModal';

import { actions, selectors } from 'core/currentUser';
import { selectors as categorySelectors } from 'core/category';

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
  category: categorySelectors.getCategory(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    deleteCurrentUserDispatch: actions.deleteCurrentUser,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(DeleteConfirmModal);
