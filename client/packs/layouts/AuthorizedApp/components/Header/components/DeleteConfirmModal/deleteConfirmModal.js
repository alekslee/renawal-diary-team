import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { toastr } from 'lib/helpers';
import { bool, func, object } from 'prop-types';

const propTypes = {
  isOpen: bool.isRequired,
  toggle: func.isRequired,
  deleteCurrentUserDispatch: func.isRequired,
};

class DeleteConfirmModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inProcess: false,
    };
  }

  render() {
    const {
      submitHandler,
      state: { initialValues, inProcess },
      props: { isOpen, toggle, category, deleteCurrentUserDispatch }
    } = this;

    return (
      <Modal
        className="account-delete-modal"
        { ...{ isOpen, toggle} }
      >
        <ModalHeader toggle={() => toggle('isOpenDeleteConfirmModal') }>

          <div className="modal-title">
            Delete Account
          </div>

        </ModalHeader>

        <ModalBody>
          <div className="delete-account-body">
            Deleting your account will remove all of your information from our database.
            This is cannot be undone
          </div>
        </ModalBody>


        <ModalFooter>
          <Button
            className="btn cancel-btn"
            onClick={() => {
              toggle('isOpenDeleteConfirmModal');
            }}
          >
            Cancel
          </Button>
          <Button
            className="btn delete-btn"
            onClick={() => {
              const callback = () => {
                toastr.success('Your sucess delete user with all data');
              }
              deleteCurrentUserDispatch({ callback });
              toggle('isOpenDeleteConfirmModal');
            }}
          >
            Delete
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
};
DeleteConfirmModal.propTypes = propTypes;

export default DeleteConfirmModal;
