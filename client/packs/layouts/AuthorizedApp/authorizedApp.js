import React, { Fragment } from 'react';
import { any, shape, string } from 'prop-types';
import Loader from 'components/Loader';
import Footer from 'components/Footer';

import { Header } from './components';

const propTypes = {
  children: any,
  firstCategory: shape({
    id: string.isRequired
  }),
  category: shape({
    id: string.isRequired
  }),
};

const AuthorizedApp = ({ children, category, firstCategory }) => {

  return (
    <Fragment>
      { !(firstCategory && category && category.root) && <Loader /> }
      <Header />
      { children }
      <Footer />
    </Fragment>
  );
};
AuthorizedApp.propTypes = propTypes;

export default AuthorizedApp;
