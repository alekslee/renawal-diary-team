import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { any, array, object, shape, string, func, bool } from 'prop-types';

import SubscribeToCategoryChangedMembersChannel from 'components/SubscribeToCategoryChangedMembersChannel';
import { Sidebar, TopBar, SmartReminderSection } from './components';
import BlockUi from 'react-block-ui';

const propTypes = {
  match: shape({
    params: shape({
      slag: string.isRequired
    }).isRequired
  }).isRequired,
  category: shape({
    id: string
  }).isRequired,
  countryCode: string.isRequired,
  currentUser: object,
  categories: array.isRequired,
  flattenCategories: array.isRequired,
  children: any,
  fetchCategoriesDispatch: func.isRequired,
  fetchSortedCategoriesDispatch: func.isRequired,
  fetchSmartReminderLayoutDataDispatch: func.isRequired,
  fetchRenewalDetailScreenDataDispatch: func.isRequired,
  fetchLeaderboardScreenDataDispatch: func.isRequired,
  setCompanyListFiltersDispatch: func.isRequired,
  setCategoriesListDispatch: func.isRequired,
  setCategoryDispatch: func.isRequired,
  setSmartRemindersListDispatch: func.isRequired,
  fetchOnChangeCategoryDataDispatch: func.isRequired,
  changeLoaderStatusDispatch: func.isRequired,
  isLoadingCategory: bool,
};

const childContextTypes = {
  toggleReminderFormModal: func,
  loadLeaderboardRef: func,
}

class SmartReminder extends Component {
  state = {
    isLoading: !(this.props.category && this.props.category.id && this.props.categories.length  && this.props.categories[0].leafChildren),
    inProcess: false,
    windowSize: window.innerWidth,
  };

  getChildContext() {
    return {
      toggleReminderFormModal: () => this.SmartReminderSection.toggleFormModal(),
      loadLeaderboardRef: (ref) => this.childLeaderboard = ref,
    };
  }

  componentDidMount() {
    this.loading = false;
    const {
      state: { isLoading },
      props: { fetchSmartReminderLayoutDataDispatch,
        changeLoaderStatusDispatch,
        fetchRenewalDetailScreenDataDispatch,
        fetchLeaderboardScreenDataDispatch,
        fetchSortedCategoriesDispatch,
        fetchCategoriesDispatch,
        setCategoryDispatch,
        countryCode,
        currentUser,
        categories,
        flattenCategories, category, match: { params: { slag } } }
    } = this;

    if (location.href.match('open-reminders=true')) {
      this.SmartReminderSection && this.SmartReminderSection.toggleFormModal()
    }

    const loadLayautData = (allCategories) => {
      let { id } = (category || {});
      if (!id) id = (allCategories.find(e => e.slag === slag) || {}).id;

      if (isLoading) {
        const callback = () => this.setState({ isLoading: false });
        this.resetCategoryOptions();
        fetchSmartReminderLayoutDataDispatch({ id, callback, errorCallback: callback });
        fetchLeaderboardScreenDataDispatch({ errorCallback: callback });
      }
      if ( !location.pathname.match(/\/have-your-say|\/vote/) ) {
        fetchRenewalDetailScreenDataDispatch({ categoryId: id, callback: (resp) => {
          if (!(resp.renewalDetail || {}).id) {
            this.childLeaderboard && this.childLeaderboard.toggle('isReviewDetailsModalOpen');
          }
        }});
      }
    }

    if (categories.length === 0) {
      const callback = cats => {
        setCategoryDispatch({ category: cats[0].leafChildren[0] });
        const allCategories = cats
          .flatMap((c) => c.leafChildren && c.leafChildren.concat(c))
          .filter(c => c);
        loadLayautData(allCategories);
      };
      const errorCallback = () => this.setState({ isLoading: false });

      fetchSortedCategoriesDispatch({ callback, errorCallback });
    } else {
      loadLayautData(flattenCategories);
    }

    this.setWindowSize();
    window.addEventListener('resize', this.setWindowSize);
  }

  setEmptyCategoryData = () => {
    const { setCategoriesListDispatch, setCategoryDispatch, setSmartRemindersListDispatch } = this.props;
    // setCategoriesListDispatch({ categories: [] });
    // setCategoryDispatch({ category: {} });
    setSmartRemindersListDispatch({ smartReminders: [] });
  }

  resetCategoryOptions = () => {
    const { setCompanyListFiltersDispatch } = this.props;

    setCompanyListFiltersDispatch({
      personalizeFilters: { criteriaQuestions: '{}' },
      companyListKeyWordFilter: { keyWord: '' },
      autocompleateCompanies: [],
      companyListTypeFilter: undefined,
      companyListCountryFilter: undefined,
      companyListCityFilter: undefined,
      companyListAddressFilter: undefined
    })
  }

  UNSAFE_componentWillReceiveProps({ category, fetchLeaderboardScreenDataDispatch, fetchRenewalDetailScreenDataDispatch, fetchSortedCategoriesDispatch, flattenCategories, location: { pathname },  match: { params: { slag } }, fetchOnChangeCategoryDataDispatch }) {
    const nextCategory = flattenCategories.find(e => e.slag === slag);

    if (location.href.match('open-reminders=true')) {
      this.SmartReminderSection && this.SmartReminderSection.toggleFormModal()
    }

    if (pathname !== this.props.location.pathname && category && category.slag && category.slag !== slag && nextCategory && nextCategory.id !== category.id) {
      this.setState({ inProcess: true });
      this.resetCategoryOptions();
      const callback = () => {
        const callback2 = () => {
          this.props.changeLoaderStatusDispatch({ status: false, type: 'category' });
          this.setState({ inProcess: false });
        }

        this.loadingCategory = false;
        fetchLeaderboardScreenDataDispatch({ callback: callback2, errorCallback: callback });

        if ( !location.pathname.match(/\/have-your-say|\/vote/) ) {
          fetchRenewalDetailScreenDataDispatch({ categoryId: nextCategory.id, callback: (resp) => {
            if (!(resp.renewalDetail || {}).id) {
              this.childLeaderboard && this.childLeaderboard.toggle('isReviewDetailsModalOpen');
            }
          }});
        }
      };

      if ( !this.loadingCategory ) {
        fetchOnChangeCategoryDataDispatch({ id: nextCategory.id, callback, errorCallback: callback });
        this.loadingCategory = true;
      }
    }
  };

  componentWillUnmount() {
    this.setEmptyCategoryData()
    window.removeEventListener('resize', this.setWindowSize);
  };

  setWindowSize = () => {
    this.setState({windowSize: window.innerWidth});
  }

  toggle = key => this.setState({ [key]: !this.state[key] });

  render() {
    const {
      state: { isLoading, inProcess, windowSize },
      props: { children, category, isLoadingCategory }
    } = this;

    if (!((category || {}).root || {}).id) return null;

    return (
      <SubscribeToCategoryChangedMembersChannel>
        <div className={ `reminder ${ category.root.enName }-container` }>
          <TopBar />
          <Container>
            <BlockUi tag='div' blocking={ isLoadingCategory }>
              <SmartReminderSection
                loadRef={ref => this.SmartReminderSection = ref }
              />
              <div className='main-wrapper'>
                { windowSize > 991 && <Sidebar /> }
                { children }
              </div>
            </BlockUi>
          </Container>
        </div>
      </SubscribeToCategoryChangedMembersChannel>
    );
  }
};

SmartReminder.propTypes = propTypes;
SmartReminder.childContextTypes = childContextTypes;

export default SmartReminder;
