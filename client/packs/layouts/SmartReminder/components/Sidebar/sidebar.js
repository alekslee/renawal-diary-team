import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Collapse, CardBody, Card, CardHeader, Button } from 'reactstrap';
import classNames from 'classnames';
import { object, array, string, bool } from 'prop-types';
import { PER_PAGE } from './constants';

import I18n from 'components/I18n';
import { defaultMessages } from 'locales/default';
import { paths } from 'layouts/constants';
import CategoryArrow from 'images/icons/CategoryArrow';
import LeaderboardsIcon from 'images/icons/LeaderboardsIcon';
import VoteIcon from 'images/icons/VoteIcon';
import MyNotesIcon from 'images/icons/MyNotesIcon';

const propTypes = {
  history: object.isRequired,
  category: object.isRequired,
  categories: array.isRequired,
  countryCode: string.isRequired,
};

class Sidebar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      currentCategoryId: this.props.category.id,
      activeCategoryId: this.props.category.id,
      isShowMore: this.checkShowMore(props)
    };
  }

  static getDerivedStateFromProps(nextProps, prevState){
    if(nextProps.category.id !== prevState.currentCategoryId){
      return { ...prevState, currentCategoryId: nextProps.category.id, activeCategoryId: nextProps.category.id };
    }
    return prevState;
  }

  checkShowMore = props => {
    const { category, categories } = props;
    const index = categories.findIndex(leafCategory => leafCategory.id === category.id);
    return index >= PER_PAGE;
  }

  switchShowMore = () => this.setState(state => ({ isShowMore: !state.isShowMore }));

  choseCategory = category => {
    const {
      state: { activeCategoryId },
      props: { history, countryCode }
    } = this;


    if (category.id === this.state.activeCategoryId) {
      this.setState({ activeCategoryId: null });
    } else {
      history.push(paths.LEADERBOARD.replace(':country', countryCode).replace(':slag', category.slag));
      if (activeCategoryId !== category.id) {
        this.setState({ activeCategoryId: category.id });
      }
    }
  }

  render() {
    const {
      switchShowMore,
      choseCategory,
      state: { activeCategoryId, isShowMore },
      props: { category, categories, renewalDetail, countryCode }
    } = this;

    const leafCategories = isShowMore ? categories : categories.slice(0, PER_PAGE);

    return(
      <ul className='reminder-sidebar'>
        {
          leafCategories.map((leafCategory, index) => {
            const isOpenCategory = activeCategoryId === leafCategory.id;

            return (
              <li className='reminder-category' key={ `${leafCategory.id}_${index}` }>
                <h4
                  className={ classNames(
                    'reminder-category-title',
                    { 'subscribed' : leafCategory.selected },
                    { 'open' : isOpenCategory }
                  ) }
                  onClick={ () => choseCategory(leafCategory) }
                >
                  { leafCategory.name }
                  <CategoryArrow />
                </h4>
                <Collapse isOpen={ isOpenCategory }>
                  <CardBody>
                    <NavLink
                      to={ paths.MY_NOTES.replace(':country', countryCode).replace(':slag', leafCategory.slag) }
                      className='nav-link'
                    >
                      <MyNotesIcon />
                      <I18n text={ defaultMessages.myNotes } />
                    </NavLink>
                    <NavLink
                      to={ paths.LEADERBOARD.replace(':country', countryCode).replace(':slag', leafCategory.slag) }
                      className='nav-link'
                    >
                      <LeaderboardsIcon />
                      <I18n text={ defaultMessages.leaderboard } />
                    </NavLink>
                    <NavLink
                      to={ paths.VOTE.replace(':country', countryCode).replace(':slag', leafCategory.slag) }
                      className='nav-link'
                    >
                      <VoteIcon />
                      Have your say!
                    </NavLink>
                  </CardBody>
                </Collapse>
              </li>
            );
          })
        }
        {
          categories.length > PER_PAGE && !isShowMore &&
            <span className='show-more'>
              <div className="show-more-wrap" onClick={ switchShowMore }>
                <span >
                  Show more
                  {/*<span className="show-more-icon"/>*/}
                </span>
              </div>
            </span>
        }
        {
          isShowMore &&
            <span className='show-less'>
              <div className="show-more-wrap" onClick={ switchShowMore }>
                <span>
                  Show less
                  {/*<span className="show-less-icon" />*/}
                </span>
              </div>
            </span>
        }

        { renewalDetail.id &&
          <li>
            <div className="widget-container">
              <div className="widget-header">
                <img className="icon" src={ (renewalDetail.currentProvider || {}).avatarUrl }/>
                <div className="name">
                  { (renewalDetail.currentProvider || {}).name }
                </div>
              </div>
              <div className="widget-body">
                You sign up with your current provider since past year
              </div>
              <NavLink
                className="widget-link"
                to={ paths.VOTE.replace(':country', countryCode).replace(':slag', category.slag) }
              >
                Leave your review
              </NavLink>
            </div>
          </li>
        }
      </ul>
    );
  }
}

Sidebar.propTypes = propTypes;

export default Sidebar;
