import { connect } from 'react-redux';
import Sidebar from './sidebar';
import { withRouter } from 'react-router-dom';

import { selectors, actions } from 'core/category';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { selectors as detailSelectors, actions as detailActions } from 'core/renewalDetail';

const mapStateToProps = state => ({
  category: selectors.getCategory(state),
  categories: selectors.getSidebarCategories(state),
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  renewalDetail: detailSelectors.getRenewalDetail(state),
});

export default connect(mapStateToProps, null)(withRouter(Sidebar));
