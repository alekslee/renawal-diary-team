import React, { Component, Fragment } from 'react';
import moment from 'moment';
import { Row, Col } from 'reactstrap';
import Select from 'react-select';
import { object, array, string, number, func } from 'prop-types';
import { defaultMessages } from 'locales/default';
import I18n from 'components/I18n';

import Loader from 'components/Loader';
import InfoIcon from 'images/svg/info.svg';
import SettingsIcon from 'images/icons/SettingsIcon';
import { paths } from 'layouts/constants';
import { MonthsList, ReminderFormModal } from './components';
import { START_YEAR, YEARS_AFTER_CURRENT } from './constants';
import { generateArrayOfYears } from 'lib/helpers/smartReminder';

import VoteIcon from 'images/icons/VoteIcon';
import MyNotesIcon from 'images/icons/MyNotesIcon';
import LeaderboardsIcon from 'images/icons/LeaderboardsIcon';

const propTypes = {
  category: object.isRequired,
  nearestReminder: object,
  history: object.isRequired,
  categoriesOptions: array.isRequired,
  flattenCategories: array.isRequired,
  yearsSelectOptions: array.isRequired,
  countryCode: string.isRequired,
  currentYear: number,
  setSmartReminderCurrentYearDispatch: func.isRequired,
  setSmartReminderYearsDispatch: func.isRequired
};

const contextTypes = {
  toggleReminderFormModal: func,
}

class SmartReminderSection extends Component {

  constructor(props, context) {
    super(props);

    this.state = {
      isOpenFormModal: false,
      selectedDate: new Date(),
      selectedReminder: null
    };

    props.loadRef && props.loadRef(this);
  }


  componentDidMount() {
    const { setSmartReminderCurrentYearDispatch, setSmartReminderYearsDispatch } = this.props;

    const currentYear = moment().year();
    setSmartReminderCurrentYearDispatch({ currentYear });

    const years = generateArrayOfYears(START_YEAR, currentYear + YEARS_AFTER_CURRENT);
    setSmartReminderYearsDispatch({ years });
  };

  onChangeLeafCategorySelect = ({ value: categoryId, label }) => {
    const { countryCode, history, flattenCategories } = this.props;
    const category = flattenCategories.find(el => el.id === categoryId);

    if (category) {
      history.push(paths.LEADERBOARD.replace(':country', countryCode).replace(':slag', category.slag));
    }
  };

  toggleFormModal = (data = { selectedDate: null, selectedReminder: null }) => {
    const { selectedDate, selectedReminder } = data;
    if (this.state.isOpenFormModal) {
      this.setState({ isOpenFormModal: false, selectedDate: null, selectedReminder: null });
    } else {
      this.setState({ isOpenFormModal: true, selectedDate: (selectedDate || new Date()), selectedReminder });
    }
  }

  onChangeSmartReminderYear = year => {
    const { setSmartReminderCurrentYearDispatch } = this.props;
    setSmartReminderCurrentYearDispatch({ currentYear: year.value });
  };

  PathSelect = ({ category, countryCode, history }) => {
    const path = (key) => paths[key].replace(':country', countryCode).replace(':slag', category.slag);

    const options = [
      { value: 'MY_NOTES',
        label: (
          <span className="category-path-option">
            <MyNotesIcon />
            <I18n text={ defaultMessages.myNotes } />
          </span>
        ) },
      { value: 'LEADERBOARD',
        label: (
          <span className="category-path-option">
            <LeaderboardsIcon />
            <I18n text={ defaultMessages.leaderboard } />
          </span>
        ) },
      { value: 'VOTE',
        label: (
          <span className="category-path-option">
            <VoteIcon />
            Have your say!
          </span>
        ) },
    ]

    const onChange = ({ value: v }) => {
      history.push( path(v) );
    };

    const value = options.find(el => location.pathname.includes( path(el.value) )) || options[1];

    return (
      <Select
        className='reminder-select category-path-select'
        classNamePrefix='Select'
        {...{ options, onChange, value }}
      />
    );
  }

  render() {
    const {
      onChangeLeafCategorySelect,
      onChangeSmartReminderYear,
      toggleFormModal,
      PathSelect,
      state: { isOpenFormModal, selectedDate, selectedReminder },
      props: { nearestReminder, category, categoriesOptions, yearsSelectOptions, currentYear, countryCode, history }
    } = this;

    return(
      <Fragment>
        {/*<div className='reminder-block'>*/}
          {/*<div className='reminder-block-top'>*/}
            {/*<h4 className='reminder-block-title'>*/}
              {/*My smart reminders*/}
            {/*</h4>*/}
            {/*<div className='reminder-block-filters'>*/}
              {/*<div className='reminder-block-dropdown'>*/}
                {/*<Select*/}
                  {/*className='reminder-select year-select'*/}
                  {/*classNamePrefix='Select'*/}
                  {/*value={ { value: currentYear, label: currentYear } }*/}
                  {/*options={ yearsSelectOptions }*/}
                  {/*clearable={ false }*/}
                  {/*onChange={ onChangeSmartReminderYear }*/}
                {/*/>*/}
              {/*</div>*/}
              {/*<div className='reminder-block-dropdown'>*/}
                {/*<Select*/}
                  {/*className='reminder-select category-select'*/}
                  {/*classNamePrefix='Select'*/}
                  {/*value={ { value: category.id, label: category.name } }*/}
                  {/*options={ categoriesOptions }*/}
                  {/*onChange={ onChangeLeafCategorySelect.bind(this) }*/}
                {/*/>*/}
              {/*</div>*/}
              {/*/!*<p>224,717 smart reminders set to date</p>*!/*/}
            {/*</div>*/}
            {/*<button*/}
              {/*className='btn reminder-block-btn'*/}
              {/*onClick={ toggleFormModal }*/}
            {/*>*/}
              {/*+ Add reminders*/}
            {/*</button>*/}
          {/*</div>*/}
          {/*<div className='reminder-bottom'>*/}
            {/*{*/}
              {/*currentYear ?*/}
                {/*<MonthsList { ...{ toggleFormModal } } />*/}
                {/*:*/}
            {/*}*/}
            {/*<div className="desktopScreen">*/}
              {/*{*/}
                {/*nearestReminder &&*/}
                {/*<div className="reminder-message-list">*/}
                  {/*<p className='reminder-message'>*/}
                    {/*<span>{ moment(nearestReminder.triggeredAt).format('DD/MM/YYYY') }</span>*/}
                    {/*{` - `}*/}
                    {/*Your next renewal is { `'${ nearestReminder.name }'` }*/}
                  {/*</p>*/}
                {/*</div>*/}
              {/*}*/}
            {/*</div>*/}
          {/*</div>*/}
        {/*</div>*/}
        <div className="tabletScreen categories-switcher categories-switcher-path">
          <h3>Please select category</h3>
          <div className="reminder-block-dropdown">
            <Select
              className='reminder-select category-select'
              classNamePrefix='Select'
              value={ { value: category.id, label: category.name } }
              options={ categoriesOptions }
              onChange={ onChangeLeafCategorySelect.bind(this) }
            />
          </div>
          <div className="reminder-block-dropdown">
            <PathSelect {...{ category, countryCode, history }}/>
          </div>
        </div>


        <ReminderFormModal
          isOpen={ isOpenFormModal }
          { ...{ selectedDate, selectedReminder } }
          toggle={ toggleFormModal }
        />
      </Fragment>
    );
  }
};
SmartReminderSection.propTypes = propTypes;
SmartReminderSection.contextTypes = contextTypes;

export default SmartReminderSection;
