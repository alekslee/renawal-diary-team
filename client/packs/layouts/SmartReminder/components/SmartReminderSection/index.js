import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import SmartReminderSection from './smartReminderSection';

import { selectors, actions } from 'core/smartReminder';
import { selectors as categorySelectors } from 'core/category';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  category: categorySelectors.getCategory(state),
  categoriesOptions: categorySelectors.getCategoriesOptions(state),
  flattenCategories: categorySelectors.getFlattenCategories(state),
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  currentYear: selectors.getCurrentYear(state),
  nearestReminder: selectors.getNearestReminder(state),
  yearsSelectOptions: selectors.getYearsSelectOptions(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setSmartReminderCurrentYearDispatch: actions.setSmartReminderCurrentYear,
    setSmartReminderYearsDispatch: actions.setSmartReminderYears
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SmartReminderSection));
