import React, { Component, Fragment }  from 'react';
import { Field } from 'redux-form';
import { object, array, bool, func, string } from 'prop-types';
import { paths } from 'layouts/constants';
import { toastr } from 'lib/helpers';

import SignInModalLink from 'components/SignInModalLink';
import SelectField from 'components/SelectField';
import DateField from 'components/DateField';
import MobileDateField from 'components/MobileDateField';
import FieldWithErrors from 'components/FieldWithErrors';

const propTypes = {
  category: object,
  history: object,
  countryCode: string.isRequired,
  currentUser: object.isRequired,
  formAttributes: object,
  categoriesOptions: array.isRequired,
  flattenCategories: array.isRequired,
  resetErrors: func.isRequired,
  errors: object,
  isUpdate: bool.isRequired,
  handleSubmit: func.isRequired,
  smartReminders: array.isRequired,
  createSmartReminderDispatch: func.isRequired,
  changePickCategoriesStepDispatch: func.isRequired,
};

class Form extends Component {

  state = {
    switcher: this.props.initialValues.reminderType,
    selectedName: '',
    categoryId: this.props.initialValues.categoryId,
    windowSize: window.innerWidth
  }

  componentDidMount() {
    this.setWindowSize();
    this.changePickCategoriesStep();
  }

  componentWillReceiveProps(props){

    if (!this.state.selectedName && props.initialValues.categoryId ) {
      this.handleCategorySelect(props.initialValues.categoryId)
    }
  }

  changePickCategoriesStep = () => {
    const {currentUser, changePickCategoriesStepDispatch} = this.props;

    if (!currentUser.id) {
      changePickCategoriesStepDispatch({ pickCategoriesStep: false });
    }
  }

  handleChange = ({ target }) => {

    this.setState({ aliasName: target && target.value })
  }

  handleCategorySelect = (categoryId) => {
    const { switcher } = this.state;
    const { flattenCategories } = this.props;

    if (switcher === 'custom') {
      return ;
    }

    const category = flattenCategories.find(e => e.id === categoryId);

    if (category) {
      if ( ["car_auto_tax", "property_tax", "licences", "passport", "government"].includes(category.code) ) {
        this.props.change('reminderType', 'custom');
        this.setState({ switcher: 'custom' });
      }

      const selectedName = category && category.name;

      const callback = () => {
        this.props.change('aliasName', this.generateDefaultAliasName(categoryId))
      };

      this.setState({ selectedName }, callback);
    }
  }

  handleMouse = (ev) => {
    const { aliasName } = this.state;

    if (aliasName) {
      return;
    }

    if (ev.type === 'mouseover') {
      this.props.change('aliasName', null)
    } else {
      this.props.change('aliasName', this.generateDefaultAliasName())
    }
  }

  handleSwitcher = ({ target: { value: switcher }}) => {
    const { categoryId } = this.props.formAttributes;

    if (switcher === 'custom') {
      this.props.change('categoryId', null)
      this.props.change('aliasName', null)
    } else {
      this.props.change('aliasName', this.generateDefaultAliasName())
      this.props.change('categoryId', this.state.categoryId)
    }
    this.props.change('reminderType', switcher)
    this.setState({ switcher, categoryId });
  }

  generateDefaultAliasName = (categoryId=null) => {
    const {
      props: { category, smartReminders },
      state: { selectedName }
    } = this;

    const categoryReminders = smartReminders
      .filter(el => +el.categoryId === +(categoryId || (category && category.id)) )

    return (`${category && category.name || selectedName } ${ (categoryReminders.length + 1) }`);
  }

  RenderCategory = ({ label }) => {
    const {
      handleCategorySelect,
      props: {
        categoriesOptions,
      },
    } = this;

    return (
      <div className='input-field'>
        <span className='input-label'>
          { label }
        </span>
        <Field
          component={ SelectField }
          className='reminder-select category-select'
          classNamePrefix='Select'
          name='categoryId'
          optionsType='nested'
          onChange={ categoryId => handleCategorySelect(categoryId) }
          options={ categoriesOptions }
        />
      </div>
    );
  }

  setWindowSize = () => {
    this.setState({ windowSize: window.innerWidth });
  }

  render() {

    const { handleSwitcher,
      handleMouse,
      handleChange,
      RenderCategory,
      generateDefaultAliasName,
      props: {
        history,
        category,
        currentUser,
        countryCode,
        formAttributes,
        categoriesOptions,
        isUpdate,
        handleSubmit,
        smartReminders,
        errors,
        createSmartReminderDispatch
      },
      state: { switcher, selectedName, windowSize }
    } = this;

    const buttonText = isUpdate ? 'Update reminder' : '+ Add reminder';
    console.log(errors);
    const createReminder = () => {
      if (!formAttributes) return;
      const { categoryId, aliasName, reminderType, triggeredAt } = formAttributes;

      const callback = () => {
        history.push(paths.LEADERBOARD.replace(':country', countryCode).replace(':slag', category && category.slag));
        toastr.success('Your reminder has been set, please Vote for your choice and help others!');
      }
      createSmartReminderDispatch({ categoryId, triggeredAt, aliasName, reminderType, callback });
    };

    const ReminderSwitcher = () => (
      <Fragment>
        <div className="reminder-switcher">
          <div className="switcher-item">
            <label className='custom-radio'>
              <input
                type='radio'
                value="select"
                name="reminderType"
                checked={ switcher === 'select' }
                onChange={ handleSwitcher }
              />
              <span className='radio-mark' />
              <span className="label-text">Select category</span>
            </label>
          </div>

          <div className="switcher-item">
            <label className='custom-radio'>
              <input
                type='radio'
                value="custom"
                name="reminderType"
                checked={ switcher === 'custom' }
                onChange={ handleSwitcher }
              />
              <span className='radio-mark' />
              <span className="label-text">Custom reminder</span>
            </label>
          </div>
        </div>
      </Fragment>
    );

    const dateField = windowSize > 767 ? DateField : MobileDateField;

    return (
      <form onSubmit={ handleSubmit }>

        <ReminderSwitcher />

        { switcher === 'select' ?
          <RenderCategory label="Category:" /> :

          <div className='input-field'>
            <span className='input-label'>Custom name:</span>

            <Field
              meta={{
                touched: !!((errors || {}).aliasName || [])[0],
                error: ((errors || {}).aliasName || [])[0]
              }}
              onChange={() => errors && this.props.resetErrors() }
              component={ FieldWithErrors }
              className="reminder-input w-100"
              placeholder="Enter your custom name…"
              name='aliasName'
             />
          </div>
        }

        <div className='input-field'>
          <span className='input-label'>Renewal Date:</span>
          <Field
            component={ dateField }
            name='triggeredAt'
          />
        </div>

        { switcher === 'custom' ?
          <RenderCategory label="Category (optional):" /> :

          <div className='input-field'
            onMouseOut={ handleMouse }
          >
            <span className='input-label'>Reminder name (optional):</span>

            <Field
              component={ FieldWithErrors }
              onChange={ handleChange }
              defaultValue={ generateDefaultAliasName() }
              className="reminder-input w-100"
              name='aliasName'
             />
          </div>
        }

  {/*
          category && category.renewalDetail && category.renewalDetail.id &&
            <div className='input-field'>
              <span className='input-label'>My Details:</span>
              <Field
                component={ SelectField }
                className='reminder-select'
                classNamePrefix='Select'
                name='renewalDetailId'
                options={ [{ value: category.renewalDetail.id, label: `${category.name} policy 1` }] }
                isClearable
              />
            </div>
        */}
        <div className='details-button'>
          {
            currentUser.id ?
              <button type='submit'>{ buttonText }</button>
              :
              <SignInModalLink onSignUpSuccess={ createReminder }>
                { buttonText }
              </SignInModalLink>
          }
        </div>
      </form>
    );
  };
};
Form.propTypes = propTypes;

export default Form;
