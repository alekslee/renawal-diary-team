import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReminderFormModal from './reminderFormModal';

import { selectors as categorySelectors } from 'core/category';
import { actions, selectors } from 'core/smartReminder';
import { formValueSelector } from 'redux-form';

const mapStateToProps = state => {
  const selector = formValueSelector('smartReminderForm');
  const categoryId = selector(state, 'categoryId') || null;
  const parentCategory = categorySelectors.getCategories(state).find(c => c.leafChildren.find(leaf => (
    leaf.id === categoryId
  )));

  return {
    category: categorySelectors.getCategory(state),
    parentCategory,
    smartReminders: selectors.getSmartReminders(state)
  };
};

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setSmartReminderCurrentYearDispatch: actions.setSmartReminderCurrentYear,
    createSmartReminderDispatch: actions.createSmartReminder,
    updateSmartReminderDispatch: actions.updateSmartReminder,
    destroySmartRemindersDispatch: actions.destroySmartReminders,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(ReminderFormModal);
