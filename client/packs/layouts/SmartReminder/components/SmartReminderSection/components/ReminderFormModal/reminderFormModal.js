import React, { Component, Fragment } from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { toastr } from 'lib/helpers';
import { object, bool, func } from 'prop-types';
import cx from 'classnames';
import { Tabs, Tab } from 'react-bootstrap-tabs';
import { Form, ReminderConfirmModal } from './components';
import Loader from 'components/Loader';
import moment from 'moment';

import Close from 'images/svg/close-dark.svg';
import Owl from 'images/svg/owl.svg';
import DatePicker from 'react-datepicker';
import MobileDateField from 'components/MobileDateField';

const propTypes = {
  category: object.isRequired,
  selectedDate: object,
  selectedReminder: object,
  isOpen: bool.isRequired,
  toggle: func.isRequired,
  createSmartReminderDispatch: func.isRequired,
  updateSmartReminderDispatch: func.isRequired,
  destroySmartRemindersDispatch: func.isRequired,
  setSmartReminderCurrentYearDispatch: func.isRequired,
  parentCategory: object,
};

class ReminderFormModal extends Component {
  state = {
    tabLabel: 'Create Reminder',
    windowSize: window.innerWidth,
  }

  componentDidMount() {
    this.setWindowSize();
  }

  setWindowSize = () => {
    this.setState({ windowSize: window.innerWidth });
  }

  handelChangeListDatePicker = (item, triggeredAt) => {
    const { categoryId, aliasName, reminderType } = item;

    this.submitHandler({ categoryId, triggeredAt, aliasName, reminderType }, item);
  }

  submitHandler = ({ categoryId, triggeredAt, aliasName, reminderType }, reminder = null ) => {
    const { selectedReminder,
      createSmartReminderDispatch,
      updateSmartReminderDispatch,
      setSmartReminderCurrentYearDispatch,
      toggle,
    } = this.props;

    const callback = (resp) => {

      if (selectedReminder || (reminder || {}).id) {
        toastr.success('Smart reminder updated');
      } else {
        toastr.success('Smart reminder created');
      }
    };

    const errorCallback = ({ errors }) => {
      this.setState({ errors });
    };

    if (selectedReminder || (reminder || {}).id) {
      updateSmartReminderDispatch({
        id: (selectedReminder || reminder).id, categoryId, triggeredAt, aliasName, reminderType, callback, errorCallback
      });
    } else {
      createSmartReminderDispatch({ categoryId, triggeredAt, aliasName, reminderType, callback, errorCallback });
    }
    triggeredAt && setTimeout(() => {
      setSmartReminderCurrentYearDispatch({ currentYear: triggeredAt.getFullYear() });
    }, 500);
  };

  handelDeleteReminder = (item) => {
    const { destroySmartRemindersDispatch } = this.props;

    const callback = () => {
      toastr.success('Smart reminder deleted');
    };

    const errorCallback = () => {
    };

    destroySmartRemindersDispatch({ ids: [item.id], callback, errorCallback });
  }

  retrieveFormInitialValues = () => {
    const { category, selectedDate, selectedReminder, aliasName, reminderType } = this.props;
    if (selectedReminder) {
      return { ...selectedReminder, triggeredAt: new Date(selectedReminder.triggeredAt) };
    }
    return {
      categoryId: category.id,
      triggeredAt: selectedDate,
      aliasName: aliasName,
      reminderType: reminderType && reminderType.length > 0 ? reminderType : 'select',
    };
  };

  CreateReminder = () => {
    const {
      submitHandler,
      retrieveFormInitialValues,
      state: { errors },
      props: { selectedReminder, category, isOpen, toggle, parentCategory }
    } = this;

    const isUpdate = !!selectedReminder;

    return (
      <div className="create-reminder-container">
        <Form
          initialValues={ retrieveFormInitialValues() }
          errors={ errors }
          resetErrors={() => { this.setState({ errors: null }) }}
          isUpdate={ isUpdate }
          onSubmit={ submitHandler }
        />
      </div>
    );
  }

  ReminderList = () => {
    const {
      handelChangeListDatePicker,
      props: { smartReminders, category },
      state: { windowSize }
    } = this;

    // const dateField = windowSize > 767 ? DateField : MobileDateField;

    return (
      <div className="list-reminder-container">
        {smartReminders.length > 0 ?
          <ul className="reminder-list">
            {smartReminders.map((item, index) => (
              <li
                key={ item.id  }
                className="reminder-item"
                onClick={ () => this[`datePicker${index}`].setOpen(true) }
              >
                <div
                  className={
                    cx('status', (item.status))
                  }
                />
                <div className="reminder-name">{item.name}</div>
                <div className="reminder-date">{moment(item.triggeredAt).format('LL')}</div>
                <label className="reminder-calendar">
                  <DatePicker
                    ref={ node => this[`datePicker${index}`] = node }
                    key={ `${item.id}-${item.triggeredAt}` }
                    dateFormat='MMMM d, yyyy'
                    minDate={ new Date() }
                    selected={ new Date(item.triggeredAt) }
                    popperPlacement="bottom"
                    onChange={ handelChangeListDatePicker.bind(this) }
                  />
                </label>
                <div className="cancel-reminder">
                  <img
                    src={ Close }
                    alt="Close"
                    onClick={ (ev) => {
                      this.ReminderConfirmModal.toggleModal(item);
                      ev.stopPropagation();
                    } }
                  />
                </div>
              </li>
            ))}
          </ul> : (
            <div className="text-center">
              <div className="empty-reminder-container">
                <div className="empty-state-image">
                  <img src={ Owl } alt="Owl" />
                </div>
                <h3>Be money smart & set your reminder!</h3>
                <p>Set your reminder & we will send you a link to the leaderboard at renewal time</p>
                <button
                  className="btn btn-empty"
                  onClick={ () => {
                    this.setState({ tabLabel: 'Create Reminder' });
                  } }
                >
                  Set reminders now
                </button>
              </div>

            </div>
          )
        }
      </div>
    );
  }

  render() {
    const {
      submitHandler,
      retrieveFormInitialValues,
      handelDeleteReminder,
      ReminderList,
      CreateReminder,
      state: { tabLabel, reminder },
      props: { selectedReminder, category, isOpen, toggle, parentCategory }
    } = this;

    const isUpdate = !!selectedReminder;

    return (
      <Fragment>
        <Modal
          className={
            cx('details-modal', (parentCategory || category.root || {}).enName)
          }
          { ...{ isOpen, toggle } }
        >
          <ModalHeader { ...{ toggle } } charCode='x' >
            { isUpdate ? 'Edit reminder' : 'Add new reminder' }
          </ModalHeader>
          <ModalBody>
            <Tabs
              selected={ tabLabel }
              onSelect={ (index, label) => this.setState({ tabLabel: label }) }
            >
              <Tab label="Create Reminder">
                <CreateReminder />
              </Tab>
              <Tab label="Reminder List">
                <ReminderList />
              </Tab>
            </Tabs>
          </ModalBody>
        </Modal>

        <ReminderConfirmModal
          loadRef={ref => this.ReminderConfirmModal = ref }
          { ...{ handelDeleteReminder } }
        />
      </Fragment>
    );
  }
};
ReminderFormModal.propTypes = propTypes;

export default ReminderFormModal;
