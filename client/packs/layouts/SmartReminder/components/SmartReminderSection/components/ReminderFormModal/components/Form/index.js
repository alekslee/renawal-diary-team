import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, getFormValues } from 'redux-form';
import { withRouter } from 'react-router-dom';
import Form from './form';

import { selectors as categorySelectors } from 'core/category';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { actions, selectors } from 'core/smartReminder';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { actions as signUpActions } from 'core/signUp';

const makeMapStateToProps = () => {
  const getCategory = categorySelectors.makeGetFormSelectedCategory();
  return (state, props) => ({
    countryCode: currentCountrySelectors.getCurrentCountryCode(state),
    smartReminders: selectors.getSmartReminders(state),
    categoriesOptions: categorySelectors.getSmartReminderCategoriesOptions(state),
    flattenCategories: categorySelectors.getFlattenCategories(state),
    category: getCategory(state, props),
    currentUser: currentUserSelectors.getCurrentUser(state),
    formAttributes: getFormValues(props.form)(state)
  });
};

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    createSmartReminderDispatch: actions.createSmartReminder,
    changePickCategoriesStepDispatch: signUpActions.changePickCategoriesStep,
  }, dispatsh)
);

export default reduxForm({
  form: 'smartReminderForm',
})(connect(makeMapStateToProps, mapDispatchToProps)(withRouter(Form)));
