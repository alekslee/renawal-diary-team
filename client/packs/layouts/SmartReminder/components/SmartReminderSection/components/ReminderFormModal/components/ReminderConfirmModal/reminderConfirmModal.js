import React, { Component } from 'react';
import { bool, func, object } from 'prop-types';
import cx from 'classnames';
import { Modal, ModalHeader, ModalFooter, ModalBody, Button } from 'reactstrap';

const propTypes = {
  handelDeleteReminder: func.isRequired,
};

class ReminderConfirmModal extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isOpenModal: false,
      item: {},
    }
    props.loadRef && props.loadRef(this);
  }

  toggleModal = (item = {}) => {
    const { isOpenModal } = this.state;
    this.setState({ isOpenModal: !isOpenModal, item })
  }

  render() {

    const {
      toggleModal,
      props: { handelDeleteReminder },
      state: { isOpenModal, item },
    } = this;

    return (
      <Modal isOpen={ isOpenModal } className="reminder-delete-modal">
        <ModalHeader>
          Do you sure want to delete?
          <div className="close"
            onClick={() => {
              toggleModal();
            }}
          >
            &times;
          </div>
        </ModalHeader>

        <ModalBody>
          <div
            className={
              cx('status', (item.status))
            }
          />
          <h3 className="reminder-name">
            { item.name }
          </h3>
        </ModalBody>

        <ModalFooter>

          <Button
            className="btn delete-btn"
            onClick={() => {
              handelDeleteReminder(item);
              toggleModal();
            }}
          >
            Delete
          </Button>

        </ModalFooter>
      </Modal>
    );
  }
}

ReminderConfirmModal.propTypes = propTypes;

export default ReminderConfirmModal;
