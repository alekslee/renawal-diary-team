import React, { Component, Fragment } from 'react';
import moment from 'moment';
import { Tooltip } from 'reactstrap';
import { toastr } from 'lib/helpers';
import { array, string, number, func, bool } from 'prop-types';

import { REMINDER_ITEM_COLORS } from 'core/smartReminder/constants';
import Loader from 'components/Loader';
import EditIcon from 'images/icons/EditIcon';
import NotificationIcon from 'images/icons/NotificationIcon';

const propTypes = {
  smartReminders: array.isRequired,
  itemId: string.isRequired,
  month: string.isRequired,
  year: number.isRequired,
  isCurrentMonth: bool.isRequired,
  toggleFormModal: func.isRequired,
  destroySmartRemindersDispatch: func.isRequired
};

class Details extends Component {
  state = {
    isOpen: false,
    inProcess: false
  };

  destroyReminder = (reminderId) => {
    const { destroySmartRemindersDispatch } = this.props;


    const callback = () => {
      toastr.success('Reminders successfully stopped');
      this.setState({ inProcess: false });
    };
    const errorCallback = () => this.setState({ inProcess: false });

    this.setState({ inProcess: true });
    destroySmartRemindersDispatch({ ids: [reminderId], callback, errorCallback });
  };

  toggleOpen = () => this.setState({ isOpen: !this.state.isOpen });

  render() {
    const {
      toggleOpen,
      destroyReminder,
      state: { isOpen },
      props: { smartReminders, itemId, month, year, toggleFormModal, isCurrentMonth }
    } = this;


    const selectedDate = (
      isCurrentMonth? moment(): moment().date(1)
    ).month(month).year(year).toDate();

    return (
      <Tooltip
        isOpen={ isOpen }
        toggle={ toggleOpen }
        trigger='hover'
        placement='bottom'
        container='.calendar-wrapper'
        className='reminder-details'
        delay={
          {
            show: 100,
            hide: 200
          }
        }
        autohide={ false }
        target={ itemId }
      >
        {
          smartReminders.length ?
            <Fragment>
              <ul className='reminders-list'>
                {
                  smartReminders.map(reminder => (
                    <li key={ reminder.id }>
                      <span className='point' style={ { backgroundColor: REMINDER_ITEM_COLORS[reminder.status] } } />
                      <span className='reminder-name'>{ reminder.aliasName || reminder.name }</span>
                      <div className='left-side'>
                        <span className='reminder-date'>{ moment(reminder.triggeredAt).format('MMM DD, YYYY') }</span>
                        <button
                          className='btn stop-btn'
                          onClick={ () => destroyReminder(reminder.id) }
                        >
                          Stop
                        </button>
                        <span
                          className='reminder-edit'
                          onClick={ () => toggleFormModal({ selectedReminder: reminder }) }
                        >
                          <EditIcon />
                        </span>
                      </div>
                    </li>
                  ))
                }
              </ul>
              <div className='buttons-group'>
                <button
                  className='btn add-reminder-btn'
                  onClick={ () => toggleFormModal({ selectedDate }) }
                >
                  + Add new Reminder
                </button>
              </div>
            </Fragment>
            :
            <p className='set-reminder-text'>Click on month to set a reminder!</p>
        }
      </Tooltip>
    );
  }
};
Details.propTypes = propTypes;

export default Details;
