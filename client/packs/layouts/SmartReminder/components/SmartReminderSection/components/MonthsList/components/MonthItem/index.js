import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MonthItem from './monthItem';

import { selectors, actions } from 'core/smartReminder';

const makeMapStateToProps = () => {
  const isCurrentMonth = selectors.makeIsCurrentMonth();
  const smartReminders = selectors.makeRemindersForCurrentMonth();
  return (state, props) => ({
    isCurrentMonth: isCurrentMonth(state, props),
    smartReminders: smartReminders(state, props)
  });
};

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
  }, dispatsh)
);

export default connect(makeMapStateToProps, mapDispatchToProps)(MonthItem);
