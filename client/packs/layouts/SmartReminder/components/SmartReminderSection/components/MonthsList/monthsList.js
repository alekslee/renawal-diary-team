import React, { Component } from 'react';
import Slider from 'react-slick';
import moment from 'moment';
import { array, number, func } from 'prop-types';

import { MonthItem } from './components';
import { SLIDER_SETTINGS } from './constants';

const propTypes = {
  years: array.isRequired,
  currentYearIndex: number.isRequired,
  startYear: number.isRequired,
  toggleFormModal: func.isRequired,
  setSmartReminderCurrentYearDispatch: func.isRequired
};

class MonthsList extends Component {
  componentDidUpdate(prevProps) {
    const { currentYearIndex } = this.props;
    if (currentYearIndex !== prevProps.currentYearIndex) this.slider.slickGoTo(currentYearIndex * 12);
  };

  changeCurrentYear = (oldIndex, newIndex) => {
    const { startYear, setSmartReminderCurrentYearDispatch } = this.props;

    const yearIncrement = Math.round(newIndex / 12);
    const oldYearIncrement = Math.round(oldIndex / 12);

    if (oldYearIncrement !== yearIncrement) {
      setSmartReminderCurrentYearDispatch({ currentYear: startYear + yearIncrement });
    };
  };

  render() {
    const {
      changeCurrentYear,
      props: { currentYearIndex, toggleFormModal, years }
    } = this;

    return (
      <div className='calendar-wrapper'>
        <Slider
          ref={ slider => this.slider = slider }
          { ...SLIDER_SETTINGS }
          initialSlide={ currentYearIndex * 12 }
          beforeChange={ changeCurrentYear }
        >
          {
            years.map(year => (
              moment.monthsShort().map(month => (
                <MonthItem key={ month } { ...{ month, year, toggleFormModal } } />
              ))
            ))
          }
        </Slider>
      </div>
    );
  }
};
MonthsList.propTypes = propTypes;

export default MonthsList;
