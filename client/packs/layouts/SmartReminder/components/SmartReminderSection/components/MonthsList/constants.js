export const SLIDER_SETTINGS = {
  infinite: false,
  slidesToShow: 12,
  slidesToScroll: 12,
  swipe: false,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 9,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
  ]
};
