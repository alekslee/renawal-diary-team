import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MonthsList from './monthsList';

import { selectors, actions } from 'core/smartReminder';

const mapStateToProps = state => ({
  currentYearIndex: selectors.getCurrentYearIndex(state),
  startYear: selectors.getStartYear(state),
  years: selectors.getYears(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setSmartReminderCurrentYearDispatch: actions.setSmartReminderCurrentYear
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(MonthsList);
