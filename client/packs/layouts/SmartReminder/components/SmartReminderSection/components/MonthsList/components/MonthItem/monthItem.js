import React, { Fragment } from 'react';
import classNames from 'classnames';
import moment from 'moment';
import { array, string, number, bool, func } from 'prop-types';

import { MAX_REMINDERS_IN_COLLAPSED_MONTH } from './constants';
import { REMINDER_ITEM_COLORS } from 'core/smartReminder/constants';
import { Details } from './components';

const propTypes = {
  smartReminders: array.isRequired,
  month: string.isRequired,
  year: number.isRequired,
  isCurrentMonth: bool.isRequired,
  toggleFormModal: func.isRequired
};

const MonthItem = ({ smartReminders, month, year, isCurrentMonth, toggleFormModal }) => {
  const itemId = `month-${month}-${year}`;
  const collapsedReminders = smartReminders.slice(0, MAX_REMINDERS_IN_COLLAPSED_MONTH);

  const selectedDate = (
    isCurrentMonth? moment(): moment().date(1)
  ).month(month).year(year).toDate();

  return (
    <Fragment>
      <div
        id={ itemId }
        className={ classNames('month', { default: isCurrentMonth }) }
        onClick={ () => toggleFormModal({ selectedDate }) }
      >
        { month.toUpperCase() }
        <div className='month-points'>
          {
            collapsedReminders.map(reminder => (
              <span key={ reminder.id } style={ { backgroundColor: REMINDER_ITEM_COLORS[reminder.status] } } />
            ))
          }
        </div>
      </div>
      <Details { ...{ smartReminders, itemId, month, year, toggleFormModal, isCurrentMonth } } />
    </Fragment>
  );
};
MonthItem.propTypes = propTypes;

export default MonthItem;
