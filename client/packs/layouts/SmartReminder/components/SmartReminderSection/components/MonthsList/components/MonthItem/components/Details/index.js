import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Details from './details';

import { selectors, actions } from 'core/smartReminder';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    destroySmartRemindersDispatch: actions.destroySmartReminders
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(Details);
