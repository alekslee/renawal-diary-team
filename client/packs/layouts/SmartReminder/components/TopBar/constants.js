import React from 'react';
import InsuranceIcon from 'images/icons/InsuranceIcon';
import BroadbandIcon from 'images/icons/BroadbandIcon';
import EnergyIcon from 'images/icons/EnergyIcon';
import SubscriptionsIcon from 'images/icons/SubscriptionsIcon';
import BusinessIcon from 'images/icons/BusinessIcon';

export const ICONS = {
  insurance: <InsuranceIcon />,
  broadband: <BroadbandIcon />,
  energy: <EnergyIcon />,
  subscriptions: <SubscriptionsIcon />,
  business: <BusinessIcon />
};
