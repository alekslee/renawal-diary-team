import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { array, object, string } from 'prop-types';
import classNames from 'classnames';

import { paths } from 'layouts/constants';
import { ICONS } from './constants';
import Slider from 'react-slick';

const propTypes = {
  categories: array.isRequired,
  category: object.isRequired,
  countryCode: string.isRequired,
  currentUser: object,
};

const NAVIGATION_SLIDER_SETTINGS = {
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
};

class TopBar extends Component {
  state = {
    windowSize: window.innerWidth
  }

  componentDidMount() {
    this.setWindowSize();
    window.addEventListener('resize', this.setWindowSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setWindowSize);
  }

  setWindowSize = () => {
    this.setState({ windowSize: window.innerWidth });
  }

  getPath = (category) => (
    paths.LEADERBOARD.replace(':country', this.props.countryCode).replace(':slag', (category.leafChildren[0] || {}).slag)
  )

  DesktopSubHeader = () => {
    const { categories, category } = this.props;

    return (
      <div className='top-bar'>
        {
          categories.map(ctg => (
            <Link
              key={ ctg.id }
              className={ classNames('top-bar-link', { 'active': category.root.id === ctg.id }, ctg.enName) }
              to={ this.getPath(ctg) }
            >
              <div className="">
                { ICONS[ctg.enName] }
              </div>
              <span>{ ctg.name }</span>
            </Link>
          ))
        }
      </div>
    );
  }

  MobileSubHeader = () => {
    const { categories, category, history } = this.props;
    const initialSlide = this.localSlideChanged ? null : categories.findIndex(c => c.id === category.root.id);

    return (
      <Slider
        { ...NAVIGATION_SLIDER_SETTINGS }
        beforeChange={ (_, index) => {
          this.localSlideChanged = true;
          history.push( this.getPath(categories[index]) )
        }}
        className="top-bar home-nav"
        {...{ initialSlide }}
      >
        {
          categories.map(ctg => (
            <div key={ ctg.id }>
              <Link
                className={ classNames('top-bar-link', { 'active': category.root.id === ctg.id }, ctg.enName) }
                to={ this.getPath(ctg) }
              >
                <div>
                  { ICONS[ctg.enName] }
                </div>
                <span>{ ctg.name }</span>
              </Link>
            </div>
          ))
        }
      </Slider>
    );
  }

  render() {
    const {
      MobileSubHeader,
      DesktopSubHeader,
      state: { windowSize },
    } = this;

    return (
      <div className="reminder-top-bar">
        { windowSize > 767 ?
          <DesktopSubHeader /> :
          <MobileSubHeader />
        }
      </div>
    );
  }
}

TopBar.propTypes = propTypes;

export default TopBar;
