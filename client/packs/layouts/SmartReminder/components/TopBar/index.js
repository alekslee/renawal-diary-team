import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import TopBar from './topBar';

import { selectors, actions } from 'core/category';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { selectors as currentUserSelectors } from 'core/currentUser';

const mapStateToProps = state => ({
  category: selectors.getCategory(state),
  categories: selectors.getCategories(state),
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  currentUser: currentUserSelectors.getCurrentUser(state),
});

export default connect(mapStateToProps, null)( withRouter(TopBar) );
