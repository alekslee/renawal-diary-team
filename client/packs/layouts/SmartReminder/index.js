import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import SmartReminder from './smartReminder';

import { selectors, actions } from 'core/category';
import { actions as companyActions } from 'core/company';
import { actions as smartReminderActions } from 'core/smartReminder';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { selectors as loaderSelectors, actions as loaderActions } from 'core/additional';
import { selectors as detailSelectors, actions as detailActions } from 'core/renewalDetail';

const mapStateToProps = state => ({
  category: selectors.getCategory(state),
  categories: selectors.getCategories(state),
  flattenCategories: selectors.getFlattenCategories(state),
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  currentUser: currentUserSelectors.getCurrentUser(state),
  isLoadingCategory: loaderSelectors.getLoadingCategoryStatus(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setCompanyListFiltersDispatch: companyActions.setCompanyListFilters,
    setCompanyListKeyWordFilter: companyActions.setCompanyListKeyWordFilter,
    setCompaniesListForAutocompleate: companyActions.setCompaniesListForAutocompleate,
    setCompaniesListCriteriaQuestionsFilter: companyActions.setCompaniesListCriteriaQuestionsFilter,

    fetchRenewalDetailScreenDataDispatch: detailActions.fetchRenewalDetailScreenData,
    fetchLeaderboardScreenDataDispatch: companyActions.fetchLeaderboardScreenData,
    fetchSortedCategoriesDispatch: actions.fetchSortedCategories,
    fetchSmartReminderLayoutDataDispatch: actions.fetchSmartReminderLayoutData,
    setCategoriesListDispatch: actions.setCategoriesList,
    setCategoryDispatch: actions.setCategory,
    fetchCategoriesDispatch: actions.fetchCategories,
    setSmartRemindersListDispatch: smartReminderActions.setSmartRemindersList,
    fetchOnChangeCategoryDataDispatch: actions.fetchOnChangeCategoryData,
    changeLoaderStatusDispatch: loaderActions.changeLoaderStatus,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SmartReminder));
