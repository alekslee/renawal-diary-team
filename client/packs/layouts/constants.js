const COUNTRY_PREFIX = '/:country';
const CATEGORIES_PREFIX = `${COUNTRY_PREFIX}/categories/:slag`;
export const paths = {
  COUNTRY_PREFIX,
  ROOT: COUNTRY_PREFIX,
  USERS_SIGN_IN: `${COUNTRY_PREFIX}/users/sign_in`,
  BECOME_A_PARTNER: `${COUNTRY_PREFIX}/become_a_partner`,
  OUR_SHARED_MISSION: `${COUNTRY_PREFIX}/our_community`,
  USERS_FORGOT_PASSWORD: `${COUNTRY_PREFIX}/users/password/new`,
  USERS_FORGOT_PASSWORD_UPDATE: `${COUNTRY_PREFIX}/users/password/edit`,
  USERS_RESEND_CONFIRMATION: `${COUNTRY_PREFIX}/users/confirmation/new`,
  USERS_RESEND_CONFIRMATION_ERROR: `${COUNTRY_PREFIX}/users/confirmation`,
  USERS_RESEND_UNLOCK: `${COUNTRY_PREFIX}/users/unlock/new`,
  USERS_RESEND_UNLOCK_ERROR: `${COUNTRY_PREFIX}/users/unlock`,
  USERS_INVITATION_ACCEPT: `${COUNTRY_PREFIX}/users/invitation/accept`,

  CATEGORIES_PREFIX,
  MY_NOTES: `${CATEGORIES_PREFIX}/my_notes`,
  LEADERBOARD: `${CATEGORIES_PREFIX}/leaderboard`,
  VOTE: `${CATEGORIES_PREFIX}/have-your-say`,
  RENEWAL_DETAILS: `${CATEGORIES_PREFIX}/renewal_details`
};