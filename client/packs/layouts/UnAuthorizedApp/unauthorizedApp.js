import React, { Component } from 'react';
import { any, func, object, shape, string } from 'prop-types';
import camelcaseKeys from 'camelcase-keys';
import queryString from 'query-string';

import Loader from 'components/Loader';
import Footer from 'components/Footer';

import { Header, SignUpModal, SignInModal, ForgotPasswordModal } from './components';
import { UnauthorizedModalsContext } from './context';
import { paths } from 'layouts/constants';
import { CLOSED_ALL_MODALS } from './constants';

const propTypes = {
  children: any,
  firstCategory: shape({
    id: string.isRequired
  }),
  category: shape({
    id: string.isRequired
  }),
  setCurrentUserDispatch: func.isRequired,
  history: object.isRequired,
  location: object.isRequired
};

class App extends Component {
  state = {
    ...CLOSED_ALL_MODALS,
    onSignUpSuccess: null
  };

  componentDidMount() {

    const {
      toggleSignInModal,
      props: { location, setCurrentUserDispatch }
    } = this;

    const {
      email,
      googleUid,
      facebookUid,
      twitterUid,
      linkedinUid
    } = camelcaseKeys(queryString.parse(location.search));
    if (email && (googleUid || facebookUid || twitterUid || linkedinUid)) {
      setCurrentUserDispatch({ currentUser: { email, googleUid, facebookUid, twitterUid, linkedinUid } });
      toggleSignInModal();
    }
  };

  toggleDoudleModal = (onSignUpSuccess, modalType) => {
    const {
      state: { isOpenDoubleModal },
      props: { setCurrentUserDispatch, history, location }
    } = this;

    if (isOpenDoubleModal) {
      setCurrentUserDispatch({ currentUser: {} });
      history.push(location.path);
      this.setState({ ...CLOSED_ALL_MODALS, onSignUpSuccess: null });
    } else {
      this.setState({ ...CLOSED_ALL_MODALS, isOpenDoubleModal: true, modalType, onSignUpSuccess });
    };
  }

  toggleSignInModal = (onSignUpSuccess = null) => {
    this.toggleDoudleModal(onSignUpSuccess, 'login');
  };

  toggleSignUpModal = onSignUpSuccess => {
    this.toggleDoudleModal(onSignUpSuccess, 'signup');
  };

  toggleForgotPasswordModal = () => {
    this.setState({ ...CLOSED_ALL_MODALS, isOpenForgotPasswordModal: !this.state.isOpenForgotPasswordModal });
  };

  render() {
    const {
      toggleSignUpModal,
      toggleSignInModal,
      toggleForgotPasswordModal,
      state: { isOpenDoubleModal, isOpenForgotPasswordModal, modalType, onSignUpSuccess },
      props: { children, firstCategory, category }
    } = this;

    return (
      <UnauthorizedModalsContext.Provider
        value={ { toggleSignUpModal, toggleSignInModal, toggleForgotPasswordModal } }
      >
        { !(firstCategory && category && category.root) && <Loader /> }
        <Header />
        <SignUpModal
          isOpen={ isOpenDoubleModal }
          toggle={ toggleSignUpModal }
          modalType={ modalType }
          toggleModalType={modalType => this.setState({ modalType }) }
          { ...{ onSignUpSuccess } }
        />
        <ForgotPasswordModal isOpen={ isOpenForgotPasswordModal } toggle={ toggleForgotPasswordModal } />
        { children }
        <Footer />
      </UnauthorizedModalsContext.Provider>
    );
  }
};

App.propTypes = propTypes;

export default App;
