import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import UnauthorizedApp from './unauthorizedApp';

import { actions } from 'core/currentUser';
import { selectors as categorySelectors } from 'core/category';

const mapStateToProps = state => ({
  firstCategory: categorySelectors.getFirstCategory(state),
  category: categorySelectors.getCategory(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setCurrentUserDispatch: actions.setCurrentUser
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(UnauthorizedApp));
