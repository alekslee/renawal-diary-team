export const CLOSED_ALL_MODALS = {
  isOpenSignUpModal: false,
  isOpenSignInModal: false,
  isOpenDoubleModal: false,
  isOpenForgotPasswordModal: false
}
