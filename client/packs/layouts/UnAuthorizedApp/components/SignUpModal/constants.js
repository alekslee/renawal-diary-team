import { Main, Categories } from './components';

export const MAIN = 'MAIN';
export const CATEGORIES = 'CATEGORIES';

export const BODY_ELEMENTS = {
  [MAIN]: Main,
  [CATEGORIES]: Categories,
};
