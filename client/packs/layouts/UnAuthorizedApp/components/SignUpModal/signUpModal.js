import React, { Component } from 'react';
import { Modal } from 'reactstrap';
import { defaultMessages } from 'locales/default';
import { bool, func, string, object } from 'prop-types';
import classNames from 'classnames';

import { deleteSubscription } from 'lib/utils';
import { subscriptionIds } from 'core/category/constants';
import { MAIN, CATEGORIES, BODY_ELEMENTS } from './constants';

const propTypes = {
  isOpen: bool.isRequired,
  toggle: func.isRequired,
  category: object.isRequired,
  onSignUpSuccess: func,
  toggleModalType: func,
  pickCategoriesStep: bool
};

class SignUpModal extends Component {
  state = {
    bodyElementKey: MAIN
  };

  setBodyElement = bodyElementKey => {
    const { pickCategoriesStep } = this.props;
    if (!pickCategoriesStep && bodyElementKey === CATEGORIES){ return; }
    this.setState({ bodyElementKey });
  };

  toggle = () => {
    const { toggle, isOpen, modalType } = this.props;
    if(isOpen) {
      this.setState({ bodyElementKey: MAIN });
      toggle();
    }
  }

  render() {
    const {
      setBodyElement,
      toggle,
      state: { bodyElementKey },
      props: { isOpen, onSignUpSuccess, toggleModalType, modalType, category }
    } = this;

    const BodyElement = BODY_ELEMENTS[bodyElementKey];
    const categoryName = category && category.root && category.root.enName

    return (
      <Modal
        { ...{ isOpen, toggle } }
        className={
          classNames('signup-modal', categoryName,  { 'categories-modal' : bodyElementKey === CATEGORIES })
        }
      >
        <BodyElement
          { ...{ onSignUpSuccess, toggle, modalType, toggleModalType } }
          setMainBodyElement={ () => setBodyElement(MAIN) }
          setCategoriesBodyElement={ () => setBodyElement(CATEGORIES) }
        />
      </Modal>
    );
  }
};
SignUpModal.propTypes = propTypes;

export default SignUpModal;
