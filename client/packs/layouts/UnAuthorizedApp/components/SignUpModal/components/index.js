import RegistrationTypes from './RegistrationTypes';
import Main from './Main';
import Categories from './Categories';

export { RegistrationTypes, Main, Categories };
