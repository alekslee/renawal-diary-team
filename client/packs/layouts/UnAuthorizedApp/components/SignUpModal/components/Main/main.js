import React, { Fragment, Component } from 'react';
import { ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {bool, func, object, string} from 'prop-types';
import { Tabs, Tab } from 'react-bootstrap-tabs';
import { toastr } from 'lib/helpers';

import GoogleImage from 'images/svg/google.svg';
import FacebookImage from 'images/svg/facebook.svg';
import TwitterImage from 'images/svg/twitt.svg';
import LinkedinImage from 'images/svg/linkedin.svg';

import I18n from 'components/I18n';
import Loader from 'components/Loader';
import { defaultMessages } from 'locales/default';
import { Form } from './components';
import LoginForm from '../../../SignInModal/components/Form';
import { oauthSignupUrls } from './constants';
import { paths } from 'layouts/constants';
import { oauthSigninUrls } from '../../../SignInModal/constants';
import SignUpModalLink from 'components/SignUpModalLink';
import Categories from '../Categories/categories';

const propTypes = {
  toggle: func.isRequired,
  toggleModalType: func.isRequired,
  signInDispatch: func.isRequired,
  setCategoriesBodyElement: func.isRequired,
  setCurrentUserDispatch: func.isRequired,
  signUpFormInitialValues: object.isRequired,
  currentUser: object.isRequired,
  checkUserFieldsUniquenessDispatch: func.isRequired,
  category: object.isRequired,
  signUpDispatch: func.isRequired,
  onSignUpSuccess: func,
  pickCategoriesStep: bool,
  history: object.isRequired,
  currentCountryCode: string.isRequired,
};

class Main extends Component {
  state = {
    inProcess: false
  };

  loginSubmitHandler = ({ email, password }) => {
    const { history, signInDispatch, onSignUpSuccess } = this.props;

    const callback = ({ country }) => {
      history.push(paths.ROOT.replace(':country', country.isoA2Code));
      toastr.success('', { component: <I18n text={ defaultMessages.deviseSessionsSignedIn } /> });
      onSignUpSuccess && onSignUpSuccess();
    };
    signInDispatch({ email, password, callback });
  };

  signUpHandler = () => {
    const {
      props: {
        category,
        signUpDispatch,
        onSignUpSuccess,
      }
    } = this;

    const callback = () => {
      onSignUpSuccess && onSignUpSuccess();
    };

    let categoriesAttributes = { categoryId: Number(category.id) };
    signUpDispatch({ categoriesAttributes, callback });
  };

  submitHandler = ({ email, password, displayName, location }) => {
    const {
      signUpHandler,
      props: {
        setCurrentUserDispatch,
        currentUser,
        setCategoriesBodyElement,
        onSignUpSuccess,
        checkUserFieldsUniquenessDispatch,
        pickCategoriesStep,
        history,
        currentCountryCode,
      }
    } = this;

    const callback = () => {
      setCurrentUserDispatch({ currentUser: { ...currentUser, email, password, displayName, ...location } });
      if (!pickCategoriesStep) {
        signUpHandler();
        history.push(paths.ROOT.replace(':country', currentCountryCode));
      } else {
        setCategoriesBodyElement();
        onSignUpSuccess && onSignUpSuccess();
      }
    };
    const errorCallback = () => this.setState({ inProcess: false });

    this.setState({ inProcess: true });
    checkUserFieldsUniquenessDispatch({ email, displayName, callback, errorCallback });
  };

  render() {
    const {
      submitHandler,
      loginSubmitHandler,
      state: { inProcess },
      props: { toggle, signUpFormInitialValues, toggleModalType, modalType },
    } = this;

    const SignUpContent = () => (
      <Fragment>
        <ModalBody className="login-block">
          { inProcess && <Loader /> }
          <Form initialValues={ signUpFormInitialValues } onSubmit={ submitHandler } />
        </ModalBody>
        <ModalFooter>
          <div className='or-block'>
            <div className='dash' />
            <span>
              <I18n text={ defaultMessages.or } />
            </span>
            <div className='dash' />
          </div>
          <div className='social-login'>
            <I18n text={ defaultMessages.signUpViaASocialLogin } />
          </div>
          <div className='social-block'>
            <a href={ oauthSignupUrls.GOOGLE } className='btn btn-circle google-background'>
              <img src={ GoogleImage } alt='Google' className='google-img' />
            </a>
            <a href={ oauthSignupUrls.FACEBOOK } className='btn btn-circle facebook-background' >
              <img src={ FacebookImage } alt='Facebook' className='facebook-img' />
            </a>
            <a href={ oauthSignupUrls.TWITTER } className='btn btn-circle twitter-background' >
              <img src={ TwitterImage } alt='Twitter' className='twitt-img' />
            </a>
            <a href={ oauthSignupUrls.LINKEDIN } className='btn btn-circle linkedin-background' >
              <img src={ LinkedinImage } alt='Linkedin' className='linkedin-img' />
            </a>
          </div>
        </ModalFooter>
      </Fragment>
    );

    const LoginContent = () => (
      <Fragment>
        <ModalBody className="login-block">
          { inProcess && <Loader /> }

          <LoginForm onSubmit={ loginSubmitHandler } />

          <div className='sign-up-text'>
            { 'Don\'t have an account?' }
            <a onClick={() => {
                toggleModalType('signup')
              }}
            >
              <I18n text={ defaultMessages.signUp } />
            </a>
          </div>
        </ModalBody>
        <ModalFooter>
          <div className='or-block'>
            <div className='dash' />
            <span>
              <I18n text={ defaultMessages.or } />
            </span>
            <div className='dash' />
          </div>
          <div className='social-login'>
            <I18n text={ defaultMessages.signUpViaASocialLogin } />
          </div>
          <div className='social-block'>
            <a href={ oauthSigninUrls.GOOGLE } className='btn btn-circle google-background'>
              <img src={ GoogleImage } alt='Google' className='google-img' />
            </a>
            <a href={ oauthSigninUrls.FACEBOOK } className='btn btn-circle facebook-background'>
              <img src={ FacebookImage } alt='Facebook' className='facebook-img' />
            </a>
            <a href={ oauthSigninUrls.TWITTER } className='btn btn-circle twitter-background'>
              <img src={ TwitterImage } alt='Twitter' className='twitt-img' />
            </a>
            <a href={ oauthSigninUrls.LINKEDIN } className='btn btn-circle linkedin-background'>
              <img src={ LinkedinImage } alt='Linkedin' className='linkedin-img' />
            </a>
          </div>
        </ModalFooter>

      </Fragment>
    );

    return (
      <Fragment>
        <ModalHeader { ...{ toggle } } charCode='x' >
          Join with us!
        </ModalHeader>

        <Tabs selected={ modalType === 'signup' ? 'Sign up' : 'Login' } >
          <Tab label="Login">
            <LoginContent />
          </Tab>
          <Tab label="Sign up">
            <SignUpContent />
          </Tab>
        </Tabs>

      </Fragment>
    );
  }
};
Main.propTypes = propTypes;

export default Main;
