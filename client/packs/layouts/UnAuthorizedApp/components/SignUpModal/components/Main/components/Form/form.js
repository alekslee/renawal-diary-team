import React from 'react';
import { Field } from 'redux-form';
import { intlShape } from 'react-intl';
import { func, bool } from 'prop-types';

import { defaultMessages } from 'locales/default';
import FieldWithErrors from 'components/FieldWithErrors';
import LocationField from 'components/LocationField';
import I18n from 'components/I18n';

const propTypes = {
  intl: intlShape.isRequired,
  handleSubmit: func.isRequired,
  pickCategoriesStep: bool
};

const Form = ({ intl: { formatMessage }, handleSubmit, pickCategoriesStep }) => {
  return (
    <form onSubmit={ handleSubmit } >
      <div className='input-field'>
        <Field
          component={ FieldWithErrors }
          type='text'
          name='email'
          placeholder={ formatMessage(defaultMessages.email) }
        />
      </div>
      <div className='input-field'>
        <Field
          component={ FieldWithErrors }
          type='password'
          name='password'
          autoComplete='off'
          placeholder={ formatMessage(defaultMessages.password) }
        />
      </div>
      <div className='input-field'>
        <Field
          component={ FieldWithErrors }
          type='text'
          name='displayName'
          autoComplete='off'
          placeholder={ formatMessage(defaultMessages.displayName) }
        />
      </div>
      <div className='input-field'>
        <Field
          component={ LocationField }
          type='text'
          name='location'
          placeholder={ formatMessage(defaultMessages.location) }
        />
      </div>
      <button type='submit' className='signup-modal-btn' >
        <I18n text={ pickCategoriesStep ? defaultMessages.next : defaultMessages.signUp } />
      </button>
    </form>
  );
};
Form.propTypes = propTypes;

export default Form;
