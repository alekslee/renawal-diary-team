import { oauthUrls } from 'core/constants';

export const oauthSignupUrls = {
  FACEBOOK: `${oauthUrls.FACEBOOK}?type=signup`,
  TWITTER: `${oauthUrls.TWITTER}?type=signup`,
  GOOGLE: `${oauthUrls.GOOGLE}?type=signup`,
  LINKEDIN: `${oauthUrls.LINKEDIN}?type=signup`
};

