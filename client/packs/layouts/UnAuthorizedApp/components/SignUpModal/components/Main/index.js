import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import Main from './main';
import { selectors as categorySelectors } from 'core/category';
import { selectors as signUpSelectors } from 'core/signUp';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

import { selectors, actions } from 'core/currentUser';

const mapStateToProps = state => ({
  signUpFormInitialValues: selectors.getSignUpFormInitialValues(state),
  currentUser: selectors.getCurrentUser(state),
  category: categorySelectors.getCategory(state),
  pickCategoriesStep: signUpSelectors.getPickCategoriesStepStatus(state),
  currentCountryCode: currentCountrySelectors.getCurrentCountryCode(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    signInDispatch: actions.signIn,
    signUpDispatch: actions.currentUserSignUp,
    setCurrentUserDispatch: actions.setCurrentUser,
    checkUserFieldsUniquenessDispatch: actions.checkUserFieldsUniqueness
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Main));
