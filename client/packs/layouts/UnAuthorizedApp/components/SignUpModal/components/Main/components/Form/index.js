import { reduxForm } from 'redux-form';
import { injectIntl } from 'react-intl';
import { ALLOWED_COUNTRIES } from 'lib/allowedCountries';
import { MINIMUM_PASSWORD_CHARACTERS } from './constants';
import Form from './form';

const validate = ({ email, password, displayName, location }) => {
  const errors = {};
  if (!email) errors.email = 'Required';
  if (!password) {
    errors.password = 'Required';
  } else if (password.length < MINIMUM_PASSWORD_CHARACTERS) {
    errors.password = `Should me more or equal than ${MINIMUM_PASSWORD_CHARACTERS}`;
  }
  if (!displayName) errors.displayName = 'Required';
  if (!(location && location.countryCode && location.lat && location.lng)) errors.location = 'Required';
  if (!errors.location && !ALLOWED_COUNTRIES.includes(location.countryCode.toLowerCase()))
    errors.location = 'Country not supported';
  return errors;
};

export default reduxForm({ form: 'signUpForm', validate })(injectIntl(Form));
