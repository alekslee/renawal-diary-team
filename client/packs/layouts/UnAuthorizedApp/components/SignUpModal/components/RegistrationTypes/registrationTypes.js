import React from 'react';
import { intlShape } from 'react-intl';
import { defaultMessages } from 'locales/default';
import queryString from 'query-string';
import { array, func } from 'prop-types';

import GoogleImage from 'images/svg/google.svg';
import FacebookImage from 'images/svg/facebook.svg';
import TwitterImage from 'images/svg/twitt.svg';
import LinkedinImage from 'images/svg/linkedin.svg';
import EmailImage from 'images/svg/mail.svg';

import { oauthSignupUrls } from './constants';
const propTypes = {
  categoriesAttributes: array.isRequired,
  setEmailRegistrationBodyElement: func.isRequired,
  intl: intlShape.isRequired
};

const RegistrationTypes = ({ intl: { formatMessage }, categoriesAttributes, setEmailRegistrationBodyElement }) => {
  const onClickOauthLinkHandler = e => {
    e.preventDefault();
    const params = $.param({ users_categories_attributes: categoriesAttributes });
    window.location.href = `${e.currentTarget.href}&${params}`;
  };

  return (
    <div>
      <a
        href={ oauthSignupUrls.FACEBOOK }
        className='btn btn-info'
        onClick={ onClickOauthLinkHandler }
      >
        <span>
          <img src={ FacebookImage } alt='Facebook' />
        </span>
        <span>{ formatMessage(defaultMessages.joinGroupWith) } Facebook</span>
      </a>
      <a
        href={ oauthSignupUrls.TWITTER }
        className='btn btn-info'
        onClick={ onClickOauthLinkHandler }
      >
        <span>
          <img src={ TwitterImage } alt='Twitter' />
        </span>
        <span>{ formatMessage(defaultMessages.joinGroupWith) } Twitter</span>
      </a>
      <a
        href={ oauthSignupUrls.GOOGLE }
        className='btn btn-info'
        onClick={ onClickOauthLinkHandler }
      >
        <span>
          <img src={ GoogleImage } alt='Google' />
        </span>
        <span>{ formatMessage(defaultMessages.joinGroupWith) } Google</span>
      </a>
      <a
        href={ oauthSignupUrls.LINKEDIN }
        className='btn btn-info'
        onClick={ onClickOauthLinkHandler }
      >
        <span>
          <img src={ LinkedinImage } alt='Linkedin' />
        </span>
        <span>{ formatMessage(defaultMessages.joinGroupWith) } Linkedin</span>
      </a>
      <button
        className='btn btn-info'
        onClick={ setEmailRegistrationBodyElement }
      >
        <span>
          <img src={ EmailImage } alt='Email' />
        </span>
        <span>{ formatMessage(defaultMessages.joinGroupWith) } Email</span>
      </button>
    </div>
  );
};
RegistrationTypes.propTypes = propTypes;

export default RegistrationTypes;
