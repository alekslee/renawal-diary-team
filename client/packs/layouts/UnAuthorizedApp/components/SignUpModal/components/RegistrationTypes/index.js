import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { injectIntl } from 'react-intl';
import RegistrationTypes from './registrationTypes';

import { selectors } from 'core/signUp';

const mapStateToProps = state => ({
  categoriesAttributes: selectors.getCategoriesAttributes(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(RegistrationTypes));
