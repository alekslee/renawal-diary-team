import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { injectIntl } from 'react-intl';
import { withRouter } from 'react-router-dom';
import Categories from './categories';

import { actions as categoryActions } from 'core/category';
import { selectors as currentUserSelectors, actions as currentUserActions } from 'core/currentUser';
import { selectors, actions } from 'core/signUp';

const mapStateToProps = state => ({
  categoriesAttributes: selectors.getCategoriesAttributes(state),
  categories: selectors.getCategories(state),
  countryCode: currentUserSelectors.getUserCountryCode(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    signUpDispatch: currentUserActions.currentUserSignUp,
    selectCategoryDispatch: actions.selectCategory,
    changeCategoriesMembersCountersDispatch: actions.changeCategoriesMembersCounters,
    subscribeToCategoriesCounterChannelDispatch: categoryActions.subscribeToCategoriesCounterChannel,
    fetchCategoriesDispatch: actions.fetchCategories
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(Categories)));
