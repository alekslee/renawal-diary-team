import React, { Fragment, Component } from 'react';
import { ModalHeader, ModalBody } from 'reactstrap';
import { toastr } from 'lib/helpers';
import { intlShape } from 'react-intl';
import { func, array, string, object } from 'prop-types';

import { deleteSubscription } from 'lib/utils';
import { subscriptionIds } from 'core/category/constants';
import { TermsModal, PrivacyModal } from './components';
import I18n from 'components/I18n';
import Loader from 'components/Loader';
import CategoriesTree from 'components/CategoriesTree';
import { defaultMessages } from 'locales/default';
import { paths } from 'layouts/constants';

const propTypes = {
  intl: intlShape.isRequired,
  categories: array.isRequired,
  categoriesAttributes: array.isRequired,
  countryCode: string.isRequired,
  history: object.isRequired,
  toggle: func.isRequired,
  setMainBodyElement: func.isRequired,
  selectCategoryDispatch: func.isRequired,
  signUpDispatch: func.isRequired,
  subscribeToCategoriesCounterChannelDispatch: func.isRequired,
  changeCategoriesMembersCountersDispatch: func.isRequired,
  fetchCategoriesDispatch: func.isRequired,
  onSignUpSuccess: func
};

class Categories extends Component {
  state = {
    isLoading: true,
    inProcess: false,
    isOpenTermsModal: false,
    isOpenPrivacyModal: false
  };

  componentDidMount() {
    const {
      changeCategoriesMembersCountersDispatch,
      subscribeToCategoriesCounterChannelDispatch,
      fetchCategoriesDispatch,
      countryCode
    } = this.props;

    const callback = () => this.setState({ isLoading: false });
    fetchCategoriesDispatch({ countryCode, callback, errorCallback: callback });

    const onReceive = ({ categoriesChangedMembers }) => {
      changeCategoriesMembersCountersDispatch({ categoriesChangedMembers });
    };
    subscribeToCategoriesCounterChannelDispatch({ onReceive });
  };

  componentWillUnmount() {
    deleteSubscription(subscriptionIds.CATEGORIES_COUNTER_CHANNEL);
  };

  signUpHandler = () => {
    const {
      categoriesAttributes,
      signUpDispatch,
      history,
      countryCode,
      onSignUpSuccess,
      intl: { formatMessage }
    } = this.props;

    if (categoriesAttributes.length === 0) {
      toastr.warning(formatMessage(defaultMessages.warningsSelectCategoriesFirst));
    } else {
      const callback = () => {
        history.push(paths.ROOT.replace(':country', countryCode.toLowerCase()));
        if (onSignUpSuccess) onSignUpSuccess();
      };
      const errorCallback = () => this.setState({ inProcess: false });

      this.setState({ inProcess: true });
      signUpDispatch({ categoriesAttributes, callback, errorCallback });
    }
  }

  toggleModal = modalName => {
    const key = `isOpen${modalName}Modal`;
    this.setState({ [key]: !this.state[key] });
  };

  render() {
    const {
      signUpHandler,
      toggleModal,
      state: { isLoading, inProcess, isOpenTermsModal, isOpenPrivacyModal },
      props: { setMainBodyElement, toggle, selectCategoryDispatch, categories }
    } = this;

    if (isLoading) return null;

    return (
      <Fragment>
        <ModalHeader { ...{ toggle } } charCode='x' >
          <button className='back' onClick={ setMainBodyElement } />
          <I18n text={ defaultMessages.pickTheGroupsThatMatterToYou } />
        </ModalHeader>
        <ModalBody className='categories-modal-body'>
          { inProcess && <Loader /> }
          <section className='pick-group-block'>
            <CategoriesTree { ...{categories, selectCategoryDispatch} } />
          </section>
          <section className='landing-bottom'>
            <div className='container'>
              <button className='signup-modal-btn' onClick={ signUpHandler } >
                <I18n text={ defaultMessages.signUp } />
              </button>
              <p className='landing-bottom-text'>
                <I18n text={ defaultMessages.byClickingJoinTheCommunityIAgree } />
                &nbsp;
                <span onClick={ () => toggleModal('Terms') } >
                  <I18n text={ defaultMessages.termsAndConditions } />
                </span>
                &nbsp;
                <I18n text={ defaultMessages.and } />
                &nbsp;
                <span onClick={ () => toggleModal('Privacy') } >
                  <I18n text={ defaultMessages.privacyPolicy } />
                </span>
              </p>
            </div>
          </section>
          <TermsModal isOpen={ isOpenTermsModal } toggle={ () => toggleModal('Terms') } />
          <PrivacyModal isOpen={ isOpenPrivacyModal } toggle={ () => toggleModal('Privacy') } />
        </ModalBody>
      </Fragment>
    );
  }
};
Categories.propTypes = propTypes;

export default Categories;
