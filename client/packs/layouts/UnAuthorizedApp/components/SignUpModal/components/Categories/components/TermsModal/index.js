import TermsModal from './termsModal';
import { injectIntl } from 'react-intl';

export default injectIntl(TermsModal);
