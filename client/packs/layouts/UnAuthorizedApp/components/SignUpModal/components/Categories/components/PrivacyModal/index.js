import PrivacyModal from './privacyModal';
import { injectIntl } from 'react-intl';

export default injectIntl(PrivacyModal);
