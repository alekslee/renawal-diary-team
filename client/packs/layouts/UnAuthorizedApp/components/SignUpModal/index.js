import { connect } from 'react-redux';
import signUpModal from './signUpModal';
import { selectors as categorySelectors } from 'core/category';
import { selectors as signUpSelectors } from 'core/signUp';



const mapStateToProps = state => ({
  category: categorySelectors.getCategory(state),
  pickCategoriesStep: signUpSelectors.getPickCategoriesStepStatus(state)
});

export default connect(mapStateToProps)(signUpModal);
