import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import SignInModal from './signInModal';

import { selectors, actions } from 'core/currentUser';

const mapStateToProps = state => ({
  signUpFormInitialValues: selectors.getSignUpFormInitialValues(state),
  currentUser: selectors.getCurrentUser(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    signInDispatch: actions.signIn,
    setCurrentUserDispatch: actions.setCurrentUser,
    checkUserFieldsUniquenessDispatch: actions.checkUserFieldsUniqueness,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SignInModal));
