import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';
import { injectIntl } from 'react-intl';
import Form from './form';

import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
  }, dispatsh)
);

const validate = ({ email, password }) => {
  const errors = {};
  if (!email) errors.email = 'Required';
  if (!password) errors.password = 'Required';
  return errors;
};

export default reduxForm({ form: 'signIn', validate })(connect(mapStateToProps, mapDispatchToProps)(injectIntl(Form)));
