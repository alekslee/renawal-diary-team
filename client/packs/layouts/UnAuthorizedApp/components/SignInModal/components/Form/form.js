import React from 'react';
import { Field } from 'redux-form';
import { Link } from 'react-router-dom';
import { intlShape } from 'react-intl';
import { defaultMessages } from 'locales/default';
import { func, string } from 'prop-types';

import { paths } from 'layouts/constants';
import FieldWithErrors from 'components/FieldWithErrors';
import ForgotPasswordModalLink from 'components/ForgotPasswordModalLink';

const propTypes = {
  handleSubmit: func.isRequired,
  intl: intlShape.isRequired,
  countryCode: string.isRequired
};

const Form = ({ handleSubmit, intl: { formatMessage }, countryCode }) => {
  return (
    <form onSubmit={ handleSubmit } >
      <div className='input-field'>
        <Field
          name='email'
          component={ FieldWithErrors }
          type='text'
          placeholder={ formatMessage(defaultMessages.email) }
        />
      </div>
      <div className='input-field'>
        <Field
          name='password'
          component={ FieldWithErrors }
          type='password'
          placeholder={ formatMessage(defaultMessages.password) }
        />
      </div>
      <div className="forgot-container">
        <ForgotPasswordModalLink className='forgot-text' >
          { formatMessage(defaultMessages.forgotPassword) }
        </ForgotPasswordModalLink>
      </div>
      <button type='submit' className='signup-modal-btn' >{ formatMessage(defaultMessages.login) }</button>
    </form>
  );
};
Form.propTypes = propTypes;

export default Form;
