import { oauthUrls } from 'core/constants';

export const oauthSigninUrls = {
  FACEBOOK: `${oauthUrls.FACEBOOK}?type=login`,
  TWITTER: `${oauthUrls.TWITTER}?type=login`,
  GOOGLE: `${oauthUrls.GOOGLE}?type=login`,
  LINKEDIN: `${oauthUrls.LINKEDIN}?type=login`
};

