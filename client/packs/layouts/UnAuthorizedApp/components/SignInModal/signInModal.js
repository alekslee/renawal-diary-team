import React, { Component, Fragment } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { object, bool, func } from 'prop-types';
import { Tabs, Tab } from 'react-bootstrap-tabs';
import SignUpModalLink from 'components/SignUpModalLink';
import Loader from 'components/Loader';
import I18n from 'components/I18n';
import { Form } from './components';
import { oauthSigninUrls } from './constants';
import { paths } from 'layouts/constants';
import { defaultMessages } from 'locales/default';

import GoogleImage from 'images/svg/google.svg';
import FacebookImage from 'images/svg/facebook.svg';
import TwitterImage from 'images/svg/twitt.svg';
import LinkedinImage from 'images/svg/linkedin.svg';

import { oauthSignupUrls } from '../SignUpModal/components/Main/constants';
import SignUpForm from '../SignUpModal/components/Main/components/Form/index';

const propTypes = {
  history: object.isRequired,
  isOpen: bool.isRequired,
  toggle: func.isRequired,
  signInDispatch: func.isRequired,
  signUpFormInitialValues: object.isRequired,
};

class SignInModal extends Component {

  state = {
    inProcess: false
  };

  submitHandler = ({ email, password }) => {
    const { history, signInDispatch } = this.props;

    const callback = ({ country }) => {
      history.push(paths.ROOT.replace(':country', country.isoA2Code));
      toastr.success('', { component: <I18n text={ defaultMessages.deviseSessionsSignedIn } /> });
    };
    signInDispatch({ email, password, callback });
  };

  submitSignUpHandler = ({ email, password, displayName, location }) => {
    const {
      setCurrentUserDispatch, currentUser, setCategoriesBodyElement, checkUserFieldsUniquenessDispatch
    } = this.props;

    const callback = () => {
      setCurrentUserDispatch({ currentUser: { ...currentUser, email, password, displayName, ...location } });
      // TODO
      // setCategoriesBodyElement();
      this.setState({ inProcess: false });
    };
    const errorCallback = () => this.setState({ inProcess: false });

    this.setState({ inProcess: true });
    checkUserFieldsUniquenessDispatch({ email, displayName, callback, errorCallback });
  };

  render() {
    const {
      submitHandler,
      submitSignUpHandler,
      state: { inProcess },
      props: { isOpen, toggle, signUpFormInitialValues }
    } = this;

    const SignUpContent = () => (
      <Fragment>

        <ModalBody className="login-block">
          { inProcess && <Loader /> }
          <SignUpForm initialValues={ signUpFormInitialValues } onSubmit={ submitSignUpHandler } />
        </ModalBody>
        <ModalFooter>
          <div className='or-block'>
            <div className='dash' />
            <span>
              <I18n text={ defaultMessages.or } />
            </span>
            <div className='dash' />
          </div>
          <div className='social-login'>
            <I18n text={ defaultMessages.signUpViaASocialLogin } />
          </div>
          <div className='social-block'>
            <a href={ oauthSignupUrls.GOOGLE } className='btn btn-circle google-background'>
              <img src={ GoogleImage } alt='Google' className='google-img' />
            </a>
            <a href={ oauthSignupUrls.FACEBOOK } className='btn btn-circle facebook-background' >
              <img src={ FacebookImage } alt='Facebook' className='facebook-img' />
            </a>
            <a href={ oauthSignupUrls.TWITTER } className='btn btn-circle twitter-background' >
              <img src={ TwitterImage } alt='Twitter' className='twitt-img' />
            </a>
            <a href={ oauthSignupUrls.LINKEDIN } className='btn btn-circle linkedin-background' >
              <img src={ LinkedinImage } alt='Linkedin' className='linkedin-img' />
            </a>
          </div>
        </ModalFooter>
      </Fragment>
    );

    const LoginContent = () => (
      <Fragment>

        <ModalBody className="login-block">
          { inProcess && <Loader /> }
          {/*<h3 className='auth-title'>Join us!</h3>*/}

          <Form onSubmit={ submitHandler } />

          <div className='sign-up-text'>
            { 'Don\'t have an account?' }
            <SignUpModalLink>
              <I18n text={ defaultMessages.signUp } />
            </SignUpModalLink>
          </div>
        </ModalBody>
        <ModalFooter>
          <div className='or-block'>
            <div className='dash' />
            <span>
              <I18n text={ defaultMessages.or } />
            </span>
            <div className='dash' />
          </div>
          <div className='social-block'>
            <a href={ oauthSigninUrls.GOOGLE } className='btn btn-circle google-background'>
              <img src={ GoogleImage } alt='Google' className='google-img' />
            </a>
            <a href={ oauthSigninUrls.FACEBOOK } className='btn btn-circle facebook-background'>
              <img src={ FacebookImage } alt='Facebook' className='facebook-img' />
            </a>
            <a href={ oauthSigninUrls.TWITTER } className='btn btn-circle twitter-background'>
              <img src={ TwitterImage } alt='Twitter' className='twitt-img' />
            </a>
            <a href={ oauthSigninUrls.LINKEDIN } className='btn btn-circle linkedin-background'>
              <img src={ LinkedinImage } alt='Linkedin' className='linkedin-img' />
            </a>
          </div>
        </ModalFooter>

      </Fragment>
    );

    return (
      <Modal className='signup-modal' { ...{ isOpen, toggle } } >
        <ModalHeader { ...{ toggle } } charCode='x' >
          Join with us!
        </ModalHeader>

        <Tabs>
          <Tab label="Login">
            <LoginContent />
          </Tab>
          <Tab label="Sign up">
            <SignUpContent />
          </Tab>
        </Tabs>

      </Modal>
    );
  }
};
SignInModal.propTypes = propTypes;

export default SignInModal;
