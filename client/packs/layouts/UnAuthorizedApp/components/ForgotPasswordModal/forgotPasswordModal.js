import React, { Component } from 'react';
import { intlShape } from 'react-intl';
import { toastr } from 'lib/helpers';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { object, string, bool, func } from 'prop-types';

import { defaultMessages } from 'locales/default';
import I18n from 'components/I18n';
import Loader from 'components/Loader';
import SignInModalLink from 'components/SignInModalLink';
import { Form } from './components';
import { paths } from 'layouts/constants';

const propTypes = {
  intl: intlShape.isRequired,
  history: object.isRequired,
  countryCode: string.isRequired,
  isOpen: bool.isRequired,
  toggle: func.isRequired,
  resetPasswordDispatch: func.isRequired
};

class ForgotPasswordModal extends Component {
  state = {
    inProcess: false
  };

  submitHandler = ({ email }) => {
    const { history, resetPasswordDispatch, countryCode, intl: { formatMessage } } = this.props;

    const callback = () => {
      this.setState({ inProcess: false });
      history.push(paths.USERS_SIGN_IN.replace(':country', countryCode));
      toastr.success(formatMessage(defaultMessages.devisePasswordsSendInstructions));
    };

    const errorCallback = () => {
      this.setState({ inProcess: false });
    };

    this.setState({ inProcess: true });
    resetPasswordDispatch({ email, callback, errorCallback });
  };

  render() {
    const {
      submitHandler,
      state: { inProcess },
      props: { isOpen, toggle }
    } = this;

    return (
      <Modal className='signup-modal' { ...{ isOpen, toggle } } >
        <ModalHeader { ...{ toggle } } charCode='x' />
        <ModalBody>
          { inProcess && <Loader /> }
          <div className='auth-block forgot-password'>
            <h3 className='forgot-title'>
              <I18n text={ defaultMessages.forgotPasswordPageTitle } />
            </h3>
            <p className='forgot-password-text'>
              <I18n text={ defaultMessages.forgotPasswordPageP1 } />
            </p>
            <Form onSubmit={ submitHandler } />
            <div className='forgot-text'>
              <I18n text={ defaultMessages.forgotPasswordPageRemeberPassword } />
              &nbsp;
              <SignInModalLink>
                <I18n text={ defaultMessages.forgotPasswordPageLogin } />
              </SignInModalLink>
            </div>
          </div>
        </ModalBody>
      </Modal>
    );
  }
};
ForgotPasswordModal.propTypes = propTypes;

export default ForgotPasswordModal;
