import React from 'react';
import { Field } from 'redux-form';
import { intlShape } from 'react-intl';
import { func, string } from 'prop-types';

import { defaultMessages } from 'locales/default';
import I18n from 'components/I18n';

const propTypes = {
  intl: intlShape.isRequired,
  handleSubmit: func.isRequired,
  countryCode: string.isRequired
};

const Form = ({ handleSubmit, countryCode, intl: { formatMessage } }) => {
  return (
    <form onSubmit={ handleSubmit }>
      <div className='input-field'>
        <Field
          type='text'
          name='email'
          component='input'
          autoComplete='email'
          placeholder={ formatMessage(defaultMessages.forgotPasswordPageEmailPlaceholder) }
          autoFocus
        />
      </div>
      <button type='submit' className='signup-modal-btn' >
        { formatMessage(defaultMessages.forgotPasswordPageSend) }
      </button>
    </form>
  );
};
Form.propTypes = propTypes;

export default Form;
