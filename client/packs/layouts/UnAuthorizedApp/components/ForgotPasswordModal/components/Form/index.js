import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';
import { injectIntl } from 'react-intl';
import Form from './form';

import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
  }, dispatsh)
);

export default reduxForm({ form: 'forgotPassword' })(connect(mapStateToProps, mapDispatchToProps)(injectIntl(Form)));
