import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { injectIntl } from 'react-intl';
import ForgotPasswordModal from './forgotPasswordModal';

import { actions } from 'core/currentUser';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    resetPasswordDispatch: actions.usersResetPassword
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(ForgotPasswordModal)));
