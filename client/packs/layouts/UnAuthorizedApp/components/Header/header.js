import React from 'react';
import { Link } from 'react-router-dom';
import { defaultMessages } from 'locales/default';
import { intlShape } from 'react-intl';
import { shape, string } from 'prop-types';

import SharedHeader from 'components/Header';
import SignUpModalLink from 'components/SignUpModalLink';
import SignInModalLink from 'components/SignInModalLink';

const propTypes = {
  intl: intlShape.isRequired
};

const Header = ({ intl: { formatMessage } }) => {
  return (
    <SharedHeader>
      <li className='nav-item auth-btn'>
        <SignInModalLink className='nav-link' >
          { formatMessage(defaultMessages.login) }
        </SignInModalLink>
      </li>
      <li className='nav-item auth-btn'>
        <SignUpModalLink className='nav-link sign-up-link' >
          { formatMessage(defaultMessages.signUp) }
        </SignUpModalLink>
      </li>
    </SharedHeader>
  );
};
Header.propTypes = propTypes;

export default Header;
