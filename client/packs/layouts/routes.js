import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import CustomRouteSwitch from 'lib/CustomRouteSwitch';
import { object, string } from 'prop-types';

import UnAuthorizedApp from './UnAuthorizedApp';
import AuthorizedApp from './AuthorizedApp';
import SmartReminder from './SmartReminder';
import {
  Landing,
  SignIn,
  BecomeAPartner,
  OurSharedMission,
  ForgotPassword,
  ForgotPasswordUpdate,
  ResendConfirmation,
  ResendUnlock,
  InvitationAccept,
  MyNotes,
  Leaderboard,
  Vote,
  RenewalDetails,
  Root
} from 'screens';
import { paths } from './constants';

const propTypes = {
  countryCode: string.isRequired
};

const Routes = ({ countryCode }) => {
  return (
    <CustomRouteSwitch path={ paths.COUNTRY_PREFIX } >
      <CustomRouteSwitch component={ UnAuthorizedApp } auth={ false } >
        <Route path={ paths.ROOT } component={ Landing } exact />

        <CustomRouteSwitch path={ paths.CATEGORIES_PREFIX } component={ SmartReminder } >
          <Route path={ paths.MY_NOTES } component={ MyNotes } exact />
          <Route path={ paths.LEADERBOARD } component={ Leaderboard } exact />
          <Route path={ paths.VOTE } component={ Vote } exact />
        </CustomRouteSwitch>

        <Route path={ paths.USERS_SIGN_IN } component={ SignIn } exact />
        <Route path={ paths.BECOME_A_PARTNER } component={ BecomeAPartner } exact />
        <Route path={ paths.OUR_SHARED_MISSION } component={ OurSharedMission } exact />
        <Route path={ paths.USERS_FORGOT_PASSWORD } component={ ForgotPassword } exact />
        <Route path={ paths.USERS_FORGOT_PASSWORD_UPDATE } component={ ForgotPasswordUpdate } exact />
        <Route path={ paths.USERS_RESEND_CONFIRMATION } component={ ResendConfirmation } exact />
        <Route path={ paths.USERS_RESEND_CONFIRMATION_ERROR } component={ ResendConfirmation } exact />
        <Route path={ paths.USERS_RESEND_UNLOCK } component={ ResendUnlock } exact />
        <Route path={ paths.USERS_RESEND_UNLOCK_ERROR } component={ ResendUnlock } exact />
        <Route path={ paths.USERS_INVITATION_ACCEPT } component={ InvitationAccept } exact />
      </CustomRouteSwitch>

      <CustomRouteSwitch component={ AuthorizedApp } auth >
        <Route path={ paths.ROOT } component={ Root } exact />

        <CustomRouteSwitch path={ paths.CATEGORIES_PREFIX } component={ SmartReminder } >
          <Route path={ paths.MY_NOTES } component={ MyNotes } exact />
          <Route path={ paths.LEADERBOARD } component={ Leaderboard } exact />
          <Route path={ paths.VOTE } component={ Vote } exact />
        </CustomRouteSwitch>

        <Route path={ paths.BECOME_A_PARTNER } component={ BecomeAPartner } exact />
        <Route path={ paths.OUR_SHARED_MISSION } component={ OurSharedMission } exact />
        <Route path={ paths.USERS_RESEND_CONFIRMATION } component={ ResendConfirmation } exact />
        <Route path={ paths.USERS_RESEND_CONFIRMATION_ERROR } component={ ResendConfirmation } exact />
        <Route path={ paths.USERS_RESEND_UNLOCK } component={ ResendUnlock } exact />
        <Route path={ paths.USERS_RESEND_UNLOCK_ERROR } component={ ResendUnlock } exact />
      </CustomRouteSwitch>
    </CustomRouteSwitch>
  );
};

Routes.propTypes = propTypes;

export default Routes;
