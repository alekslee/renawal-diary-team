import { connect } from 'react-redux';
import Routes from './routes';

import { selectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: selectors.getCurrentCountryCode(state)
});

export default connect(mapStateToProps, null)(Routes);
