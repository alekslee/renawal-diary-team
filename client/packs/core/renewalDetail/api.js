import { graphQLRequest } from 'lib/utils';
import {
  fetchRenewalDetail,
  fetchRenewalDetailFormOptions,
  fetchPersonalOptions,
  createRenewalDetail,
  updateRenewalDetail,
  fetchRenewalDetailFormAutocompleteOptions,
  fetchRenewalDetailCriteriasQuestionsCarModelsOptions,
  fetchRenewalDetailCriteriasQuestionsCarTrimOptions,
  fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptions,
  fetchRenewalDetailCriteriasQuestionsPetBreedOptions,
  fetchMobileModelsOptions,
} from './queries';

const api = {
  fetchRenewalDetail: variables => graphQLRequest({
    query: fetchRenewalDetail,
    variables
  }),
  fetchRenewalDetailPersonalOptions: ({ categoryId }) => graphQLRequest({
    query: fetchPersonalOptions,
    variables: { categoryId },
  }),
  fetchRenewalDetailScreenData: ({ categoryId }) => graphQLRequest({
    queries: [
      {
        query: fetchRenewalDetail,
        variables: { categoryId }
      },
      {
        query: fetchRenewalDetailFormOptions,
        variables: { categoryId },
        responseKey: 'formOptions'
      }
    ]
  }),
  createRenewalDetail: variables => graphQLRequest({
    query: createRenewalDetail,
    variables: { input: variables }
  }),
  updateRenewalDetail: variables => graphQLRequest({
    query: updateRenewalDetail,
    variables: { input: variables }
  }),
  fetchRenewalDetailFormAutocompleteOptions: variables => graphQLRequest({
    query: fetchRenewalDetailFormAutocompleteOptions,
    variables
  }),
  fetchRenewalDetailCriteriasQuestionsCarModelsOptions: variables => graphQLRequest({
    query: fetchRenewalDetailCriteriasQuestionsCarModelsOptions,
    variables
  }),
  fetchRenewalDetailCriteriasQuestionsCarTrimOptions: variables => graphQLRequest({
    query: fetchRenewalDetailCriteriasQuestionsCarTrimOptions,
    variables
  }),
  fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptions: variables => graphQLRequest({
    query: fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptions,
    variables
  }),
  fetchRenewalDetailCriteriasQuestionsPetBreedOptions: variables => graphQLRequest({
    query: fetchRenewalDetailCriteriasQuestionsPetBreedOptions,
    variables
  }),
  fetchMobileModelsOptions: variables => graphQLRequest({
    query: fetchMobileModelsOptions,
    variables
  }),
};

export default api;
