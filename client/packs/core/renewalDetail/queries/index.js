import fetchRenewalDetail from './fetchRenewalDetail.graphql';
import fetchRenewalDetailFormOptions from './fetchRenewalDetailFormOptions.graphql';
import fetchPersonalOptions from './fetchPersonalOptions.graphql';
import createRenewalDetail from './createRenewalDetail.graphql';
import updateRenewalDetail from './updateRenewalDetail.graphql';
import fetchMobileModelsOptions from './fetchMobileModelsOptions.graphql';
import fetchRenewalDetailFormAutocompleteOptions from './fetchRenewalDetailFormAutocompleteOptions.graphql';
import fetchRenewalDetailCriteriasQuestionsCarModelsOptions from
  './fetchRenewalDetailCriteriasQuestionsCarModelsOptions.graphql';
import fetchRenewalDetailCriteriasQuestionsCarTrimOptions from
  './fetchRenewalDetailCriteriasQuestionsCarTrimOptions.graphql';
import fetchRenewalDetailCriteriasQuestionsPetBreedOptions from
  './fetchRenewalDetailCriteriasQuestionsPetBreedOptions.graphql';
import fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptions from
    './fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptions.graphql';

export {
  fetchRenewalDetail,
  fetchRenewalDetailFormOptions,
  fetchPersonalOptions,
  createRenewalDetail,
  updateRenewalDetail,
  fetchMobileModelsOptions,
  fetchRenewalDetailFormAutocompleteOptions,
  fetchRenewalDetailCriteriasQuestionsCarModelsOptions,
  fetchRenewalDetailCriteriasQuestionsCarTrimOptions,
  fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptions,
  fetchRenewalDetailCriteriasQuestionsPetBreedOptions
};
