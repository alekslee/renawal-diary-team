import { call, fork, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import { setError } from 'lib/utils';

import api from './api';
import { types, selectors } from './index';
import { selectors as categorySelectors, types as categoryTypes } from 'core/category';
import { transformCompaniesOptionsToAttributes } from 'lib/helpers/companies';
import {
  setRenewalDetail,
  setRenewalDetailFormOptions,
  updateRenewalDetailFormOptions
} from 'lib/helpers/renewalDetails';
import { setCategoryRenewalDetail } from 'lib/helpers/categories';
// import { selectCategorySortedLeafElement, setCategoryRenewalDetail } from 'lib/helpers/categories';

export function* fetchRenewalDetail({ payload: { categoryId, year }, errorCallback, callback }) {
  try {
    const renewalDetail = yield call(api.fetchRenewalDetail, { categoryId, year });
    yield* setRenewalDetail(renewalDetail || { year });

    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* createRenewalDetail({ payload: { params, categoryId }, errorCallback, callback }) {
  try {
    const comapnyAttrs = yield* transformCompaniesOptionsToAttributes({
      currentProvider: { ...params.currentProvider, companyType: 'provider' },
      buyingTypeCompany: { ...params.buyingTypeCompany, companyType: params.buyingType }
    });
    const category = yield select(categorySelectors.getCategory);
    categoryId = categoryId || category.id;
    const resp = yield call(api.createRenewalDetail,
      { ...params, ...comapnyAttrs, categoryId, currentProvider: undefined, buyingTypeCompany: undefined }
    );

    if (!resp.errors.length) {
      yield* setCategoryRenewalDetail({ id: resp.renewalDetail.id, ...params });

      const categories = yield select(categorySelectors.getCategories);
      // if (categories.length && !category.selected) yield* selectCategorySortedLeafElement(category.id);
      if (callback) callback();
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* updateRenewalDetail({ payload: { params }, errorCallback, callback }) {
  try {
    const comapnyAttrs = yield* transformCompaniesOptionsToAttributes({
      currentProvider: { ...params.currentProvider, companyType: 'provider' },
      buyingTypeCompany: { ...params.buyingTypeCompany, companyType: params.buyingType }
    });
    const renewalDetail = yield select(selectors.getRenewalDetail);
    const resp = yield call(api.updateRenewalDetail,
      {
        ...params,
        ...comapnyAttrs,
        renewalDetailId: renewalDetail.id,
        currentProvider: undefined,
        buyingTypeCompany: undefined
      }
    );

    if (!resp.errors.length) {
      yield* setCategoryRenewalDetail(params);
      if (callback) callback();
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchRenewalDetailPersonalOptions({ payload: { categoryId }, errorCallback, callback }) {
  try {
    const resp = yield call(api.fetchRenewalDetailPersonalOptions, { categoryId });

    yield(
      put({
        type: types.SET_RENEWAL_DETAIL_PERSONAL_OPTIONS,
        payload: { personalFilterOptions: JSON.parse(resp.personalCriteriaFilterOptions) },
      })
    );

    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchRenewalDetailScreenData({ payload: { categoryId }, errorCallback, callback }) {
  try {
    const resp = yield call(api.fetchRenewalDetailScreenData, { categoryId });
    yield* setRenewalDetail(resp.renewalDetail);
    yield* setRenewalDetailFormOptions(resp.formOptions);

    if (callback) callback(resp);
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchRenewalDetailFormAutocompleteOptions({ payload, errorCallback, callback }) {
  try {
    const resp = yield call(api.fetchRenewalDetailFormAutocompleteOptions, payload);
    yield* updateRenewalDetailFormOptions(resp);
    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchRenewalDetailCriteriasQuestionsCarModelsOptions({ payload: { make }, errorCallback, callback }) {
  try {
    const resp = yield call(api.fetchRenewalDetailCriteriasQuestionsCarModelsOptions, { make });

    yield put({ type: types.SET_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_CAR_MODELS_OPTIONS, payload: { options: resp } });
    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchRenewalDetailCriteriasQuestionsCarTrimOptions({
  payload: { make, model },
  errorCallback,
  callback
}) {
  try {
    const resp = yield call(api.fetchRenewalDetailCriteriasQuestionsCarTrimOptions, { make, model });

    yield put({ type: types.SET_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_CAR_TRIM_OPTIONS, payload: { options: resp } });
    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchMobileModelsOptions({
  payload: { make },
  errorCallback,
  callback
}) {
  try {
    const resp = yield call(api.fetchMobileModelsOptions, { make });

    yield put({ type: types.SET_MOBILE_MODEL_OPTIONS, payload: { options: resp } });
    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptions({
  payload: { make, model },
  errorCallback,
  callback
  }) {
  try {
    const resp = yield call(api.fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptions, { make, model });

    yield put({
      type: types.SET_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_MOTOR_BIKE_ENGINE_SIZE_OPTIONS,
      payload: { options: resp }
    });
    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchRenewalDetailCriteriasQuestionsPetBreedOptions({
  payload: { petType }, errorCallback, callback
}) {
  try {
    const resp = yield call(api.fetchRenewalDetailCriteriasQuestionsPetBreedOptions, { petType });

    yield put({ type: types.SET_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_PET_TYPE_OPTIONS, payload: { options: resp } });
    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* tagWatch() {
  yield takeLatest(types.FETCH_RENEWAL_DETAIL, fetchRenewalDetail);
  yield takeLatest(types.CREATE_RENEWAL_DETAIL, createRenewalDetail);
  yield takeLatest(types.FETCH_RENEWAL_DETAIL_SCREEN_DATA, fetchRenewalDetailScreenData);
  yield takeLatest(types.FETCH_RENEWAL_DETAIL_PERSONAL_OPTIONS, fetchRenewalDetailPersonalOptions);
  yield takeLatest(types.UPDATE_RENEWAL_DETAIL, updateRenewalDetail);
  yield takeLatest(types.FETCH_RENEWAL_DETAIL_FORM_AUTOCOMPLETE_OPTIONS, fetchRenewalDetailFormAutocompleteOptions);
  yield takeLatest(types.FETCH_MOBILE_MODEL_OPTIONS, fetchMobileModelsOptions);
  yield takeLatest(
    types.FETCH_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_CAR_MODELS_OPTIONS,
    fetchRenewalDetailCriteriasQuestionsCarModelsOptions
  );
  yield takeLatest(
    types.FETCH_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_CAR_TRIM_OPTIONS,
    fetchRenewalDetailCriteriasQuestionsCarTrimOptions
  );
  yield takeLatest(
    types.FETCH_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_PET_BREED_OPTIONS,
    fetchRenewalDetailCriteriasQuestionsPetBreedOptions
  );
  yield takeLatest(
    types.FETCH_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_MOTOR_BIKE_ENGINE_SIZE_OPTIONS,
    fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptions
  );
}

export const renewalDetailSagas = [
  fork(tagWatch)
];
