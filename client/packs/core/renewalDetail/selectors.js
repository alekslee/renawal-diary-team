import { createSelector } from 'reselect';
import { getFormValues } from 'redux-form';

const getRenewalDetail = state => state.renewalDetail.object;
const getProviders = state => state.renewalDetail.formOptions.providerCompanies;
const getFormOptions = state => state.renewalDetail.formOptions;
const getFormValuesByName = (state, props) => getFormValues(props.form)(state);

export const selectors = {
  getRenewalDetail,
  getFormOptions,
  getIsRenewalDetailExists: createSelector(
    [getRenewalDetail], renewalDetail => !!(renewalDetail && renewalDetail.id)
  ),
  getPersonalOptions: state => state.renewalDetail.personalFilterOptions,
  getCriteriaQuestionsOptions: state => state.renewalDetail.formOptions.criteriaQuestionsOptions,
  getPricesQuestionsOptions: state => state.renewalDetail.formOptions.pricesQuestionsOptions,
  getRenewalDetailPossibleYears: state => state.renewalDetail.formOptions.renewalDetailPossibleYears,
  getRenewalDetailYear: createSelector(
    [getRenewalDetail], renewalDetail => (renewalDetail.year || new Date().getFullYear())
  ),
  getProvidersOptions: createSelector(
    [getProviders],
    providers => {
      if (!providers) return [];
      return providers.map(provider => (
        { value: provider.id, label: provider.name, ...provider }
      ));
    }
  ),
  getMixTypeOptions: createSelector(
    [getFormOptions],
    (formOptions) => {
      if (Object.entries(formOptions).length === 0) return [];
      const items = [
        ...formOptions.providerCompanies,
        ...formOptions.brokerCompanies,
        ...formOptions.comparsionSiteCompanies
      ];
      return items.map(item => (
        { value: item.id, label: item.name, ...item }
      ));
    }
  ),
  makeGetBuyingTypeOptions: () => (
    createSelector(
      [getFormOptions, getFormValuesByName],
      (formOptions, formValues) => {
        if (Object.entries(formOptions).length === 0) return [];
        let items = [];
        switch (formValues && formValues.buyingType) {
          case 'provider':
            items = formOptions.providerCompanies;
            break;
          case 'broker':
            items = [...formOptions.brokerCompanies, ...formOptions.comparsionSiteCompanies];
            break;
          case 'comparsion_site':
            items = [...formOptions.comparsionSiteCompanies, ...formOptions.brokerCompanies];
            break;
          default: {
          }
        }

        return items.map(item => (
          { value: item.id, label: item.name, ...item }
        ));
      }
    )
  ),
};
