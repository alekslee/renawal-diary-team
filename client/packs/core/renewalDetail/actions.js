import { types } from './constants';

export const actions = {
  fetchRenewalDetail: ({ categoryId, year, callback, errorCallback }) => ({
    type: types.FETCH_RENEWAL_DETAIL, payload: { categoryId, year }, callback, errorCallback
  }),
  setRenewalDetail: ({ renewalDetail }) => ({
    type: types.SET_RENEWAL_DETAIL, payload: { renewalDetail }
  }),
  createRenewalDetail: ({ params, categoryId, callback, errorCallback }) => ({
    type: types.CREATE_RENEWAL_DETAIL, payload: { params, categoryId }, callback, errorCallback
  }),
  updateRenewalDetail: ({ params, callback, errorCallback }) => ({
    type: types.UPDATE_RENEWAL_DETAIL, payload: { params }, callback, errorCallback
  }),
  fetchRenewalDetailPersonalOptions: ({ categoryId, callback, errorCallback }) => ({
    type: types.FETCH_RENEWAL_DETAIL_PERSONAL_OPTIONS, payload: { categoryId }, callback, errorCallback
  }),
  fetchRenewalDetailScreenData: ({ categoryId, callback, errorCallback }) => ({
    type: types.FETCH_RENEWAL_DETAIL_SCREEN_DATA, payload: { categoryId }, callback, errorCallback
  }),
  setRenewalDetailFormOptions: ({ formOptions }) => ({
    type: types.SET_RENEWAL_DETAIL_FORM_OPTIONS, payload: { formOptions }
  }),
  fetchRenewalDetailFormAutocompleteOptions: ({ countryCode, code, depth, callback, errorCallback }) => ({
    type: types.FETCH_RENEWAL_DETAIL_FORM_AUTOCOMPLETE_OPTIONS,
    payload: { countryCode, categoryCode: code, categoryDepth: depth },
    callback,
    errorCallback
  }),
  fetchRenewalDetailCriteriasQuestionsCarModelsOptions: ({ make, callback, errorCallback }) => ({
    type: types.FETCH_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_CAR_MODELS_OPTIONS, payload: { make }, callback, errorCallback
  }),
  fetchRenewalDetailCriteriasQuestionsCarTrimOptions: ({ make, model, callback, errorCallback }) => ({
    type: types.FETCH_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_CAR_TRIM_OPTIONS,
    payload: { make, model },
    callback,
    errorCallback
  }),
  fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptions: ({ make, model, callback, errorCallback }) => ({
    type: types.FETCH_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_MOTOR_BIKE_ENGINE_SIZE_OPTIONS,
    payload: { make, model },
    callback,
    errorCallback
  }),
  fetchMobileModelsOptions: ({ make, model, callback, errorCallback }) => ({
    type: types.FETCH_MOBILE_MODEL_OPTIONS,
    payload: { make, model },
    callback,
    errorCallback
  }),
  fetchRenewalDetailCriteriasQuestionsPetBreedOptions: ({ petType, callback, errorCallback }) => ({
    type: types.FETCH_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_PET_BREED_OPTIONS,
    payload: { petType },
    callback,
    errorCallback
  }),
};
