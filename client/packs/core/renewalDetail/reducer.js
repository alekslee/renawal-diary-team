import update from 'immutability-helper';

import { types } from './constants';

const initialState = {
  object: {},
  formOptions: {},
  personalFilterOptions: {}
};

export function renewalDetailReducer(state = initialState, { payload, type }) {
  switch (type) {
    case types.SET_RENEWAL_DETAIL_PERSONAL_OPTIONS:
      return update(state, { personalFilterOptions: { $set: payload.personalFilterOptions } });
    case types.SET_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_PET_TYPE_OPTIONS:
      return update(state, {
        formOptions: {
          criteriaQuestionsOptions: {
            breed: {
              options: { $set: payload.options }
            }
          }
        }
      });
    case types.SET_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_CAR_TRIM_OPTIONS:
      return update(state, {
        formOptions: {
          criteriaQuestionsOptions: {
            trim: {
              options: { $set: payload.options }
            }
          }
        }
      });
    case types.SET_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_MOTOR_BIKE_ENGINE_SIZE_OPTIONS:
      return update(state, {
        formOptions: {
          criteriaQuestionsOptions: {
            engine_size: {
              options: { $set: payload.options }
            }
          }
        }
      });
    case types.SET_RENEWAL_DETAIL_CRITERIAS_QUESTIONS_CAR_MODELS_OPTIONS:
      return update(state, {
        formOptions: {
          criteriaQuestionsOptions: {
            model: {
              options: { $set: payload.options }
            }
          }
        }
      });
    case types.SET_MOBILE_MODEL_OPTIONS:
      return update(state, {
        formOptions: {
          criteriaQuestionsOptions: {
            model: {
              options: { $set: payload.options }
            }
          }
        }
      });
    case types.SET_RENEWAL_DETAIL:
      return update(state, { object: { $set: payload.renewalDetail } });
    case types.SET_RENEWAL_DETAIL_FORM_OPTIONS:
      return update(state, { formOptions: { $set: payload.formOptions } });
    default: {
      return state;
    }
  }
}
