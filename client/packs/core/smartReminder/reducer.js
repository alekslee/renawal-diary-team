import update from 'immutability-helper';

import { types } from './constants';

const initialState = {
  list: [],
  currentYear: null,
  yearsList: []
};

export function smartReminderReducer(state = initialState, { payload, type }) {
  state = { ...initialState, ...state };
  switch (type) {
    case types.SET_SMART_REMINDERS_LIST:
      return update(state, {
        list: { $set: payload.smartReminders },
      });
    case types.SET_SMART_REMINDER_CURRENT_YEAR:
      return update(state, { currentYear: { $set: payload.currentYear } });
    case types.SET_SMART_REMINDER_YEARS:
      return update(state, { yearsList: { $set: payload.years } });
    default: {
      return state;
    }
  }
}
