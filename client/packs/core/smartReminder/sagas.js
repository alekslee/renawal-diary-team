import { fork, takeLatest } from 'redux-saga/effects';
import { setError, apiWrapper } from 'lib/utils';
import {
  pushSmartReminderAndOrder, replaceSmartReminderAndOrder, removeSmartReminders
} from 'lib/helpers/smartReminder';
import { types } from './constants';

import api from './api';

export function* createSmartReminder(data) {

  yield apiWrapper(data, {
    func: api.createSmartReminder,
    onSuccess: function* (resp) {
      yield pushSmartReminderAndOrder(resp.smartReminder);
    },
    onFailure: (resp) => {

    },
  })

}

export function* updateSmartReminder(data) {

  yield apiWrapper(data, {
    func: api.updateSmartReminder,
    onSuccess: function* (resp) {
      yield replaceSmartReminderAndOrder(resp.smartReminder);
    },
    onFailure: function* () {

    },
  })
}

export function* destroySmartReminders(data) {

  yield apiWrapper(data, {
    func: api.destroySmartReminders,
    onSuccess: function* () {
      const { payload: { ids } } = data;
      yield removeSmartReminders(ids);
    },
    onFailure: function* () {

    },
  })
}

export function* smartReminderWatch() {
  yield takeLatest(types.CREATE_SMART_REMINDER, createSmartReminder);
  yield takeLatest(types.UPDATE_SMART_REMINDER, updateSmartReminder);
  yield takeLatest(types.DESTROY_SMART_REMINDERS, destroySmartReminders);
}

export const smartReminderSagas = [
  fork(smartReminderWatch)
];
