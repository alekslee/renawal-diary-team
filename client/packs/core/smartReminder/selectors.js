import { createSelector } from 'reselect';
import moment from 'moment';

const getPropsMonth = (state, props) => props.month;
const getSmartReminders = state => state.smartReminder.list;
const getYears = state => state.smartReminder.yearsList;
const getCurrentYear = state => state.smartReminder.currentYear;

export const selectors = {
  getSmartReminders,
  makeIsCurrentMonth: () => createSelector(
    [getPropsMonth, getCurrentYear],
    (month, currentYear) => {
      const momentDate = moment();
      return month === momentDate.format('MMM') && currentYear === momentDate.year();
    }
  ),
  makeRemindersForCurrentMonth: () => (
    createSelector(
      [getSmartReminders, getPropsMonth, getCurrentYear],
      (smartReminders, month, currentYear) => {
        return smartReminders.filter(reminder => {
          const date = moment(reminder.triggeredAt);
          return date.format('MMM') === month && date.year() === currentYear;
        });
      }
    )
  ),
  getNearestReminder: createSelector(getSmartReminders,
    (reminders) => {
      const remds = reminders.sort((a,b) => {
        return new Date(a.triggeredAt) - new Date(b.triggeredAt);
      })
      return remds[0];
  }),
  getYears,
  getCurrentYear,
  getCurrentYearIndex: createSelector(
    [getYears, getCurrentYear], (years, currentYear) => years.findIndex(year => year === currentYear)
  ),
  getYearsSelectOptions: createSelector([getYears], years => years.map(year => ({ value: year, label: year }))),
  getStartYear: createSelector([getYears], years => years[0])
};
