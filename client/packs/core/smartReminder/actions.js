import { types } from './constants';

export const actions = {
  createSmartReminder: ({ categoryId, triggeredAt, callback, errorCallback, aliasName, reminderType }) => ({
    type: types.CREATE_SMART_REMINDER, payload: { categoryId, triggeredAt, aliasName, reminderType }, callback, errorCallback
  }),
  updateSmartReminder: ({ id, categoryId, triggeredAt, aliasName, reminderType, callback, errorCallback }) => ({
    type: types.UPDATE_SMART_REMINDER,
    payload: { id, categoryId, triggeredAt, aliasName, reminderType },
    callback,
    errorCallback
  }),
  destroySmartReminders: ({ ids, callback, errorCallback }) => ({
    type: types.DESTROY_SMART_REMINDERS, payload: { ids }, callback, errorCallback
  }),
  setSmartRemindersList: ({ smartReminders, customSmartReminders }) => ({
    type: types.SET_SMART_REMINDERS_LIST, payload: { smartReminders, customSmartReminders }
  }),
  setSmartReminderCurrentYear: ({ currentYear }) => ({
    type: types.SET_SMART_REMINDER_CURRENT_YEAR, payload: { currentYear }
  }),
  setSmartReminderYears: ({ years }) => ({ type: types.SET_SMART_REMINDER_YEARS, payload: { years } })
};
