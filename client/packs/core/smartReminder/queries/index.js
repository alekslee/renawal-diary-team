import createSmartReminder from './createSmartReminder.graphql';
import updateSmartReminder from './updateSmartReminder.graphql';
import destroySmartReminders from './destroySmartReminders.graphql';

export { createSmartReminder, updateSmartReminder, destroySmartReminders };
