import { graphQLRequest } from 'lib/utils';
import { createSmartReminder, updateSmartReminder, destroySmartReminders } from './queries';

const api = {
  createSmartReminder: variables => graphQLRequest({
    query: createSmartReminder,
    variables: { input: variables }
  }),
  updateSmartReminder: variables => graphQLRequest({
    query: updateSmartReminder,
    variables: { input: variables }
  }),
  destroySmartReminders: variables => graphQLRequest({
    query: destroySmartReminders,
    variables: { input: variables }
  })
};

export default api;
