export const selectors = {
  getLoadingStatus: state => state.additional.isLoading,
  getLoadingCategoryStatus: state => state.additional.isLoadingCategory,
};