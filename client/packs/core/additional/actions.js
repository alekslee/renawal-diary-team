import { types } from './constants';

export const actions = {
  changeLoaderStatus: ({ status, type }) => ({
    type: types.CHANGE_LOADER_STATUS, payload: { status, type }
  }),
};