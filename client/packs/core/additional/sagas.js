import { fork, put, takeLatest } from 'redux-saga/effects';
import { types } from './constants';

export function* changeLoaderStatus({ payload: { status, type } }) {
  if (type === 'company') {
    yield put({ type: types.SET_LOADER_STATUS, payload: { isLoading: status } });
  } else {
    yield put({ type: types.SET_LOADER_CATEGORY_STATUS, payload: { isLoadingCategory: status } });
  }
}

export function* additionalWatch() {
  yield takeLatest(types.CHANGE_LOADER_STATUS, changeLoaderStatus);
}

export const additionalSagas = [
  fork(additionalWatch)
];
