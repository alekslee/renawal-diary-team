import update from 'immutability-helper';
import { types } from './constants';

const initialState = {
  isLoading: false,
  isLoadingCategory: false
};

export function additionalReducer(state = initialState, { payload, type }) {
  switch (type) {
    case types.SET_LOADER_STATUS:
      return update(state, { isLoading: { $set: payload.isLoading } });
    case types.SET_LOADER_CATEGORY_STATUS:
      return update(state, { isLoadingCategory: { $set: payload.isLoadingCategory } });
    default: {
      return state;
    }
  }
}
