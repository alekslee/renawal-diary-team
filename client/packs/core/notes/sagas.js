import { call, fork, put, takeLatest } from 'redux-saga/effects';
import api from './api';
import { types } from './index';
import { setError } from 'lib/utils';

export function* createNote({ payload: { title, body, files, subject, categoryId }, errorCallback, callback }) {
  try {
    const resp = yield call(api.createNote, { title, body, files, subject, categoryId });

    if (!resp.note || resp.errors.length) {
      if (resp.errors) setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    } else {
      if (callback) callback(resp);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchList({ payload: { year, categoryId }, errorCallback, callback }) {
  try {
    const resp = yield call(api.fetchList, { year, categoryId });
    if (callback) callback(resp);
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* tagWatch() {
  yield takeLatest(types.CREATE_NOTE, createNote);
  yield takeLatest(types.FETCH_NOTE_LIST, fetchList);
}

export const noteSagas = [
  fork(tagWatch)
];
