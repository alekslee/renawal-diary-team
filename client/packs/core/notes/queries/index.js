import createNote from './createNote.graphql';
import noteList from './noteList.graphql';

export { createNote, noteList };
