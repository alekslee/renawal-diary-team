import { types } from './constants';

export const actions = {
  createNote: ({ title, body, files, categoryId, callback, errorCallback }) => ({
    type: types.CREATE_NOTE,
    payload: { title, body, files, categoryId },
    callback,
    errorCallback
  }),
  fetchList: ({ year, categoryId, callback, errorCallback }) => ({
    type: types.FETCH_NOTE_LIST,
    payload: { categoryId, year },
    callback,
    errorCallback
  })
};