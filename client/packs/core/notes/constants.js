//
// ACTION TYPES
//

export const types = {
  CREATE_NOTE: 'CREATE_NOTE',
  FETCH_NOTE_LIST: 'FETCH_NOTE_LIST',
};
