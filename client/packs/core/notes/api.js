import { graphQLRequest } from 'lib/utils';
import { createNote, noteList } from './queries';

const api = {
  fetchList: variables => graphQLRequest({
    query: noteList,
    variables
  }),
  createNote: ({ categoryId, title, body, files }) => graphQLRequest({
    query: createNote,
    type: 'multipart',
    data: [
      { key: 'variables[input][categoryId]', value: categoryId },
      { key: 'variables[input][title]', value: title },
      { key: 'variables[input][body]', value: body },
      ...files.map(file =>  ({ key: 'variables[input][files][]', value: file }) )
    ],
  }),
};

export default api;