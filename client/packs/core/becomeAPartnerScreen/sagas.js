import { call, fork, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import { types } from './constants';
import { setError } from 'lib/utils';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

import api from './api';

export function* becomeAPartnerSendMail({ payload: { name, email, body }, errorCallback, callback }) {
  try {
    yield call(api.becomeAPartnerSendMail, { name, email, body });

    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* tagWatch() {
  yield takeLatest(types.BECOME_A_PARTNER_SEND_MAIL, becomeAPartnerSendMail);
}

export const becomeAPartnerScreenSagas = [
  fork(tagWatch)
];
