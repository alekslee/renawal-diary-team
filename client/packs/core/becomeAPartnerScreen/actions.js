import { types } from './constants';

export const actions = {
  becomeAPartnerSendMail: ({ name, email, body, callback, errorCallback }) => ({
    type: types.BECOME_A_PARTNER_SEND_MAIL, payload: { name, email, body }, callback, errorCallback
  })
};
