import { graphQLRequest } from 'lib/utils';
import { becomeAPartnerSendMail } from './queries';

const api = {
  becomeAPartnerSendMail: ({ name, email, body }) => graphQLRequest({
    query: becomeAPartnerSendMail,
    variables: { input: { name, email, body } }
  })
};

export default api;
