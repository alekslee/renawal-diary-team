export const BASE_APP_URL = 'https://renewaldiary.com';

export const oauthUrls = {
  FACEBOOK: '/users/auth/facebook',
  TWITTER: '/users/auth/twitter',
  GOOGLE: '/users/auth/google_oauth2',
  LINKEDIN: '/users/auth/linkedin'
};
