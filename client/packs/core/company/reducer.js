import update from 'immutability-helper';

import { types, INITIAL_ORDER_BY_COLUMN } from './constants';

const initialState = {
  object: {},
  list: [],
  autocompleateCompanies: [],
  personalizeFilters: {},
  cityOptions: [],
  regionOptions: [],
  totalCount: undefined,
  filterChanged: false,

  companyListTypeFilter: undefined,
  companyListCityFilter: undefined,
  companyListCheckedPlacesFilter: undefined,
  companyListCriteriaQuestionsFilter: undefined,
  companyListCountryFilter: undefined,
  companyListTimeFilter: undefined,
  companyListKeyWordFilter: undefined,
  companyListZipCodeFilter: undefined,
  companyListAddressFilter: undefined,

  countriesSelectOptions: undefined,
  isShortCompaniesList: true,
  companiesListPage: 1,
  companyListPerPage: undefined,
  averagePrice: undefined,
  recommendedSource: undefined,
  recommendedProvider: undefined,
  orderByColumn: INITIAL_ORDER_BY_COLUMN,
  personalizeFilterStatus: undefined,
  categoriesPersonalizeFilters: [],
};

export function companyReducer(state = initialState, { payload, type }) {
  state = { ...initialState, ...state };
  switch (type) {
    case types.RESET_COMPANY_ORDER_BY_COLUMN:
      return update(state, { orderByColumn: { $set: INITIAL_ORDER_BY_COLUMN } });
    case types.SET_ORDER_BY_COLUMN:
      return update(state, { orderByColumn: { $set: payload.orderByColumn } });
    case types.SET_COMPANY_RECOMMENDED_PROVIDER:
      return update(state, { recommendedProvider: { $set: payload.recommendedProvider } });
    case types.SET_COMPANY_RECOMMENDED_SOURCE:
      return update(state, { recommendedSource: { $set: payload.recommendedSource } });
    case types.SET_COMPANY_AVERAGE_PRICE:
      return update(state, { averagePrice: { $set: payload.averagePrice } });
    case types.SET_COMPANY_COMPANIES_LIST_PAGE:
      return update(state, { companiesListPage: { $set: payload.companiesListPage } });
    case types.SET_COMPANY_IS_SHORT_COMPANIES_LIST:
      return update(state, { isShortCompaniesList: { $set: payload.isShortCompaniesList } });
    case types.SET_COMPANY_LIST_TIME_FILTER:
      return update(state, { companyListTimeFilter: { $set: payload.companyListTimeFilter } });
    case types.SET_COMPANY_LIST_COUNTRY_FILTER:
      return update(state, { companyListCountryFilter: { $set: payload.companyListCountryFilter } });
    case types.SET_COMPANY_COUNTRIES_SELECT_OPTIONS:
      return update(state, { countriesSelectOptions: { $set: payload.countriesSelectOptions } });
    case types.SET_COMPANY_LIST_CHECKED_PLACES_FILTER:
      return update(state, { companyListCheckedPlacesFilter: { $set: payload.checkedPlaces } });
    case types.SET_COMPANY_LIST_CRITERIA_QUESTIONS_FILTER:
      return update(state, { companyListCriteriaQuestionsFilter: { $set: payload.criteriaQuestions } });
    case types.SET_COMPANY_LIST_KEY_WORD_FILTER:
      return update(state, { companyListKeyWordFilter: { $set: payload.companyListKeyWordFilter } });
    case types.SET_COMPANY_LIST_ZIP_CODE_FILTER:
      return update(state, { companyListZipCodeFilter: { $set: payload.companyListZipCodeFilter } });
    case types.SET_COMPANY_LIST_TYPE_FILTER:
      return update(state, { companyListTypeFilter: { $set: payload.companyType } });
    case types.SET_COMPANY_LIST_CITY_FILTER:
      return update(state, { companyListCityFilter: { $set: payload.companyListCityFilter } });
    case types.SET_PER_PAGE_COMPANIES_LIST:
      return update(state, { companyListPerPage: { $set: payload.companyListPerPage } });
    case types.SET_COMPANY_LIST_FILTERS:
      const toUpdate = Object.keys(payload.filters).reduce((hash, key) => {
        const setData = key === 'personalizeFilters' ?
          { criteriaQuestions: { $set: payload.filters[key].criteriaQuestions } } :
          { $set: payload.filters[key] }

        return Object.assign(hash, {
          [key]: setData
        })
      }, {});
      return update(state, toUpdate);
    case types.SET_COMPANY_TOTAL_COUNT:
      return update(state, { totalCount: { $set: payload.totalCount } });
    case types.SET_COMPANIES_LIST:
      return update(state, { list: { $set: payload.companies } });
    case types.SET_AUTOCOMPLEATE_COMPANIES_LIST:
      return update(state, { autocompleateCompanies: { $set: payload.autocompleateCompanies } });
    case types.SET_PERSONALIZE_FILTER_STATUS:
      return update(state, { personalizeFilterStatus: { $set: payload.personalizeFilterStatus } });
    case types.SET_PERSONALIZE_FILTER:
      return update(state, { personalizeFilters: { $set: payload.personalizeFilters } });
    case types.SET_CRITERIA_QUESTIONS:
      return update(state, { personalizeFilters: { criteriaQuestions: { $set: payload.criteriaQuestions } } });
    case types.SET_CITY_OPTIONS:
      return update(state, { cityOptions: { $set: payload.cityOptions } });
    case types.SET_REGION_OPTIONS:
      return update(state, { regionOptions: { $set: payload.regionOptions } });
    case types.SET_FILTER_STATUS:
      return update(state, { filterChanged: { $set: payload.filterChanged } });
    case types.SET_COMPANY_LIST_ADDRESS_FILTER:
      return update(state, { companyListAddressFilter: { $set: payload.companyListAddressFilter } });
    case types.SET_CATEGORIES_PERSONALIZE_FILTERS:
      return update(state, { categoriesPersonalizeFilters: { $set: payload.categoriesPersonalizeFilters } });
    default: {
      return state;
    }
  }
}
