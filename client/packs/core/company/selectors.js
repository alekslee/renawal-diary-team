import { createSelector } from 'reselect';
import { COMPANIES_PER_PAGE } from './constants';

const getCompanies = state => state.company.list;
const getCompaniesTotalCount = state => state.company.totalCount;

export const selectors = {
  getCompanies,
  getCompaniesTotalCount,
  getCompanyListPerPage: state => state.company.companyListPerPage,
  getCompanyListCheckedPlacesFilter: state => state.company.companyListCheckedPlacesFilter,

  getCompanyListTypeFilter: state => state.company.companyListTypeFilter,
  getCompanyListCountryFilter: state => state.company.companyListCountryFilter,
  getCompanyListTimeFilter: state => state.company.companyListTimeFilter,
  getCompanyListKeyWordFilter: state => state.company.companyListKeyWordFilter,
  getCompanyListZipCodeFilter: state => state.company.companyListZipCodeFilter,
  getCompanyListCityFilter: state => state.company.companyListCityFilter,
  getCompanyListAddressFilter: state => state.company.companyListAddressFilter,
  getCompanyListCriteriaQuestionsFilter: state => state.company.companyListCriteriaQuestionsFilter,

  getCountrySelectOptions: state => state.company.countriesSelectOptions,
  getOrderByColumn: state => state.company.orderByColumn,
  getAutocompleateCompanies: state => state.company.autocompleateCompanies,
  getPagesCount: createSelector(
    [getCompaniesTotalCount],
    (totalCount) => {
      return Math.ceil(totalCount / COMPANIES_PER_PAGE);
    }
  ),
  getIsShortCompaniesList: state => state.company.isShortCompaniesList,
  getCompaniesListPage: state => state.company.companiesListPage,
  getRecomendedSource: state => state.company.recommendedSource,
  getRecomendedProvider: state => state.company.recommendedProvider,
  getAveragePrice: state => state.company.averagePrice,
  getPersonalFilterOptions: state => {
    const opt = (state.company.personalizeFilters || {}).personalCriteriaFilterOptions;
    if (opt) return JSON.parse(opt);
  },
  getPersonalizeFilter: state => {
    const opt = (state.company.personalizeFilters || {}).criteriaQuestions;
    if (opt) return JSON.parse(opt);
  },
  getCityOptions: state => state.company.cityOptions,
  getRegionOptions: state => state.company.regionOptions,
  getFilterStatus: state => state.company.filterChanged,
  getPersonalizeFilterStatus: state => state.company.personalizeFilterStatus,
  getCategoriesPersonalizeFilters: state => state.company.categoriesPersonalizeFilters,
};
