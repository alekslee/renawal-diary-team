import { createSubscription, graphQLRequest } from 'lib/utils';
import {
  fetchCompaniesList,
  countriesSelectOptions,
  personalizeFilters,
  createCategoryPersonalizeFilter,
  fetchCompaniesListForAutocompleate,
  fetchCityOptions,
  fetchRegionOptions,
  createCompany,
} from './queries';

const api = {
  fetchCompaniesList: variables => graphQLRequest({
    query: fetchCompaniesList,
    variables
  }),
  fetchCountriesSelectOptions: variables => graphQLRequest({
    query: countriesSelectOptions,
    variables
  }),
  fetchLeaderboardScreenData: variables => graphQLRequest({
    queries: [
      {
        query: fetchCompaniesList,
        variables
      },
      {
        query: personalizeFilters,
        variables: { categoryId:  variables.categoryId }
      }
    ]
  }),
  fetchCompaniesListForAutocompleate: variables => graphQLRequest({
    query: fetchCompaniesListForAutocompleate,
    variables
  }),
  createCategoryPersonalizeFilter: variables => graphQLRequest({
    query: createCategoryPersonalizeFilter,
    variables: { input: variables }
  }),
  fetchCityOptions: variables => graphQLRequest({
    query: fetchCityOptions,
    variables
  }),
  fetchRegionOptions: variables => graphQLRequest({
    query: fetchRegionOptions,
    variables
  }),
  createCompany: variables => graphQLRequest({
    query: createCompany,
    variables: { input: variables }
  }),
};

export default api;
