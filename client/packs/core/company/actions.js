import { types } from './constants';

const actionWrapper = (type, payload) => {
  const { callback, errorCallback } = payload;

  return ({
    type: types[type], payload, callback, errorCallback
  });
}

export const actions = {
  fetchCompaniesList: ({ page, perPage, companyType, callback, errorCallback }) => ({
    type: types.FETCH_COMPANIES_LIST, payload: { page, perPage, companyType }, callback, errorCallback
  }),
  createCompany: payload => actionWrapper('CREATE_COMPANY', payload),
  setCompaniesList: ({ companies }) => ({ type: types.SET_COMPANIES_LIST, payload: { companies } }),
  fetchCompanyListTypeFilter: ({ companyType, callback, errorCallback }) => ({
    type: types.FETCH_COMPANY_LIST_TYPE_FILTER, payload: { companyType }, callback, errorCallback
  }),
  setCompaniesListTypeFilter: ({ companyType }) => ({
    type: types.SET_COMPANY_LIST_TYPE_FILTER, payload: { companyType }
  }),
  fetchCompanyListCheckedPlacesFilter: ({ checkedPlaces, callback, errorCallback }) => ({
    type: types.FETCH_COMPANY_LIST_CHECKED_PLACES_FILTER, payload: { checkedPlaces }, callback, errorCallback
  }),
  setCompaniesListCheckedPlacesFilter: ({ checkedPlaces }) => ({
    type: types.SET_COMPANY_LIST_CHECKED_PLACES_FILTER, payload: { checkedPlaces }
  }),

  fetchLeaderboardScreenData: ({ callback, errorCallback }) => ({
    type: types.FETCH_LEADERBOARD_SCREEN_DATA, payload: {}, callback, errorCallback
  }),
  fetchCountriesSelectOptions: ({ callback, errorCallback }) => ({
    type: types.FETCH_COUNTRIES_SELECT_OPTIONS, payload: {}, callback, errorCallback
  }),
  fetchCompanyListCountryFilter: ({ companyListCountryFilter, callback, errorCallback }) => ({
    type: types.FETCH_COMPANY_LIST_COUNTRY_FILTER, payload: { companyListCountryFilter }, callback, errorCallback
  }),
  fetchCompaniesListCriteriaQuestionsFilter: ({ criteriaQuestions, categoryId, callback, errorCallback }) => ({
    type: types.FETCH_COMPANY_LIST_CRITERIA_QUESTIONS_FILTER, payload: { criteriaQuestions }, callback, errorCallback
  }),
  setCompaniesListCriteriaQuestionsFilter: ({ criteriaQuestions, callback, errorCallback }) => ({
    type: types.SET_COMPANY_LIST_CRITERIA_QUESTIONS_FILTER, payload: { criteriaQuestions }, callback, errorCallback
  }),
  setCompanyListCountryFilter: ({ companyListCountryFilter }) => ({
    type: types.SET_COMPANY_LIST_COUNTRY_FILTER, payload: { companyListCountryFilter }
  }),

  setCompanyListKeyWordFilter: ({ companyListKeyWordFilter }) => ({
    type: types.SET_COMPANY_LIST_KEY_WORD_FILTER, payload: { companyListKeyWordFilter }
  }),
  fetchCompanyListKeyWordFilter: ({ companyListKeyWordFilter, callback, errorCallback }) => ({
    type: types.FETCH_COMPANY_LIST_KEY_WORD_FILTER, payload: { companyListKeyWordFilter }, callback, errorCallback
  }),

  setCompanyListZipCodeFilter: ({ companyListZipCodeFilter }) => ({
    type: types.SET_COMPANY_LIST_ZIP_CODE_FILTER, payload: { companyListZipCodeFilter }
  }),
  fetchCompanyListZipCodeFilter: ({ companyListZipCodeFilter, callback, errorCallback }) => ({
    type: types.FETCH_COMPANY_LIST_ZIP_CODE_FILTER, payload: { companyListZipCodeFilter }, callback, errorCallback
  }),

  fetchCompaniesListForAutocompleate: ({ categoryId, keyWord, callback, errorCallback }) => ({
    type: types.FETCH_AUTOCOMPLEATE_COMPANIES_LIST, payload: { categoryId, keyWord }, callback, errorCallback
  }),
  setCompaniesListForAutocompleate: ({ autocompleateCompanies, callback, errorCallback }) => ({
    type: types.SET_AUTOCOMPLEATE_COMPANIES_LIST, payload: { autocompleateCompanies }, callback, errorCallback
  }),
  setCompanyTotalCount: ({ totalCount }) => ({
    type: types.SET_COMPANY_TOTAL_COUNT, payload: { totalCount }
  }),
  setCompanyCountriesSelectOptions: ({ countriesSelectOptions }) => ({
    type: types.SET_COMPANY_COUNTRIES_SELECT_OPTIONS, payload: { countriesSelectOptions }
  }),
  fetchCompanyListTimeFilter: ({ companyListTimeFilter, callback, errorCallback }) => ({
    type: types.FETCH_COMPANY_LIST_TIME_FILTER, payload: { companyListTimeFilter }, callback, errorCallback
  }),
  setCompanyListTimeFilter: ({ companyListTimeFilter }) => ({
    type: types.SET_COMPANY_LIST_TIME_FILTER, payload: { companyListTimeFilter }
  }),
  fetchCompanyListCityFilter: ({ companyListCityFilter, callback, errorCallback }) => ({
    type: types.FETCH_COMPANY_LIST_CITY_FILTER, payload: { companyListCityFilter }, callback, errorCallback
  }),
  setCompanyListCityFilter: ({ companyListCityFilter }) => ({
    type: types.SET_COMPANY_LIST_CITY_FILTER, payload: { companyListCityFilter }
  }),
  setCompanyIsShortCompaniesList: ({ isShortCompaniesList }) => ({
    type: types.SET_COMPANY_IS_SHORT_COMPANIES_LIST, payload: { isShortCompaniesList }
  }),
  setPerPageCompaniesList: (companyListPerPage) => ({
    type: types.SET_PER_PAGE_COMPANIES_LIST, payload: { companyListPerPage }
  }),
  setCompanyCompaniesListPage: ({ companiesListPage }) => ({
    type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage }
  }),
  updateCompaniesOrdering: ({ ordering, callback, errorCallback }) => ({
    type: types.UPDATE_COMPANIES_ORDERING, payload: { ordering }, callback, errorCallback
  }),
  fetchCityOptions: ({ countryCode, keyWord, callback, errorCallback }) => ({
    type: types.FETCH_CITY_OPTIONS, payload: { countryCode, keyWord }, callback, errorCallback
  }),
  setCityOptions: ({ cityOptions }) => ({
    type: types.SET_CITY_OPTIONS, payload: { cityOptions }
  }),
  fetchRegionOptions: ({ countryCode, keyWord, callback, errorCallback }) => ({
    type: types.FETCH_REGION_OPTIONS, payload: { countryCode, keyWord }, callback, errorCallback
  }),
  setRegionOptions: ({ regionOptions }) => ({
    type: types.SET_REGION_OPTIONS, payload: { regionOptions }
  }),
  resetCompanyOrderByColumn: () => ({ type: types.RESET_COMPANY_ORDER_BY_COLUMN }),
  setCompanyDefaultFilterOptions: ({ callback }) => ({ type: types.SET_COMPANY_DEFAULT_FILTER_OPTIONS, callback }),
  createCategoryPersonalizeFilter: ({ criteriaQuestions, categoryId, noFetch, callback, errorCallback }) => ({
    type: types.CREATE_CATEGORY_PERSONALIZE_FILTER, payload: { criteriaQuestions, categoryId, noFetch }, callback, errorCallback
  }),
  setFilterStatus: ({ filterChanged, callback, errorCallback }) => ({
    type: types.SET_FILTER_STATUS,
    payload: { filterChanged }, callback, errorCallback
  }),
  setCompanyListFilters: filters => ({ type: types.SET_COMPANY_LIST_FILTERS, payload: { filters }}),
  fetchCompanyListFilters: ({ callback, errorCallback, ...filters }) => ({ type: types.FETCH_COMPANY_LIST_FILTERS, payload: { filters }, callback, errorCallback }),
  changePersonalizeFilterStatus: ({ personalizeFilterStatus }) => ({
    type: types.CHANGE_PERSONALIZE_FILTER_STATUS, payload: { personalizeFilterStatus }
  }),
  resetCompanyListFilters: ({ callback, errorCallback, resetCriteria } = {}) => {
    const filters = {
      companyListCheckedPlacesFilter: undefined,
      companyListCountryFilter: undefined,
      companyListCityFilter: undefined,
      companyListTimeFilter: undefined,
      companyListKeyWordFilter: undefined,
      companyListZipCodeFilter: undefined,
      companyListTypeFilter: undefined,
      companyListAddressFilter: undefined
    };
    if (resetCriteria) filters['companyListCriteriaQuestionsFilter'] = undefined;
    if (resetCriteria) filters['personalizeFilters'] = { criteriaQuestions: '{}' };

    return ({ type: types.FETCH_COMPANY_LIST_FILTERS, payload: { filters }, callback, errorCallback });
  }
};
