import { call, fork, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import { setError, apiWrapper } from 'lib/utils';

import {
  retrieveCompaniesFilterAttributes,
  retrieveLeaderBoardPersonalizeFilters,
  retrieveCategoryPersonalizeFilters,
} from 'lib/helpers/companies';

import { selectors as categorySelectors } from 'core/category';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { selectors } from './selectors';
import {
  types,
  EMPTY_COMPANY_TYPES_FILTERS,
  FIRST_COMPANIES_PER_PAGE,
  COMPANIES_PER_PAGE,
  CHECKED_PLACES_OPTIONS,
  TIME_OPTIONS,
  DEFAULT_CHECKED_PLACES_FILTER,
  DEFAULT_LAST_BUY_FILTER
} from './constants';
import { types as loaderTypes } from 'core/additional';
import { changeLoaderStatus } from 'core/additional/sagas';

import api from './api';

export function* fetchLeaderboardScreenData({ payload: {}, errorCallback, callback }) {
  try {
    const params = yield* retrieveCompaniesFilterAttributes();

    if (!params.categoryId) {
      console.error('categoryId is null');
      return null;
    }
    yield put({ type: loaderTypes.SET_LOADER_STATUS, payload: { isLoading: true } });

    const resp = yield call(api.fetchLeaderboardScreenData, params);

    yield put({ type: types.SET_COMPANIES_LIST, payload: { companies: resp.categoryCompanies.companies } });
    yield put({ type: types.SET_PERSONALIZE_FILTER_STATUS, payload: { personalizeFilterStatus: resp.personalizeFilters.isActive } });
    yield put({ type: types.SET_COMPANY_TOTAL_COUNT, payload: { totalCount: resp.categoryCompanies.totalCount } });
    yield put({
      type: types.SET_COMPANY_RECOMMENDED_SOURCE,
      payload: { recommendedSource: resp.categoryCompanies.recommendedSource }
    });
    yield put({
      type: types.SET_COMPANY_RECOMMENDED_PROVIDER,
      payload: { recommendedProvider: resp.categoryCompanies.recommendedProvider }
    });
    yield put({
      type: types.SET_COMPANY_AVERAGE_PRICE,
      payload: { averagePrice: resp.categoryCompanies.averagePrice }
    });

    const personalizeFilters = yield* retrieveLeaderBoardPersonalizeFilters(resp.personalizeFilters);
    yield put({ type: types.SET_PERSONALIZE_FILTER, payload: { personalizeFilters: personalizeFilters } });

    yield put({ type: loaderTypes.SET_LOADER_STATUS, payload: { isLoading: false } });
    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchCountriesSelectOptions({ payload: {}, errorCallback, callback }) {
  try {
    const params = yield* retrieveCompaniesFilterAttributes();
    const resp = yield call(api.fetchCountriesSelectOptions, params);

    yield put({
      type: types.SET_COMPANY_COUNTRIES_SELECT_OPTIONS,
      payload: { countriesSelectOptions: resp.countriesSelectOptions }
    });
    // let companyListCountryFilter = yield select(selectors.getCompanyListCountryFilter);
    // companyListCountryFilter = resp.countriesSelectOptions.find(({ value }) => (
    //   value.toLowerCase() === (companyListCountryFilter && companyListCountryFilter.value &&
    //     companyListCountryFilter.value.toLowerCase())
    // ));
    // yield put({ type: types.SET_COMPANY_LIST_COUNTRY_FILTER, payload: { companyListCountryFilter } });

    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchCompaniesList({ payload: { page, perPage, filterChanged }, errorCallback, callback }) {
  //throw new Error('sdsd')
  yield put({ type: types.SET_FILTER_STATUS, payload: { filterChanged } });
  yield put({ type: loaderTypes.SET_LOADER_STATUS, payload: { isLoading: true } });

  try {
    const params = yield* retrieveCompaniesFilterAttributes(page, perPage);
    const resp = yield call(api.fetchCompaniesList, params);
    yield put({ type: types.SET_FILTER_STATUS, payload: { filterChanged: false } });

    if (!resp.errors.length) {
      yield put({ type: types.SET_COMPANIES_LIST, payload: { companies: resp.companies } });
      yield put({ type: types.SET_COMPANY_TOTAL_COUNT, payload: { totalCount: resp.totalCount } });
      yield put({ type: types.SET_COMPANY_RECOMMENDED_SOURCE, payload: { recommendedSource: resp.recommendedSource } });
      yield put({
        type: types.SET_COMPANY_RECOMMENDED_PROVIDER,
        payload: { recommendedProvider: resp.recommendedProvider }
      });
      yield put({
        type: types.SET_COMPANY_AVERAGE_PRICE,
        payload: { averagePrice: resp.averagePrice }
      });
      yield put({ type: loaderTypes.SET_LOADER_STATUS, payload: { isLoading: false } });

      if (callback) callback();
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchCompanyListTypeFilter({ payload: { companyType }, errorCallback, callback }) {
  yield put({ type: types.SET_COMPANY_LIST_TYPE_FILTER, payload: { companyType } });
  yield put({ type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage: 1 } });

  yield* fetchCompaniesList({ payload: { filterChanged: true }, errorCallback, callback });
}

export function* fetchCompanyListCheckedPlacesFilter({ payload: { checkedPlaces }, errorCallback, callback }) {
  yield put({ type: types.SET_COMPANY_LIST_CHECKED_PLACES_FILTER, payload: { checkedPlaces } });
  yield put({ type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage: 1 } });

  yield* fetchCompaniesList({ payload: { filterChanged: true }, errorCallback, callback });
}

export function* fetchCompanyListCountryFilter({ payload: { companyListCountryFilter }, errorCallback, callback }) {
  yield put({ type: types.SET_COMPANY_LIST_COUNTRY_FILTER, payload: { companyListCountryFilter } });
  yield put({ type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage: 1 } });

  yield* fetchCompaniesList({ payload: { filterChanged: true }, errorCallback, callback });
}

export function* fetchCompanyListTimeFilter({ payload: { companyListTimeFilter }, errorCallback, callback }) {
  yield put({ type: types.SET_COMPANY_LIST_TIME_FILTER, payload: { companyListTimeFilter } });
  yield put({ type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage: 1 } });

  yield* fetchCompaniesList({ payload: { filterChanged: true }, errorCallback, callback });
}

export function* fetchCompaniesListCriteriaQuestionsFilter({ payload: { criteriaQuestions }, errorCallback, callback }) {
  let personalizeFilterStatus = yield select(selectors.getPersonalizeFilterStatus);

  if (personalizeFilterStatus) {
    yield put({ type: types.SET_COMPANY_LIST_CRITERIA_QUESTIONS_FILTER, payload: { criteriaQuestions } });
  }
  yield put({ type: types.SET_CRITERIA_QUESTIONS, payload: { criteriaQuestions } });
  yield* fetchCompaniesList({ payload: { filterChanged: true }, errorCallback, callback });
  yield put({ type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage: 1 } });

}

export function* fetchCompanyListKeyWordFilter({ payload: { companyListKeyWordFilter }, errorCallback, callback }) {
  yield put({ type: types.SET_COMPANY_LIST_KEY_WORD_FILTER, payload: { companyListKeyWordFilter } });
  yield put({ type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage: 1 } });

  yield* fetchCompaniesList({ payload: { filterChanged: true }, errorCallback, callback });
}

export function* fetchCompanyListZipCodeFilter({ payload: { companyListZipCodeFilter }, errorCallback, callback }) {
  yield put({ type: types.SET_COMPANY_LIST_ZIP_CODE_FILTER, payload: { companyListZipCodeFilter } });
  yield put({ type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage: 1 } });

  yield* fetchCompaniesList({ payload: { filterChanged: true }, errorCallback, callback });
}

export function* fetchCompanyListCityFilter({ payload: { companyListCityFilter }, errorCallback, callback }) {
  yield put({ type: types.SET_COMPANY_LIST_CITY_FILTER, payload: { companyListCityFilter } });
  yield put({ type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage: 1 } });

  yield* fetchCompaniesList({ payload: { filterChanged: true }, errorCallback, callback });
}

export function* fetchCompanyListAddressFilter({ payload: { companyListAddressFilter }, errorCallback, callback }) {
  yield put({ type: types.SET_COMPANY_LIST_ADDRESS_FILTER, payload: { companyListAddressFilter } });
  yield put({ type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage: 1 } });

  yield* fetchCompaniesList({ payload: { filterChanged: true }, errorCallback, callback });
}

export function* fetchCompanyListFilters({ payload: { filters }, callback, errorCallback }) {
  yield put({ type: types.SET_COMPANY_LIST_FILTERS, payload: { filters } });
  yield put({ type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage: 1 } });

  yield* fetchCompaniesList({ payload: { filterChanged: true }, errorCallback, callback });
}

export function* fetchCompaniesListForAutocompleate({ payload: { categoryId, keyWord }, errorCallback, callback }) {
  try {
    const resp = yield call(api.fetchCompaniesListForAutocompleate, { categoryId, keyWord, autocompleate: 'true' });
    yield put({ type: types.SET_AUTOCOMPLEATE_COMPANIES_LIST, payload: { autocompleateCompanies: resp.companies } });
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* updateCompaniesOrdering({ payload: { ordering }, errorCallback, callback }) {
  try {
    yield put({ type: types.SET_ORDER_BY_COLUMN, payload: { orderByColumn: ordering } });
    yield* fetchCompaniesList({ payload: { filterChanged: true } });
    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* createCompany(data) {

  yield apiWrapper(data, {
    func: api.createCompany,
    onSuccess: function* (resp) {
      // yield pushSmartReminderAndOrder(resp.smartReminder);
    },
    onFailure: function* (resp) {

    },
  })

}

export function* setCompanyDefaultFilterOptions({ callback }) {
  const renewalDetail = yield select(categorySelectors.getRenewalDetail);
  const country = yield select(currentUserSelectors.getUserCountry);
  const currentCountry = yield select(currentCountrySelectors.getCurrentCountry);
  const countrySelectOptions = (yield select(selectors.getCountrySelectOptions)) || [];
  let countryCode;

  if (renewalDetail) {

    countryCode = (renewalDetail.criteriaQuestions && renewalDetail.criteriaQuestions.location &&
        renewalDetail.criteriaQuestions.location.countryCode &&
        renewalDetail.criteriaQuestions.location.countryCode.toLowerCase()) ||
      country.isoA2Code || currentCountry.isoA2Code;
  } else {
    countryCode = (country && country.isoA2Code) || currentCountry.isoA2Code;
  }

  const checkedPlacesValue = (renewalDetail && renewalDetail.checkedPlaces) || DEFAULT_CHECKED_PLACES_FILTER;
  const checkedPlaces = CHECKED_PLACES_OPTIONS.find(({ value }) => value === checkedPlacesValue);
  yield put({ type: types.SET_COMPANY_LIST_CHECKED_PLACES_FILTER, payload: { checkedPlaces } });

  const timeFilterValue = (renewalDetail && renewalDetail.lastBuy) || DEFAULT_LAST_BUY_FILTER;
  const companyListTimeFilter = TIME_OPTIONS.find(({ value }) => value === timeFilterValue);
  yield put({ type: types.SET_COMPANY_LIST_TIME_FILTER, payload: { companyListTimeFilter } });

  const companyListCountryFilter = countrySelectOptions.find(({ value }) => value === countryCode) ||
    { value: countryCode };
  yield put({ type: types.SET_COMPANY_LIST_COUNTRY_FILTER, payload: { companyListCountryFilter } });
  if (callback) callback();
}

export function* createCategoryPersonalizeFilter({ payload: { criteriaQuestions, categoryId, noFetch }, errorCallback, callback }) {

  try {
    const isActive = yield select(selectors.getPersonalizeFilterStatus);
    const criteriaQuestionsAttributes = isActive ? criteriaQuestions : null;

    if (!isActive){
      const personalizeFilters = yield* retrieveCategoryPersonalizeFilters(categoryId, criteriaQuestions);

      yield put({
        type: types.SET_CATEGORIES_PERSONALIZE_FILTERS,
        payload: { categoriesPersonalizeFilters: personalizeFilters }
      });
    }

    const resp = yield call(api.createCategoryPersonalizeFilter, {
      criteriaQuestions: criteriaQuestionsAttributes, isActive, categoryId
    });

    if (!resp.errors.length) {
      if (!noFetch) {
        yield* fetchCompaniesListCriteriaQuestionsFilter({ payload: { criteriaQuestions } });
      }
      if (callback) callback();
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchCityOptions({ payload: { countryCode, keyWord }, errorCallback, callback }) {
  try {
    const resp = yield call(api.fetchCityOptions, { countryCode, keyWord });
    yield put({ type: types.SET_CITY_OPTIONS, payload: { cityOptions: resp } });

    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchRegionOptions({ payload: { countryCode, keyWord }, errorCallback, callback }) {
  try {
    const resp = yield call(api.fetchRegionOptions, { countryCode, keyWord });
    yield put({ type: types.SET_REGION_OPTIONS, payload: { regionOptions: resp } });

    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* changePersonalizeFilterStatus({ payload: { personalizeFilterStatus }, errorCallback, callback }) {
  try {
    yield put({ type: types.SET_PERSONALIZE_FILTER_STATUS, payload: { personalizeFilterStatus } });

    if (!personalizeFilterStatus) {
      yield put({ type: types.SET_COMPANY_LIST_CRITERIA_QUESTIONS_FILTER, payload: { criteriaQuestions: "{}" } });
    }

    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* companyWatch() {
  yield takeLatest(types.FETCH_COMPANIES_LIST, fetchCompaniesList);
  yield takeLatest(types.FETCH_AUTOCOMPLEATE_COMPANIES_LIST, fetchCompaniesListForAutocompleate);
  yield takeLatest(types.FETCH_COMPANY_LIST_TYPE_FILTER, fetchCompanyListTypeFilter);
  yield takeLatest(types.FETCH_LEADERBOARD_SCREEN_DATA, fetchLeaderboardScreenData);

  yield takeLatest(types.FETCH_COUNTRIES_SELECT_OPTIONS, fetchCountriesSelectOptions);

  yield takeLatest(types.FETCH_COMPANY_LIST_CHECKED_PLACES_FILTER, fetchCompanyListCheckedPlacesFilter);
  yield takeLatest(types.FETCH_COMPANY_LIST_CRITERIA_QUESTIONS_FILTER, fetchCompaniesListCriteriaQuestionsFilter);
  yield takeLatest(types.FETCH_COMPANY_LIST_COUNTRY_FILTER, fetchCompanyListCountryFilter);
  yield takeLatest(types.FETCH_COMPANY_LIST_TIME_FILTER, fetchCompanyListTimeFilter);
  yield takeLatest(types.FETCH_COMPANY_LIST_KEY_WORD_FILTER, fetchCompanyListKeyWordFilter);
  yield takeLatest(types.FETCH_COMPANY_LIST_ZIP_CODE_FILTER, fetchCompanyListZipCodeFilter);
  yield takeLatest(types.FETCH_COMPANY_LIST_ADDRESS_FILTER, fetchCompanyListAddressFilter);

  yield takeLatest(types.FETCH_COMPANY_LIST_CITY_FILTER, fetchCompanyListCityFilter);
  yield takeLatest(types.UPDATE_COMPANIES_ORDERING, updateCompaniesOrdering);
  yield takeLatest(types.SET_COMPANY_DEFAULT_FILTER_OPTIONS, setCompanyDefaultFilterOptions);
  yield takeLatest(types.CREATE_CATEGORY_PERSONALIZE_FILTER, createCategoryPersonalizeFilter);
  yield takeLatest(types.FETCH_CITY_OPTIONS, fetchCityOptions);
  yield takeLatest(types.FETCH_REGION_OPTIONS, fetchRegionOptions);
  yield takeLatest(types.FETCH_COMPANY_LIST_FILTERS, fetchCompanyListFilters);
  yield takeLatest(types.CHANGE_PERSONALIZE_FILTER_STATUS, changePersonalizeFilterStatus);
  yield takeLatest(types.CREATE_COMPANY, createCompany);
  yield takeLatest(loaderTypes.CHANGE_LOADER_STATUS, changeLoaderStatus);
}

export const companySagas = [
  fork(companyWatch)
];
