import { createSelector } from 'reselect';

export const selectors = {
  getCurrentCountry: state => state.currentCountry.object,
  getCurrentCountryCode: state => state.currentCountry.object.isoA2Code
};
