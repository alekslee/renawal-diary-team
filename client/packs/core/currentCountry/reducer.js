import update from 'immutability-helper';

import { types } from './constants';

const initialState = {
  object: {}
};

export function currentCountryReducer(state = initialState, { payload, type }) {
  switch (type) {
    case types.SET_CURRENT_COUNTRY:
      return update(state, { object: { $set: payload.currentCountry } });
    default: {
      return state;
    }
  }
}
