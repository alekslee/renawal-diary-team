import { types } from './constants';

export const actions = {
  setCurrentCountry: ({ currentCountry }) => ({
    type: types.SET_CURRENT_COUNTRY, payload: { currentCountry }
  })
};
