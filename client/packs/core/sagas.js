import { all } from 'redux-saga/effects';
import { currentUserSagas } from './currentUser';
import { currentCountrySagas } from './currentCountry';
import { becomeAPartnerScreenSagas } from './becomeAPartnerScreen';
import { categorySagas } from './category';
import { companySagas } from './company';
import { renewalDetailSagas } from './renewalDetail';
import { feedbackSagas } from './feedback';
import { formSliderSagas } from './formSlider';
import { smartReminderSagas } from './smartReminder';
import { signUpSagas } from './signUp';
import { voteSagas } from './votes';
import { noteSagas } from './notes';
import 'babel-polyfill';

export default function* sagas() {
  yield all([
    ...currentUserSagas,
    ...currentCountrySagas,
    ...becomeAPartnerScreenSagas,
    ...categorySagas,
    ...companySagas,
    ...renewalDetailSagas,
    ...feedbackSagas,
    ...formSliderSagas,
    ...smartReminderSagas,
    ...signUpSagas,
    ...voteSagas,
    ...noteSagas
  ]);
}
