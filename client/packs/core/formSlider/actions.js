import { types } from './constants';

export const actions = {
  setActiveSliderId: ({ activeSliderID }) => ({
    type: types.SET_ACTIVE_SLIDER_ID, payload: { activeSliderID }
  })
};
