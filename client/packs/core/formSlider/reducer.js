import update from 'immutability-helper';

import { types } from './constants';

const initialState = {
  activeSliderID: null
};

export function formSliderReducer(state = initialState, { payload, type }) {
  switch (type) {
    case types.SET_ACTIVE_SLIDER_ID:
      return update(state, { activeSliderID: { $set: payload.activeSliderID } });
    default: {
      return state;
    }
  }
}
