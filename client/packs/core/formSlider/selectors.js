import { createSelector } from 'reselect';

export const selectors = {
  getActiveSliderId: state => state.formSlider.activeSliderID
};
