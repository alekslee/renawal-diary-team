import { call, fork, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import { setError } from 'lib/utils';

import api from './api';

export function* formSliderWatch() {
}

export const formSliderSagas = [
  fork(formSliderWatch)
];
