import { createSubscription, graphQLRequest } from 'lib/utils';
import {
  categorySubscribe,
  categoriesSubscribeMembers,
  categorySubscribeMembers,
  countryCategories,
  fetchCategory,
  fetchSmartReminders,
  fetchCustomSmartReminders,
  userCountryCategories
} from './queries';
import { subscriptionIds } from './constants';

const api = {
  subscribeToCategoriesCounterChannel: ({ onReceive, onError, onCompleted }) => createSubscription({
    subscriptionId: subscriptionIds.CATEGORIES_COUNTER_CHANNEL,
    query: categoriesSubscribeMembers,
    onReceive,
    onError,
    onCompleted
  }),
  subscribeToCategoryCounterChannel: ({ onReceive, onError, onCompleted }) => createSubscription({
    subscriptionId: subscriptionIds.CATEGORY_COUNTER_CHANNEL,
    query: categorySubscribeMembers,
    onReceive,
    onError,
    onCompleted
  }),
  categorySubscribe: variables => graphQLRequest({
    query: categorySubscribe,
    variables: { input: variables }
  }),
  fetchCategory: ({ id }) => graphQLRequest({
    query: fetchCategory,
    variables: { id }
  }),
  fetchCategories: ({ countryCode }) => graphQLRequest({
    query: countryCategories,
    variables: { countryCode }
  }),
  fetchSortedCategories: () => graphQLRequest({
    query: userCountryCategories
  }),
  fetchSmartReminderLayoutData: ({ id }) => graphQLRequest({
    queries: [
      {
        query: fetchCategory,
        variables: { id }
      },
      {
        query: fetchSmartReminders,
        variables: { categoryId: id }
      },
    ]
  }),
  fetchOnChangeCategoryData: ({ id }) => graphQLRequest({
    queries: [
      {
        query: fetchCategory,
        variables: { id }
      },
      {
        query: fetchSmartReminders,
        variables: { categoryId: id }
      },
    ]
  })
};

export default api;
