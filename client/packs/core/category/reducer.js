import update from 'immutability-helper';

import { types } from './constants';

const initialState = {
  object: {},
  list: [],
};

export function categoryReducer(state = initialState, { payload, type }) {
  switch (type) {
    case types.CHANGE_CATEGORY_USERS_COUNT:
      return update(state, { object: { usersCount: { $set: payload.usersCount } } });
    case types.SET_CATEGORY:
      return update(state, { object: { $set: payload.category } });
    case types.SET_CATEGORIES_LIST:
      return update(state, { list: { $set: payload.categories } });
    case types.SET_CATEGORY_LIST_SORTED_LEAF_CHILDREN:
      return update(state, {
        list: {
          [payload.parentIndex]: {
            leafChildren: { $set: payload.leafChildren }
          }
        }
      });
    case types.SELECT_CATEGORY:
      return update(state, {
        list: {
          [payload.parentIndex]: {
            leafChildren: {
              [payload.index]: {
                selected: { $set: payload.selected }
              }
            }
          }
        }
      });
    case types.SET_CATEGORY_SUBSCRIBE:
      return update(state, { object: { selected: { $set: payload.selected } } });
    case types.SET_CATEGORY_RENEWAL_DETAIL:
      return update(state, { object: { renewalDetail: { $set: payload.renewalDetail } } });
    default: {
      return state;
    }
  }
}
