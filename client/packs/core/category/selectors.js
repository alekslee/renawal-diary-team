import { formValueSelector } from 'redux-form';
import { createSelector } from 'reselect';
import { EXCLUDED_CATEGORIES } from './constants';

const getExtendedCategories = state => (state.category.list || []);
const getCategories = createSelector(
  [getExtendedCategories],
  categories => {
    return (categories || []).filter(category => !EXCLUDED_CATEGORIES.includes(category.enName));
  }
);
const getAllLeafCategories = createSelector(
  [getCategories],
  categories => {
    let allLeafCategories = [];
    categories.forEach(({ leafChildren }) => {
      if (leafChildren) allLeafCategories = allLeafCategories.concat(leafChildren);
    });
    return allLeafCategories;
  }
);
const getCategory = state => state.category.object;
const getRenewalDetail = state => state.category.object.renewalDetail;
const getRenewalDetailId = state => state.category.object.renewalDetail.id;
const getFormSelectedCategoryId = (state, props) => formValueSelector(props.form)(state, 'categoryId');
const getSidebarCategories = createSelector(
  [getCategories, getCategory],
  (categories, category) => {
    const rootCategory = categories.find(rootCat => rootCat.id === category.root.id) || {};
    return rootCategory.leafChildren || [];
  }
);

export const selectors = {
  getCategoryId: state => state.category.object.id,
  getRenewalDetailId,
  getRenewalDetail,
  getCategories,
  getCategory,
  getExtendedCategories,
  getCategoriesAttributes: createSelector(
    [getAllLeafCategories],
    leafCategories => {
      let categoriesAttributes = [];
      leafCategories.forEach(category => {
        if (category.selected) categoriesAttributes.push({ categoryId: parseInt(category.id) });
      });
      return categoriesAttributes;
    }
  ),
  getIsExistsRenewalDetail: createSelector([getRenewalDetail], renewalDetail => !!(renewalDetail && renewalDetail.id)),
  getFirstCategory: createSelector(
    [getCategories],
    categories => {
      if (!categories || categories.length === 0) return null;
      return (categories[0].leafChildren || [])[0];
    }
  ),
  getLeafCategoriesOptions: createSelector(
    [getCategory],
    category => {
      return (category.root.leafChildren || []).map(({ id, name }) => {
        return { value: id, label: name };
      });
    }
  ),
  getCategoriesOptions: createSelector(
    [getCategories],
    categories => {
      return categories.map(root => ({
        label: root.name,
        options: (root.leafChildren).map(({ id, name }) => ({ value: id, label: name }))
      }));
    }
  ),
  getSmartReminderCategoriesOptions: createSelector(
    [getExtendedCategories],
    categories => {
      return categories.map(root => ({
        label: root.name,
        options: root.leafChildren.map(({ id, name }) => ({ value: id, label: name }))
      }));
    }
  ),
  getFlattenCategories: createSelector(
    [getExtendedCategories],
    categories => categories.flatMap(el => [].concat(el.leafChildren, el))
  ),
  getSidebarCategories,
  makeGetFormSelectedCategory: () => (
    createSelector(
      [getSidebarCategories, getFormSelectedCategoryId],
      (categories, categoryId) => categories.find(category => category.id === categoryId)
    )
  ),
};
