import { call, fork, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import { types } from './constants';
import { setError, apiWrapper } from 'lib/utils';
import { selectors } from './selectors';
import { types as smartReminderTypes } from 'core/smartReminder';

import { selectCategorySortedLeafElement } from 'lib/helpers/categories';
import { types as loaderTypes } from 'core/additional';
import { changeLoaderStatus } from 'core/additional/sagas';
import { pushCategories } from 'lib/helpers/categories';
import api from './api';

export function* subscribeToCategoriesCounterChannel({ payload: { onReceive, onError, onCompleted } }) {
  yield call(api.subscribeToCategoriesCounterChannel, { onReceive, onError, onCompleted });
}

export function* subscribeToCategoryCounterChannel({ payload: { onReceive, onError, onCompleted } }) {
  yield call(api.subscribeToCategoryCounterChannel, { onReceive, onError, onCompleted });
}

export function* fetchCategories(data) {
  yield apiWrapper(data, {
    func: api.fetchCategories,
    onSuccess: function* (resp) {
      yield pushCategories(resp);
    },
    onFailure: (resp) => {

    },
  });

}

export function* fetchCategory({ payload: { id }, errorCallback, callback }) {
  try {
    const category = yield call(api.fetchCategory, { id });
    const criteriaQuestions = category.renewalDetail && category.renewalDetail.criteriaQuestions &&
      JSON.parse(category.renewalDetail.criteriaQuestions);
    const renewalDetail = { ...category.renewalDetail, criteriaQuestions };

    if (category) {
      yield put({ type: types.SET_CATEGORY, payload: { category: { ...category, renewalDetail } } });
      if (callback) callback();
    } else {
      yield* setError([`Category with id ${id} doesn't exists`]);
      if (errorCallback) errorCallback();
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchSmartReminderLayoutData({ payload: { id }, errorCallback, callback }) {
  try {
    const resp = yield call(api.fetchSmartReminderLayoutData, { id });
    yield put({ type: types.SET_CATEGORY, payload: { category: resp.category } });
    yield put({ type: smartReminderTypes.SET_SMART_REMINDERS_LIST, payload: { smartReminders: resp.smartReminders } });
    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchOnChangeCategoryData({ payload: { id }, errorCallback, callback }) {
  yield put({ type: loaderTypes.SET_LOADER_CATEGORY_STATUS, payload: { isLoadingCategory: true } });

  try {
    const resp = yield call(api.fetchSmartReminderLayoutData, { id });
    yield put({ type: types.SET_CATEGORY, payload: { category: resp.category } });
    yield put({ type: smartReminderTypes.SET_SMART_REMINDERS_LIST, payload: { smartReminders: resp.smartReminders } });
    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* categorySubscribe({ payload: { categoryId, selected }, errorCallback, callback }) {
  try {
    const { category } = yield call(api.categorySubscribe, { categoryId, selected: `${selected}` });
    yield put({ type: types.SET_CATEGORY_SUBSCRIBE, payload: { selected: category.selected } });
    yield *selectCategorySortedLeafElement(categoryId, selected);

    if (callback) callback();
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* fetchSortedCategories(data) {
  yield apiWrapper(data, {
    func: api.fetchSortedCategories,
    onSuccess: function* (resp) {
      yield pushCategories(resp);
    },
    onFailure: (resp) => {

    },
  });
}

export function* categoryWatch() {
  yield takeLatest(types.SUBSCRIBE_TO_CATEGORIES_COUNTER_CHANNEL, subscribeToCategoriesCounterChannel);
  yield takeLatest(types.CATEGORY_SUBSCRIBE, categorySubscribe);
  yield takeLatest(types.FETCH_CATEGORIES, fetchCategories);
  yield takeLatest(types.FETCH_CATEGORY, fetchCategory);
  yield takeLatest(types.SUBSCRIBE_TO_CATEGORY_COUNTER_CHANNEL, subscribeToCategoryCounterChannel);
  yield takeLatest(types.FETCH_SMART_REMINDER_LAYOUT_DATA, fetchSmartReminderLayoutData);
  yield takeLatest(types.FETCH_ON_CHANGE_CATEGORY_DATA, fetchOnChangeCategoryData);
  yield takeLatest(loaderTypes.CHANGE_LOADER_STATUS, changeLoaderStatus);
  yield takeLatest(types.FETCH_SORTED_CATEGORIES, fetchSortedCategories);
}

export const categorySagas = [
  fork(categoryWatch)
];
