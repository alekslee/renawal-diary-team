import categorySubscribeMembers from './categorySubscribeMembers.graphql';
import categoriesSubscribeMembers from './categoriesSubscribeMembers.graphql';
import categorySubscribe from './categorySubscribe.graphql';
import countryCategories from './countryCategories.graphql';
import fetchCategory from './fetchCategory.graphql';
import fetchSmartReminders from './fetchSmartReminders.graphql';
import fetchCustomSmartReminders from './fetchCustomSmartReminders.graphql';
import userCountryCategories from './userCountryCategories.graphql';

export {
  categorySubscribe,
  categorySubscribeMembers,
  categoriesSubscribeMembers,
  countryCategories,
  fetchCategory,
  fetchSmartReminders,
  fetchCustomSmartReminders,
  userCountryCategories,
};
