//
// ACTION TYPES
//

export const types = {
  FETCH_ON_CHANGE_CATEGORY_DATA: 'FETCH_ON_CHANGE_CATEGORY_DATA',
  FETCH_SMART_REMINDER_LAYOUT_DATA: 'FETCH_SMART_REMINDER_LAYOUT_DATA',
  FETCH_CATEGORY_BY_COUNTRY: 'FETCH_CATEGORY_BY_COUNTRY',
  CHANGE_CATEGORY_USERS_COUNT: 'CHANGE_CATEGORY_USERS_COUNT',
  SELECT_CATEGORY: 'SELECT_CATEGORY',
  CLEAR_SELECTED_CATEGORIES: 'CLEAR_SELECTED_CATEGORIES',
  SET_CATEGORY_LIST_SORTED_LEAF_CHILDREN: 'SET_CATEGORY_LIST_SORTED_LEAF_CHILDREN',
  SET_CATEGORIES_LIST: 'SET_CATEGORIES_LIST',
  FETCH_CATEGORIES: 'FETCH_CATEGORIES',
  FETCH_SORTED_CATEGORIES: 'FETCH_SORTED_CATEGORIES',
  SUBSCRIBE_TO_CATEGORIES_COUNTER_CHANNEL: 'SUBSCRIBE_TO_CATEGORIES_COUNTER_CHANNEL',
  SUBSCRIBE_TO_CATEGORY_COUNTER_CHANNEL: 'SUBSCRIBE_TO_CATEGORY_COUNTER_CHANNEL',
  CHANGE_CATEGORIES_MEMBERS_COUNTERS: 'CHANGE_CATEGORIES_MEMBERS_COUNTERS',
  FETCH_CATEGORY: 'FETCH_CATEGORY',
  SET_CATEGORY: 'SET_CATEGORY',
  SET_CATEGORY_RENEWAL_DETAIL: 'SET_CATEGORY_RENEWAL_DETAIL',
  SET_CATEGORY_SUBSCRIBE: 'SET_CATEGORY_SUBSCRIBE',
  CATEGORY_SUBSCRIBE: 'CATEGORY_SUBSCRIBE',
};

export const subscriptionIds = {
  CATEGORIES_COUNTER_CHANNEL: 'categoriesCounterChannel',
  CATEGORY_COUNTER_CHANNEL: 'categoryCounterChannel'
};

export const  EXCLUDED_CATEGORIES = [
  'government'
];