import { types } from './constants';

export const actions = {
  subscribeToCategoriesCounterChannel: ({ onReceive, onError, onCompleted }) => ({
    type: types.SUBSCRIBE_TO_CATEGORIES_COUNTER_CHANNEL, payload: { onReceive, onError, onCompleted }
  }),
  subscribeToCategoryCounterChannel: ({ onReceive, onError, onCompleted }) => ({
    type: types.SUBSCRIBE_TO_CATEGORY_COUNTER_CHANNEL, payload: { onReceive, onError, onCompleted }
  }),
  fetchCategories: ({ countryCode, callback, errorCallback }) => ({
    type: types.FETCH_CATEGORIES, payload: { countryCode }, callback, errorCallback
  }),
  selectCategory: ({ parentIndex, index, selected }) => ({
    type: types.SELECT_CATEGORY, payload: { parentIndex, index, selected }
  }),
  setCategoriesList: ({ categories }) => ({
    type: types.SET_CATEGORIES_LIST, payload: { categories }
  }),
  fetchCategory: ({ id, callback, errorCallback }) => ({
    type: types.FETCH_CATEGORY, payload: { id }, callback, errorCallback
  }),
  changeCategoryUsersCount: ({ usersCount }) => ({
    type: types.CHANGE_CATEGORY_USERS_COUNT, payload: { usersCount }
  }),
  categorySubscribe: ({ categoryId, selected, callback }) => ({
    type: types.CATEGORY_SUBSCRIBE, payload: { categoryId, selected }, callback
  }),
  fetchSmartReminderLayoutData: ({ id, callback, errorCallback }) => ({
    type: types.FETCH_SMART_REMINDER_LAYOUT_DATA, payload: { id }, callback, errorCallback
  }),
  setCategory: ({ category }) => ({ type: types.SET_CATEGORY, payload: { category } }),
  fetchOnChangeCategoryData: ({ id, callback, errorCallback }) => ({
    type: types.FETCH_ON_CHANGE_CATEGORY_DATA, payload: { id }, callback, errorCallback
  }),
  fetchSortedCategories: ({ callback, errorCallback }) => ({
    type: types.FETCH_SORTED_CATEGORIES, callback, errorCallback
  }),
};
