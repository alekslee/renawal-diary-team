import { graphQLRequest } from 'lib/utils';
import {
  logOutUser,
  signInUser,
  updateUser,
  resetPassword,
  deleteUser,
  changeUserResetedPassword,
  currentUserSignUp,
  resendCurrentUserConfirmationInstructions,
  resendCurrentUserUnlockInstructions,
  currentUserInvitationAccept,
  checkUserFieldsUniqueness
} from './queries';

const api = {
  logOut: () => graphQLRequest({
    query: logOutUser,
    variables: { input: {} }
  }),
  signIn: ({ email, password }) => graphQLRequest({
    query: signInUser,
    variables: { input: { type: 'web' } },
    data: { user: { email, password } }
  }),
  updateCurrentUser: ({ avatar, displayName }) => graphQLRequest({
    query: updateUser,
    type: 'multipart',
    data: {
      variables_keys: 'input/avatar;input/displayName',
      'input/avatar': avatar,
      'input/displayName': displayName
    }
  }),
  usersResetPassword: ({ email }) => graphQLRequest({
    query: resetPassword,
    variables: { input: { email } }
  }),
  deleteCurrentUser: ({}) => graphQLRequest({
    query: deleteUser,
    variables: { input: {} },
  }),
  changeUserResetedPassword: ({ password, passwordConfirmation, resetPasswordToken }) => graphQLRequest({
    query: changeUserResetedPassword,
    variables: { input: { password, passwordConfirmation, resetPasswordToken } }
  }),
  currentUserSignUp: ({
    email,
    password,
    displayName,
    lat,
    lng,
    categoriesAttributes,
    googleUid,
    facebookUid,
    twitterUid,
    linkedinUid
  }) => graphQLRequest({
    query: currentUserSignUp,
    variables: {
      input: {
        email,
        password,
        displayName,
        lat,
        lng,
        usersCategoriesAttributes: categoriesAttributes,
        googleUid,
        facebookUid,
        twitterUid,
        linkedinUid
      }
    }
  }),
  resendCurrentUserConfirmationInstructions: ({ email }) => graphQLRequest({
    query: resendCurrentUserConfirmationInstructions,
    variables: { input: { email } }
  }),
  resendCurrentUserUnlockInstructions: ({ email }) => graphQLRequest({
    query: resendCurrentUserUnlockInstructions,
    variables: { input: { email } }
  }),
  currentUserInvitationAccept: ({ displayName, lat, lng, password, invitationToken }) => graphQLRequest({
    query: currentUserInvitationAccept,
    variables: { input: { displayName, lat, lng, password, invitationToken } }
  }),
  checkUserFieldsUniqueness: ({ email, displayName }) => graphQLRequest({
    queries: [
      {
        query: checkUserFieldsUniqueness,
        variables: { input: { fieldName: 'email', fieldValue: email } },
        responseKey: 'emailValidation'
      },
      {
        query: checkUserFieldsUniqueness,
        variables: { input: { fieldName: 'displayName', fieldValue: displayName } },
        responseKey: 'displayNameValidation'
      }
    ]
  })
};

export default api;
