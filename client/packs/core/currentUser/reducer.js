import update from 'immutability-helper';

import { types } from './constants';

const initialState = {
  object: {},
  resendUnlockInstructionsFormErrors: undefined,
  resendConfirmationInstructionsFormErrors: undefined
};

export function currentUserReducer(state = initialState, { payload, type }) {
  switch (type) {
    case types.SET_CURRENT_USER_CONFIRMATION_INSTRUCTIONS_ERRORS:
      return update(state, { resendConfirmationInstructionsFormErrors: { $set: payload.errors } });
    case types.SET_CURRENT_USER_UNLOCK_INSTRUCTIONS_ERRORS:
      return update(state, { resendUnlockInstructionsFormErrors: { $set: payload.errors } });
    case types.SET_CURRENT_USER:
      return update(state, { object: { $set: payload.currentUser } });
    default: {
      return state;
    }
  }
}
