import { types } from './constants';

export const actions = {
  setCurrentUser: ({ currentUser }) => ({
    type: types.SET_CURRENT_USER, payload: { currentUser }
  }),
  logOut: ({ callback, errorCallback }) => ({
    type: types.LOG_OUT, callback, errorCallback
  }),
  signIn: ({ email, password, callback, errorCallback }) => ({
    type: types.SIGN_IN, payload: { email, password }, callback, errorCallback
  }),
  updateCurrentUser: ({ avatar, displayName, callback, errorCallback }) => ({
    type: types.UPDATE_CURRENT_USER, payload: { avatar, displayName }, callback, errorCallback
  }),
  deleteCurrentUser: ({ callback, errorCallback }) => ({
    type: types.DELETE_CURRENT_USER, payload: { }, callback, errorCallback
  }),
  usersResetPassword: ({ email, callback, errorCallback }) => ({
    type: types.USERS_RESET_PASSWORD, payload: { email }, callback, errorCallback
  }),
  changeUserResetedPassword: ({ password, passwordConfirmation, resetPasswordToken, callback, errorCallback }) => ({
    type: types.CHANGE_USER_RESETED_PASSWORD,
    payload: { password, passwordConfirmation, resetPasswordToken },
    callback,
    errorCallback
  }),
  currentUserSignUp: ({ categoriesAttributes, callback, errorCallback }) => ({
    type: types.CURRENT_USER_SIGN_UP, payload: { categoriesAttributes }, callback, errorCallback
  }),
  resendCurrentUserConfirmationInstructions: ({ email, callback, errorCallback }) => ({
    type: types.RESEND_CURRENT_USER_CONFIRMATION_INSTRUCTIONS, payload: { email }, callback, errorCallback
  }),
  resendCurrentUserUnlockInstructions: ({ email, callback, errorCallback }) => ({
    type: types.RESEND_CURRENT_USER_UNLOCK_INSTRUCTIONS, payload: { email }, callback, errorCallback
  }),
  setCurrentUserConfirmationInstructionsErrors: ({ errors }) => ({
    type: types.SET_CURRENT_USER_CONFIRMATION_INSTRUCTIONS_ERRORS, payload: { errors }
  }),
  setCurrentUserUnlockInstructionsErrors: ({ errors }) => ({
    type: types.SET_CURRENT_USER_UNLOCK_INSTRUCTIONS_ERRORS, payload: { errors }
  }),
  currentUserInvitationAccept: ({ displayName, password, lat, lng, invitationToken, callback, errorCallback }) => ({
    type: types.CURRENT_USER_INVITATION_ACCEPT,
    payload: { displayName, password, lat, lng, invitationToken },
    callback,
    errorCallback
  }),
  checkUserFieldsUniqueness: ({ email, displayName, callback, errorCallback }) => ({
    type: types.CHECK_USER_FIELDS_UNIQUENESS, payload: { email, displayName }, callback, errorCallback
  })
};
