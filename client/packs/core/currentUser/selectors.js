import { createSelector } from 'reselect';

const getCurrentUser = state => state.currentUser.object;
export const selectors = {
  getCurrentUser,
  getUserCountry: state => state.currentUser.object.country,
  getUserCountryCode: state => state.currentUser.object.countryCode,
  getSignUpFormInitialValues: createSelector(
    [getCurrentUser],
    ({ email, password, displayName, address, lat, lng, countryCode }) => ({
      email, password, displayName, location: { address: address || '', lat, lng, countryCode }
    })
  ),
  getCurrentUserResendConfirmationFormErrors: state => state.currentUser.resendConfirmationInstructionsFormErrors,
  getCurrentUserResendUnlockFormErrors: state => state.currentUser.resendUnlockInstructionsFormErrors
};
