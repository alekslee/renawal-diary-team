import { call, fork, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import { setError, apiWrapper } from 'lib/utils';
import { stopSubmit } from 'redux-form';
import { toastr } from 'lib/helpers';
import Cookies from 'js-cookie';

import { selectors } from './selectors';
import { types as currentCountryTypes } from 'core/currentCountry';
import { types as categoryTypes } from 'core/category';

import api from './api';
import { types, ACCESS_TOKEN_KEY, UID_KEY, CLIENT_ID_KEY } from './constants';

export function* logOut({ errorCallback, callback }) {
  try {
    const resp = yield call(api.logOut);

    if (!resp.errors.length) {
      yield put({ type: types.SET_CURRENT_USER, payload: { currentUser: {} } });
      if (callback) callback();
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* signIn({ payload: { email, password }, errorCallback, callback }) {
  try {
    const resp = yield call(api.signIn, { email, password });

    if (!resp.errors.length) {
      let country = resp.user.country;
      yield put({ type: types.SET_CURRENT_USER, payload: { currentUser: resp.user } });
      yield put({ type: currentCountryTypes.SET_CURRENT_COUNTRY, payload: { currentCountry: country } });

      toastr.success('Your reminder has been set, please Vote for your choice and help others!');

      if (callback) callback({ country });
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* updateCurrentUser({ payload: { avatar, displayName }, errorCallback, callback }) {
  try {
    const resp = yield call(api.updateCurrentUser, { avatar, displayName });
    if (!resp.errors.length) {
      const currentUser = yield select(selectors.getCurrentUser);
      yield put({ type: types.SET_CURRENT_USER, payload: { currentUser: { ...currentUser, ...resp.user } } });
      if (callback) callback();
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* deleteCurrentUser(data) {

  yield apiWrapper(data, {
    func: api.deleteCurrentUser,
    onSuccess: function* () {
      yield put({ type: types.SET_CURRENT_USER, payload: { currentUser: {} } });
    },
    onFailure: function* () {

    },
  })

  // try {
  //   const resp = yield call(api.updateCurrentUser, { avatar, displayName });
  //   if (!resp.errors.length) {
  //     const currentUser = yield select(selectors.getCurrentUser);
  //     yield put({ type: types.SET_CURRENT_USER, payload: { currentUser: { ...currentUser, ...resp.user } } });
  //     if (callback) callback();
  //   } else {
  //     yield* setError(resp.errors);
  //     if (errorCallback) errorCallback(resp.errors);
  //   }
  // } catch(err) {
  //   setError(err);
  //   if (errorCallback) errorCallback(err);
  // }
}

export function* usersResetPassword({ payload: { email }, errorCallback, callback }) {
  try {
    const resp = yield call(api.usersResetPassword, { email });
    if (!resp.errors.length) {
      if (callback) callback();
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* changeUserResetedPassword({
  payload: { password, passwordConfirmation, resetPasswordToken },
  errorCallback,
  callback
}) {
  try {
    const resp = yield call(api.changeUserResetedPassword, { password, passwordConfirmation, resetPasswordToken });
    if (!resp.errors.length) {
      yield put({ type: types.SET_CURRENT_USER, payload: { currentUser: resp.user } });
      if (callback) callback();
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* currentUserSignUp({ payload: { categoriesAttributes }, errorCallback, callback }) {
  try {
    const {
      email, password, displayName, lat, lng, facebookUid, googleUid, twitterUid, linkedinUid
    } = yield select(selectors.getCurrentUser);

    const resp = yield call(api.currentUserSignUp, {
      email, password, displayName, lat, lng, categoriesAttributes, facebookUid, googleUid, twitterUid, linkedinUid
    });
    if (!resp.errors.length) {
      let country = resp.user.country;
      yield put({ type: types.SET_CURRENT_USER, payload: { currentUser: resp.user } });
      yield put({ type: currentCountryTypes.SET_CURRENT_COUNTRY, payload: { currentCountry: country } });

      toastr.success('Your reminder has been set, please Vote for your choice and help others!');

      if (callback) callback();
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* resendCurrentUserConfirmationInstructions({ payload: { email }, errorCallback, callback }) {
  try {
    const resp = yield call(api.resendCurrentUserConfirmationInstructions, { email });
    if (!resp.errors.length) {
      if (callback) callback();
    } else {
      yield put(stopSubmit('resendConfirmation', { email: resp.errors[0] }));
      yield put({ type: types.SET_CURRENT_USER, payload: { currentUser: { email: resp.email } } });
      yield put({ type: types.SET_CURRENT_USER_CONFIRMATION_INSTRUCTIONS_ERRORS, payload: { errors: resp.errors } });
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* resendCurrentUserUnlockInstructions({ payload: { email }, errorCallback, callback }) {
  try {
    const resp = yield call(api.resendCurrentUserUnlockInstructions, { email });
    if (!resp.errors.length) {
      if (callback) callback();
    } else {
      yield put(stopSubmit('resendUnlock', { email: resp.errors[0] }));
      yield put({ type: types.SET_CURRENT_USER, payload: { currentUser: { email: resp.email } } });
      yield put({ type: types.SET_CURRENT_USER_UNLOCK_INSTRUCTIONS_ERRORS, payload: { errors: resp.errors } });
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* currentUserInvitationAccept({
  payload: { displayName, invitationToken, lat, lng, password },
  errorCallback,
  callback
}) {
  try {
    const resp = yield call(api.currentUserInvitationAccept, { displayName, invitationToken, lat, lng, password });
    if (!resp.errors.length) {
      let country = resp.user.country;
      delete resp.user.country;
      yield put({ type: types.SET_CURRENT_USER, payload: { currentUser: resp.user } });
      yield put({ type: currentCountryTypes.SET_CURRENT_COUNTRY, payload: { currentCountry: country } });
      if (callback) callback();
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* checkUserFieldsUniqueness({ payload: { email, displayName }, errorCallback, callback }) {
  try {
    const { emailValidation, displayNameValidation } = yield call(
      api.checkUserFieldsUniqueness, { email, displayName }
    );

    if (emailValidation.isValid && displayNameValidation.isValid) {
      if (callback) callback();
    } else {
      const errors = {};
      if (!emailValidation.isValid) {
        errors.email = 'Email already been taken';
        toastr.error(errors.email);
      }
      if (!displayNameValidation.isValid) {
        errors.displayName = 'Display name already been taken';
        toastr.error(errors.displayName);
      }
      yield put(stopSubmit('signUpForm', errors));
      if (errorCallback) errorCallback();
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* currentUserWatch() {
  yield takeLatest(types.UPDATE_CURRENT_USER, updateCurrentUser);
  yield takeLatest(types.DELETE_CURRENT_USER, deleteCurrentUser);
  yield takeLatest(types.LOG_OUT, logOut);
  yield takeLatest(types.SIGN_IN, signIn);
  yield takeLatest(types.USERS_RESET_PASSWORD, usersResetPassword);
  yield takeLatest(types.CHANGE_USER_RESETED_PASSWORD, changeUserResetedPassword);
  yield takeLatest(types.CURRENT_USER_SIGN_UP, currentUserSignUp);
  yield takeLatest(types.RESEND_CURRENT_USER_CONFIRMATION_INSTRUCTIONS, resendCurrentUserConfirmationInstructions);
  yield takeLatest(types.RESEND_CURRENT_USER_UNLOCK_INSTRUCTIONS, resendCurrentUserUnlockInstructions);
  yield takeLatest(types.CURRENT_USER_INVITATION_ACCEPT, currentUserInvitationAccept);
  yield takeLatest(types.CHECK_USER_FIELDS_UNIQUENESS, checkUserFieldsUniqueness);
}

export const currentUserSagas = [
  fork(currentUserWatch)
];
