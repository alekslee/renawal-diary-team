import { combineReducers } from 'redux';
import { currentUserReducer } from './currentUser';
import { currentCountryReducer } from './currentCountry';
import { becomeAPartnerScreenReducer } from './becomeAPartnerScreen';
import { categoryReducer } from './category';
import { companyReducer } from './company';
import { renewalDetailReducer } from './renewalDetail';
import { feedbackReducer } from './feedback';
import { formSliderReducer } from './formSlider';
import { smartReminderReducer } from './smartReminder';
import { signUpReducer } from './signUp';
import { voteReducer } from './votes';
import { noteReducer } from './notes';
import { additionalReducer } from './additional';

import { reducer as toastrReducer } from 'react-redux-toastr';
import { reducer as formReducer } from 'redux-form';
import { connectRouter } from 'connected-react-router';

const appReducer = history => {
  const reducers = {
    toastr: toastrReducer,
    form: formReducer,
    currentUser: currentUserReducer,
    currentCountry: currentCountryReducer,
    becomeAPartnerScreen: becomeAPartnerScreenReducer,
    category: categoryReducer,
    company: companyReducer,
    renewalDetail: renewalDetailReducer,
    feedback: feedbackReducer,
    formSlider: formSliderReducer,
    smartReminder: smartReminderReducer,
    signUp: signUpReducer,
    votesCounter: voteReducer,
    note: noteReducer,
    additional: additionalReducer,
  };

  if (history) {
    return combineReducers({ ...reducers, router: connectRouter(history) });
  }
  return combineReducers(reducers);

};

export default appReducer;
