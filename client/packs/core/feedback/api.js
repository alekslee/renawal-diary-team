import { graphQLRequest } from 'lib/utils';
import { createFeedback } from './queries';

const api = {
  createFeedback: variables => graphQLRequest({
    query: createFeedback,
    variables: { input: variables }
  })
};

export default api;
