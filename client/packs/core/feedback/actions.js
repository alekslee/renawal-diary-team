import { types } from './constants';

export const actions = {
  createFeedback: ({ params, callback, errorCallback }) => ({
    type: types.CREATE_FEEDBACK, payload: { ...params }, callback, errorCallback
  })
};
