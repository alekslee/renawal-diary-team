import { call, fork, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import { setError } from 'lib/utils';

import api from './api';
import { types } from './index';
import { selectors as categorySelectors, types as categoryTypes } from 'core/category';
import { selectors as renewalDetailSelectors } from 'core/renewalDetail';
import { transformCompaniesOptionsToAttributes } from 'lib/helpers/companies';
// import { selectCategorySortedLeafElement, setCategoryRenewalDetail } from 'lib/helpers/categories';
import { setCategoryRenewalDetail } from 'lib/helpers/categories';

export function* createFeedback({
  payload: { renewalDetailAttributes, createSmartReminder, triggeredAt, insightsComment, year, categoryId }, errorCallback, callback
}) {

  try {
    const comapnyAttrs = yield* transformCompaniesOptionsToAttributes({
      currentProvider: { ...renewalDetailAttributes.currentProvider, companyType: 'provider' },
      buyingTypeCompany: {
        ...renewalDetailAttributes.buyingTypeCompany,
        companyType: renewalDetailAttributes.buyingType
      }
    });
    const category = yield select(categorySelectors.getCategory);
    const year = yield select(renewalDetailSelectors.getRenewalDetailYear);
    const resp = yield call(api.createFeedback,
      {
        renewalDetailAttributes: {
          ...renewalDetailAttributes, ...comapnyAttrs, currentProvider: undefined, buyingTypeCompany: undefined
        },
        insightsComment,
        createSmartReminder,
        triggeredAt,
        year,
        categoryId: category.id || categoryId
      }
    );

    if (!resp.errors.length) {
      yield* setCategoryRenewalDetail({ id: resp.feedback.renewalDetailId, ...renewalDetailAttributes });

      const categories = yield select(categorySelectors.getCategories);
      // if (categories.length && !category.selected) yield* selectCategorySortedLeafElement(category.id);
      if (callback) callback();
    } else {
      yield* setError(resp.errors);
      if (errorCallback) errorCallback(resp.errors);
    }
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* tagWatch() {
  yield takeLatest(types.CREATE_FEEDBACK, createFeedback);
}

export const feedbackSagas = [
  fork(tagWatch)
];
