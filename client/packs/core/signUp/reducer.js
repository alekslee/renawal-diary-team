import update from 'immutability-helper';

import { types } from './constants';

const initialState = {
  list: [],
  pickCategoriesStep: true
};

export function signUpReducer(state = initialState, { payload, type }) {
  switch (type) {
    case types.SET_SIGN_UP_CATEGORIES_LIST:
      return update(state, { list: { $set: payload.categories } });
    case types.SELECT_SIGN_UP_CATEGORY:
      return update(state, {
        list: {
          [payload.parentIndex]: {
            leafChildren: {
              [payload.index]: {
                selected: { $set: payload.selected }
              }
            }
          }
        }
      });
    case types.SET_PICK_CATEGORIES_STEP:
      return update(state, { pickCategoriesStep: { $set: payload.pickCategoriesStep } });
    default: {
      return state;
    }
  }
}
