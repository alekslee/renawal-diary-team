import { graphQLRequest } from 'lib/utils';
import { countryCategories } from './queries';

const api = {
  fetchCategories: ({ countryCode }) => graphQLRequest({
    query: countryCategories,
    variables: { countryCode }
  })
};

export default api;
