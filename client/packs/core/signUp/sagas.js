import { call, fork, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import { types } from './constants';
import { setError, apiWrapper } from 'lib/utils';
import { pushSignUpCategories } from 'lib/helpers/categories';

import { selectors } from './selectors';

import api from './api';

export function* fetchCategories(data) {
  yield apiWrapper(data, {
    func: api.fetchCategories,
    onSuccess: function* (resp) {
      yield pushSignUpCategories(resp);
    },
    onFailure: (resp) => {

    },
  });

}

export function* changeCategoriesMembersCounters({ payload: { categoriesChangedMembers } }) {
  const categories = yield select(selectors.getCategories);
  const newCategories = yield* retrieveCategoriesWithChangedCounters(categories, categoriesChangedMembers);
  yield put({ type: types.SET_SIGN_UP_CATEGORIES_LIST, payload: { categories: newCategories } });
}

export function* retrieveCategoriesWithChangedCounters(categories, categoriesChangedMembers) {
  let changedCategory;
  return categories.map(category => {
    changedCategory = categoriesChangedMembers.find(ct => ct.enName === category.enName);
    if (changedCategory) {
      return {
        ...category,
        usersForAllDescendantsAndAllCountriesCount: changedCategory.usersForAllDescendantsAndAllCountriesCount
      };
    }
    return category;
  });
}

export function* changePickCategoriesStep({ payload: { pickCategoriesStep } }) {
  yield put({ type: types.SET_PICK_CATEGORIES_STEP, payload: { pickCategoriesStep: pickCategoriesStep } });
}

export function* signUpWatch() {
  yield takeLatest(types.FETCH_SIGN_UP_CATEGORIES, fetchCategories);
  yield takeLatest(types.CHANGE_SIGN_UP_CATEGORIES_MEMBERS_COUNTERS, changeCategoriesMembersCounters);
  yield takeLatest(types.CHANGE_PICK_CATEGORIES_STEP, changePickCategoriesStep);
}

export const signUpSagas = [
  fork(signUpWatch)
];
