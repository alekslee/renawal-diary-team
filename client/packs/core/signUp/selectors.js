import { createSelector } from 'reselect';

const getCategories = state => state.signUp.list;
const getPickCategoriesStepStatus = state => state.signUp.pickCategoriesStep;
const getAllLeafCategories = createSelector(
  [getCategories],
  categories => {
    let allLeafCategories = [];
    categories.forEach(({ leafChildren }) => {
      if (leafChildren) allLeafCategories = allLeafCategories.concat(leafChildren);
    });
    return allLeafCategories;
  }
);

export const selectors = {
  getCategories,
  getPickCategoriesStepStatus,
  getCategoriesAttributes: createSelector(
    [getAllLeafCategories],
    leafCategories => {
      let categoriesAttributes = [];
      leafCategories.forEach(category => {
        if (category.selected) categoriesAttributes.push({ categoryId: parseInt(category.id) });
      });
      return categoriesAttributes;
    }
  ),
};
