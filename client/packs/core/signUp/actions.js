import { types } from './constants';

export const actions = {
  fetchCategories: ({ countryCode, callback, errorCallback }) => ({
    type: types.FETCH_SIGN_UP_CATEGORIES, payload: { countryCode }, callback, errorCallback
  }),
  selectCategory: ({ parentIndex, index, selected }) => ({
    type: types.SELECT_SIGN_UP_CATEGORY, payload: { parentIndex, index, selected }
  }),
  changeCategoriesMembersCounters: ({ categoriesChangedMembers }) => ({
    type: types.CHANGE_SIGN_UP_CATEGORIES_MEMBERS_COUNTERS, payload: { categoriesChangedMembers }
  }),
  changePickCategoriesStep: ({ pickCategoriesStep }) => ({
    type: types.CHANGE_PICK_CATEGORIES_STEP, payload: { pickCategoriesStep: false }
  })
};