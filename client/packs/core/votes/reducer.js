import update from 'immutability-helper';
import { types } from './constants';

const initialState = {
  value: undefined
};

export function voteReducer(state = initialState, { payload, type }) {
  switch (type) {
    case types.FETCH_VOTES_COUNT:
      return update(state, { value: { $set: payload.value } });
    default: {
      return state;
    }
  }
}
