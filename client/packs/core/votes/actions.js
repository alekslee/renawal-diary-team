import { types } from './constants';

export const actions = {
  subscribeToVoteCounts: ({ period, rootCategoryId, onReceive }) => ({
    type: types.SUBSCRIBE_VOTES_COUNT,
    payload: { period, rootCategoryId, onReceive },
  }),
  fetchVotesCount: ({ period, rootCategoryId, callback, errorCallback }) => ({
    type: types.FETCH_VOTES_COUNT_REQUEST,
    payload: { period, rootCategoryId },
    callback,
    errorCallback
  })
};