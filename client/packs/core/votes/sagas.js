import { call, fork, put, takeLatest } from 'redux-saga/effects';
import api from './api';
import { types } from './index';
import { setError } from 'lib/utils';

export function* fetchVotesCount({ payload: { period, rootCategoryId }, errorCallback, callback }) {
  try {
    const resp = yield call(api.votesCount, { period, rootCategoryId });

    yield put({ type: types.FETCH_VOTES_COUNT, payload: { value: resp.value } });

    if (callback) callback(resp);
  } catch(err) {
    setError(err);
    if (errorCallback) errorCallback(err);
  }
}

export function* subscribeToVoteCounts({ payload: { period, rootCategoryId, onReceive, onError, onCompleted } }) {
  yield call(api.subscribeToVoteCounts, { period, rootCategoryId, onReceive, onError, onCompleted });
}

export function* tagWatch() {
  yield takeLatest(types.FETCH_VOTES_COUNT_REQUEST, fetchVotesCount);
  yield takeLatest(types.SUBSCRIBE_VOTES_COUNT, subscribeToVoteCounts);
}

export const voteSagas = [
  fork(tagWatch)
];
