import { createSubscription, graphQLRequest } from 'lib/utils';
import { votesCount, votesCountSubscribe } from './queries';

const api = {
  subscribeToVoteCounts: ({ period, rootCategoryId, onReceive, onError, onCompleted }) => createSubscription({
    subscriptionId: `${rootCategoryId}-votes-count`,
    query: votesCountSubscribe,
    variables: { period, rootCategoryId: +rootCategoryId },
    onReceive,
    onError,
    onCompleted
  }),
  votesCount: ({ period, rootCategoryId }) => graphQLRequest({
    query: votesCount,
    variables: { period, rootCategoryId: +rootCategoryId }
  })
};

export default api;