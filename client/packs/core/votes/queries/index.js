import votesCount from './votesCount.graphql';
import votesCountSubscribe from './votesCountSubscribe.graphql';

export { votesCount, votesCountSubscribe };
