import ReactOnRails from 'react-on-rails';

import ServerRendering from './serverRendering';

ReactOnRails.register({
  ServerRendering
});

