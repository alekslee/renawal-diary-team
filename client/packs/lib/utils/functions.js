import { call } from 'redux-saga/effects';
import { toastr } from 'lib/helpers';
import { SubmissionError } from 'redux-form';

export function* apiWrapper({ payload, callback, errorCallback }, { func, onSuccess, onFailure }) {

  const resp = yield call(func, payload);
  const { errors } = resp;

  try {

    if (!!errors && Object.keys(errors).length > 0) {
      errorCallback && errorCallback(resp);
      if (onFailure) {
        yield* onFailure(resp);
      }
    } else {
      if (onSuccess) {
        yield* onSuccess(resp);
      }
      callback && callback(resp);
    }

  } catch (error) {
    errorCallback && errorCallback(resp);

    if (onFailure) {
      yield* onFailure(resp);
    }

    toastr.error('Error!', `${error}`);
  }
};

export const setError = errors => {
  if (errors.join) {
    toastr.error('Error!', errors.join('; '));
  } else {
    // eslint-disable-next-line
    console.log(errors);
  }
};
