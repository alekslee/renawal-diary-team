import { BUYING_TYPE_OPTIONS } from 'components/ProviderDetailsFormData/constants';
import { LAST_BUY_OPTIONS, CHECKED_PLACES_OPTIONS } from 'core/renewalDetail/constants';

export const RENEWAL_DETAIL_FORM_INITIAL_VALUES = {
  usingProvider: true,
  lastBuy: LAST_BUY_OPTIONS[0].value,
  buyingType: BUYING_TYPE_OPTIONS[0].value,
  buyingTypeCompany: undefined,
  currentProvider: undefined,
  checkedPlaces: CHECKED_PLACES_OPTIONS[0].value
};
