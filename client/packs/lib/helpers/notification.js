import React from 'react';
import { toastr } from 'react-redux-toastr';
import NotificationPopup from 'components/NotificationPopup';

const notify = (action, opts) => {
  // const tid = `${Math.random()}`;
  const title = opts[0];
  let message = null;
  let options = {};

  if (opts[1] instanceof Object) {
    options = opts[1];
  } else {
    message = opts[1] || null;
    options = opts[2] || {};
  }

  const icon = <div className="toastr-icon" />;

  const params = {
    icon: icon,
    timeOut: 3000,
    showCloseButton: false,
    closeOnToastrClick: false,
    removeOnHover: true,
    removeOnHoverTimeOut: 500,
    className: 'custom-toastr',
    component: (
      <NotificationPopup
        { ...{ icon, title, message }}
      />
    )
  }

  toastr[action]('', Object.assign(params, options) );
}

class Toastr {
  static success = (...opts) => notify('success', opts);
  static info = (...opts) => notify('info', opts);
  static warning = (...opts) => notify('warning', opts);
  static error = (...opts) => notify('error', opts);
  static message = (...opts) => notify('message', opts);
  static confirm = (...opts) => notify('confirm', opts);
};

export default Toastr;