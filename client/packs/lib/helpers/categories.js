import { select, put } from 'redux-saga/effects';
import { selectors, types } from 'core/category';
import { types as signUpTypes } from 'core/signUp';

export function* selectCategorySortedLeafElement(categoryId, selected) {
  const categories = yield select(selectors.getCategories);
  let parentIndex, parentCategory, index, leafCategory, leafChildren;
  categories.forEach((category, parentI) => {
    if (!parentCategory) {
      category.leafChildren.forEach((leafCat, i) => {
        if (leafCat.id === categoryId) {
          parentCategory = category;
          parentIndex = parentI;
          index = i;
          leafCategory = leafCat;
        }
      });
    }
  });
  leafChildren = [...parentCategory.leafChildren];
  leafChildren[index] = { ...leafCategory, selected };

  leafChildren = leafChildren.sort((catA, catB) => {
    if (catA.selected && catB.selected) {
      return catA.id - catB.id;
    }
    if (catA.selected) return -1;
    if (catB.selected) return 1;
  });

  yield put({ type: types.SET_CATEGORY_LIST_SORTED_LEAF_CHILDREN, payload: { parentIndex, leafChildren } });
}

export function* setCategoryRenewalDetail({ id, checkedPlaces, lastBuy, criteriaQuestions }) {
  const renewalDetail = yield select(selectors.getRenewalDetail);
  let params = {
    id,
    checkedPlaces,
    lastBuy,
    criteriaQuestions: criteriaQuestions && typeof criteriaQuestions === 'string' ?
      JSON.parse(criteriaQuestions)
      :
      criteriaQuestions
  };
  Object.keys(params).forEach(key => { if(params[key] === undefined) delete params[key]; });

  yield put({ type: types.SET_CATEGORY_RENEWAL_DETAIL, payload: { renewalDetail: { ...renewalDetail, ...params } } });
};

export function* pushCategories(leafCategories) {
  const categories = yield* buildTreeCategories(leafCategories);
  yield put({ type: types.SET_CATEGORIES_LIST, payload: { categories } });
}

export function* pushSignUpCategories(leafCategories) {
  const categories = yield* buildTreeCategories(leafCategories);
  yield put({ type: signUpTypes.SET_SIGN_UP_CATEGORIES_LIST, payload: { categories } });
}

export function* buildTreeCategories(leafCategories) {
  let categories = leafCategories.filter(category => category.parentId == null);

  categories.forEach(root => {
    root.leafChildren = leafCategories.filter(category => category.parentId == root.id);
    root.leafChildren.forEach(child => {
      child.root = root;
      const leafChildren = leafCategories.filter(category => category.parentId === +child.id);

      if (!!leafChildren.length) {
        root.leafChildren = [...root.leafChildren, ...leafChildren]
          .filter(category => category.depth === 2);
      }
    });

    root.leafChildren.forEach(leaf => {
      leaf.root = { code: root.code, enName: root.enName, id: root.id, slag: root.slag };
    });

    root.leafChildren.sort((catA, catB) => {
      if (catA.selected && catB.selected) {
        return catA.id - catB.id;
      }
      if (catA.selected) return -1;
      if (catB.selected) return 1;
    });
  });

  return categories;
}
