import { put, select } from 'redux-saga/effects';

import { selectors, types } from 'core/renewalDetail';
import { RENEWAL_DETAIL_FORM_INITIAL_VALUES } from './constants';

export function* setRenewalDetail(renewalDetail) {

  const updatedRenewalDetail = !renewalDetail ? {} : {
    ...renewalDetail,
    criteriaQuestions: renewalDetail.criteriaQuestions && JSON.parse(renewalDetail.criteriaQuestions),
    pricesQuestions: renewalDetail.pricesQuestions && JSON.parse(renewalDetail.pricesQuestions)
  };

  yield put({ type: types.SET_RENEWAL_DETAIL, payload: { renewalDetail: updatedRenewalDetail } });
};

export function* setRenewalDetailFormOptions(formOptions) {
  yield put({
    type: types.SET_RENEWAL_DETAIL_FORM_OPTIONS,
    payload: {
      formOptions: {
        ...formOptions,
        criteriaQuestionsOptions: JSON.parse(formOptions.criteriaQuestionsOptions),
        pricesQuestionsOptions: JSON.parse(formOptions.pricesQuestionsOptions)
      }
    }
  });
};

export function* updateRenewalDetailFormOptions(formOptions) {
  const originalFormOptions = yield select(selectors.getFormOptions);
  const criteriaQuestionsOptions = formOptions.criteriaQuestionsOptions ?
    JSON.parse(formOptions.criteriaQuestionsOptions) : originalFormOptions.criteriaQuestionsOptions;
  const pricesQuestionsOptions = formOptions.pricesQuestionsOptions ?
    JSON.parse(formOptions.pricesQuestionsOptions) : originalFormOptions.pricesQuestionsOptions;

  yield put({
    type: types.SET_RENEWAL_DETAIL_FORM_OPTIONS,
    payload: {
      formOptions: {
        ...originalFormOptions, ...formOptions, criteriaQuestionsOptions, pricesQuestionsOptions
      }
    }
  });
};

export const retrieveFormInitialValues = ({
  isRenewalDetailExists, renewalDetail, criteriaQuestionsOptions, pricesQuestionsOptions, currentUser, category
}) => {
  const criteriaQuestions = retrieveInitialValuesForDynamicQuestions({
    options: criteriaQuestionsOptions,
    values: (renewalDetail.criteriaQuestions || {}),
    isRenewalDetailExists,
    currentUser,
    category
  });
  const pricesQuestions = retrieveInitialValuesForDynamicQuestions({
    options: pricesQuestionsOptions,
    values: (renewalDetail.pricesQuestions || {}),
    isRenewalDetailExists,
    currentUser,
    category
  });
  if (!isRenewalDetailExists) return { ...RENEWAL_DETAIL_FORM_INITIAL_VALUES, criteriaQuestions, pricesQuestions };

  const { currentProvider, buyingTypeCompany } = renewalDetail;
  return {
    ...RENEWAL_DETAIL_FORM_INITIAL_VALUES,
    ...renewalDetail,
    id: undefined,
    year: undefined,
    usingProvider: renewalDetail.usingProvider === null ? true : renewalDetail.usingProvider,
    criteriaQuestions,
    pricesQuestions,
    currentProvider: currentProvider &&
      { ...currentProvider, value: currentProvider.id, label: currentProvider.name },
    buyingTypeCompany: (buyingTypeCompany &&
      { ...buyingTypeCompany, value: buyingTypeCompany.id, label: buyingTypeCompany.name }) || {}
  };
};

const retrieveInitialValuesForDynamicQuestions = ({
  options, values, isRenewalDetailExists, currentUser: { address, lat, lng, countryCode }, category
}) => {
  let questions = {};
  for (let key in options) {
    if ((!isRenewalDetailExists || values[key] === undefined) && key === 'location') {
      questions[key] = { address, lat, lng, countryCode };
    } else if ((!isRenewalDetailExists || values[key] === undefined) &&
      category.code === 'health_insurance' && key === 'price') {
      questions[key] = { period: 'per_month' };
    } else if (!isRenewalDetailExists || values[key] === undefined) {
      questions[key] = options[key].default_value;
    } else {
      questions[key] = values[key];
    }
  };
  return questions;
};
