import { select, put } from 'redux-saga/effects';
import { selectors, types } from 'core/smartReminder';

export function* pushSmartReminderAndOrder(smartReminder) {
  let smartReminders = yield select(selectors.getSmartReminders);
  smartReminders = yield* orderSmartReminders([...smartReminders, smartReminder]);
  yield put({ type: types.SET_SMART_REMINDERS_LIST, payload: { smartReminders: smartReminders } });
};

export function* replaceSmartReminderAndOrder(smartReminder) {
  const smartReminders = yield select(selectors.getSmartReminders);
  const smartReminderIndex = smartReminders.findIndex(reminder => reminder.id === smartReminder.id);
  let reminders = [...smartReminders];
    reminders[smartReminderIndex] = smartReminder;
    reminders = yield* orderSmartReminders(reminders);

  yield put({ type: types.SET_SMART_REMINDERS_LIST, payload: { smartReminders: reminders } });
};

export function* removeSmartReminders(ids) {
  const smartReminders = yield select(selectors.getSmartReminders);
  let reminders = [...smartReminders];
  let idIndex;

  ids.forEach(id => {
    idIndex = reminders.findIndex(reminder => reminder.id === id);
    reminders.splice(idIndex, 1);
  });

  yield put({ type: types.SET_SMART_REMINDERS_LIST, payload: { smartReminders: reminders } });
};

export function* orderSmartReminders(smartReminders) {
  return smartReminders.sort((a, b) => {
    return new Date(a.triggeredAt) - new Date(b.triggeredAt);
  });
};

export const generateArrayOfYears = (startYear, endYear) => {
  let years = [];
  for (let year = startYear; year <= endYear; year++) {
    years.push(year);
  };
  return years;
};
