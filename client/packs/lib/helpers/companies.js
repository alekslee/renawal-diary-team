import { select, put } from 'redux-saga/effects';

import { selectors as categorySelectors } from 'core/category';
import { selectors, types } from 'core/company';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { EMPTY_COMPANY_TYPES_FILTERS, FIRST_COMPANIES_PER_PAGE, COMPANIES_PER_PAGE } from 'core/company/constants';
import { types as loaderTypes } from 'core/additional';

export function* retrieveLeaderBoardPersonalizeFilters(filters) {
  let personalizeFilters = filters;

  const isActive = yield select(selectors.getPersonalizeFilterStatus);
  const category = yield select(categorySelectors.getCategory);
  const filtersList = yield select(selectors.getCategoriesPersonalizeFilters);
  const filtersHash = filtersList.find(filter => filter.category === category.id);

  if (!isActive && !!filtersHash) {
    personalizeFilters.criteriaQuestions = filtersHash.criteriaQuestions;
  }
  return personalizeFilters;
}

export function* retrieveCategoryPersonalizeFilters(categoryId, criteriaQuestions) {
  const filterHash = { category: categoryId, criteriaQuestions };

  let filtersList = yield select(selectors.getCategoriesPersonalizeFilters);
  let filter = filtersList.findIndex(filter => filter.category === categoryId);

  if (!filter) {
    filtersList[filter] = filterHash;
  } else {
    filtersList.push(filterHash);
  }
  return filtersList;
}

export function* transformCompaniesOptionsToAttributes(companies) {
  let attributes = {};
  let item;
  for (let key in companies) {
    if (companies[key]) {
      item = companies[key];
      if (item.value) {
        attributes[`${key}Id`] = item.value;
      } else if (item.label) {
        attributes[`${key}Attributes`] = { name: item.label, companyType: item.companyType };
      } else if (!item.value && !item.label) {
        attributes[`${key}Id`] = 0;
      }
    }
  }
  return attributes;
}

export function* retrieveCompaniesFilterAttributes(page, perPage) {
  const categoryId = yield select(categorySelectors.getCategoryId);

  let companyType = yield select(selectors.getCompanyListTypeFilter);
  companyType = companyType && companyType.value;
  if (EMPTY_COMPANY_TYPES_FILTERS.includes(companyType)) companyType = undefined;

  let checkedPlaces = yield select(selectors.getCompanyListCheckedPlacesFilter);
  checkedPlaces = checkedPlaces && checkedPlaces.value;

  let city = yield select(selectors.getCompanyListCityFilter);
  city = city && city.value;

  let lastBuy = yield select(selectors.getCompanyListTimeFilter);
  lastBuy = lastBuy && lastBuy.value;

  const criteriaQuestions = yield select(selectors.getCompanyListCriteriaQuestionsFilter);

  let keyWord = yield select(selectors.getCompanyListKeyWordFilter);
  keyWord = keyWord && keyWord.keyWord;

  let zipCode = yield select(selectors.getCompanyListZipCodeFilter);
  zipCode = zipCode && zipCode.zipCode;

  let address = yield select(selectors.getCompanyListAddressFilter);
  address = address && address.value;

  const countryCode = yield* retrieveDefaultCompanyCountryCodeFilter();

  let orderByColumn = yield select(selectors.getOrderByColumn);

  const savedPerPage = yield select(selectors.getCompanyListPerPage);
  const isShortCompaniesList = yield select(selectors.getIsShortCompaniesList);

  const perPageFinal = savedPerPage || (isShortCompaniesList ? FIRST_COMPANIES_PER_PAGE : COMPANIES_PER_PAGE) || perPage;

  let pageFinal = page;
  if (!pageFinal) pageFinal = yield select(selectors.getCompaniesListPage);

  return {
    categoryId,
    page: pageFinal,
    perPage: perPageFinal,
    companyType,
    checkedPlaces,
    countryCode,
    city,
    criteriaQuestions,
    lastBuy,
    keyWord,
    zipCode,
    address,
    orderColumn: orderByColumn.column,
    orderDirection: orderByColumn.direction
  };
}

export function* retrieveDefaultCompanyCountryCodeFilter() {
  const country = yield select(currentUserSelectors.getUserCountry);
  let countryCode = yield select(selectors.getCompanyListCountryFilter);
  let currentCountry = yield select(currentCountrySelectors.getCurrentCountry);
  countryCode = (countryCode && countryCode.value) || (country || currentCountry).isoA2Code;
  return countryCode;
};

export function* setDefaultValues() {
  yield put({ type: types.SET_COMPANY_COMPANIES_LIST_PAGE, payload: { companiesListPage: 1 } });
}
