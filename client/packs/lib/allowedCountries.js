import CountriesCategories from '../../../config/country_categories.yml';

export const ALLOWED_COUNTRIES = Object.keys(CountriesCategories.countries);
