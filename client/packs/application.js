import React, { Fragment, Component } from 'react';
import { object, any, array, string } from 'prop-types';
import ReduxToastr from 'react-redux-toastr';
import { IntlProvider } from 'react-intl';
import { toastr } from 'lib/helpers';
import { StaticRouter } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { history } from 'core/store';
import Layouts from 'layouts';
import { connectToSocket } from 'lib/utils';
import NotificationPopup from 'components/NotificationPopup';
import SlackFeedback, { themes, SlackIcon } from 'react-slack-feedback';

const propTypes = {
  locale: string.isRequired,
  messages: object.isRequired,
  location: string.isRequired,
  context: object.isRequired,
  toastrData: array,
  countryCode: string.isRequired
};

const uploadImage = (image, success, error) => {
  var form = new FormData()
  form.append('image', image)
  return fetch('/api/upload', { method: 'POST', data: form })
    .then(({ url }) => success(url))
    .catch(err => error(err))
}

class Application extends Component {

  state = {
    openSlackFeedback: false
  }

  componentDidMount() {
    const { toastrData, countryCode } = this.props;
    if (toastrData && toastrData.length) {
      toastrData.forEach(data => {
        toastr[data.type](data.message);
      });
    };
    connectToSocket(countryCode);
  }

  render() {
    const { locale, messages, location, countryCode, context } = this.props;

    const sendToServer = (payload, success, error) => {
      // return fetch('https://hooks.slack.com/services/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', {
      return fetch(`/${countryCode}/feedback`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
      })
      .then(() => {
        success && success();
        this.SlackFeedback.setState({ open: false });
        toastr.success('Success', 'Thanks for your feedback!');
      })
      .catch(error)
    }

    return (
      <IntlProvider key={ locale } { ...{ locale, messages } } >
        <Fragment>
          {
            typeof window === 'undefined' ?
              <StaticRouter { ...{ location, context } } >
                <Layouts />
              </StaticRouter>
              :
              <ConnectedRouter history={ history } >
                <Layouts />
              </ConnectedRouter>
          }
          <div className="slack-feedback">
            <SlackFeedback
              open={ false }
              ref={ (ref) => this.SlackFeedback = ref }
              disabled={ false }
              errorTimeout={ 3 * 1000 }
              icon={ () => <div /> }
              sentTimeout={ 2 * 1000 }
              showChannel={ false }
              // showIcon={true}
              channel="#general"
              theme={ themes.default } // (optional) See src/themes/default for default theme
              user="Slack Feedback" // The logged in user (default = "Unknown User")
              // onImageUpload={(image, success,error) =>
              //   uploadImage(image).then(({ url }) => success(url)).catch(error)
              // }
              onSubmit={ (payload, success, error) =>
                sendToServer(payload).then(success).catch(error)
              }
            />
          </div>

          <ReduxToastr
            timeOut={ 0 }
            newestOnTop={ false }
            position="bottom-left"
            transitionIn="fadeIn"
            transitionOut="fadeOut"
            preventDuplicates={ false }
            showCloseButton
            closeOnToastrClick
          />
        </Fragment>
      </IntlProvider>
    );
  }
};
Application.propTypes = propTypes;

export default Application;
