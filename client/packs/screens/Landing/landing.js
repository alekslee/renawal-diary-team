import React, { Fragment, Component } from 'react';
import { array, string, func, object } from 'prop-types';

import Loader from 'components/Loader';
import { SubheaderSection, SelectCategorySection, BenefitsSection, SignUpSection } from './components';

const propTypes = {
  categories: array.isRequired,
  category: object.isRequired,
  countryCode: string.isRequired,
  fetchCategoriesDispatch: func.isRequired,
  setCategoriesListDispatch: func.isRequired,
  setCategoryDispatch: func.isRequired
};

class Landing extends Component {
  state = {
    isLoading: this.props.categories.length === 0 || !this.props.categories[0].leafChildren
  };

  componentDidMount() {
    const {
      state: { isLoading },
      props: { countryCode, fetchCategoriesDispatch, setCategoryDispatch }
    } = this;

    if (isLoading) {
      const callback = categories => {
        setCategoryDispatch({ category: categories[0].leafChildren[0] });
        this.setState({ isLoading: false });
      };
      const errorCallback = () => this.setState({ isLoading: false });

      fetchCategoriesDispatch({ countryCode, callback, errorCallback });
    }
  }

  componentWillUnmount() {
    const { setCategoriesListDispatch, setCategoryDispatch } = this.props;
    // setCategoriesListDispatch({ categories: [] });
    // setCategoryDispatch({ category: {} });
  }

  render() {
    const {
      state: { isLoading },
      props: { categories, category }
    } = this;

    if (isLoading || !category) return <Loader />;

    return (
      <div className={ `${(category.root || {}).enName}-wrapper`  }>
        <div className='category-background'>
          {/*<div className="blurOverlay" />*/}
          <SubheaderSection />
          <SelectCategorySection />
        </div>
        <BenefitsSection />
        <SignUpSection />
      </div>
    );
  };
};

Landing.propTypes = propTypes;

export default Landing;
