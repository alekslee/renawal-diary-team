import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SubheaderSection from './subheaderSection';

import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { selectors, actions } from 'core/category';

const mapStateToProps = state => ({
  category: selectors.getCategory(state),
  categories: selectors.getCategories(state),
  countryCode: currentCountrySelectors.getCurrentCountryCode(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setCategoryDispatch: actions.setCategory,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(SubheaderSection);