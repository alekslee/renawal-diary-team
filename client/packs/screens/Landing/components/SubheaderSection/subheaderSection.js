import React, { Component } from 'react';
import classNames from 'classnames';
import { object, array, string, func } from 'prop-types';
import Slider from 'react-slick';

import InsuranceIcon from 'images/icons/InsuranceIconHome';
import BroadbandIcon from 'images/icons/BroadbandIconHome';
import EnergyIcon from 'images/icons/EnergyIconHome';
import SubscriptionsIcon from 'images/icons/SubscriptionsIconHome';
import BusinessIcon from 'images/icons/BusinessIconHome';

export const ICONS = {
  insurance: <InsuranceIcon />,
  broadband: <BroadbandIcon />,
  energy: <EnergyIcon />,
  subscriptions: <SubscriptionsIcon />,
  business: <BusinessIcon />
};


const propTypes = {
  category: object.isRequired,
  categories: array.isRequired,
  countryCode: string.isRequired,
  setCategoryDispatch: func.isRequired
};

const NAVIGATION_SLIDER_SETTINGS = {
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
};

class SubheaderSection extends Component {
  state = {
    windowSize: 768
  }

  componentDidMount() {
    this.setWindowSize();
    window.addEventListener('resize', this.setWindowSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setWindowSize);
  }

  setWindowSize = () => {
    this.setState({windowSize: window.innerWidth});
  }

  selectCategory = rootCategory => this.props.setCategoryDispatch({ category: rootCategory.leafChildren[0] });

  DesktopSubHeader = ({ categories, category }) => (
    <div className='top-bar home-nav landing-nav'>
      {
        categories.map(ctg => (
          <div
            key={ ctg.id }
            className={ classNames('top-bar-link', { 'active': (category.root || category).id === ctg.id }, ctg.enName) }
            onClick={ () => this.selectCategory(ctg) }
          >
            <span className="landing-category-icon">
              { ICONS[ctg.enName] }
            </span>
            { ctg.name }
          </div>
        ))
      }
    </div>
  )

  MobileSubHeader = ({ categories, category }) => (
    <Slider
      { ...NAVIGATION_SLIDER_SETTINGS }
      beforeChange={ (_, index) => this.selectCategory( categories[index] ) }
      className="top-bar home-nav landing-nav"
    >
      {
        categories.map(ctg => (
          <div key={ ctg.id }>
            <div
              className={ classNames('top-bar-link', { 'active': (category.root || {}).id === ctg.id }, ctg.enName) }
              onClick={ () => this.selectCategory(ctg) }
            >
              <div className="landing-category-icon">
                { ICONS[ctg.enName] }
              </div>
              { ctg.name }
            </div>
          </div>
        ))
      }
    </Slider>
  )

  render() {
    const {
      MobileSubHeader,
      DesktopSubHeader,
      props: { categories, category },
      state: { windowSize },
    } = this;

    return (
      <div>
        { windowSize > 767 ?
          <DesktopSubHeader {...{ category, categories }} /> :
          <MobileSubHeader {...{ category, categories }} />
        }
      </div>
    );
  }
}

SubheaderSection.propTypes = propTypes;

export default SubheaderSection;
