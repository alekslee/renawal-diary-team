import React from 'react';
import CheckboxIcon from 'images/icons/CheckboxIcon';
import {} from 'prop-types';

const propTypes = {};

const BenefitsSection = () => {
  return(
    <div className='benefits'>
      <div className='container'>
        <h3 className='benefits-title'>Benefits of membership</h3>
        <div className='benefits-grid'>
          <div className='grid-item'>
            <h5>
              <CheckboxIcon />
              Smarter Notifications
            </h5>
            <p>Get notified of the businesses with the most votes for value and service at renewal time</p>
          </div>
          <div className='grid-item'>
            <h5>
              <CheckboxIcon />
              Smarter Decisions
            </h5>
            <p>Compare your prices, share reviews & insights , get support from other members</p>
          </div>
          <div className='grid-item'>
            <h5>
              <CheckboxIcon />
              Smarter Management
            </h5>
            <p>Manage all your renewal decisions in one easy place</p>
          </div>
        </div>
      </div>
    </div>
  );
};

BenefitsSection.propTypes = propTypes;

export default BenefitsSection;