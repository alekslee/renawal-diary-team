import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import SelectCategorySection from './selectCategorySection';

import { selectors, actions } from 'core/category';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  category: selectors.getCategory(state),
  country: currentCountrySelectors.getCurrentCountry(state),
  flattenCategories: selectors.getFlattenCategories(state),
  categoriesOptions: selectors.getCategoriesOptions(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setCategoryDispatch: actions.setCategory,
    setCategoriesListDispatch: actions.setCategoriesList
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SelectCategorySection));
