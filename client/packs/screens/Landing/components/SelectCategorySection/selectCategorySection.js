import React from 'react';
import Select from 'react-select';
import { object, array, func } from 'prop-types';

import { TITLES } from './constants';
import { paths } from 'layouts/constants';
import VoteIcon from 'images/icons/VoteIcon';
// import VotesCounter from 'components/VotesCounter';

const propTypes = {
  category: object.isRequired,
  flattenCategories: array.isRequired,
  country: object.isRequired,
  history: object.isRequired,
  categoriesOptions: array.isRequired,
  setCategoryDispatch: func.isRequired,
  setCategoriesListDispatch: func.isRequired
};

const SelectCategorySection = ({
  category, flattenCategories, country, history, categoriesOptions, setCategoryDispatch, setCategoriesListDispatch
}) => {
  const goToTheLeaderboard = () => {
    history.push(paths.LEADERBOARD.replace(':country', country.isoA2Code).replace(':slag', category.slag));
  };

  const goToTheVote = () => {
    history.push(paths.VOTE.replace(':country', country.isoA2Code).replace(':slag', category.slag));
  };

  return (
    <div className='home'>
      <div className='container'>
        <h1 className='home-title'>{ TITLES[category.root.enName].replace(':country', country.name) }</h1>
        <div className='home-description'>
          <p>Let's make smarter decisions together!</p>
          <p>Every provider, broker and price comparsion site in one place.</p>
        </div>
        <div className='search-block'>
          <Select
            value={ { label: category.name, value: category.id } }
            className='reminder-select category-select'
            classNamePrefix='Select'
            options={ categoriesOptions }
            onChange={ ({ value }) => {
              const category = flattenCategories.find(({ id }) => +id === +value);
              category && setCategoryDispatch({ category });
            } }
          />
          <button
            onClick={ goToTheLeaderboard }
          >
            Explore
          </button>
        </div>
        {/*<VotesCounter />*/}
        <button className='home-vote-btn' onClick={ goToTheVote } >
          <VoteIcon />
          Have your say!
        </button>
      </div>
    </div>
  );
};
SelectCategorySection.propTypes = propTypes;

export default SelectCategorySection;
