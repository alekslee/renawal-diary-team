export const TITLES = {
  insurance: 'Where are the people of :country finding their best value in Insurance?',
  broadband: 'Where are the people of :country finding their best value in Broadband?',
  energy: 'Where are the people of :country finding their best value in Energy?',
  subscriptions: 'Where are the people of :country finding their best value in Subscriptions?',
  business: 'Where are small businesses of :country finding their best value?'
};
