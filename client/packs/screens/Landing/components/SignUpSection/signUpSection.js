import React from 'react';
import SignUpModalLink from 'components/SignUpModalLink';
import {} from 'prop-types';

const propTypes = {};

const SignUpSection = () => {
  return(
    <div className='signup-section'>
      <div className='container'>
        <div className='sign-up'>
          <div className='sign-up-text'>
            <h4>Let’s make smarter decisions together</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
          </div>
          <SignUpModalLink className='btn sign-up-btn'>Sign up</SignUpModalLink>
        </div>
      </div>
    </div>
  );
};

SignUpSection.propTypes = propTypes;

export default SignUpSection;