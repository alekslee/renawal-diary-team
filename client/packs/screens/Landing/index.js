import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Landing from './landing';

import { actions, selectors } from 'core/category';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  category: selectors.getCategory(state),
  categories: selectors.getCategories(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchCategoriesDispatch: actions.fetchCategories,
    setCategoriesListDispatch: actions.setCategoriesList,
    setCategoryDispatch: actions.setCategory
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
