import { EditorState } from 'draft-js';

export const INITIAL_VALUES = {
  lastBuy: undefined,
  currentProvider: undefined,
  buyingType: undefined,
  buyingTypeCompany: undefined,
  checkedPlaces: undefined,
  insightsComment: EditorState.createEmpty()
};

export const STEPS = [
  'provider',
  'criteria',
  // 'price-details',
  'rating',
  'insights',
]
