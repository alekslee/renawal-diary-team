import React, { Fragment, Component } from 'react';
import { toastr } from 'lib/helpers';
import { draftToMarkdown } from 'markdown-draft-js';
import { convertToRaw } from 'draft-js';
import { Row, Col } from 'reactstrap';
import { shape, string, number, bool, object, func } from 'prop-types';

import Loader from 'components/Loader';
import BenefitsInfoBlock from 'components/BenefitsInfoBlock';
import I18n from 'components/I18n';
import { paths } from 'layouts/constants';
import { Form, Header } from './components';
import { INITIAL_VALUES } from './constants';
import { defaultMessages } from 'locales/default';
import { retrieveFormInitialValues } from 'lib/helpers/renewalDetails';

const propTypes = {
  category: shape({
    id: string,
    renewalDetail: object
  }).isRequired,
  countryCode: string.isRequired,
  history: object.isRequired,
  currentUser: object.isRequired,
  isRenewalDetailExists: bool.isRequired,
  renewalDetail: object,
  criteriaQuestionsOptions: object,
  pricesQuestionsOptions: object,
  createFeedbackDispatch: func.isRequired,
  fetchRenewalDetailScreenDataDispatch: func.isRequired,
  setRenewalDetailFormOptionsDispatch: func.isRequired,
  setRenewalDetailDispatch: func.isRequired,
  formAttributes: object,
  changeLoaderStatusDispatch: func.isRequired,
};

class Vote extends Component {
  state = {
    isLoading: true,
    inProcess: false,
    isConfirmModalOpen: false,
  };

  componentDidMount() {
    const { setRenewalDetailDispatch, fetchRenewalDetailScreenDataDispatch, category } = this.props;

    const callback = () => {
      this.setState({ isLoading: false });
      // setRenewalDetailDispatch({ renewalDetail: {} });
    }

    fetchRenewalDetailScreenDataDispatch({ categoryId: category.id, callback, errorCallback: callback });
  };

  submitHandler = ({
    lastBuy,
    currentProvider,
    buyingType,
    buyingTypeCompany,
    checkedPlaces,
    currentProviderRate,
    currentProviderRateComment,
    buyingTypeCompanyRate,
    buyingTypeCompanyRateComment,
    insightsComment,
    criteriaQuestions,
    pricesQuestions,
    priceRate,
    serviceRate,
    claimRate,
    claimWithBussiness,
    triggeredAt,
    createSmartReminder
  }) => {
    const {
      countryCode,
      createFeedbackDispatch,
      category,
      history,
      fetchOnChangeCategoryDataDispatch,
      formAttributes,
      changeLoaderStatusDispatch
    } = this.props;

    const callback = () => {
      this.setState({ inProcess: false });
      history.push(paths.LEADERBOARD.replace(':country', countryCode).replace(':slag', category.slag).concat('?vote-success=1') );
      // toastr.success('Thank you for voting!');

      if (createSmartReminder) {
        const callback2 = () => changeLoaderStatusDispatch({ status: false, type: 'category' });
        fetchOnChangeCategoryDataDispatch({ id: category.id, callback: callback2 });
        toastr.success('Smart reminder created!');
      }
    };

    const errorCallback = () => {
      this.setState({ inProcess: false });
    };

    this.setState({ inProcess: true });

    createFeedbackDispatch({
      params: {
        renewalDetailAttributes: {
          lastBuy,
          buyingType,
          checkedPlaces,
          buyingTypeCompany,
          currentProviderRate,
          currentProviderRateComment,
          buyingTypeCompanyRate,
          buyingTypeCompanyRateComment,
          priceRate,
          serviceRate,
          claimRate,
          claimWithBussiness,
          currentProvider: currentProvider || buyingTypeCompany,
          criteriaQuestions: JSON.stringify(criteriaQuestions),
          pricesQuestions: JSON.stringify(pricesQuestions)
        },
        triggeredAt,
        createSmartReminder,
        insightsComment: insightsComment, //&& draftToMarkdown(convertToRaw(insightsComment.getCurrentContent()))
      },
      callback,
      errorCallback
    });
  };

  retrieveInitialValues = () => {
    const {
      criteriaQuestionsOptions,
      pricesQuestionsOptions,
      currentUser,
      category
    } = this.props;


    return retrieveFormInitialValues({
      isRenewalDetailExists: false,
      renewalDetail: {},
      criteriaQuestionsOptions,
      pricesQuestionsOptions,
      currentUser,
      category
    });
  };

  toggle = (key, companyModalType) => this.setState({ [key]: !this.state[key], companyModalType });

  render() {
    const {
      submitHandler,
      retrieveInitialValues,
      toggle,
      state: { isLoading, inProcess, isConfirmModalOpen, isNewProviderModalOpen, companyModalType },
      props: { category }
    } = this;

    if (isLoading) return null;

    return (
      <div className='have-your-say-wrapper'>
        <div className='reminder-main have-your-say'>
          {/*<Header />*/}
          {/*<div className='reminder-form-item subheader main-subheader'>*/}
          {/*  <h2>*/}
          {/*    <I18n text={ defaultMessages.voteFormTitle } />*/}
          {/*  </h2>*/}
          {/*  <p>*/}
          {/*    <I18n text={ defaultMessages.voteFormSubtitle } />*/}
          {/*  </p>*/}
          {/*</div>*/}
          <div className='reminder-main-body'>
            <Form
              initialValues={ retrieveInitialValues() }
              { ...{ submitHandler, toggle, isConfirmModalOpen, companyModalType, isNewProviderModalOpen }}
            />
          </div>
        </div>
        <div className="info-sidebar" />
        {/*<BenefitsInfoBlock />*/}
      </div>
    );
  };
};

Vote.propTypes = propTypes;

export default Vote;
