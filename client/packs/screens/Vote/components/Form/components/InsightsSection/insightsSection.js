import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import moment from 'moment';
import { generateArrayOfYears } from 'lib/helpers/smartReminder';

import Select from 'react-select';
import DateField from 'components/DateField';
import MobileDateField from 'components/MobileDateField';
import FieldWithErrors from 'components/FieldWithErrors';
import { array, func, number, object } from 'prop-types';
import Switch from "react-switch";
import cx from "classnames";

const propTypes = {
  yearsSelectOptions: array.isRequired,
  currentYear: number,
  windowSize: number,
  formAttributes: object,
  smartReminders: array.isRequired,
  category: object.isRequired,
  setSmartReminderCurrentYearDispatch: func.isRequired,
  change: func.isRequired,
};

class InsightsSection extends Component {

  state = {
    dateSelect: moment().add(1,'hour').toDate(),
  }

  componentDidMount() {
    const { dateSelect } = this.state;
    this.props.change('aliasName', this.generateDefaultAliasName())
    this.props.change('triggeredAt', dateSelect)
  }

  toggle = key => this.setState({ [key]: !this.state[key] });

  toggleEnableReminder = () => {
    const { createSmartReminder } = (this.props.formAttributes || {});
    this.props.change('createSmartReminder', !createSmartReminder);
  }

  onDateFieldChange = (date) => {
    const dateSelect = moment(date).add(1,'hour').toDate();
    this.setState({ dateSelect });
    this.props.change('triggeredAt', dateSelect);
  }

  onChangeSmartReminderYear = year => {
    const { setSmartReminderCurrentYearDispatch } = this.props;
    setSmartReminderCurrentYearDispatch({ currentYear: year.value });
  };

  generateDefaultAliasName = (categoryId=null) => {
    const {
      props: { category, smartReminders },
      state: { selectedName }
    } = this;

    const categoryReminders = smartReminders
      .filter(el => +el.categoryId === +(categoryId || (category && category.id)) )

    return (`${category && category.name || selectedName } ${ (categoryReminders.length + 1) }`);
  }

  handleMouse = (ev) => {
    const { aliasName } = this.state;
    if (aliasName) return;

    if (ev.type === 'mouseover') {
      this.props.change('aliasName', null)
    } else {
      this.props.change('aliasName', this.generateDefaultAliasName())
    }
  }

  handleChange = ({ target }) => {

    this.setState({ aliasName: target && target.value })
  }

  render() {
    const {
      handleMouse,
      handleChange,
      onDateFieldChange,
      generateDefaultAliasName,
      onChangeSmartReminderYear,
      toggle,
      toggleEnableReminder,
      state: { dateSelect, confirmed },
      props: {
        currentYear,
        windowSize,
        yearsSelectOptions,
        withoutInsightsComment,
        createCheckbox,
      }
    } = this;

    const { createSmartReminder } = (this.props.formAttributes || {});

    const dateField = windowSize < 768 ? MobileDateField : DateField;

    return (
      <div className="top-part">
        <div className='reminder-form-item'>
          { !withoutInsightsComment &&
            <Fragment>
              <h3>Please share your shopping insights here</h3>
              <div className='inputs-group'>
                <Field
                  name='insightsComment'
                  component="textarea"
                  className="reminder-textarea"
                />
              </div>
            </Fragment>
          }

          <div className="set-reminder-title">
           { createCheckbox ?
            <label className="red-checkbox left">
              <input
                type='checkbox'
                checked={ createSmartReminder || false }
                onChange={ () => toggleEnableReminder() }
              />
              <span className='checkmark' />
              <h3>Set reminder now <span>(Optional)</span></h3>
            </label>
            :
            <span className="toggle-personal m-0">
              <h3 className="mr-2">Set reminder now <span>(Optional)</span></h3>
              <Switch
                onChange={ () => toggleEnableReminder() }
                checked={ createSmartReminder || false }
                className={ cx('react-switch', { actived: createSmartReminder }) }
                height={ 14 }
                width={ 34 }
              />
            </span>
          }

          </div>
          <div className="inputs-group inner-group wide-inputs">
            <div className="grid-2">
              <div className={ cx('dropdown-container datepicker', { disabled: !createSmartReminder }) }>

                <Field
                  component={ dateField }
                  onChange={onDateFieldChange.bind(this)}
                  className="datepicker-example"
                  name='triggeredAt'
                  disabled={ !createSmartReminder }
                />

              </div>

            </div>
          </div>
          <div className="inputs-group inner-group wide-inputs">
            <div className="grid-2"
                 onMouseOut={ handleMouse }
            >

              <Field
                component={ FieldWithErrors }
                onChange={ handleChange }
                defaultValue={ generateDefaultAliasName() }
                className="reminder-input w-100"
                placeholder="Enter your Reminder name…"
                name='aliasName'
                disabled={ !createSmartReminder }
              />

            </div>
          </div>
        </div>
      </div>
    );
  }
};
InsightsSection.propTypes = propTypes;

export default InsightsSection;
