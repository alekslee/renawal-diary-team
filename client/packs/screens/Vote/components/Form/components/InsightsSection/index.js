import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import insightsSection from './insightsSection';
import { getFormValues } from 'redux-form';

import { selectors, actions } from 'core/smartReminder';
import { selectors as categorySelectors } from 'core/category';

const mapStateToProps = (state, props) => ({
  formAttributes: getFormValues(props.form)(state),
  currentYear: selectors.getCurrentYear(state),
  yearsSelectOptions: selectors.getYearsSelectOptions(state),
  smartReminders: selectors.getSmartReminders(state),
  category: categorySelectors.getCategory(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setSmartReminderCurrentYearDispatch: actions.setSmartReminderCurrentYear,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(insightsSection);