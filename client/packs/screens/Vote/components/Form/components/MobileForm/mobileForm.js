import React, { Component, Fragment } from 'react';
import { object, string, func, bool, number } from 'prop-types';
import cx from 'classnames';
import { ICONS } from 'components/FormSlider/constants';

import SignUpModalLink from 'components/SignUpModalLink';
import I18n from 'components/I18n';
import { defaultMessages } from 'locales/default';
import { InsightsSection } from '../../components';
import PriceDetails from '../../../../../../images/icons/PriceDetails/PriceDetails';
import ProviderDetailsFormData from 'components/ProviderDetailsFormData';
import CriteriaQuestionsFormData from 'components/CriteriaQuestionsFormData';
import RatingFormData from 'components/RatingFormData';
import CheckboxIcon from 'images/icons/CheckboxIcon';
import { Tabs, Tab } from 'react-bootstrap-tabs';
import AnimateHeight from 'react-animate-height';
import { STEPS } from '../../../../constants';

const propTypes = {
  currentUser: object.isRequired,
  formAttributes: object,
  change: func.isRequired,
  invalid: bool.isRequired,
  submitFailed: bool,
  dirty: bool.isRequired,
  form: string.isRequired,
  criteriaQuestionsOptions: object.isRequired,
  pricesQuestionsOptions: object.isRequired,
  createFeedbackDispatch: func.isRequired,
  windowSize: number.isRequired
};

class MobileForm extends Component {

  StepContentHead = ({ formIcon, title, index, lastStep }) => {
    const { currentStep } = this.props;
    // const stepPercent = Math.round(currentStep / (STEPS.length - 1) * 1000)/ 10;

    return (
      <div className={ cx('vote-wizard-pill', { 'last-step': lastStep }) } >
        {/*{!lastStep &&*/}
        {/*  <div className="vote-wizard-pill-head">*/}

        {/*    <div className="points">+ 5 points</div>*/}

        {/*    <span>*/}
        {/*      {`Step ${ index + 1 }`}*/}
        {/*    </span>*/}

        {/*    {*/}
        {/*    // <span className="step">*/}
        {/*    //   {`${ stepPercent }% Compleate`}*/}
        {/*    // </span>*/}
        {/*    }*/}
        {/*</div> }*/}

        <div className="vote-wizard-pill-body text-center" >
          { formIcon &&
            <div className="wizard-pill-icon">
              { ICONS[formIcon] }
              </div>
          }
          <div>
            { title }
          </div>

          { lastStep &&
            <p>You have passed all the steps now you can vote</p>
          }
        </div>

      </div>
    );
  }

  StepComponent = ({ type, formIcon, children, title, lastStep }) => {
    const { currentStep, toStep } = this.props;
    const { StepContentHead } = this;

    const open = STEPS[currentStep] === type;
    const index = STEPS.findIndex(e => e === type);

    const Compleate = () => (
      <div
        className={ cx('form-slider', { active: '' }) }
        onClick={() => toStep(index)}
      >
        <h3 className='form-slider-title'>
          <span className="form-slider-icon">
            {formIcon && ICONS[formIcon]}
          </span>
          <div className="form-title-text">
            <span className="title">
              { title }
            </span>
            {/*<span className="sub-title">*/}
            {/*  +5 points*/}
            {/*</span>*/}
          </div>
        </h3>
        <div className="step">
          <CheckboxIcon />
          <span>Step { index + 1 }</span>
        </div>
      </div>
    );

    // if (index > currentStep ) {
    //   return null;
    // }

    return (
      <div className={ cx({ "step-container": index <= currentStep })}>
        <AnimateHeight
          duration={ 100 }
          height={ open || index > currentStep ? '0' : 'auto' }
        >
           <Compleate />
        </AnimateHeight>

        <AnimateHeight
          duration={ 1000 }
          height={ open ? 'auto' : 0 }
        >
          <StepContentHead
            { ...{ formIcon, title, index, lastStep } }
          />

          { children }
        </AnimateHeight>
      </div>
    );
  }

  render() {
    const {
      StepComponent,
      props: {
        LastStep,
        change,
        dirty,
        form,
        currentProvider,
        buyingTypeCompany,
        invalid,
        criteriaQuestionsOptions,
        pricesQuestionsOptions,
        formAttributes,
        isConfirmModalOpen,
        toggle,
        createFeedbackDispatch,
        currentStep,
        children,
        windowSize,
        submitFailed,
      },
     } = this;
    const disabled = invalid;

    return (
      <Fragment>

        <StepComponent
          title='My Choice'
          formIcon='ProviderDetails'
          type="provider"
        >
          <ProviderDetailsFormData
            { ...{ form, change, dirty, invalid, toggle, submitFailed }}
          />
          {children}
        </StepComponent>

        <StepComponent
          title="My criteria" // (This enables us to match people like you)
          formIcon="MyCriteria"
          type="criteria"
        >
          <CriteriaQuestionsFormData
            namePrefix="criteriaQuestions"
            criteriaOptions={ criteriaQuestionsOptions }
            { ...{ form, disabled, invalid } }
          />
          {children}
        </StepComponent>

{/*        <StepComponent
          title="What did you pay?" // (Optional) (Compare your prices)
          formIcon="PriceDetails"
          type="price-details"
        >
          <CriteriaQuestionsFormData
            namePrefix="pricesQuestions"
            criteriaOptions={ pricesQuestionsOptions }
            { ...{ form, disabled } }
          />
          {children}
        </StepComponent>*/}

        <StepComponent
          title="Customer Satisfaction" //  (Help other members)
          formIcon="CustomerSatisfaction"
          type="rating"
        >
            <RatingFormData
              { ...{ change, form, dirty, invalid } }
            />

          {children}
        </StepComponent>

        <StepComponent
          title="My insights" // (Optional)
          formIcon="MyInsights"
          type="insights"
        >
          <InsightsSection
            windowSize={ windowSize }
            { ...{ change, form } }
          />
          {children}
        </StepComponent>
      </Fragment>
    )

  };
};
MobileForm.propTypes = propTypes;

export default MobileForm;
