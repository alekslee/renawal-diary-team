import React, { Component, Fragment } from 'react';
import { object, string, func, bool } from 'prop-types';
import cx from 'classnames';
import { ICONS } from 'components/FormSlider/constants';

import SignUpModalLink from 'components/SignUpModalLink';
import I18n from 'components/I18n';
import { defaultMessages } from 'locales/default';
import { InsightsSection } from '../../components';
import PriceDetails from '../../../../../../images/icons/PriceDetails/PriceDetails';
import ProviderDetailsFormData from 'components/ProviderDetailsFormData';
import CriteriaQuestionsFormData from 'components/CriteriaQuestionsFormData';
import RatingFormData from 'components/RatingFormData';
import CheckboxIcon from 'images/icons/CheckboxIcon';
import { Tabs, Tab } from 'react-bootstrap-tabs';
import { STEPS } from '../../../../constants';

const propTypes = {
  currentUser: object.isRequired,
  formAttributes: object,
  category: object.isRequired,
  change: func.isRequired,
  invalid: bool.isRequired,
  submitFailed: bool,
  dirty: bool.isRequired,
  form: string,
  criteriaQuestionsOptions: object.isRequired,
  pricesQuestionsOptions: object.isRequired,
  createFeedbackDispatch: func.isRequired
};

class Form extends Component {


  StepContentHead = ({ formIcon, title, index }) => {
    const { currentStep } = this.props;
    // const stepPercent = Math.round(currentStep / (STEPS.length - 1) * 1000)/ 10;

    return (
      <div className="vote-wizard-pill" >
        {/*<div className="vote-wizard-pill-head">*/}

        {/*  <div className="points">+5 points</div>*/}

        {/*  <span className="steps">{`Step ${ index + 1 }`}</span>*/}

        {/* {*/}
        {/*   // <span>*/}
        {/*   //  {`${ stepPercent }% Compleate`}*/}
        {/*   // </span>*/}
        {/* }*/}
        {/*</div>*/}

        <div className="vote-wizard-pill-body text-center">

          <div className="icon-container">
            {formIcon && ICONS[formIcon]}
          </div>

          <div className="title">
            { title }
          </div>
        </div>

      </div>
    );
  }

  StepComponent = ({ type, formIcon, children, title }) => {
    const { currentStep } = this.props;
    const { StepContentHead } = this;

    const open = STEPS[currentStep] === type;
    const index = STEPS.findIndex(e => e === type);

    // if (index > currentStep) {
    //   return null;
    // }

    return (
      <Fragment>
        <StepContentHead
          { ...{ formIcon, title, index } }
        />
        { children }
      </Fragment>
    );
  }

  render() {

    const {
      StepComponent,
      props: {
        LastStep,
        category,
        change,
        dirty,
        form,
        invalid,
        submitFailed,
        criteriaQuestionsOptions,
        pricesQuestionsOptions,
        formAttributes,
        isConfirmModalOpen,
        toggle,
        createFeedbackDispatch,
        currentStep,
        syncErrors,
      },
     } = this;

    const disabled = invalid;

    const components = {
        provider: (
          <StepComponent
            title='My Choice'
            formIcon='ProviderDetails'
            type="provider"
          >
            <ProviderDetailsFormData
              { ...{ form, change, dirty, invalid, toggle, submitFailed, syncErrors }}
            />
          </StepComponent>
        ),

        criteria: (
          <StepComponent
            title="My criteria" // (This enables us to match people like you)
            formIcon="MyCriteria"
            type="criteria"
          >
            <CriteriaQuestionsFormData
              namePrefix="criteriaQuestions"
              criteriaOptions={ criteriaQuestionsOptions }
              { ...{ form, disabled, invalid } }
            />

          </StepComponent>
          ),

        // ['price-details']: (

        //     <StepComponent
        //       title="What did you pay?" // (Optional) (Compare your prices)
        //       formIcon="PriceDetails"
        //       type="price-details"
        //     >
        //       <CriteriaQuestionsFormData
        //         namePrefix="pricesQuestions"
        //         criteriaOptions={ pricesQuestionsOptions }
        //         { ...{ form, disabled } }
        //       />

        //     </StepComponent>
        // ),

        rating: (
          <StepComponent
            title="Customer Satisfaction" //  (Help other members)
            formIcon="CustomerSatisfaction"
            type="rating"
          >
            <RatingFormData
              { ...{ change, form, dirty, invalid } }
            />

          </StepComponent>
        ),

        insights: (
          <StepComponent
            title="My insights" // (Optional)
            formIcon="MyInsights"
            type="insights"
          >
            <InsightsSection
              { ...{ change, form } }
            />

          </StepComponent>
        )
      }

    const steps = STEPS.slice(0, currentStep + 1);

    return (
      <Fragment>

        <Tabs
          selected={ components[ STEPS[currentStep] ].props.title }
          onSelect={(index) => {
            this.props.toStep(index)
          }}
        >

          { steps.map(key => {
            const { title } = components[key].props;

            return (
              <Tab key={key} label={title}>
                { components[key] }
              </Tab>
            );
          }) }

        </Tabs>


      </Fragment>
    )

  };
};
Form.propTypes = propTypes;

export default Form;
