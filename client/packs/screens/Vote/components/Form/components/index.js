import InsightsSection from './InsightsSection';
import MobileForm from './MobileForm';
import DesktopForm from './DesktopForm';

export { InsightsSection, MobileForm, DesktopForm };
