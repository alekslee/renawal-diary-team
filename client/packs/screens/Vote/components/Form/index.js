import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, getFormValues, submit } from 'redux-form';
import Form from './form';

import { selectors } from 'core/renewalDetail';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { actions as feedbackActions } from 'core/feedback';
import { selectors as categorySelectors } from 'core/category';
import { actions as signUpActions, selectors as signUpSelectors } from 'core/signUp';

const mapStateToProps = (state, props) => ({
  formAttributes: getFormValues(props.form)(state),
  criteriaQuestionsOptions: selectors.getCriteriaQuestionsOptions(state),
  pricesQuestionsOptions: selectors.getPricesQuestionsOptions(state),
  currentUser: currentUserSelectors.getCurrentUser(state),
  category: categorySelectors.getCategory(state),
  pickCategoriesStep: signUpSelectors.getPickCategoriesStepStatus(state)
});

const validate = ({ lastBuy, currentProvider, buyingType, buyingTypeCompany, checkedPlaces }) => {
  const errors = {};
  if (!lastBuy) errors.lastBuy = 'Required';
  if (!currentProvider || !currentProvider.label) errors.currentProvider = 'Required';
  if (!buyingType) errors.buyingType = 'Required';

  if (buyingType !== 'provider' && (!buyingTypeCompany || !buyingTypeCompany.label)) {
    errors.buyingTypeCompany = 'Required';
  }

  if (!checkedPlaces) errors.checkedPlaces = 'Required';
  return errors;
};

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    createFeedbackDispatch: feedbackActions.createFeedback,
    changePickCategoriesStepDispatch: signUpActions.changePickCategoriesStep
  }, dispatsh)
);

export default reduxForm({ form: 'feedbackForm', validate })(connect(mapStateToProps, mapDispatchToProps)(Form));
