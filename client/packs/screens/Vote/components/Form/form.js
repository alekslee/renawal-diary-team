import React, { Component, Fragment } from 'react';
import { toastr } from 'lib/helpers';
import { object, string, func, bool } from 'prop-types';
import cx from 'classnames';
import { ICONS } from 'components/FormSlider/constants';

import { InsightsSection, MobileForm, DesktopForm } from './components';
import { VoteConfirmModal, NewProviderModal } from '../../components';
import SignUpModalLink from 'components/SignUpModalLink';

import { STEPS } from '../../constants';

const propTypes = {
  currentUser: object.isRequired,
  formAttributes: object,
  category: object.isRequired,
  submitHandler: func.isRequired,
  change: func.isRequired,
  invalid: bool.isRequired,
  submitFailed: bool,
  dirty: bool.isRequired,
  form: string,
  criteriaQuestionsOptions: object.isRequired,
  pricesQuestionsOptions: object.isRequired,
  createFeedbackDispatch: func.isRequired,
  changePickCategoriesStepDispatch: func.isRequired
};

class Form extends Component {

  state = {
    currentStep: 0,
    windowSize: window.innerWidth,
    confirmed: true,
  };

  componentDidMount() {
    this.setWindowSize();
    this.changePickCategoriesStep();
    window.addEventListener('resize', this.setWindowSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setWindowSize);
  }

  changePickCategoriesStep = () => {
    const {currentUser, changePickCategoriesStepDispatch} = this.props;

    if (!currentUser.id) {
      changePickCategoriesStepDispatch({ pickCategoriesStep: false });
    }
  }

  setWindowSize = () => {
    this.setState({ windowSize: window.innerWidth });
  }

  toggleChecked = ({ checked: confirmed }) => this.setState({ confirmed });

  toNextStep = () => {
    const { currentStep } = this.state;

    this.toStep(currentStep + 1);
  };

  toPrevStep = () => {
    const { currentStep, formAttributes } = this.state;

    this.toStep(currentStep - 1, formAttributes);
  };

  toStep = step => {
    const { setSubmitFailedDispatch, form, syncErrors } = this.props;
    const lastStep = STEPS.length - 1;

    if (step < 0 || step > lastStep) return;

    this.setState({ currentStep: step });
  };

  createFeedback = () => {
    const {
      props: {
        createFeedbackDispatch,
        category,
        formAttributes
      },
      state: { currentStep, windowSize },
    } = this;

    const {
      lastBuy,
      buyingType,
      checkedPlaces,
      currentProvider,
      buyingTypeCompany,
      currentProviderRate,
      currentProviderRateComment,
      buyingTypeCompanyRate,
      buyingTypeCompanyRateComment,
      criteriaQuestions,
      pricesQuestions,
      insightsComment,
      triggeredAt,
      createSmartReminder
    } = formAttributes;

    createFeedbackDispatch({
      params: {
        renewalDetailAttributes: {
          lastBuy,
          buyingType,
          checkedPlaces,
          currentProvider,
          buyingTypeCompany,
          currentProviderRate,
          currentProviderRateComment,
          buyingTypeCompanyRate,
          buyingTypeCompanyRateComment,
          criteriaQuestions: JSON.stringify(criteriaQuestions),
          pricesQuestions: JSON.stringify(pricesQuestions)
        },
        triggeredAt,
        createSmartReminder,
        insightsComment: insightsComment, //&& draftToMarkdown(convertToRaw(insightsComment.getCurrentContent())),
        categoryId: category.id
      }
    });
  }

  RenderBtns = () => {
    const {
      toNextStep,
      toPrevStep,
      createFeedback,
      toggleChecked,
      submitVoteForm,
      state: {
        currentStep,
        confirmed
      },
      props: {
        invalid,
        currentUser,
        toggle,
      }
    } = this;

    const disabled = invalid;

    return (
      <div className="form-bottom have-your-say-bottom text-center">
        <div className="btn-container">
          { currentStep !== 0 &&
          <button
            type="button" className="btn cancel"
            onClick={ toPrevStep }
          >
            Back
          </button>
          }

          { currentStep !== (STEPS.length - 1) ?
            <Fragment>
              <button
                type="button" className="btn next"
                onClick={ () => {
                  const { currentProvider, buyingTypeCompany, buyingType } = (this.props.formAttributes || {});

                  const provider = currentProvider || buyingTypeCompany;
                  let cond2 = true;
                  if ( buyingType !== "provider" ) cond2 = !!(buyingTypeCompany || {}).id;
                  if ( (provider || {}).id && cond2 ) {
                    this.setState({ showError: false });
                    toNextStep()
                  } else {
                    this.setState({ showError: true });
                  }
                }}
              >
              Next
              </button>
            </Fragment> : <Fragment>
              {
                currentUser.id ?
                  <button
                    type='button'
                    className='btn reminder-btn small-btn'
                    disabled={ invalid || !confirmed }
                    onClick={ () => {
                      submitVoteForm();
                    } }
                  >
                    Vote
                  </button>
                  :
                  <SignUpModalLink className='btn reminder-btn small-btn' onSignUpSuccess={ createFeedback }>
                    Vote
                  </SignUpModalLink>
              }
            </Fragment>
          }

          {currentStep === (STEPS.length - 1) && <label className='red-checkbox left'>
            <input
              type='checkbox'
              checked={ confirmed }
              onChange={ ev => toggleChecked(ev.target) }
            />
            <span className='checkmark' />
            By clicking the vote button, I confirm that I am a customer of the business I voted for and that I agree to the t&c's of renewal diary
          </label>}
        </div>

        { currentStep !== (STEPS.length - 1) &&
        <p>
          Please tap ‘Next’ and go to the next Step { currentStep + 2 }.
        </p>
        }

      </div>
    );
  }

  LastStep = () => {
    const {
      createFeedback,
      toggleChecked,
      state: {
        confirmed,
      },
      props: {
        currentUser,
        invalid,
        toggle,
      }
    } = this;

    return (
      <div className='have-your-say-bottom'>
        <label className='red-checkbox left'>
          <input
            type='checkbox'
            checked={ confirmed }
            onChange={ ev => toggleChecked(ev.target) }
          />
          <span className='checkmark' />
          By clicking the vote button, I confirm that I am a customer of the business I voted for and that I agree to the t&c's of renewal diary
        </label>
        {
          currentUser.id ?
            <button
              type='button'
              className='btn reminder-btn small-btn'
              disabled={ invalid || !confirmed }
              // onClick={ () => {
              //   toggle('isConfirmModalOpen');
              // } }
            >
              Vote
            </button>
            :
            <SignUpModalLink className='btn reminder-btn small-btn' onSignUpSuccess={ createFeedback }>
              Vote
            </SignUpModalLink>
        }
      </div>
    );
  }

  submitVoteForm = () => {
    const { formRef } = this.refs;
    formRef.dispatchEvent(new Event('submit'));
  };

  render() {
    const {
      LastStep,
      FormBody,
      toNextStep,
      toPrevStep,
      RenderBtns,
      props: {
        invalid,
        isConfirmModalOpen,
        isNewProviderModalOpen,
        companyModalType,
        toggle,
        category,
        submitHandler,
        currentUser,
        criteriaQuestionsOptions,
        pricesQuestionsOptions,
        formAttributes
      },
      state: {
        currentStep,
        confirmed,
        windowSize,
        showError,
      },
    } = this;
    const { currentProvider, buyingTypeCompany, buyingType } = (this.props.formAttributes || {});

    const disabled = invalid;
    const lastStep = STEPS.length - 1;

    const handleFormSubmit = e => {
      submitHandler(formAttributes);
      if (this.props.invalid) toastr.error('Form is invalid. Please fix errors');
    };

    return (
      <Fragment>
        <form
          onSubmit={ handleFormSubmit }
          ref="formRef"
          className={ cx({'mobile-form': windowSize <= 768 }) }
        >
          { windowSize > 768 ?
            <DesktopForm
              currentStep={ currentStep }
              windowSize={ windowSize }
              LastStep={ LastStep }
              toStep={ this.toStep.bind(this) }
              { ...this.props }
              submitFailed={ showError }
              syncErrors={{
                buyingTypeCompany: !!(buyingTypeCompany || {}).id,
                currentProvider: !!(currentProvider || {}).id,
              }}
            />
            :
            <MobileForm
              currentStep={ currentStep }
              windowSize={ windowSize }
              LastStep={ LastStep }
              toStep={ this.toStep.bind(this) }
              { ...this.props }
              submitFailed={ showError }
              syncErrors={{
                buyingTypeCompany: !(buyingTypeCompany || {}).id,
                currentProvider: !(currentProvider || {}).id,
              }}
            >
              <RenderBtns />
            </MobileForm>
          }

        </form>

        {windowSize > 768 && <RenderBtns /> }

        {/*<VoteConfirmModal*/}
        {/*  { ...{ confirmed, windowSize, isConfirmModalOpen, toggle, category } }*/}
        {/*/>*/}

        <NewProviderModal
          { ...{ confirmed, windowSize, companyModalType, isNewProviderModalOpen, toggle, category } }
        />
      </Fragment>
    );
  };
};
Form.propTypes = propTypes;

export default Form;
