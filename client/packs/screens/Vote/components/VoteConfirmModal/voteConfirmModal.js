import React, { Component } from 'react';
import { bool, func, object } from 'prop-types';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import cx from 'classnames';

const propTypes = {
  isConfirmModalOpen: bool.isRequired,
  toggle: func.isRequired,
  category: object.isRequired,
};

class VoteConfirmModal extends Component {

  // state = {
  //   confirmed: false
  // }

  // toggleChecked = ({ checked: confirmed }) => this.setState({ confirmed });

  render() {

    const {
      // toggleChecked,
      // state: { confirmed },
      props: { windowSize, confirmed, isConfirmModalOpen, toggle, category },
    } = this;

    return (
      <Modal
        isOpen={ isConfirmModalOpen }
        toggle={ () => toggle('isConfirmModalOpen') }
        className={cx('vote-confirm-modal', category.root.enName)}
      >
        <ModalHeader toggle={ () => toggle('isConfirmModalOpen') }>
          Please confirm
        </ModalHeader>

        {
          /*
                <ModalBody>
                  <label className='red-checkbox left'>
                    <input
                      type='checkbox'
                      onChange={ev => toggleChecked(ev.target)}
                    />
                    <span className='checkmark' />
                    I am not an employee of this bussines as per our terms and conditions
                  </label>

                </ModalBody>
          */
        }

        <ModalFooter>

          <Button className="btn btn-empty">
            Cancel
          </Button>

          <Button
            className="btn reminder-btn"
            disabled={!confirmed}
            onClick={() => {
              toggle('isConfirmModalOpen');
            }}
          >
            Ok
          </Button>

        </ModalFooter>
      </Modal>
    );
  }
}

VoteConfirmModal.propTypes = propTypes;

export default VoteConfirmModal;
