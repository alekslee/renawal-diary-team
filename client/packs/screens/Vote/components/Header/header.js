import React, { Component } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { bool, object, string, number, array, func } from 'prop-types';

import DropdownComponent from 'components/DropdownComponent';

const propTypes = {
  renewalDetailYear: number.isRequired,
  renewalDetailPossibleYears: array.isRequired,
  category: object.isRequired,
  fetchRenewalDetailDispatch: func.isRequired
};

class Header extends Component {
  state = {
  };

  loadRenewalDetailPerYear = year => {
    const { fetchRenewalDetailDispatch, category } = this.props;

    const callback = () => {}

    fetchRenewalDetailDispatch({ categoryId: category.id, year, callback, errorCallback: callback });
  };

  render() {
    const {
      loadRenewalDetailPerYear,
      state: { },
      props: { category, renewalDetailYear, renewalDetailPossibleYears }
    } = this;

    return (
      <div className='reminder-main-header'>
        <h3>
          Have your say!
        </h3>
      </div>
    );
  };
};
Header.propTypes = propTypes;

export default Header;
