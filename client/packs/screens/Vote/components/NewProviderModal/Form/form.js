import React, { Component } from 'react';
import { bool, func, object } from 'prop-types';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import cx from 'classnames';
import { Field } from 'redux-form';
import SelectField from 'components/SelectField';
import LocationField from 'components/LocationField';
import FieldWithErrors from 'components/FieldWithErrors';
import { toastr } from 'lib/helpers';
import SignInModalLink from 'components/SignInModalLink';

const propTypes = {
  // isNewProviderFormOpen: bool,
  toggle: func.isRequired,
  createCompanyDispatch: func.isRequired,
  category: object.isRequired,
  currentCountry: object.isRequired,
  currentUser: object,
  formAttributes: object,
  // companyModalType:
};

class NewProviderForm extends Component {

  state = {}

  resetErrors = () => { this.setState({ errors: null }) }

  metaError = (name) => {
    const { errors } = this.state;
    const mess = errors && (errors[name] || [])[0];
    if (!mess) return null;
    return ({ meta: { touched: !!mess, error: mess }});
  }

  handleSubmit = (ev) => {
    const { formAttributes, toggle, handleSubmit, companyModalType, createCompanyDispatch } = this.props;
    const { categoryId, location: { countryCode }, name, url } = (formAttributes || {});

    const callback = (ss) => {
      toastr.success("Success", `New ${companyModalType} successed created`);
      toggle();
    }

    const errorCallback = ({ errors }) => {
      this.setState({ errors });
    }

    createCompanyDispatch({ categoryId, countryCode, name, url, companyModalType, callback, errorCallback });
  }

  render() {

    const {
      resetErrors,
      metaError,
      state: { errors },
      props: { windowSize,
        currentUser,
        handleSubmit,
        currentCountry,
        confirmed,
        category,
        companyModalType,
        categoriesOptions,
      },
    } = this;

    return (
      <form onSubmit={ handleSubmit(this.handleSubmit) }>

        <ModalBody>
          <h3>
           If you dont find your provider in the list you can create it here
          </h3>
          <label className='input-field location'>
            <Field
              component={ LocationField }
              type='text'
              name='location'
              country={ currentCountry }
              onChange={ resetErrors }
              placeholder="Contry"
            />
          </label>

          <div className='input-field webkit-scrollbar'>
            <Field
              component={ SelectField }
              className='reminder-select category-select'
              classNamePrefix='Select'
              name='categoryId'
              optionsType='nested'
              onChange={ resetErrors }
              options={ categoriesOptions }
              { ...metaError('categoryId') }
            />
          </div>

          <label className='input-field'>
            <Field
              component={ FieldWithErrors }
              type='text'
              name='name'
              placeholder="Enter bussiness name"
              onChange={ resetErrors }
              className="reminder-input"
              { ...metaError('name') }
            />
          </label>

          <label className='input-field'>
            <Field
              component={ FieldWithErrors }
              type='text'
              name='url'
              placeholder="http://"
              onChange={ resetErrors }
              className="reminder-input"
              { ...metaError('url') }
            />
          </label>

        </ModalBody>

        <ModalFooter>
         { currentUser.id ?
          <Button
            className="btn"
            // disabled={!confirmed}
            type="submit"
          >
            + Add {`${ companyModalType }`}
          </Button> :
          <SignInModalLink onSignUpSuccess={() => { /* TODO */ } }>
            <Button
              className="btn"
              type="button"
            >
              + Add {`${ companyModalType }`}
            </Button>
          </SignInModalLink>
        }

        </ModalFooter>

      </form>
    );
  }
}

NewProviderForm.propTypes = propTypes;

export default NewProviderForm;
