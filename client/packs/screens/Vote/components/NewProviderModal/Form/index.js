import Form from './form';
import { withRouter } from 'react-router-dom';
import { reduxForm, getFormValues } from 'redux-form';
import { bindActionCreators } from 'redux';
import { selectors as categorySelectors } from 'core/category';
import { connect } from 'react-redux';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { actions as companyActions } from 'core/company';
import { selectors as currentUserSelectors } from 'core/currentUser';

const mapStateToProps = (state, props) => ({
  categoriesOptions: categorySelectors.getSmartReminderCategoriesOptions(state),
  formAttributes: getFormValues(props.form)(state),
  currentCountry: currentCountrySelectors.getCurrentCountry(state),
  currentUser: currentUserSelectors.getCurrentUser(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    createCompanyDispatch: companyActions.createCompany,
  }, dispatsh)
);

const validate = ({ location, categoryId, name, url }) => {
  const errors = {};

  if (!location) errors.location = 'Location Required';
  if (!(location && location.countryCode)) errors.location = 'Location Required';
  if (!categoryId) errors.categoryId = 'Category Required';
  if (!name) errors.name = 'Name must be filled';
  if (!url) errors.url = 'Url must be filled';

  return errors;
};

export default reduxForm({
  form: 'newProviderForm',
  validate,
})(connect(mapStateToProps, mapDispatchToProps)(withRouter(Form)));
