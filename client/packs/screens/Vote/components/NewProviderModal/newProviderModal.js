import React, { Component } from 'react';
import { bool, func, object } from 'prop-types';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import cx from 'classnames';
import { Field } from 'redux-form';
import SelectField from 'components/SelectField';
import LocationField from 'components/LocationField';
import FieldWithErrors from 'components/FieldWithErrors';
import { toastr } from 'lib/helpers';
import ModalForm from './Form';

const propTypes = {
  isNewProviderModalOpen: bool,
  toggle: func.isRequired,
  category: object.isRequired,
  formAttributes: object,
};

class NewProviderModal extends Component {

  render() {

    const {
      props: { windowSize,
        companyModalType,
        isNewProviderModalOpen,
        toggle, category,
        categoriesOptions,
      },
    } = this;

    return (
      <Modal
        isOpen={ isNewProviderModalOpen }
        toggle={ () => toggle('isNewProviderModalOpen') }
        className={cx('new-provider-modal', category.root.enName)}
      >
        <ModalHeader toggle={ () => toggle('isNewProviderModalOpen') }>
          Add New { companyModalType }
        </ModalHeader>

        <ModalForm
          initialValues={{
            categoryId: category.id,
          }}
          toggle={() => toggle('isNewProviderModalOpen') }
          { ...{ category, companyModalType }}
        />
      </Modal>
    );
  }
}

NewProviderModal.propTypes = propTypes;

export default NewProviderModal;
