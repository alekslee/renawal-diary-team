import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {getFormValues, reduxForm, reset} from 'redux-form';
import Vote from './Vote';

import { selectors, actions } from 'core/renewalDetail';
import { actions as feedbackActions } from 'core/feedback';
import { selectors as categorySelectors, actions as categoryActions } from 'core/category';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { selectors as loaderSelectors, actions as loaderActions } from 'core/additional';

const mapStateToProps = (state, props )=> ({
  category: categorySelectors.getCategory(state),
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  currentUser: currentUserSelectors.getCurrentUser(state),
  renewalDetail: selectors.getRenewalDetail(state),
  isRenewalDetailExists: selectors.getIsRenewalDetailExists(state),
  criteriaQuestionsOptions: selectors.getCriteriaQuestionsOptions(state),
  pricesQuestionsOptions: selectors.getPricesQuestionsOptions(state),
  formAttributes: getFormValues(props.form)(state),
  isLoading: loaderSelectors.getLoadingStatus(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchRenewalDetailScreenDataDispatch: actions.fetchRenewalDetailScreenData,
    fetchOnChangeCategoryDataDispatch: categoryActions.fetchOnChangeCategoryData,
    setRenewalDetailDispatch: actions.setRenewalDetail,
    setRenewalDetailFormOptionsDispatch: actions.setRenewalDetailFormOptions,
    createFeedbackDispatch: feedbackActions.createFeedback,
    changeLoaderStatusDispatch: loaderActions.changeLoaderStatus,
  }, dispatsh)
);

export default reduxForm({ form: 'feedbackForm' })(connect(mapStateToProps, mapDispatchToProps)(Vote));
