import React, { Fragment, Component } from 'react';
import { object, string, bool, array, func } from 'prop-types';
import Loader from 'components/Loader';
import SignUpModalLink from 'components/SignUpModalLink';
import { CriteriaBlock, TogglePersonalFilter } from './components';
import RadioButtonField from 'components/RadioButtonField';
import SelectAndAutocompleteInput from 'components/SelectAndAutocompleteInput';
import RatingFormData from 'components/RatingFormData';
import { InsightsSection } from 'screens/Vote/components/Form/components';
import { Field } from 'redux-form';
import { toastr } from 'lib/helpers';
import { defaultMessages } from 'locales/default';
import I18n from 'components/I18n';

import { TYPES_CONVERTER } from './constants';
import { LAST_BUY_OPTIONS, CHECKED_PLACES_OPTIONS } from 'core/renewalDetail/constants';
import { USING_PROVIDER_OPTIONS, BUYING_TYPE_OPTIONS, PLEASE_NAME_TEXT } from 'components/ProviderDetailsFormData/constants';

import Close from 'images/svg/close-dark.svg';
import cx from 'classnames';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

const propTypes = {
  criteriaQuestionsOptions: object.isRequired,
  // countriesSelectOptions: array.isRequired,
  providersOptions: array.isRequired,
  category: object.isRequired,
  toggle: func.isRequired,
  currentUser: object.isRequired,
  form: string.isRequired,
  isPopoverOpen: bool.isRequired,
  personalizeFilterStatus: bool,
};

const STEPS = [
  'provider',
  'standart',
  'criteria',
  'rating',
  'smart-reminder',
];

class PersonalizeYourResults extends Component {

  state = {
    windowSize: window.innerWidth,
    hasProvider: null,
    currentStep: 0,
  }

  componentDidMount() {
    this.setWindowSize();
    window.addEventListener('resize', this.setWindowSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setWindowSize);
  };

  setWindowSize = () => {
    this.setState({ windowSize: window.innerWidth });
  }

  toggleFilter = (questions) => {
    const {
      props: { toggle, createCategoryPersonalizeFilterDispatch, fetchCompaniesListCriteriaQuestionsFilterDispatch, currentUser, category },
    } = this;

    const categoryId = category.id;
    const criteriaQuestions = JSON.stringify((questions || {}).criteriaQuestions);

    if (currentUser.id) {
      createCategoryPersonalizeFilterDispatch({ categoryId, criteriaQuestions });
    } else {
      fetchCompaniesListCriteriaQuestionsFilterDispatch({ criteriaQuestions });
    }

    toggle('isPopoverOpen');
  }

  handleResetCategoryPersonalizeFilter = () => {
    this.toggleFilter({});
  };

  handleCreateCategoryPersonalizeFilter = () => {
    const {
      props: { formAttributes, category, setCompanyListFiltersDispatch, fetchRenewalDetailScreenDataDispatch, createFeedbackDispatch },
      state: { hasProvider },
    } = this;

    if (formAttributes.location) {
      const companyListCountryFilter = { value: formAttributes.location.countryCode, label: formAttributes.location.countryCode };
      const companyListCityFilter = { value: formAttributes.location.city, label: formAttributes.location.city };
      const companyListAddressFilter = { value: formAttributes.location.address };

      setCompanyListFiltersDispatch({
        companyListCountryFilter,
        companyListCityFilter,
        companyListAddressFilter
      });
    }

    // if (hasProvider === true) {
    //   const {
    //     lastBuy,
    //     buyingType,
    //     checkedPlaces,
    //     currentProvider,
    //     buyingTypeCompany,
    //     currentProviderRate,
    //     currentProviderRateComment,
    //     buyingTypeCompanyRate,
    //     buyingTypeCompanyRateComment,
    //     criteriaQuestions,
    //     pricesQuestions,
    //     insightsComment,
    //     triggeredAt,
    //     createSmartReminder
    //   } = (formAttributes || {});

    //   const callback = () => {
    //     toastr.success("Success");
    //     fetchRenewalDetailScreenDataDispatch({ categoryId: category.id });
    //   }

    //   createFeedbackDispatch({
    //     callback,
    //     params: {
    //       renewalDetailAttributes: {
    //         lastBuy,
    //         buyingType,
    //         checkedPlaces,
    //         currentProvider,
    //         buyingTypeCompany,
    //         currentProviderRate,
    //         currentProviderRateComment,
    //         buyingTypeCompanyRate,
    //         buyingTypeCompanyRateComment,
    //         criteriaQuestions: JSON.stringify(criteriaQuestions),
    //         pricesQuestions: JSON.stringify(pricesQuestions)
    //       },
    //       triggeredAt,
    //       createSmartReminder,
    //       insightsComment: insightsComment, //&& draftToMarkdown(convertToRaw(insightsComment.getCurrentContent())),
    //       categoryId: category.id
    //     }
    //   });
    // }

    this.toggleFilter(formAttributes);
  }

  render() {
    const syncErrors = {};
    const {
      handleCreateCategoryPersonalizeFilter,
      handleResetCategoryPersonalizeFilter,
      props: { criteriaQuestionsOptions, personalizeFilterStatus, formAttributes, buyingTypeOptions, providersOptions, toggle, currentUser, change, form, dirty, invalid, isPopoverOpen, category },
      state: { windowSize, hasProvider, currentStep }
    } = this;

    const {
      lastBuy,
      buyingType,
      checkedPlaces,
      currentProvider,
      buyingTypeCompany,
      currentProviderRate,
      currentProviderRateComment,
      buyingTypeCompanyRate,
      buyingTypeCompanyRateComment,
    } = (formAttributes || {});

    const categoryName = category && category.root && category.root.enName;

    return (
      <Fragment >
        <div className="filterPopUp">

          <Modal
            isOpen={ isPopoverOpen }
            toggle={ () => toggle('isPopoverOpen') }
            className={
              `filter-modal ${categoryName}`
            }
          >
            <ModalHeader
              toggle={ () => toggle('isPopoverOpen') }
              charCode='x'
            >
              Personalize your results
            </ModalHeader>


            <ModalBody>

{/*
              <div className="modal-switcher">
                <span className="switcher-title">
                  Save My Preferences
                </span>

                <TogglePersonalFilter />
              </div>
*/}
              <div>
                <CriteriaBlock
                  { ...{ criteriaQuestionsOptions, form } }
                />
              </div>

            </ModalBody>

            <ModalFooter>
              <Button
                className="btn next"
                onClick={ () => {
                  handleCreateCategoryPersonalizeFilter();
                } }
              >
                  Apply
              </Button>
            </ModalFooter>

            {/*<button*/}
            {/*className="close"*/}
            {/*onClick={ () => toggle('isPopoverOpen') }*/}
            {/*>*/}
            {/*<img src={ Close } alt="Close" />*/}
            {/*</button>*/}



          </Modal>
        </div>
      </Fragment>
    );
  };
};

PersonalizeYourResults.propTypes = propTypes;

export default PersonalizeYourResults;
