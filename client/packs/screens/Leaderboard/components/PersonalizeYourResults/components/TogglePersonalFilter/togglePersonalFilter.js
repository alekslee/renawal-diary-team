import React, { Component } from 'react';
import Switch from 'react-switch';
import cx from 'classnames';
import { bool, func } from 'prop-types';

const propTypes = {
  personalizeFilterStatus: bool,
  changePersonalizeFilterStatusDispatch: func.isRequired,
};

class TogglePersonalFilter extends Component {
  handleChange = checked => {
    const { changePersonalizeFilterStatusDispatch } = this.props;

    changePersonalizeFilterStatusDispatch({ personalizeFilterStatus: checked });
  };

  render() {
    const {
      handleChange,
      props: { personalizeFilterStatus }
    } = this;

    return (
      <div className="toggle-personal">
        <span>
          { personalizeFilterStatus ? 'Saved' : 'Save' }
        </span>

        <span>
          <Switch
            onChange={ handleChange }
            checked={ personalizeFilterStatus || false }
            className={ cx('react-switch', { actived: personalizeFilterStatus }) }
            height={ 14 }
            width={ 34 }
          />
        </span>
      </div>
    );
  }
}
TogglePersonalFilter.propTypes = propTypes;

export default TogglePersonalFilter;
