import TogglePersonalFilter from './togglePersonalFilter';
import { bindActionCreators } from 'redux';
import { actions, selectors } from 'core/company';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
  personalizeFilterStatus: selectors.getPersonalizeFilterStatus(state),
});

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    changePersonalizeFilterStatusDispatch: actions.changePersonalizeFilterStatus,
  }, dispatch)
);

export default  connect(mapStateToProps, mapDispatchToProps)(TogglePersonalFilter);
