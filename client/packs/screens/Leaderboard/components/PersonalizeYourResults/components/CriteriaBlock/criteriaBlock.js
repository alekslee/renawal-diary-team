import React, { Fragment, Component } from 'react';
import { object, string, bool, array, number, func } from 'prop-types';

import { TYPES_CONVERTER } from '../../constants';

import cx from 'classnames';

const propTypes = {
  criteriaQuestionsOptions: object.isRequired,
  splitParts: bool,
  part: number,
  // countriesSelectOptions: array.isRequired,
  form: string.isRequired,
};

class CriteriaBlock extends Component {

  state = {

  }

  render() {
    const {
      props: { criteriaQuestionsOptions, form, namePrefix,
        splitParts, part
      },
      state: { windowSize }
    } = this;

    let questions = [];
    let question, ReactElement;
    let QuestionsOptions = criteriaQuestionsOptions;

    if (!QuestionsOptions) {
      return null;
    }
    // if (Object.keys(QuestionsOptions).length === 0) return <Fragment />;

    if (splitParts) {
      let length = Object.keys(QuestionsOptions).length
      let keys = [
        Object.keys(QuestionsOptions).slice(0, (length / 2) - 1),
        Object.keys(QuestionsOptions).slice((length / 2) - 1, length)
      ][part]

      QuestionsOptions = (keys || []).reduce((memo, key) => {
        memo[key] = QuestionsOptions[key]
        return memo;
      }, {});
    }

    for (let key in QuestionsOptions) {
      if (key !== 'subheader') {
        question = QuestionsOptions[key];
        ReactElement = TYPES_CONVERTER[question.type];
        if (ReactElement) {
          questions.push(<ReactElement
            { ...{ key,
              name: key, question,
              namePrefix: namePrefix || '',
              form: form,
              disabled: false
            } }
          />);
        }
      }
    };

    return (
      <Fragment >
        <div className="top-part">

          { questions }
        </div>
      </Fragment>
    );
  };
};

CriteriaBlock.propTypes = propTypes;

export default CriteriaBlock;
