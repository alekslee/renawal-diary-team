import React from 'react';
import { Field } from 'redux-form';
import { object, string, bool } from 'prop-types';

import PriceField from 'components/PriceField';

const propTypes = {
  question: object.isRequired,
  name: string.isRequired,
  disabled: bool,
  namePrefix: string.isRequired
};

const PriceCriteria = ({ question, disabled, name, namePrefix }) => {
  return (
    <div className='top-part-item'>
      <h5>{ question.title }</h5>
      <div className='grid-3'>
        <Field
          name={ `${namePrefix}[${name}]` }
          currencySymbol={ question.currencySymbol }
          disabled={ disabled }
          component={ PriceField }
        />
      </div>
    </div>
  );
};
PriceCriteria.propTypes = propTypes;

export default PriceCriteria;
