import React from 'react';
import { Field } from 'redux-form';
import { object, string, bool } from 'prop-types';

import RadioButtonField from 'components/RadioButtonField';

const propTypes = {
  question: object.isRequired,
  name: string.isRequired,
  disabled: bool,
  namePrefix: string.isRequired
};

const RadioCriteria = ({ question, name, disabled, namePrefix }) => {
  return (
    <div className='top-part-item'>
      <h3>{ question.title }</h3>
      <div className={ `radio-container rate-radio ${question.className}` }>
        <Field
          name={ `${namePrefix}[${name}]` }
          options={ question.options }
          disabled={ disabled }
          component={ RadioButtonField }
        />
      </div>
    </div>
  );
};
RadioCriteria.propTypes = propTypes;

export default RadioCriteria;
