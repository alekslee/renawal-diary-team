import React, { Fragment, Component } from 'react';
import { object, string, bool, func } from 'prop-types';

import SelectCriteria from '../SelectCriteria';
import Loader from 'components/Loader';

const propTypes = {
  question: object.isRequired,
  name: string.isRequired,
  disabled: bool,
  namePrefix: string.isRequired,
  make: string,
  fetchRenewalDetailCriteriasQuestionsCarTrimOptionsDispatch: func.isRequired,
  fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptionsDispatch: func.isRequired
};

class MotorBikeModelCriteria extends Component {
  state = {
    inProcess: false
  };

  onChangeHandler = value => {

    const {
      fetchRenewalDetailCriteriasQuestionsCarTrimOptionsDispatch,
      fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptionsDispatch,
      make
    } = this.props;

    const callback = () => this.setState({ inProcess: false });

    this.setState({ inProcess: true });
    fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptionsDispatch({
      make, model: value, callback, errorCallback: callback
    });
    fetchRenewalDetailCriteriasQuestionsCarTrimOptionsDispatch({
      make, model: value, callback, errorCallback: callback
    });
  };

  render() {
    const {
      onChangeHandler,
      state: { inProcess },
      props: { question, name, disabled, namePrefix }
    } = this;

    return (
      <Fragment>
        <SelectCriteria { ...{ question, name, disabled, namePrefix } } onChange={ onChangeHandler } />
      </Fragment>
    );
  }
};
MotorBikeModelCriteria.propTypes = propTypes;

export default MotorBikeModelCriteria;
