import React, { Fragment } from 'react';
import { Field } from 'redux-form';
import { object, string, bool, func } from 'prop-types';

import SelectField from 'components/SelectField';

const propTypes = {
  question: object.isRequired,
  name: string.isRequired,
  disabled: bool,
  onChange: func,
  namePrefix: string.isRequired
};

const defaultProps = {
  onChange: () => {}
};

const SelectCriteria = ({ question, disabled, name, namePrefix, onChange }) => {
  if (Object.keys(question.options).length === 0) return <Fragment />;

  return (
    <div className='top-part-item'>
      <h3>{ question.title }</h3>
      <div className={ question.className } >
        <Field
          name={ `${namePrefix}[${name}]` }
          className='reminder-select'
          classNamePrefix='Select'
          disabled={ disabled }
          options={ question.options }
          component={ SelectField }
          onChange={ onChange }
        />
      </div>
    </div>
  );
};
SelectCriteria.propTypes = propTypes;
SelectCriteria.defaultProps = defaultProps;

export default SelectCriteria;
