import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formValueSelector } from 'redux-form';
import SavingMetrePriceCriteria from './savingMetrePriceCriteria';

import { selectors, actions } from 'core/category';

const mapStateToProps = (state, props) => {
  const pricesQuestions = formValueSelector(props.form)(state, 'pricesQuestions');
  return {
    havingSavingMetre: pricesQuestions && pricesQuestions.having_saving_metre
  };
};

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(SavingMetrePriceCriteria);
