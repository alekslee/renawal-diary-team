import React from 'react';
import { Field } from 'redux-form';
import { object, bool, string } from 'prop-types';

import FieldWithErrors from 'components/FieldWithErrors';

const propTypes = {
  question: object.isRequired,
  name: string.isRequired,
  disabled: bool,
  namePrefix: string.isRequired
};

const StringCriteria = ({ question, name, namePrefix, disabled }) => {
  return (
    <div className='top-part-item'>
      <h5>{ question.title }</h5>
      <div className='grid-2'>
        <Field
          name={ `${namePrefix}[${name}]` }
          type='text'
          className='reminder-input'
          disabled={ disabled }
          component={ FieldWithErrors }
        />
      </div>
    </div>
  );
};
StringCriteria.propTypes = propTypes;

export default StringCriteria;
