import React from 'react';
import { Field } from 'redux-form';
import { object, string, func, bool } from 'prop-types';

import AutocompleteField from 'components/AutocompleteField';

const propTypes = {
  question: object.isRequired,
  name: string.isRequired,
  disabled: bool,
  namePrefix: string.isRequired
};

const AutocompleteCriteria = ({ question, name, disabled, namePrefix }) => {
  return (
    <div className='top-part-item'>
      <h3>{ question.title }</h3>
      <div className={ question.className } >
        <Field
          name={ `${namePrefix}[${name}]` }
          className='reminder-input'
          disabled={ disabled }
          items={ question.options }
          component={ AutocompleteField }
        />
      </div>
    </div>
  );
};
AutocompleteCriteria.propTypes = propTypes;

export default AutocompleteCriteria;
