import React, { Fragment } from 'react';
import { object, string, bool } from 'prop-types';

import SelectCriteria from '../SelectCriteria';

const propTypes = {
  question: object.isRequired,
  typeOfCoverRequired: string.isRequired,
  name: string.isRequired,
  namePrefix: string.isRequired,
  disabled: bool
};

const ChildrenCountCriteria = ({ question, typeOfCoverRequired, name, namePrefix, disabled }) => {
  if (typeOfCoverRequired !== 'family') return <Fragment />;

  return (
    <SelectCriteria { ...{ question, name, disabled, namePrefix } } />
  );
};
ChildrenCountCriteria.propTypes = propTypes;

export default ChildrenCountCriteria;
