import React, { Component } from 'react';
import { Field } from 'redux-form';
import { object, string, bool, func } from 'prop-types';

import LocationField from 'components/LocationField';
import Loader from 'components/Loader';

const propTypes = {
  question: object.isRequired,
  name: string.isRequired,
  namePrefix: string.isRequired,
  category: object.isRequired,
  disabled: bool,
  fetchRenewalDetailFormAutocompleteOptionsDispatch: func.isRequired
};

class LocationCriteria extends Component {
  state = {
    inProcess: false
  };

  changeLocationHandler = (e, { countryCode }, prevValue) => {
    const { category: { depth, code }, fetchRenewalDetailFormAutocompleteOptionsDispatch } = this.props;

    if (countryCode && countryCode !== prevValue.countryCode) {
      const callback = () => this.setState({ inProcess: false });

      this.setState({ inProcess: true });
      fetchRenewalDetailFormAutocompleteOptionsDispatch({
        depth, code, countryCode, callback, errorCallback: callback
      });
    }
  };

  render() {
    const {
      changeLocationHandler,
      state: { inProcess },
      props: { question, name, disabled, namePrefix }
    } = this;

    return (
      <div className='top-part-item'>
        <h3>{ question.title }</h3>
        <div className={ question.className }>
          <Field
            name={ `${namePrefix}[${name}]` }
            options={ question.options }
            component={ LocationField }
            disabled={ disabled }
            placeholder='Please enter City, town'
            onChange={ changeLocationHandler }
          />
        </div>
      </div>
    );
  };
};
LocationCriteria.propTypes = propTypes;

export default LocationCriteria;
