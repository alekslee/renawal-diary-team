import React from 'react';
import { Field } from 'redux-form';
import { object, string, bool } from 'prop-types';

import PriceField from 'components/PriceField';
import SelectField from 'components/SelectField';

const propTypes = {
  question: object.isRequired,
  name: string.isRequired,
  disabled: bool,
  namePrefix: string.isRequired
};

const PricePerPeriodCriteria = ({ question, disabled, name, namePrefix }) => {
  return (
    <div className='top-part-item'>
      <h5>{ question.title }</h5>
      <div className='grid-3'>
        <Field
          name={ `${namePrefix}[${name}][value]` }
          currencySymbol={ question.currencySymbol }
          disabled={ disabled }
          component={ PriceField }
        />
        <Field
          name={ `${namePrefix}[${name}][period]` }
          className='reminder-select'
          classNamePrefix='Select'
          disabled={ disabled }
          options={ question.periodOptions }
          component={ SelectField }
        />
      </div>
    </div>
  );
};
PricePerPeriodCriteria.propTypes = propTypes;

export default PricePerPeriodCriteria;
