import React, { Component } from 'react';
import { bool, func, object } from 'prop-types';
import CopyOrShare from 'components/CopyOrShare';
import SocialShareButtons from 'components/SocialShareButtons';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';

import Logo from 'images/calendar.svg';

const propTypes = {
  isModalOpen: bool.isRequired,
  toggle: func.isRequired,
  category: object.isRequired
};


const InviteModal = ({ isModalOpen, toggle, category }) => (
  <Modal
    isOpen={ isModalOpen }
    toggle={ () => toggle('isModalOpen') }
    className={ `invite-modal leaderboard-modal ${ category.root.enName }` }
  >
    <ModalHeader toggle={ () => toggle('isModalOpen') }>
      Invite new members
    </ModalHeader>
    <ModalBody>
      <img src={ Logo } alt='Logo' />
      <p className='logo-text'>Renewal Diary</p>
      <p className='category-text'>The { category.name } Group</p>
      <p className='category-text'>The more members the smarter we get</p>
      <p className='members-text'>{ category.usersCount } members</p>
      <div className='copy-white'>
        <CopyOrShare modalTooltip />
      </div>
    </ModalBody>
    <ModalFooter>
      <div className='or-block'>
        <div className='dash' />
        <span>or</span>
        <div className='dash' />
      </div>
      <SocialShareButtons />
    </ModalFooter>
  </Modal>
);

InviteModal.propTypes = propTypes;

export default InviteModal;
