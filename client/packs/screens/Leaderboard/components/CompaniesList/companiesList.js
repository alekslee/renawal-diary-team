import React, { Fragment, Component } from 'react';
import { array, number, bool, func } from 'prop-types';

import { CompanyItemMobile, CompanyItem, Pagination, TableHeader } from './components';
import { COMPANIES_PER_PAGE, FIRST_COMPANIES_PER_PAGE } from 'core/company/constants';
import InfiniteScroll from 'react-infinite-scroll-component';
import BlockUi from 'react-block-ui';

const propTypes = {
  companies: array.isRequired,
  pagesCount: number.isRequired,
  isShortCompaniesList: bool.isRequired,
  companiesListPage: number.isRequired,
  fetchCompaniesListDispatch: func.isRequired,
  setCompanyIsShortCompaniesListDispatch: func.isRequired,
  setCompanyCompaniesListPageDispatch: func.isRequired,
  filterChanged: bool,
  setFilterStatusDispatch: func.isRequired,
  isLoading: bool.isRequired,
};

class CompaniesList extends Component {
  state = {
    windowSize: window.innerWidth,
    companiesList: [],
    hasMorePage: true,
  };

  componentDidMount() {
    this.setWindowSize();
    const { windowSize } = this.state;

    if (windowSize <= 768) {
      this.showMoreHandler()
    }

    window.addEventListener('resize', this.setWindowSize);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const {
      loadPage,
      state: { companiesList, windowSize },
      props: { companiesListPage } } = this;
    const { companies } = nextProps;
    const hasMorePage = companies.length > 0;

    if (nextProps.filterChanged && !nextProps.isLoading) {
      this.setState({ companiesList: [], hasMorePage });
      loadPage(1, COMPANIES_PER_PAGE);
      return ;
    }

    if (windowSize <= 768 && hasMorePage) {
      if (companiesListPage === 1) {

        this.setCompaniesList(companies, { hasMorePage });
      } else {
        const cIDs = companiesList.map(c => c.id);
        const excludedCompanies = companies.filter(el => !cIDs.includes(el.id));

        if (excludedCompanies.length > 0) {
          this.setCompaniesList(companiesList.concat(excludedCompanies), { hasMorePage });
        }
      }
    }
  };

  componentWillUnmount() {
    window.removeEventListener('resize', this.setWindowSize);
  }

  setCompaniesList = (companies, { hasMorePage }) => {
    this.setState({ companiesList: companies, hasMorePage });
  };

  setWindowSize = () => {
    this.setState({ windowSize: window.innerWidth });
  }

  loadPage = (nextPage, perPage) => {
    const {
      props: { fetchCompaniesListDispatch, setCompanyCompaniesListPageDispatch, changeLoaderStatusDispatch },
      state: { windowSize },
    } = this;

    const callback = () => changeLoaderStatusDispatch({ status: false, type: 'company' });
    setCompanyCompaniesListPageDispatch({ companiesListPage: nextPage });
    fetchCompaniesListDispatch({ page: nextPage, perPage, callback, errorCallback: callback });
  };

  showMoreHandler = () => {
    const {
      loadPage,
      props: { setCompanyIsShortCompaniesListDispatch }
    } = this;

    setCompanyIsShortCompaniesListDispatch({ isShortCompaniesList: false });
    loadPage(1, COMPANIES_PER_PAGE);
  };

  showLessHandler = () => {
    const {
      loadPage,
      props: { setCompanyIsShortCompaniesListDispatch }
    } = this;

    setCompanyIsShortCompaniesListDispatch({ isShortCompaniesList: true });
    loadPage(1, FIRST_COMPANIES_PER_PAGE);
  };

  loadMorePage = () => {
    const {
      loadPage,
      state: { companiesList, hasMorePage },
      props: { companies, filterChanged, setFilterStatusDispatch, companiesListPage }
    } = this;

    if (hasMorePage) {
      loadPage(companiesListPage + 1, COMPANIES_PER_PAGE);
    }

    filterChanged && setFilterStatusDispatch({ filterChanged: false });
  };

  render() {
    const {
      loadPage,
      showMoreHandler,
      showLessHandler,
      loadMorePage,
      state: { windowSize, companiesList, hasMorePage },
      props: { companies, pagesCount, isShortCompaniesList, companiesListPage, isLoading }
    } = this;

    const CompanyItemComponent =  windowSize > 767 ? CompanyItem : CompanyItemMobile;
    const ListOfCompanies = windowSize > 767 ? companies : companiesList;

    const DefaultCompaniesTable = (
      <table className='leaderboard-table'>
        {/*{ windowSize > 767 &&*/}
        {/*<TableHeader /> }*/}
        <tbody>
          { ListOfCompanies.map(company => (
            <CompanyItemComponent key={ company.id } { ...{ company } } />
          ))}
        </tbody>
      </table>
    );

    const infinityScrollWrapper = (
      <InfiniteScroll
        dataLength={ companiesList.length }
        next={ loadMorePage }
        hasMore={ hasMorePage }
      >
        { DefaultCompaniesTable }
      </InfiniteScroll>
    );

    const CompaniesTable = windowSize > 767 ? DefaultCompaniesTable : infinityScrollWrapper;

    return (
      <Fragment>
        <BlockUi tag='div' blocking={ isLoading }>
          {
            ListOfCompanies.length > 0 ?

              CompaniesTable :

              <div className="empty-state-leaderboard">
                <div className="empty-state-image" />
                <h3>OOPS!</h3>
                <p>We have not found any results in your region…</p>
              </div>
          }
          {
            companies.length > 0 && windowSize > 767 && (
              isShortCompaniesList ?
                <div className='see-more' >
                  <div className="show-more-wrap" onClick={ showMoreHandler }>
                    <span>
                      Show more businesses
                      {/*<span className="show-more-icon"/>*/}
                    </span>
                  </div>
                </div>
                :
                <Fragment>
                  <Pagination { ...{ pagesCount, page: companiesListPage } } onPageChanged={ loadPage } />
                  <div className='see-more' >
                    <div className="show-more-wrap" onClick={ showLessHandler }>
                      <span>
                        Show less businesses
                        {/*<span className="show-less-icon" />*/}
                      </span>
                    </div>
                  </div>
                </Fragment>
            )
          }
        </BlockUi>
      </Fragment>
    );
  }
};
CompaniesList.propTypes = propTypes;

export default CompaniesList;
