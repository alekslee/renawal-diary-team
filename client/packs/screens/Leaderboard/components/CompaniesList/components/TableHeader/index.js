import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TableHeader from './tableHeader';

import { selectors, actions } from 'core/company';

const mapStateToProps = state => ({
  orderByColumn: selectors.getOrderByColumn(state)
});

export default connect(mapStateToProps, null)(TableHeader);
