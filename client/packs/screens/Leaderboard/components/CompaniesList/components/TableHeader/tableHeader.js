import React, { Component } from 'react';
import { object, func } from 'prop-types';
import { SATISFECTION_COLUMN, INSIGHTS_COLUMN, ASC, DESC } from 'core/company/constants';

import SortDown from 'images/svg/sort-down-icon.svg';
import SortUp from 'images/svg/sort-up-icon.svg';

import Loader from 'components/Loader';

const propTypes = {
  orderByColumn: object.isRequired,
};

const contextTypes = {
  updateCompaniesOrderingDispatch: func.isRequired,
  resetCompanyOrderByColumnDispatch: func.isRequired,
}

class TableHeader extends Component {
  state = {
  }

  componentWillUnmount() {
    const { resetCompanyOrderByColumnDispatch } = this.context;
    resetCompanyOrderByColumnDispatch();
  };

  changeOrdering = ordering => {
    const { updateCompaniesOrderingDispatch } = this.context;

    const callback = () => {}
    updateCompaniesOrderingDispatch({ ordering, callback, errorCallback: callback });
  };

  render() {
    const {
      state: { },
      props: { orderByColumn, changeOrdering }
    } = this;

    return (
      <thead>
        <tr>
          <td>
            Best Value
          </td>
          <td>Business</td>
          <td>
            Customer Satisfaction
            {
              !(orderByColumn.column === SATISFECTION_COLUMN && orderByColumn.direction === ASC) ?
                <span
                  className='table-arrow'
                  onClick={ () => changeOrdering({ column: SATISFECTION_COLUMN, direction: ASC }) }
                >
                  <img src={ SortUp } alt='sort up' />
                </span> :
                <span
                  className='table-arrow'
                  onClick={ () => changeOrdering({ column: SATISFECTION_COLUMN, direction: DESC }) }
                >
                  <img src={ SortDown } alt='sort down' />
                </span>
            }
          </td>
          <td>% Share of Votes</td>
        </tr>
      </thead>
    );
  }
};
TableHeader.propTypes = propTypes;
TableHeader.contextTypes = contextTypes;

export default TableHeader;
