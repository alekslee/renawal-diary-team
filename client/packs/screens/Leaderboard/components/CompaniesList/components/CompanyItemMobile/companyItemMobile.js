import React from 'react';
import { object, number } from 'prop-types';

import RatingBadIcon from 'images/icons/RatingBadIcon';
import RatingNormalIcon from 'images/icons/RatingNormalIcon';
import RatingGoodIcon from 'images/icons/RatingGoodIcon';
import ReactStars from "react-stars";

const propTypes = {
  company: object.isRequired
};

const CompanyItem = ({ company }) => {
  let RatingIcon;
  if (company.satisfaction < 7) {
    RatingIcon = RatingBadIcon;
  } else if (company.satisfaction < 9) {
    RatingIcon = RatingNormalIcon;
  } else {
    RatingIcon = RatingGoodIcon;
  };

  return (
    <tr>
      <td>
        <div className="mobile-table-pill" >
          <div className="mobile-table-pill-head">
            <span>
              <span className="circle" >
                { company.rank }
              </span>
            </span>

            <span>
              {/*{ company.satisfaction || 0 }/10*/}
              {/*<RatingIcon />*/}
              {company.members || 0} purchase
            </span>
          </div>

          <div className="mobile-table-pill-body">
            <div className="img-container">
              <img src={ company.avatarUrl } alt='company avatar' />
            </div>
            <p>
              { company.name }
              { company.direct && ' (Direct)' }
            </p>

            <div className="table-rating-starts">
              {/*<span className="rating-count">4</span>*/}
              <ReactStars
                className="rate-icon-parent"
                value={ company.rating || 0 }
                count={ 5 }
                size={ 16 }
                color1="#e3e5ea"
                color2="#efce4a"
                edit={ false }
              />
              <span className="insights">({company.insights} insights)</span>
            </div>

            <a
              href={ company.url }
              target='_blank'
              rel='noopener noreferrer'
              className="btn btn-success"
            >
              Visit Website
            </a>

          </div>

          {/*<div*/}
          {/*  className="mobile-table-pill-footer"*/}
          {/*  style={ {*/}
          {/*    display: 'flex'*/}
          {/*  } }*/}
          {/*>*/}
          {/*  <span>*/}
          {/*    { company.members }%*/}
          {/*  </span>*/}

          {/*  <div className="scale">*/}
          {/*    /!*<div className="fill"*!/*/}
          {/*    /!*style={{*!/*/}
          {/*    /!*width: `${company.members * 0.7}%`*!/*/}
          {/*    /!*}}> </div>*!/*/}
          {/*    <div*/}
          {/*      className="fill"*/}
          {/*      style={ {*/}
          {/*        width: `${company.members}%`*/}
          {/*      } }*/}
          {/*    />*/}
          {/*  </div>*/}

          {/*  /!*<span>{company.insights} votes</span>*!/*/}
          {/*</div>*/}

        </div>
      </td>
    </tr>
  );
};
CompanyItem.propTypes = propTypes;

export default CompanyItem;
