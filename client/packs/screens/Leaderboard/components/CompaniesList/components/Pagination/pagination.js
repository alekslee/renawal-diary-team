import React from 'react';
import { func, number } from 'prop-types';
import classNames from 'classnames';
import PaginationArrow from 'images/icons/PaginationArrow';

import { PAGES_BEFORE_DOTS } from './constants';

const propTypes = {
  onPageChanged: func.isRequired,
  pagesCount: number.isRequired,
  page: number.isRequired
};

const Pagination = ({ pagesCount, page, onPageChanged }) => {
  let pages = [];
  for (let i = 1; i <= pagesCount; i++) {
    if (i === 1 || Math.abs(i - page) <= PAGES_BEFORE_DOTS || i === pagesCount) {
      pages.push(
        <span
          key={ i }
          className={ classNames(
            'page-number', { 'active': page === i }
          ) }
          onClick={ () => onPageChanged(i) }
        >
          { i }
        </span>
      );
    } else if (pages[pages.length - 1].key !== 'dotsBefore' && pages[pages.length - 1].key !== 'dotsAfter') {
      pages.push(
        <span
          className='page-dots'
          key={ (i - page) < 0 ? 'dotsBefore' : 'dotsAfter' }
        >
          ...
        </span>
      );
    }
  };

  return (
    <div className='pagination'>
      <span className={ classNames('pagination-arrow left', {'disable': page === 1}) } onClick={ () => page !== 1 && onPageChanged(page - 1) } >
        <PaginationArrow />
      </span>
      { pages }
      <span className={ classNames('pagination-arrow right', {'disable': page === pagesCount}) } onClick={ () => page !== pagesCount && onPageChanged(page + 1) } >
        <PaginationArrow />
      </span>
    </div>
  );
};
Pagination.propTypes = propTypes;

export default Pagination;
