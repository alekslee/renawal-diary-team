import React from 'react';
import { object, number } from 'prop-types';
import { CircularProgressbarWithChildren } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

import RatingBadIcon from 'images/icons/RatingBadIcon';
import RatingNormalIcon from 'images/icons/RatingNormalIcon';
import RatingGoodIcon from 'images/icons/RatingGoodIcon';
import ReactStars from 'react-stars';

const propTypes = {
  company: object.isRequired
};

const CompanyItem = ({ company }) => {
  let RatingIcon;
  if (company.satisfaction < 7) {
    RatingIcon = RatingBadIcon;
  } else if (company.satisfaction < 9) {
    RatingIcon = RatingNormalIcon;
  } else {
    RatingIcon = RatingGoodIcon;
  };

  return (
    <tr>
      <td className="rank-cell">
        <div className="circle">
          { company.rank }
        </div>
      </td>
      <td className='name-cell'>
        <div className="table-logo">
          <img src={ company.avatarUrl } alt='company avatar' />
        </div>
        <div className='business-name'>
          <p>{ company.name } { company.direct && ' (Direct)' }</p>
          <div className="table-rating-starts">
            {/*<span className="rating-count">4</span>*/}
            <ReactStars
              className="rate-icon-parent"
              value={ company.rating || 0 }
              count={ 5 }
              size={ 16 }
              color1="#e3e5ea"
              color2="#efce4a"
              edit={ false }
            />
            <span className="insights">({company.insights} insights)</span>
          </div>
        </div>
      </td>
      {/*<td className='rating-cell'>*/}
      {/*  <div className='rating-flex'>*/}
      {/*    {`${ company.satisfaction || 0 } / 10`}*/}
      {/*    <RatingIcon />*/}
      {/*  </div>*/}
      {/*</td>*/}
      <td>
        <div className="table-rating">
          <CircularProgressbarWithChildren
            value={ company.members || 0 }
            styles={ {
              path: {
                transform: 'rotate(0.5turn)',
                transformOrigin: 'center center',
              },
              trail: {
                transform: 'rotate(0.5turn)',
                transformOrigin: 'center center',
              },
              text: {
                fontSize: '22px',
              },
            } }
          >
            <span>{ `${ company.members ? `${company.members}%` : 0 }` }</span>
          </CircularProgressbarWithChildren>
          <div className="votes-number">
            {company.members} Bought here
          </div>
        </div>

      </td>
      <td>
        {company.url &&
        <a
          href={ company.url }
          target='_blank'
          rel='noopener noreferrer'
          className="table-link"
        >
          Visit Website
        </a>
        }
      </td>
    </tr>
  );
};
CompanyItem.propTypes = propTypes;

export default CompanyItem;
