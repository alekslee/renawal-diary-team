import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CompaniesList from './companiesList';

import { selectors, actions } from 'core/company';
import { selectors as loaderSelectors, actions as loaderActions } from 'core/additional';

const mapStateToProps = state => ({
  pagesCount: selectors.getPagesCount(state),
  companies: selectors.getCompanies(state),
  isShortCompaniesList: selectors.getIsShortCompaniesList(state),
  companiesListPage: selectors.getCompaniesListPage(state),
  filterChanged: selectors.getFilterStatus(state),
  isLoading: loaderSelectors.getLoadingStatus(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    updateCompaniesOrderingDispatch: actions.updateCompaniesOrdering,
    fetchCompaniesListDispatch: actions.fetchCompaniesList,
    setCompanyIsShortCompaniesListDispatch: actions.setCompanyIsShortCompaniesList,
    setCompanyCompaniesListPageDispatch: actions.setCompanyCompaniesListPage,
    setPerPageCompaniesListDispatch: actions.setPerPageCompaniesList,
    setFilterStatusDispatch: actions.setFilterStatus,
    changeLoaderStatusDispatch: loaderActions.changeLoaderStatus,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(CompaniesList);
