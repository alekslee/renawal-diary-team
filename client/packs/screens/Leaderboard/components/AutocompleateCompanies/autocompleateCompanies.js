import React, { Component, Fragment } from 'react';
import { object, string, func, array } from 'prop-types';

import SearchIcon from 'images/icons/Search';
import Autocomplete from 'components/Autocomplete';

const propTypes = {
  formAttributes: object,
  category: object,
  fetchCompanyListKeyWordFilterDispatch: func.isRequired,
  fetchCompaniesListForAutocompleateDispatch: func.isRequired,
  autocompleateCompanies: array,
};

class AutocompleateCompanies extends Component {

  state = {
    inputValue: {},
  }

  handleCompanyListKeyWordFilter = (selectKeyWord=null) => {
    let { keyWord } = this.props.formAttributes || {};
    keyWord = selectKeyWord || (keyWord && keyWord.label);
    const companyListKeyWordFilter = { keyWord };

    this.setState({ isLoading: true, inputValue: { label: keyWord } });
    const callback = () => this.setState({ isLoading: false });
    this.props.fetchCompanyListKeyWordFilterDispatch({ companyListKeyWordFilter, callback, errorCallback: callback });
  }

  changeInputHandler = inputValue => {
    if (inputValue && !inputValue.label) inputValue = {};

    this.props.fetchCompaniesListForAutocompleateDispatch({ categoryId: this.props.category.id, keyWord: inputValue.label });
    this.setState({ inputValue });
  };

  selectInputHandler = ({ label }) => {
    this.handleCompanyListKeyWordFilter(label);
  }

  render() {
    const {
      handleCompanyListKeyWordFilter,
      changeInputHandler,
      selectInputHandler,
      props: {
        autocompleateCompanies,
      },
      state: { inputValue  },
    } = this;

    const items = autocompleateCompanies.map(({ id, name }) => (
      { label: name, value: id }
    ));

    return (
      <Fragment>

        <button
          className="search-icon"
          onClick={ () => handleCompanyListKeyWordFilter() }
        >
          <SearchIcon />
        </button>

        <Autocomplete
          items={ items }
          className="reminder-input"
          placeholder="Search businesses…"
          value={ inputValue.label }
          onChange={ changeInputHandler }
          onSelect={ selectInputHandler }
        />

      </Fragment>
    );
  }
}

AutocompleateCompanies.propTypes = propTypes;

export default AutocompleateCompanies;


