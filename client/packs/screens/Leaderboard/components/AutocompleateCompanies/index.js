

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AutocompleateCompanies from './autocompleateCompanies';
import { reduxForm, getFormValues } from 'redux-form';

import { selectors, actions } from 'core/company';
import { selectors as categorySelectors } from 'core/category';

const mapStateToProps = (state, props) => ({
  autocompleateCompanies: selectors.getAutocompleateCompanies(state),
  formAttributes: getFormValues(props.form)(state),
  category: categorySelectors.getCategory(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchCompanyListKeyWordFilterDispatch: actions.fetchCompanyListKeyWordFilter,
    fetchCompaniesListForAutocompleateDispatch: actions.fetchCompaniesListForAutocompleate,
  }, dispatsh)
);

export default reduxForm({ form: 'bussinesFilterForm' })(
  connect(mapStateToProps, mapDispatchToProps)(AutocompleateCompanies)
);