import React, { Fragment } from 'react';
import { object } from 'prop-types';
import GroupIcon from 'images/icons/GroupIcon';

const propTypes = {
  provider: object.isRequired,
  source: object.isRequired,
  category: object.isRequired
};

// Works only without filters
const GroupRecomendationBlock = ({ provider, source, averagePrice, category }) => {
  return (
    <div className='group-recomendations'>
      <div className='recommendation-text'>
        <h4>
          <div className="group-icon">
            <GroupIcon />
          </div>
          Group Insights
        </h4>
        <p>
          Most people shopping around for
          &quot;{ category.name }&quot;
          based on your details have recommended
          &nbsp;
          <a href={ provider.url } target='_blank' rel='noopener noreferrer' >{ provider.name }</a>
          &nbsp;
          as the best provider
          {
            source.id === provider.id ?
              <Fragment>.</Fragment>
              :
              <Fragment>
                &nbsp;
                and
                &nbsp;
                <a href={ source.url } target='_blank' rel='noopener noreferrer' >{ source.name }</a>
                &nbsp;
                as the best source.
              </Fragment>
          }
          {
          //<br />
          //The avarage price is { averagePrice || 0 } €
          }
        </p>
      </div>

      {/*<div className="recommendation-btn-container">
        <button className='btn recommendation-btn'>See full insights</button>
      </div>*/}
    </div>
  );
};
GroupRecomendationBlock.propTypes = propTypes;

export default GroupRecomendationBlock;
