import React, { Fragment, Component } from 'react';
import { object, func } from 'prop-types';
import Loader from 'components/Loader';
import Select from 'react-select';
import { TIME_OPTIONS } from 'core/company/constants';
import { COMPANY_TYPE_OPTIONS } from './constants';

const propTypes = {
  companyListTimeFilter: object,
  companyListTypeFilter: object,
  fetchCompanyListTimeFilterDispatch: func.isRequired,
  fetchCompanyListTypeFilterDispatch: func.isRequired,
};

const contextTypes = {
  updateCompaniesOrderingDispatch: func.isRequired
};

class SelectFiltersSection extends Component {

  state = {
    inProcess: false,
    windowSize: window.innerWidth
  };

  componentDidMount() {
    this.setWindowSize();
    window.addEventListener('resize', this.setWindowSize);
  };

  componentWillUnmount() {
    window.removeEventListener('resize', this.setWindowSize);
  };

  setWindowSize = () => {
    this.setState({ windowSize: window.innerWidth });
  };

  onChangeTimeFilter = companyListTimeFilter => {
    const { fetchCompanyListTimeFilterDispatch } = this.props;

    this.setState({ inProcess: true });
    const callback = () => this.setState({ inProcess: false });
    fetchCompanyListTimeFilterDispatch({ companyListTimeFilter, callback, errorCallback: callback });
  };

  onChangeCompanyTypeFilter = companyListTypeFilter => {
    const { fetchCompanyListTypeFilterDispatch } = this.props;

    this.setState({ inProcess: true });
    const callback = () => this.setState({ inProcess: false });
    fetchCompanyListTypeFilterDispatch({ companyType: companyListTypeFilter, callback, errorCallback: callback });
  };

  FiltersSection = () => {
    const {
      onChangeTimeFilter,
      onChangeCompanyTypeFilter,
      props: { companyListTimeFilter, companyListTypeFilter }
    } = this;

    return (
      <Fragment>
        <div className='filter-item'>
          <div className="label mr-2">
            Best businesses:
          </div>
          <Select
            className='reminder-select  w-100'
            classNamePrefix='Select'
            options={ COMPANY_TYPE_OPTIONS }
            value={ companyListTypeFilter || COMPANY_TYPE_OPTIONS[0] }
            onChange={ onChangeCompanyTypeFilter }
          />
        </div>
        <div className='filter-item'>
          <div className="label mr-2">
            Time:
          </div>
          <Select
            className='reminder-select w-100'
            classNamePrefix='Select'
            options={ TIME_OPTIONS }
            value={ companyListTimeFilter || TIME_OPTIONS[0] }
            onChange={ onChangeTimeFilter }
          />
        </div>
      </Fragment>
    );
  };

  render() {
    const {
      FiltersSection,
      state: { inProcess, windowSize }
    } = this;

    return(
      <Fragment>
        { windowSize > 769 && <FiltersSection /> }
      </Fragment>
    );
  };
};
SelectFiltersSection.propTypes = propTypes;
SelectFiltersSection.contextTypes = contextTypes;

export default SelectFiltersSection;
