import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SelectFiltersSection from './selectFiltersSection';
import { selectors, actions } from 'core/company';

const mapStateToProps = state => ({
  companyListTimeFilter: selectors.getCompanyListTimeFilter(state),
  companyListTypeFilter: selectors.getCompanyListTypeFilter(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchCompanyListTimeFilterDispatch: actions.fetchCompanyListTimeFilter,
    fetchCompanyListTypeFilterDispatch: actions.fetchCompanyListTypeFilter,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(SelectFiltersSection);
