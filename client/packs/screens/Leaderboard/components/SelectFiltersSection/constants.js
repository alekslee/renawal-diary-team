export const COMPANY_TYPE_OPTIONS = [
  { value: 'all', label: 'Overall Winners' },
  { value: 'provider', label: 'Top Providers' },
  { value: 'broker', label: 'Top Brokers' },
  { value: 'comparsion_site', label: 'Top Price Comparison Sites' }
];

