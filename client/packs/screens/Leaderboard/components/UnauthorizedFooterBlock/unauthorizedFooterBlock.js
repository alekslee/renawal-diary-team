import React from 'react';
import { Link } from 'react-router-dom';
import { object, string } from 'prop-types';

import CheckIcon from 'images/svg/benefits-checkbox.svg';
import PersonalizeResultsIcon from 'images/icons/PersonalizeResultsIcon';
import { paths } from 'layouts/constants';

const propTypes = {
  category: object.isRequired,
  countryCode: string.isRequired
};

const UnauthorizedFooterBlock = ({ category, countryCode }) => (
  <div className='recommendation'>
    <div className='recommendation-text'>
      <h4>Benefits of membership</h4>
      <ul className='benefits-list'>
        <li>
          <img src={ CheckIcon } alt='checkbox' />
          Set reminders & get a link to a personalized leaderboard at renewal time
        </li>
        <li>
          <img src={ CheckIcon } alt='checkbox' />
          Compare what you are paying with others
        </li>
        <li>
          <img src={ CheckIcon } alt='checkbox' />
          Get group offers
        </li>
      </ul>
    </div>
    <Link
      to={ paths.RENEWAL_DETAILS.replace(':country', countryCode).replace(':slag', category.slag) }
      className='btn recommendation-sign-btn'
    >
      <PersonalizeResultsIcon />
      Personalize your results
    </Link>
  </div>
);
UnauthorizedFooterBlock.propTypes = propTypes;

export default UnauthorizedFooterBlock;
