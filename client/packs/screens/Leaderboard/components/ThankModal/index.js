import ThankModal from './thankModal';
import { injectIntl } from 'react-intl';

export default injectIntl(ThankModal);
