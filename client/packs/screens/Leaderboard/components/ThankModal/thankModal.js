import React, { Component } from 'react';
import { bool, func, object } from 'prop-types';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import cx from 'classnames';

import {
  EmailShareButton,
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton
} from 'react-share';

import { BASE_APP_URL } from 'core/constants';
import { defaultMessages } from 'locales/default';

import Check from 'images/icons/CheckboxIcon';
import Email from 'images/icons/Email';
import Facebook from 'images/icons/Facebook';
import LinkedIn from 'images/icons/LinkedIn';
import Twitter from 'images/icons/Twitter';
import WhatsApp from 'images/icons/WhatsApp';

const propTypes = {
  toggle: func.isRequired,
  category: object.isRequired,
};

class ThankModal extends Component {

  // state = {
  //   confirmed: false
  // }

  // toggleChecked = ({ checked: confirmed }) => this.setState({ confirmed });

  render() {

    const {
      // toggleChecked,
      // state: { confirmed },
      props: { windowSize, confirmed, isThankModalOpen, toggle, category, intl: { formatMessage } },
    } = this;

    const inviteMessage = formatMessage(defaultMessages.inviteMessage);

    return (
      <Modal
        isOpen={ isThankModalOpen }
        toggle={ () => toggle('isThankModalOpen') }
        className={ cx('thank-modal', category.root.enName) }
      >
        <ModalHeader toggle={ () => toggle('isThankModalOpen') } />
        <ModalBody>
          <div className="modal-top-box">
            <div className="thank-title">Thank you for voting!</div>
            <div className="thank-dialog"/>
          </div>
          <div className="thank-content">
            <div className="content-title">Each time you vote, you help us create</div>
            <ul className="thank-list">
              <li>
                <span className="icon"><Check /></span>
                <span>Recommendations for people just like you</span>
              </li>
              <li>
                <span className="icon"><Check /></span>
                <span>More transparent and competitive markets</span>
              </li>
            </ul>
            <div className='leaderboard-social-list'>
              <EmailShareButton
                className='social-link email'
                url={ BASE_APP_URL }
                subject={ formatMessage(defaultMessages.inviteMailSubject) }
                body={ `${inviteMessage} ${BASE_APP_URL}` }
              >
                <span className="icon">
                  <Email />
                </span>
              </EmailShareButton>
              <FacebookShareButton url={ BASE_APP_URL } quote={ inviteMessage } className='social-link facebook' >
                <span className="icon">
                  <Facebook />
                </span>
              </FacebookShareButton>
              <TwitterShareButton url={ BASE_APP_URL } title={ inviteMessage } className='social-link twitter'>
                <span className="icon">
                  <Twitter />
                </span>
              </TwitterShareButton>
              <LinkedinShareButton url={ BASE_APP_URL } title={ inviteMessage } className='social-link linkedin' >
                <span className="icon">
                  <LinkedIn />
                </span>
              </LinkedinShareButton>
              <WhatsappShareButton url={ BASE_APP_URL } title={ inviteMessage } className='social-link whatsApp'>
                <span className="icon">
                  <WhatsApp />
                </span>
              </WhatsappShareButton>
            </div>
            <div className="invite-text">Invite friends to renewal diary & get voting!</div>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button
            className="btn reminder-btn"
            onClick={() => {
              toggle('isThankModalOpen');
            }}
          >
            Ok
          </Button>

        </ModalFooter>
      </Modal>
    );
  }
}

ThankModal.propTypes = propTypes;

export default ThankModal;
