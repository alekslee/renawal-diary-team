import React, { Component } from 'react';
import { toastr } from 'lib/helpers';
import { Tooltip } from 'reactstrap';
import { object, string, func, array } from 'prop-types';
import { paths } from 'layouts/constants';

import SearchIcon from 'images/icons/Search';
import Calendar from 'images/svg/calendar-check';
import CalendarAdd from 'images/svg/calendar-add';
import CloseDark from 'images/svg/close-dark';

import { Field } from 'redux-form';
import FieldWithErrors from 'components/FieldWithErrors';
import Loader from 'components/Loader';
import AutocompleteCompanies from '../AutocompleateCompanies';

const propTypes = {
  category: object.isRequired,
  currentUser: object.isRequired,
  categorySubscribeDispatch: func.isRequired,
  countryCode: string.isRequired,
  categories: array,
};

class Header extends Component {
  state = {
    isOpenInfoTooltip: false,
    isOpenDiaryTooltip: false,
    joinToCategory: false,
    disabledSubscribe: false,
  };

  toggle = key => this.setState({ [key]: !this.state[key] });

  handleSubscribeToCategory = ({ selected }) => {
    const { category, categories, categorySubscribeDispatch } = this.props;

    this.setState({ disabledSubscribe: true });
    const callback = () => {
      this.setState({ disabledSubscribe: false });
      if (selected) {
        toastr.success(`You have successfully Subscribed to '${category.name}'`);
      } else {
        toastr.success(`You have successfully Unsubscribed from '${category.name}'`);
      }
    };
    const errorCallback = () => this.setState({ disabledSubscribe: false });

    categorySubscribeDispatch({ categoryId: category.id, selected, callback, errorCallback });
  }

  render() {
    const {
      toggle,
      handleSubscribeToCategory,
      state: { isOpenInfoTooltip, isOpenDiaryTooltip, joinToCategory, inputValue, disabledSubscribe },
      props: { category, currentUser }
    } = this;

    const isUserLogin = Object.entries(currentUser).length != 0;

    const joinBtn = (category.selected) ? (<button
      className="join"
      disabled={ disabledSubscribe }
      onClick={ () => handleSubscribeToCategory({ selected: false }) } >
      <span>
        Joined
      </span>
      {` - ${category.usersCount}`}
    </button>) : (<button
      className="join"
      disabled={ disabledSubscribe }
      onClick={ () => handleSubscribeToCategory({ selected: true }) }
    >
      <span>
        Join
      </span>
      {` - ${category.usersCount}`}
    </button>);

    return (
      <div className='reminder-main-header'>
        <div className='title-group'>
          <Tooltip
            isOpen={ isOpenInfoTooltip }
            toggle={ () => toggle('isOpenInfoTooltip') }
            trigger='hover'
            placement='top-start'
            className={ `${category.root && category.root.code} info-message` }
            target='info-icon'
            hideArrow
          >
            <span className="close-tooltip" onClick={ () => toggle('isOpenInfoTooltip') }>
              <img src={ CloseDark } alt="Close"/>
            </span>
            <p>
              We are the ‘{ category.name }’ hunter group. We are consumers working together to find the
              businesses offering the best value and service. We share our decisions and insights so we
              can all make smarter decisions together. Please invite friends and family so we are stronger
              together!
            </p>
          </Tooltip>
          <h3>
            { category.name }
            <span
              id='info-icon'
              className={ isOpenInfoTooltip ? 'active' : '' }
              onClick={ () => toggle('isOpenInfoTooltip') }
            >i
            </span>
          </h3>
          {/*{ isUserLogin && (category.usersCount !== undefined) && joinBtn }*/}
          <button
            className="calendar-join"
            onClick={()=> {
              this.context.toggleReminderFormModal()
            }}
            id="diary-btn"
          >
            <img src={ CalendarAdd } alt="Calendar" />
            <span>Add to my Diary </span>
          </button>
          <Tooltip
            isOpen={ isOpenDiaryTooltip }
            toggle={ () => toggle('isOpenDiaryTooltip') }
            trigger='hover'
            placement='top-start'
            className={ `${category.root && category.root.code} info-message diary-text` }
            target='diary-btn'
            hideArrow
          >
            <p>
              Click to add to your diary. We will send you a personalized link to the leaderboard at renewal time
            </p>
          </Tooltip>
        </div>

        <div className="search-box">

          <AutocompleteCompanies />

        </div>
      </div>
    );
  }
};
Header.propTypes = propTypes;
Header.contextTypes = {
  toggleReminderFormModal: func,
}
export default Header;
