

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from './header';
import { selectors as categorySelectors, actions as categoryActions } from 'core/category';
import { selectors as currentUserSelectors } from 'core/currentUser';

const mapStateToProps = (state, props) => ({
  currentUser: currentUserSelectors.getCurrentUser(state),
  categories: categorySelectors.getCategories(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    categorySubscribeDispatch: categoryActions.categorySubscribe
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(Header);

