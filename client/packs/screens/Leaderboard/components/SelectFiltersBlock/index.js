import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SelectFiltersBlock from './selectFiltersBlock';

import { selectors, actions } from 'core/company';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { selectors as categorySelectors } from 'core/category';


const mapStateToProps = state => ({
  companyListCheckedPlacesFilter: selectors.getCompanyListCheckedPlacesFilter(state),
  countriesSelectOptions: selectors.getCountrySelectOptions(state),
  companyListCountryFilter: selectors.getCompanyListCountryFilter(state),
  companyListTimeFilter: selectors.getCompanyListTimeFilter(state),
  companyListTypeFilter: selectors.getCompanyListTypeFilter(state),
  personalizeFilter: selectors.getPersonalizeFilter(state),
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  category: categorySelectors.getCategory(state),
  country: currentUserSelectors.getUserCountry(state),
  currentCountry: currentCountrySelectors.getCurrentCountry(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchCityOptionsDispatch: actions.fetchCityOptions,
    fetchRegionOptionsDispatch: actions.fetchRegionOptions,
    fetchCompaniesListDispatch: actions.fetchCompaniesList,
    fetchCompanyListCheckedPlacesFilterDispatch: actions.fetchCompanyListCheckedPlacesFilter,
    fetchCompanyListTimeFilterDispatch: actions.fetchCompanyListTimeFilter,
    fetchCompanyListTypeFilterDispatch: actions.fetchCompanyListTypeFilter,
    fetchCompanyListFiltersDispatch: actions.fetchCompanyListFilters,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(SelectFiltersBlock);
