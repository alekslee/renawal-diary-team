import React, { Fragment, Component } from 'react';
import Select from 'react-select';
import { object, array, bool, func, number, string } from 'prop-types';
import AnimateHeight from 'react-animate-height';
import AutocompleteCompanies from '../AutocompleateCompanies';
import LocationField from 'components/LocationField';
import { AddNoteModal } from 'screens/MyNotes';
import { toastr } from 'lib/helpers';

import Close from 'images/svg/close-dark';
import FiltersIcon from 'images/icons/FiltersIcon';
import AddNote from 'images/icons/AddNote';
import CheckboxIcon from 'images/icons/CheckboxIcon';
import CitySelect from './components/CitySelect';
import SearchIcon from 'images/icons/Search';

import Loader from 'components/Loader';
import { CHECKED_PLACES_OPTIONS, TIME_OPTIONS } from 'core/company/constants';
import { COMPANY_TYPE_OPTIONS } from './constants';
import cx from 'classnames';
import { CATEGORIES } from '../../../../layouts/UnAuthorizedApp/components/SignUpModal/constants';

const BEST_OPTIONS = [
  { value: 'best_value', label: 'Best Value' },
  { value: 'best_service', label: 'Best Service' },
];

const propTypes = {
  companyListCheckedPlacesFilter: object,
  // countriesSelectOptions: array.isRequired,
  companyListCountryFilter: object,
  companyListTimeFilter: object,
  companyListTypeFilter: object,
  personalizeFilter: object,
  countryCode: string.isRequired,
  fetchCityOptionsDispatch: func.isRequired,
  fetchRegionOptionsDispatch: func.isRequired,
  fetchCompanyListCheckedPlacesFilterDispatch: func.isRequired,
  fetchCompanyListTimeFilterDispatch: func.isRequired,
  fetchCompanyListTypeFilterDispatch: func.isRequired,
  fetchCompanyListFiltersDispatch: func.isRequired,
  fetchCompaniesListDispatch: func.isRequired,
  handleResetCompanyListFilters: func.isRequired,
  category: object.isRequired,
  currentCountry: object.isRequired,
  country: object,
};

const contextTypes = {
  updateCompaniesOrderingDispatch: func.isRequired,
  resetCompanyOrderByColumnDispatch: func.isRequired,
};

class SelectFiltersBlock extends Component {

  state = {
    inProcess: false,
    windowSize: window.innerWidth,
    mobileFilter: false,
    desktopFilter: true,
    isAddNoteModalOpen: false,
    bestSort: 'best_value'
  };

  componentDidMount() {
    // let { value: newCountryCode } = (this.props.companyListCountryFilter || {});
    // if (newCountryCode !== this.countryCode) {
    //   this.countryCode = newCountryCode;
    //   this.onChangeCountryCode(newCountryCode);
    // }

    this.mounted = true;
    this.setWindowSize();
    window.addEventListener('resize', this.setWindowSize);

    if ( ['broadband', 'energy'].includes(this.props.category.root.enName) ) {
      console.log('TODO : broadband, energy default filter logic');
    }
    // const EnergyBroadBandCategory = ['broadband', 'energy'].includes(this.props.category.root.enName);
    // if (EnergyBroadBandCategory) {
    //   this.onChangeCompanyTypeFilter(COMPANY_TYPE_OPTIONS[1])
    // }
  }

  // UNSAFE_componentWillReceiveProps(nextProps) {
  //   let { value: newCountryCode } = (nextProps.companyListCountryFilter || {});
  //   const { value: countryCode } = (this.props.companyListCountryFilter || {});

  //   if (!newCountryCode) newCountryCode = this.props.countryCode;

  //   if (newCountryCode && newCountryCode !== countryCode && newCountryCode !== this.countryCode) {
  //     this.countryCode = newCountryCode;
  //     this.onChangeCountryCode(newCountryCode);
  //   }
  // }

  componentWillUnmount() {
    this.mounted = false;
    window.removeEventListener('resize', this.setWindowSize);
  };

  setWindowSize = () => {
    this.setState({ windowSize: window.innerWidth });
  }

  onChangeCheckedPlacesFilter = checkedPlaces => {
    const { fetchCompanyListCheckedPlacesFilterDispatch } = this.props;

    this.setState({ inProcess: true });
    const callback = () => this.setState({ inProcess: false });
    fetchCompanyListCheckedPlacesFilterDispatch({ checkedPlaces, callback, errorCallback: callback });
  };

  onChangeCountryCode = (countryCode, keyWord = null) => {

    const { fetchCityOptionsDispatch, fetchRegionOptionsDispatch } = this.props;
    if (countryCode === 'us') {
      fetchRegionOptionsDispatch({ countryCode, keyWord });
    } else if (countryCode) {
      fetchCityOptionsDispatch({ countryCode, keyWord });
    }
  }

  onChangeCountryFilter = companyListCountryFilter => {
    const { fetchCompanyListFiltersDispatch } = this.props;

    this.setState({ inProcess: true });
    const callback = () => this.setState({ inProcess: false });

    this.onChangeCountryCode(companyListCountryFilter.value);

    fetchCompanyListFiltersDispatch({
      companyListZipCodeFilter: undefined,
      companyListCityFilter: undefined,
      companyListCountryFilter,
      callback, errorCallback: callback,
    });
  };


  toggle = key => this.setState({ [key]: !this.state[key] });

  onChangeTimeFilter = companyListTimeFilter => {
    const { fetchCompanyListTimeFilterDispatch } = this.props;

    this.setState({ inProcess: true });
    const callback = () => this.setState({ inProcess: false });
    fetchCompanyListTimeFilterDispatch({ companyListTimeFilter, callback, errorCallback: callback });
  };

  onChangeCompanyTypeFilter = companyListTypeFilter => {
    const { fetchCompanyListTypeFilterDispatch } = this.props;

    this.setState({ inProcess: true });
    const callback = () => this.setState({ inProcess: false });
    fetchCompanyListTypeFilterDispatch({ companyType: companyListTypeFilter, callback, errorCallback: callback });
  };

  onChangeLocationFilter = location => {
    const { fetchCompanyListFiltersDispatch } = this.props;

    if (location.countryCode || location.city || location.address) {
      this.setState({ inProcess: true });
      const callback = () => this.setState({ inProcess: false });

      fetchCompanyListFiltersDispatch({
        companyListCityFilter: { value: location.city || location.address },
        companyListCountryFilter: { value: (location.countryCode || '').toLocaleLowerCase() },
        callback, errorCallback: callback,
      });
    } else {
      toastr.warning('no any location detected');
      console.log(location);
    }
  };

  DesctopRankFilters = () => {
    const { companyListTypeFilter } = this.props;
    const { onChangeCompanyTypeFilter } = this;

    return (
      <div className="reminder-form-item top-filters">
        { COMPANY_TYPE_OPTIONS.map(({ label, value }, inx) => (
          <label className='custom-radio' key={ inx }>
            <input
              type='radio'
              value={ value }
              checked={ value === (companyListTypeFilter || COMPANY_TYPE_OPTIONS[0]).value }
              name="topFilter"
              onChange={ ({ target }) => {
                const companyListTypeFilter = COMPANY_TYPE_OPTIONS.find(({ value }) => value === target.value );
                onChangeCompanyTypeFilter(companyListTypeFilter);
              } }
            />
            <span className='radio-mark' />
            { label }
          </label>
        )) }
      </div>
    );
  }

  ReminderFormItems = () => {
    const {
      onChangeCheckedPlacesFilter,
      onChangeTimeFilter,
      onChangeCountryFilter,
      onChangeCompanyTypeFilter,
      onChangeLocationFilter,
      state: { inProcess, searchCity, windowSize },
      props: {
        companyListCheckedPlacesFilter,
        companyListTimeFilter,
        companyListCountryFilter,
        // countriesSelectOptions,
        companyListTypeFilter,
        personalizeFilter,
        category,
        currentCountry,
        country
      }
    } = this;

    const EnergyBroadBandCategory = ['broadband', 'energy'].includes(category.root.enName);

    // let countryValue = companyListCountryFilter;
    // if (personalizeFilter.location) {
    //   countryValue = countryValue || countriesSelectOptions.find(el => el.value === (personalizeFilter.location.countryCode || '').toLocaleLowerCase() );
    // }
    // countryValue = countryValue || countriesSelectOptions.find(el => el.value === this.props.countryCode );

    return (
      <div
        className='reminder-form-item reminder-form-item-filter'
        key={ `${companyListCheckedPlacesFilter}${companyListTimeFilter}${companyListCountryFilter}${companyListTypeFilter}` }
      >
        {/*<div className='select-container'>*/}
        {/*  <span className='input-label'>People who checked</span>*/}
        {/*  <Select*/}
        {/*    className='reminder-select places-select'*/}
        {/*    classNamePrefix='Select'*/}
        {/*    options={ CHECKED_PLACES_OPTIONS }*/}
        {/*    value={ companyListCheckedPlacesFilter || CHECKED_PLACES_OPTIONS[0] }*/}
        {/*    onChange={ onChangeCheckedPlacesFilter }*/}
        {/*  />*/}
        {/*</div>*/}

        <div className='select-container'>
          <span className='input-label'>Time</span>
          <Select
            className='reminder-select'
            classNamePrefix='Select'
            options={ TIME_OPTIONS }
            value={ companyListTimeFilter || TIME_OPTIONS[0] }
            onChange={ onChangeTimeFilter }
          />
        </div>
        {/*<div className='select-container location-select'>*/}
        {/*  <div className="location-search">*/}
        {/*    <span className='input-label'>Location</span>*/}

        {/*    <LocationField*/}
        {/*      placeholder='Please enter City, town'*/}
        {/*      input={ {onChange: () => {}} }*/}
        {/*      onSelect={ onChangeLocationFilter }*/}
        {/*      meta={ {*/}
        {/*        touched: null, error: null*/}
        {/*      } }*/}
        {/*      country={ country || currentCountry }*/}
        {/*      // disabled={ disabled }*/}
        {/*      // onChange={ changeLocationHandler }*/}
        {/*    />*/}
        {/*  </div>*/}
        {/*</div>*/}
        {
          /*
        <div className='select-container location-select'>
          <span className='input-label'>Country</span>
          <Select
            className='reminder-select'
            classNamePrefix='Select'
            value={ countryValue }
            options={ countriesSelectOptions }
            onChange={ onChangeCountryFilter }
          />
        </div>
  */
        }
        {
          /*
        <div className='select-container location-select'>
          <span className='input-label'>
            { (countryValue || {}).value === 'us' ? 'State' : 'City' }
          </span>

          <CitySelect
            countryCode={ (countryValue || {}).value }
            onProcessSet={ inProcess => this.setState({ inProcess }) }
            { ...{ personalizeFilter } }
          />

        </div>
  */
        }
        <div className='select-container'>
          <span className='input-label'>Sort</span>
          <Select
            className='reminder-select'
            classNamePrefix='Select'
            value={
              companyListTypeFilter ||
              (EnergyBroadBandCategory ? COMPANY_TYPE_OPTIONS[1] : COMPANY_TYPE_OPTIONS[0])
            }
            options={ COMPANY_TYPE_OPTIONS }
            onChange={ onChangeCompanyTypeFilter }
          />
        </div>
      </div>
    );
  }

  handleCompanyListKeyWordFilter = (selectKeyWord=null) => {
    let { keyWord } = this.props.formAttributes || {};
    keyWord = selectKeyWord || (keyWord && keyWord.label);
    const companyListKeyWordFilter = { keyWord };

    this.setState({ isLoading: true, inputValue: { label: keyWord } });
    const callback = () => this.setState({ isLoading: false });
    this.props.fetchCompanyListKeyWordFilterDispatch({ companyListKeyWordFilter, callback, errorCallback: callback });
  }

  changeInputHandler = inputValue => {
    if (inputValue && !inputValue.label) inputValue = {};

    this.props.fetchCompaniesListForAutocompleateDispatch({ categoryId: this.props.category.id, keyWord: inputValue.label });
    this.setState({ inputValue });
  };

  selectInputHandler = ({ label }) => {
    this.handleCompanyListKeyWordFilter(label);
  }

  handleBestSort = (value) => {
    const { updateCompaniesOrderingDispatch } = this.context;

    this.setState({ bestSort: value, inProcess: true });
    const callback = () => this.setState({ inProcess: false });
    updateCompaniesOrderingDispatch({ ordering: { column: value }, callback, errorCallback: callback });
  }

  render() {
    const {
      toggle,
      ReminderFormItems,
      DesctopRankFilters,
      handleBestSort,
      onChangeCompanyTypeFilter,
      state: { inProcess, windowSize, mobileFilter, bestSort, desktopFilter, isAddNoteModalOpen },
      props: {
        companyListTypeFilter,
        fetchCompaniesListDispatch,
        category
      },
      context: {
        updateCompaniesOrderingDispatch,
      }
    } = this;
    const EnergyBroadBandCategory = ['broadband', 'energy'].includes(category.root.enName);
    const mobileScreen = windowSize < 768;

    return (
      <Fragment>
        {/*{mobileScreen &&*/}
        {/*  <div className="mobile-filter" onClick={ () => toggle('mobileFilter') }>*/}
        {/*    <span>*/}
        {/*      Advanced filters*/}
        {/*    </span>*/}
        {/*    {!mobileFilter ? <FiltersIcon /> : <div className="close"><img src={ Close }  alt="Close" /></div>}*/}
        {/*  </div>*/}
        {/*}*/}
        {/*{windowSize > 767 &&*/}
        {/*<div*/}
        {/*  className={*/}
        {/*    cx('sorting-desktop',  { 'active' : desktopFilter })*/}
        {/*  }*/}
        {/*>*/}
        {/*  {windowSize > 767 && <div>*/}
        {/*    { BEST_OPTIONS.map((el, index) => (*/}
        {/*      <label className="custom-radio" key={ index } >*/}
        {/*        <input*/}
        {/*          type="radio"*/}
        {/*          name="best-sorting"*/}
        {/*          value={ el.value }*/}
        {/*          checked={ bestSort === el.value }*/}
        {/*          onChange={ (e) => handleBestSort(e.target.value) }*/}
        {/*        />*/}
        {/*        <span className="radio-mark" />*/}
        {/*        <span className="label-text">*/}
        {/*          { el.label }*/}
        {/*        </span>*/}
        {/*      </label>*/}
        {/*    )) }*/}
        {/*  </div>}*/}
        {/*  <div className='select-container'>*/}
        {/*    <Select*/}
        {/*      name="best-sorting"*/}
        {/*      className='reminder-select'*/}
        {/*      classNamePrefix='Select'*/}
        {/*      defaultValue={ BEST_OPTIONS[0] }*/}
        {/*      onChange={ (e) => handleBestSort(e.value) }*/}
        {/*      options={ BEST_OPTIONS }*/}
        {/*    />*/}
        {/*  </div>*/}
        {/*  <div className='select-container'>*/}
        {/*    <Select*/}
        {/*      name="best-sorting"*/}
        {/*      className='reminder-select wide-select'*/}
        {/*      classNamePrefix='Select'*/}
        {/*      onChange={ onChangeCompanyTypeFilter }*/}
        {/*      defaultValue={ EnergyBroadBandCategory ? COMPANY_TYPE_OPTIONS[1] : COMPANY_TYPE_OPTIONS[0] }*/}
        {/*      options={ COMPANY_TYPE_OPTIONS }*/}
        {/*    />*/}
        {/*  </div>*/}
        {/*  {*/}
        {/*  <div*/}
        {/*    className="add-note-btn"*/}
        {/*    onClick={ () => toggle('isAddNoteModalOpen') }*/}
        {/*  >*/}
        {/*    <AddNote />*/}
        {/*    <span>Add note</span>*/}
        {/*  </div>*/}
        {/*  }*/}

        {/*  <div*/}
        {/*    className={ cx('filter-trigger ml-auto',  { 'active' : desktopFilter }) }*/}
        {/*    onClick={ () => toggle('desktopFilter') }*/}
        {/*  >*/}
        {/*    <FiltersIcon />*/}
        {/*    <span>Search Filters</span>*/}
        {/*  </div>*/}
        {/*</div>}*/}
        {
          // <AddNoteModal
          //   isModalOpen={ isAddNoteModalOpen }
          //   toggle={ () => toggle('isAddNoteModalOpen') }
          // />
        }
        {/*windowSize > 767 &&
        <AnimateHeight
          duration={ 250 }
          height={ desktopFilter ? 'auto' : 0 }
        >
          <div className="filter-container">
            <div className="second-filter">
              <ReminderFormItems />
              <div className="clear-all-container">
                <button
                  className="btn clear-all"
                  onClick={ () => this.props.handleResetCompanyListFilters() }
                  style={ { boxShadow: 'none' } }
                >
                  Clear filters
                </button>

                <button
                  className="btn reminder-btn small-btn"
                  onClick={ () => {
                    this.setState({ inProcess: true });
                    const callback = () => this.setState({ inProcess: false });
                    fetchCompaniesListDispatch({ callback, errorCallback: callback });
                  } }
                >
                  Apply
                </button>
              </div>
            </div>
            <DesctopRankFilters />
          </div>
        </AnimateHeight>
        */}

        {/*{windowSize < 768 &&*/}
        {/*  <div className="second-filter">*/}
        {/*    <AnimateHeight*/}
        {/*      duration={ 250 }*/}
        {/*      height={ (!mobileScreen || mobileFilter) ? 'auto' : 0 }*/}
        {/*    >*/}
        {/*      <ReminderFormItems />*/}
        {/*    </AnimateHeight>*/}
        {/*  </div>*/}
        {/*}*/}
        {windowSize < 768 && <ReminderFormItems />}
        {/*{windowSize < 768 &&*/}
        {/*<div className="best-sorting radio-container">*/}
        {/*  { BEST_OPTIONS.map((el, index) => (*/}
        {/*    <label className="radio-button" key={ index } >*/}
        {/*      <input*/}
        {/*        type="radio"*/}
        {/*        name="best-sorting"*/}
        {/*        value={ el.value }*/}
        {/*        onChange={ (e) => handleBestSort(e.target.value) }*/}
        {/*        checked={ bestSort === el.value }*/}
        {/*      />*/}
        {/*      <div>*/}
        {/*        <span><CheckboxIcon /></span>*/}

        {/*        { el.label }*/}
        {/*      </div>*/}
        {/*    </label>*/}
        {/*  )) }*/}
        {/*</div>}*/}

      </Fragment>
    );
  }
};
SelectFiltersBlock.propTypes = propTypes;
SelectFiltersBlock.contextTypes = contextTypes;

export default SelectFiltersBlock;
