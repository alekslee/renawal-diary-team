import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CitySelect from './citySelect';

import { selectors, actions } from 'core/company';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  companyListCityFilter: selectors.getCompanyListCityFilter(state),
  cityOptions: selectors.getCityOptions(state),
  regionOptions: selectors.getRegionOptions(state),
  currentUser: currentUserSelectors.getCurrentUser(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchCompanyListFiltersDispatch: actions.fetchCompanyListFilters,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(CitySelect);
