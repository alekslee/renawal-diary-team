import React, { Fragment, Component } from 'react';
import Select from 'react-select';
import { object, array, bool, func, number, string } from 'prop-types';

const propTypes = {
  fetchCompanyListFiltersDispatch: func.isRequired,
  onProcessSet: func.isRequired,
  countryCode: string,
  currentUser: object,
  personalizeFilter: object,
  companyListCityFilter: object,
};

class CitySelect extends Component {

  state = {
    zipCode: null,
    selectedCity: null,
    searchCity: null,
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    let { countryCode: newCountryCode, companyListCityFilter, personalizeFilter } = nextProps;
    const { countryCode } = this.props;

    if (newCountryCode && newCountryCode !== countryCode) {
      // (((personalizeFilter || {}).location || {}).city)
      this.setState({ selectedCity: companyListCityFilter, zipCode: null, searchCity: null });
    }
  }

  onSelectCityInput = (obj, { action }) => {
    const { fetchCompanyListFiltersDispatch, countryCode, onProcessSet } = this.props;

    const callback = () => { onProcessSet(false) };

    if ( (obj.label || '').includes('zip code') ) {
      this.setState({ selectedCity: null, zipCode: obj.value });
      onProcessSet(true);

      fetchCompanyListFiltersDispatch({
        companyListZipCodeFilter: { zipCode: obj.value },
        companyListCityFilter: undefined,
        callback, errorCallback: callback,
      });
    } else {
      this.setState({ selectedCity: obj, zipCode: null });
      onProcessSet(true);

      fetchCompanyListFiltersDispatch({
        companyListZipCodeFilter: undefined,
        companyListCityFilter: obj,
        callback, errorCallback: callback,
      });

    }
  };

  onKeyDownCityInput = ({ key }) => {
    const { zipCode } = this.state;
    const { fetchCompanyListFiltersDispatch, countryCode, onProcessSet } = this.props;

    if ( key === 'Enter' && (zipCode || '').match(/\d+/) ) {
      this.setState({ inProcess: true, selectedCity: null });
      onProcessSet(true);

      const callback = () => { onProcessSet(false) };

      fetchCompanyListFiltersDispatch({
        companyListZipCodeFilter: { zipCode },
        companyListCityFilter: undefined,
        callback, errorCallback: callback,
      });
    }
  }

  onChangeCityInput = (value, { action }) => {
    if ( [ 'input-blur', 'menu-close'].includes(action) ) return;
    if (value && value.match(/\d+/)) {
      this.setState({ zipCode: value, searchCity: null });
    } else {
      this.setState({ searchCity: value, zipCode: null });
    }
  };

  render() {
    const {
      onSelectCityInput,
      onChangeCityInput,
      onKeyDownCityInput,
      state: { inProcess, selectedCity, zipCode, searchCity},
      props: {
        countryCode,
        cityOptions,
        regionOptions,
        currentUser,
        personalizeFilter,
      }
    } = this;

    const selectedCityValue = (selectedCity || {}).value;

    let cityStateOptions = (countryCode === 'us' ? regionOptions : cityOptions) || [];

    let userCityState = cityStateOptions.find(el =>
      el.value === currentUser.city ||
      el.value === selectedCityValue
    );

    if (searchCity) {
      cityStateOptions = cityStateOptions.filter(el => el.value.match( new RegExp(searchCity,'i')));
    }
    cityStateOptions = cityStateOptions
      .slice(0, 20);

    if (zipCode) {
      userCityState = { value: zipCode, label: `zip code ( ${zipCode} )`};
      cityStateOptions = cityStateOptions.concat(userCityState);
    } else if (userCityState) {
      cityStateOptions = [ userCityState ].concat(cityStateOptions);
    }

    return (
      <Select
        className='reminder-select places-select'
        classNamePrefix='Select'
        options={ cityStateOptions }
        value={ userCityState || null }
        onKeyDown={ onKeyDownCityInput }
        onChange={ onSelectCityInput }
        onInputChange={ onChangeCityInput }
      />
    );
  }
}

CitySelect.propTypes = propTypes;

export default CitySelect;
