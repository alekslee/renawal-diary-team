import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import { string, object, bool, array, func, number } from 'prop-types';
import classNames from 'classnames';
import RadioButtonGroup from 'components/RadioButtonGroup';
import NumberField from 'components/NumberField';
import { intlShape } from 'react-intl';
import { retrieveFormInitialValues } from 'lib/helpers/renewalDetails';
import { toastr } from 'lib/helpers';
import cx from 'classnames';
import ReviewDetailsModal from 'components/ReviewDetailsModal';

import {
  EmailShareButton,
  FacebookShareButton,
  TwitterShareButton,
  LinkedinShareButton,
  WhatsappShareButton
} from 'react-share';
import { BASE_APP_URL } from 'core/constants';
import { defaultMessages } from 'locales/default';
import { AddNoteModal } from 'screens/MyNotes';

import Loader from 'components/Loader';
import { paths } from 'layouts/constants';
import {
  CompaniesList,
  SelectFiltersBlock,
  GroupRecomendationBlock,
  Header,
  UnauthorizedFooterBlock,
  InviteModal,
  PersonalizeYourResults,
  SelectFiltersSection,
  ThankModal
} from './components';
import Refresh from 'images/svg/refresh';
import VoteIcon from 'images/icons/VoteIcon';
import EditIcon2 from 'images/icons/EditIcon2';
import Email from 'images/icons/Email';
import Facebook from 'images/icons/Facebook';
import LinkedIn from 'images/icons/LinkedIn';
import Twitter from 'images/icons/Twitter';
import WhatsApp from 'images/icons/WhatsApp';
import AddNote from 'images/icons/AddNote';
import { Tooltip } from 'reactstrap';
import Switch from "react-switch";

const propTypes = {
  intl: intlShape.isRequired,
  countryCode: string.isRequired,
  category: object.isRequired,
  history: object.isRequired,
  currentUser: object.isRequired,
  recommendedSource: object,
  recommendedProvider: object,
  countriesSelectOptions: array,
  personalFilterOptions: object,
  criteriaQuestionsOptions: object,
  personalizeFilter: object,
  companies: array.isRequired,
  totalCount: number,
  averagePrice: number,
  companyListTypeFilter: object,
  fetchCompanyListTypeFilterDispatch: func.isRequired,
  fetchLeaderboardScreenDataDispatch: func.isRequired,
  setCompaniesListDispatch: func.isRequired,
  setCompanyTotalCountDispatch: func.isRequired,
  setCompanyCountriesSelectOptionsDispatch: func.isRequired,
  setCompanyDefaultFilterOptionsDispatch: func.isRequired,
  createCategoryPersonalizeFilterDispatch: func.isRequired,
  resetCompanyListFiltersDispatch: func.isRequired,
};

const childContextTypes = {
  updateCompaniesOrderingDispatch: func.isRequired,
  resetCompanyOrderByColumnDispatch: func.isRequired,
}

const contextTypes = {
  loadLeaderboardRef: func.isRequired,
}

class Leaderboard extends Component {

  constructor(props, context) {
    super(props);

    this.state = {
      isLoading: !(props.totalCount && props.countriesSelectOptions),
      isModalOpen: false,
      isPopoverOpen: false,
      isOpen: false,
      defaultQuestion: {},
      isAddNoteModalOpen: false,
      isOpenDiaryTooltip: false,
      isThankModalOpen: false,
      yourDetails: true,
      isReviewDetailsModalOpen: false,
    };

    context.loadLeaderboardRef && context.loadLeaderboardRef(this);
  }


  getChildContext() {
    const { updateCompaniesOrderingDispatch,
            resetCompanyOrderByColumnDispatch } = this.props;

    return {
      updateCompaniesOrderingDispatch,
      resetCompanyOrderByColumnDispatch
    };
  }

  componentDidMount() {
    const {
      toggle,
      state: { isLoading },
      props: {
        history,
        fetchLeaderboardScreenDataDispatch,
        fetchRenewalDetailScreenDataDispatch,
        countryCode,
        category,
        setCompanyDefaultFilterOptionsDispatch,
        fetchCountriesSelectOptionsDispatch,
      }
    } = this;

    if ((history.location.search || '').match('vote-success=1')) {
      toggle('isThankModalOpen');
      history.push(history.location.pathname)
    }

    if ((history.location.search || '').match('company-approve-status')) {
      if ( history.location.search.match('company-approve-status=success') ) {
        toastr.success('Success Approved!')
      } else {
        toastr.error('Approve Failed')
      }

      history.push(history.location.pathname)
    }

    const callback = () => {
      // this.setState({ isLoading: false });
      // setRenewalDetailDispatch({ renewalDetail: {} });
    }

    setCompanyDefaultFilterOptionsDispatch({ callback: () => {
      if (isLoading) {
        const callback = () => this.setState({ isLoading: false });
        fetchLeaderboardScreenDataDispatch({ callback, errorCallback: callback });
      }
    }});

  };

  componentWillUnmount() {
    const {
      setCompaniesListDispatch,
      setCompanyTotalCountDispatch,
      setCompanyCountriesSelectOptionsDispatch
    } = this.props;

    // setCompaniesListDispatch({ companies: [] });
    setCompanyTotalCountDispatch({ totalCount: undefined });
    setCompanyCountriesSelectOptionsDispatch({ countriesSelectOptions: undefined });
  };

  toggle = key => this.setState({ [key]: !this.state[key] });

  handleChange = checked => {
    if (checked) {
      this.props.setCompanyDefaultFilterOptionsDispatch({ callback: () => {
        this.props.fetchLeaderboardScreenDataDispatch({ callback: null, errorCallback: null });
      }});
    }else {
      this.handleResetCompanyListFilters();
    }

    this.setState({ yourDetails: checked });
  }

  defaultAnswers = () => {
    const { category, criteriaQuestionsOptions } = this.props;
    let answers = {}

    if (category.root.code === 'energy') {
      Object.assign(answers, { energy_type: category.code })
    } else if (category.root.code === 'broadband') {
      Object.assign(answers, { broadband_package: (criteriaQuestionsOptions.broadband_package || {}).default_value })
    }
    return { criteriaQuestions: answers };
  };

  retrieveInitialValues = () => {

    const {
      isRenewalDetailExists,
      renewalDetail,
      criteriaQuestionsOptions,
      currentUser,
      category
    } = this.props;

    return retrieveFormInitialValues({
      isRenewalDetailExists,
      renewalDetail,
      criteriaQuestionsOptions,
      currentUser,
      category
    });
  }

  // onChangeLeafCategorySelect = leafCategoryValue => {
  //   const { history, countryCode } = this.props;
  //   history.push(paths.LEADERBOARD.replace(':country', countryCode).replace(':slag', leafCategoryValue.slag));
  // };
  handleResetCompanyListFilters = () => {
    const {
      currentUser,
      category,
      createCategoryPersonalizeFilterDispatch,
      resetCompanyListFiltersDispatch,
    } = this.props;

    this.setState({ isLoading: true });
    const callback = () => this.setState({ isLoading: false });
    if (currentUser.id) createCategoryPersonalizeFilterDispatch({ noFetch: true, categoryId: category.id, criteriaQuestions: '{}' });
    resetCompanyListFiltersDispatch({ resetCriteria: true, callback, errorCallback: callback });
  }

  render() {
    const {
      toggle,
      RankFilters,
      defaultAnswers,
      retrieveInitialValues,
      handleResetCompanyListFilters,
      handleChange,
      // onChangeLeafCategorySelect,
      state: { isLoading, isModalOpen, isPopoverOpen, isReviewDetailsModalOpen, isAddNoteModalOpen, isOpenDiaryTooltip, isThankModalOpen, yourDetails },
      props: { currentUser, countryCode, recommendedSource, recommendedProvider, averagePrice,
        resetCompanyListFiltersDispatch,
        fetchRenewalDetailScreenDataDispatch,
        category,
        totalCount,
        personalFilterOptions,
        criteriaQuestionsOptions,
        personalizeFilter, intl: { formatMessage }
      },
    } = this;
    const inviteMessage = formatMessage(defaultMessages.inviteMessage);
    const isCriteria = criteriaQuestionsOptions && Object.keys(criteriaQuestionsOptions).length !== 0;

    return (
      <div className='leaderboard-container'>
        <div className='reminder-main leaderboard'>
          {/*<div className="main-subheader">*/}
          {/*  <p>Where did you find your best value for { category.name } </p>*/}
          {/*  <Link*/}
          {/*    to={ paths.VOTE.replace(':country', countryCode).replace(':slag', category.slag) }*/}
          {/*    className='have-your-say-btn'*/}
          {/*    id="haveYouSay"*/}
          {/*  >*/}
          {/*    <VoteIcon />*/}
          {/*    Have your say!*/}
          {/*  </Link>*/}
          {/*</div>*/}
          <Header { ...{ category, countryCode } } />
          <div className='reminder-main-body'>
            <div className='leaderboard-actions'>

              <div className="leaderboard-actions-part">
                <div className="invite-link">Invite friends</div>
                <div className='leaderboard-social-list'>
                  <EmailShareButton
                    className='social-link email'
                    url={ BASE_APP_URL }
                    subject={ formatMessage(defaultMessages.inviteMailSubject) }
                    body={ `${inviteMessage} ${BASE_APP_URL}` }
                  >
                    <span className="icon">
                      <Email />
                    </span>
                  </EmailShareButton>
                  <FacebookShareButton url={ BASE_APP_URL } quote={ inviteMessage } className='social-link facebook' >
                    <span className="icon">
                      <Facebook />
                    </span>
                  </FacebookShareButton>
                  <TwitterShareButton url={ BASE_APP_URL } title={ inviteMessage } className='social-link twitter'>
                    <span className="icon">
                      <Twitter />
                    </span>
                  </TwitterShareButton>
                  <LinkedinShareButton url={ BASE_APP_URL } title={ inviteMessage } className='social-link linkedin' >
                    <span className="icon">
                      <LinkedIn />
                    </span>
                  </LinkedinShareButton>
                  <WhatsappShareButton url={ BASE_APP_URL } title={ inviteMessage } className='social-link whatsApp'>
                    <span className="icon">
                      <WhatsApp />
                    </span>
                  </WhatsappShareButton>
                </div>
              </div>
              <div className="leaderboard-actions-part">
                {/*<div className="toggle-personal">*/}
                {/*  <span>*/}
                {/*    Use your Detаils?*/}
                {/*  </span>*/}

                {/*  <span>*/}
                {/*    <Switch*/}
                {/*      checked={ yourDetails }*/}
                {/*      className={ cx('react-switch', { actived: yourDetails }) }*/}
                {/*      height={ 14 }*/}
                {/*      width={ 34 }*/}
                {/*      onChange={ handleChange }*/}
                {/*    />*/}
                {/*  </span>*/}
                {/*</div>*/}
                <Link
                  to={ paths.VOTE.replace(':country', countryCode).replace(':slag', category.slag) }
                  className='have-your-say-btn'
                  id="haveYouSay"
                >
                  <VoteIcon />
                  Have your say!
                </Link>
                {
                  isCriteria ?
                    <div
                      className='icon-link edit-link'
                      onClick={ (ev) => {
                        if (ev.target === ev.currentTarget ||
                          ev.target === ev.currentTarget.querySelector('span')) {
                          toggle('isPopoverOpen');
                        }
                      } }
                    >
                      <span className='icon'>
                        <EditIcon2 />
                      Personalize your results
                      </span>
                      {
                        isPopoverOpen &&
                        <PersonalizeYourResults
                          dirty
                          initialValues={ Object.assign(defaultAnswers(), retrieveInitialValues()) }
                          { ...{ category, toggle, countryCode, criteriaQuestionsOptions, fetchRenewalDetailScreenDataDispatch, isPopoverOpen } }
                        />
                      }
                    </div>
                    :
                    <span className='icon-link edit-link' />
                }
                <Tooltip
                  isOpen={ isOpenDiaryTooltip }
                  toggle={ () => toggle('isOpenDiaryTooltip') }
                  trigger='hover'
                  placement='top'
                  className={ `${category.root && category.root.code} info-message have-you-say--text` }
                  target='haveYouSay'
                  hideArrow
                >
                  <p>
                    Share where you found your best value
                  </p>
                </Tooltip>
                {/*<div*/}
                  {/*className="icon-link edit-link add-note-link"*/}
                  {/*onClick={ () => toggle('isAddNoteModalOpen') }*/}
                {/*>*/}
                  {/*<span className="icon">*/}
                    {/*<AddNote />*/}
                    {/*<span>Add note</span>*/}
                  {/*</span>*/}
                {/*</div>*/}
                {
                  // <AddNoteModal
                  //   isModalOpen={ isAddNoteModalOpen }
                  //   toggle={ () => toggle('isAddNoteModalOpen') }
                  // />
                }
              </div>
            </div>

            <SelectFiltersBlock

              {...{ handleResetCompanyListFilters }}
            />

            <div className="reminder-form-item filter-result">
              {/*<div className="results w-25">*/}
              {/*  Results: { totalCount }*/}
              {/*</div>*/}
              <SelectFiltersSection
                {...{ handleResetCompanyListFilters }}
              />
              {/*<div*/}
              {/*  className="clean-filters"*/}
              {/*  onClick={ () => handleResetCompanyListFilters() }*/}
              {/*>*/}
              {/*  <img src={ Refresh } alt="Refresh" />*/}
              {/*  <span>*/}
              {/*    Clear filter*/}
              {/*  </span>*/}
              {/*</div>*/}
            </div>
            <div className="businesses-results">
              <div className="result-info"> <span>Top 3</span> recommendations for you based on your details</div>
              <div className="result-info result"><span>{totalCount}</span> businesses found</div>
            </div>

            <CompaniesList />
          </div>

          {
            !!(recommendedSource && recommendedProvider) &&
              <GroupRecomendationBlock
                key={ `${recommendedSource.id}-${recommendedProvider.id}-${averagePrice}` }
                source={ recommendedSource }
                provider={ recommendedProvider }
                averagePrice={ averagePrice }
                category={ category }
              />
          }

          <div className="widget-container">
            <div className="widget-header">
              <div className="icon"></div>
              <div className="name">
                123.ie (Direct)
              </div>
            </div>
            <div className="widget-body">
              You sign up with your current provider since past year
            </div>
            <a href="javascript:;" className="widget-link">Leave your review</a>
          </div>
        </div>

        {
/*
          currentUser.id ?
            !!(recommendedSource && recommendedProvider) &&
              <GroupRecomendationBlock
                source={ recommendedSource }
                provider={ recommendedProvider }
                category={ category }
              />
            :
            <UnauthorizedFooterBlock category={ category } countryCode={ countryCode } />
*/
        }
        <InviteModal { ...{ isModalOpen, toggle, category } } />

        <ThankModal
          { ...{  isThankModalOpen, toggle, category, formatMessage } }
        />

        { isReviewDetailsModalOpen &&
          <ReviewDetailsModal
            isOpen={ isReviewDetailsModalOpen }
            initialValues={ Object.assign(defaultAnswers(), retrieveInitialValues()) }
            dirty
            toggle={() => this.toggle('isReviewDetailsModalOpen')}
          />
        }
      </div>
    );
  };
};
Leaderboard.propTypes = propTypes;
Leaderboard.contextTypes = contextTypes;
Leaderboard.childContextTypes = childContextTypes;

export default Leaderboard;
