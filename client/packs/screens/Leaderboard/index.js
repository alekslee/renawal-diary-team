import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import Leaderboard from './leaderboard';

import { selectors } from 'core/category';
import { selectors as companySelectors, actions as companyActions } from 'core/company';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { selectors as detailSelectors, actions as detailActions } from 'core/renewalDetail';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  category: selectors.getCategory(state),
  averagePrice: companySelectors.getAveragePrice(state),
  recommendedSource: companySelectors.getRecomendedSource(state),
  recommendedProvider: companySelectors.getRecomendedProvider(state),
  leafCategoriesOptions: selectors.getLeafCategoriesOptions(state),
  isExistsRenewalDetail: selectors.getIsExistsRenewalDetail(state),
  companies: companySelectors.getCompanies(state),
  countriesSelectOptions: companySelectors.getCountrySelectOptions(state),
  currentUser: currentUserSelectors.getCurrentUser(state),

  criteriaQuestionsOptions: detailSelectors.getCriteriaQuestionsOptions(state),
  renewalDetail: detailSelectors.getRenewalDetail(state),
  isRenewalDetailExists: detailSelectors.getIsRenewalDetailExists(state),

  personalFilterOptions: companySelectors.getPersonalFilterOptions(state),
  personalizeFilter: companySelectors.getPersonalizeFilter(state),
  companyListTypeFilter: companySelectors.getCompanyListTypeFilter(state),
  totalCount: companySelectors.getCompaniesTotalCount(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    createCategoryPersonalizeFilterDispatch: companyActions.createCategoryPersonalizeFilter,
    resetCompanyListFiltersDispatch: companyActions.resetCompanyListFilters,
    fetchCompanyListTypeFilterDispatch: companyActions.fetchCompanyListTypeFilter,
    fetchLeaderboardScreenDataDispatch: companyActions.fetchLeaderboardScreenData,
    fetchCountriesSelectOptionsDispatch: companyActions.fetchCountriesSelectOptions,

    fetchRenewalDetailScreenDataDispatch: detailActions.fetchRenewalDetailScreenData,

    setCompaniesListDispatch: companyActions.setCompaniesList,
    setCompanyTotalCountDispatch: companyActions.setCompanyTotalCount,
    setCompanyCountriesSelectOptionsDispatch: companyActions.setCompanyCountriesSelectOptions,
    setCompanyDefaultFilterOptionsDispatch: companyActions.setCompanyDefaultFilterOptions,

    updateCompaniesOrderingDispatch: companyActions.updateCompaniesOrdering,
    resetCompanyOrderByColumnDispatch: companyActions.resetCompanyOrderByColumn,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(Leaderboard)));