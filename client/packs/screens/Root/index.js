import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Root from './root';

import { selectors, actions } from 'core/category';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  firstCategory: selectors.getFirstCategory(state),
  categories: selectors.getCategories(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchSortedCategoriesDispatch: actions.fetchSortedCategories
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(Root);
