import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { string, shape, func } from 'prop-types';

import { paths } from 'layouts/constants';
import Loader from 'components/Loader';

const propTypes = {
  countryCode: string.isRequired,
  firstCategory: shape({
    id: string.isRequired
  }),
  fetchSortedCategoriesDispatch: func.isRequired,
};

// Do it with separatly component because we should load list of categories at first and only after this make Redirect
class Root extends Component {
  state = {
    isLoading: true
  };

  componentDidMount() {
    const { fetchSortedCategoriesDispatch } = this.props;
    const callback = () => this.setState({ isLoading: false });
    fetchSortedCategoriesDispatch({ callback });
  };

  render () {
    const {
      state: { isLoading },
      props: { countryCode, firstCategory }
    } = this;

    if (isLoading || !firstCategory) return <Loader />;

    return (
      <Redirect
        from={ paths.ROOT }
        to={ paths.LEADERBOARD.replace(':country', countryCode).replace(':slag', firstCategory.slag) }
        exact
      />
    );
  }
};

Root.propTypes = propTypes;

export default Root;
