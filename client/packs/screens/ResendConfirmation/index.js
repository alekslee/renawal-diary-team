import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { stopSubmit } from 'redux-form';
import ResendConfirmation from './resendConfirmation';

import { selectors, actions } from 'core/currentUser';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  currentUser: selectors.getCurrentUser(state),
  resendConfirmationInstructionsFormErrors: selectors.getCurrentUserResendConfirmationFormErrors(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    resendCurrentUserConfirmationInstructionsDispatch: actions.resendCurrentUserConfirmationInstructions,
    setFormErrorDispatch: error => stopSubmit('resendConfirmation', { email: error }),
    setCurrentUserDispatch: actions.setCurrentUser,
    setCurrentUserConfirmationInstructionsErrorsDispatch: actions.setCurrentUserConfirmationInstructionsErrors
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ResendConfirmation));
