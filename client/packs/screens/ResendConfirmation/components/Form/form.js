import React from 'react';
import { func, array } from 'prop-types';
import { Field } from 'redux-form';
import { intlShape } from 'react-intl';

import I18n from 'components/I18n';
import FieldWithErrors from 'components/FieldWithErrors';
import { defaultMessages } from 'locales/default';

const propTypes = {
  handleSubmit: func.isRequired,
  intl: intlShape.isRequired
};

const Form = ({ intl: { formatMessage }, handleSubmit }) => {
  return (
    <form onSubmit={ handleSubmit } >
      <div className='input-field' >
        <Field
          name='email'
          component={ FieldWithErrors }
          type='text'
          placeholder={ formatMessage(defaultMessages.email) }
        />
      </div>
      <button type='submit' className='btn auth-btn'>
        <I18n text={ defaultMessages.resendConfirmationInstructions } />
      </button>
    </form>
  );
};
Form.propTypes = propTypes;

export default Form;
