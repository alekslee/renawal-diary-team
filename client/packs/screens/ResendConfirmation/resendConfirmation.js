import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { SubmissionError } from 'redux-form';
import { toastr } from 'lib/helpers';
import { string, func, object, shape, number, array } from 'prop-types';

import I18n from 'components/I18n';
import Loader from 'components/Loader';
import { Form } from './components';
import { defaultMessages } from 'locales/default';
import { paths } from 'layouts/constants';

const propTypes = {
  countryCode: string.isRequired,
  history: object.isRequired,
  currentUser: shape({
    id: number,
    email: string.isRequired
  }).isRequired,
  resendConfirmationInstructionsFormErrors: array,
  resendCurrentUserConfirmationInstructionsDispatch: func.isRequired,
  setFormErrorDispatch: func.isRequired,
  setCurrentUserDispatch: func.isRequired,
  setCurrentUserConfirmationInstructionsErrorsDispatch: func.isRequired
};

class ResendConfirmation extends Component {
  state = {
    inProcess: false
  };

  componentWillUnmount() {
    const { setCurrentUserDispatch, setCurrentUserConfirmationInstructionsErrorsDispatch } = this.props;
    setCurrentUserDispatch({ currentUser: {} });
    setCurrentUserConfirmationInstructionsErrorsDispatch({});
  }

  submitHandler = ({ email }) => {
    const {
      resendCurrentUserConfirmationInstructionsDispatch,
      countryCode,
      history,
      currentUser,
      resendConfirmationInstructionsFormErrors,
      setFormErrorDispatch
    } = this.props;

    const callback = () => {
      if (currentUser.id) {
        history.push(paths.ROOT.replace(':country', countryCode));
      } else {
        history.push(paths.USERS_SIGN_IN.replace(':country', countryCode));
      }
      toastr.success('', { component: <I18n text={ defaultMessages.deviseConfirmationsSendInstructions } /> });
    };

    if (
      resendConfirmationInstructionsFormErrors
      && resendConfirmationInstructionsFormErrors.length
      && currentUser.email === email
    ) {
      return setFormErrorDispatch(resendConfirmationInstructionsFormErrors[0]);
    }

    const errorCallback = () => {
      this.setState({ inProcess: false });
    };

    this.setState({ inProcess: true });
    resendCurrentUserConfirmationInstructionsDispatch({ email, callback, errorCallback });
  };

  render() {
    const {
      submitHandler,
      state: { inProcess },
      props: { countryCode, currentUser }
    } = this;

    return (
      <section className='auth-container'>
        { inProcess && <Loader /> }
        <div className='auth-block resend-confirmation' >
          <h3 className='auth-title' >
            <I18n text={ defaultMessages.resendConfirmationInstructions } />
          </h3>
          <Form initialValues={ { email: currentUser.email } } onSubmit={ submitHandler } />
          <div>
            <Link to={ paths.USERS_SIGN_IN.replace(':country', countryCode) } >
              <I18n text={ defaultMessages.login } />
            </Link>
          </div>
          <div>
            <Link to={ paths.USERS_FORGOT_PASSWORD.replace(':country', countryCode) } >
              <I18n text={ defaultMessages.forgotPassword } />
            </Link>
          </div>
        </div>
      </section>
    );
  }
};
ResendConfirmation.propTypes = propTypes;

export default ResendConfirmation;
