import React, { Component } from 'react';
import queryString from 'query-string';
import { object, string, func } from 'prop-types';
import { toastr } from 'lib/helpers';

import Loader from 'components/Loader';
import I18n from 'components/I18n';
import { Form } from './components';
import { defaultMessages } from 'locales/default';
import { paths } from 'layouts/constants';

const propTypes = {
  countryCode: string.isRequired,
  history: object.isRequired,
  currentUserInvitationAcceptDispatch: func.isRequired
};

class InvitationAccept extends Component {
  state = {
    inProcess: false
  };

  submitHandler = ({ location, password, displayName }) => {
    const {
      history,
      countryCode,
      currentUserInvitationAcceptDispatch
    } = this.props;

    const callback = () => {
      history.push(paths.ROOT.replace(':country', countryCode));
      toastr.success('', { component: <I18n text={ defaultMessages.deviseRegistrationsSignedUp } /> });
    };

    const errorCallback = () => {
      this.setState({ inProcess: false });
    };

    const invitationToken = queryString.parse(window.location.search).invitation_token;
    this.setState({ inProcess: true });
    currentUserInvitationAcceptDispatch({
      password,
      lat: location.lat,
      lng: location.lng,
      displayName,
      invitationToken,
      callback,
      errorCallback
    });
  };

  render() {
    const {
      submitHandler,
      state: { inProcess }
    } = this;

    return (
      <section className='auth-container'>
        { inProcess && <Loader /> }
        <div className='auth-block accept-invite' >
          <h3 className='auth-title' >
            <I18n text={ defaultMessages.acceptYourInvitation } />
          </h3>
          <Form onSubmit={ submitHandler } />
        </div>
      </section>
    );
  }
};
InvitationAccept.propTypes = propTypes;

export default InvitationAccept;
