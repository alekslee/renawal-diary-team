import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import InvitationAccept from './invitationAccept';

import { actions } from 'core/currentUser';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    currentUserInvitationAcceptDispatch: actions.currentUserInvitationAccept
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(InvitationAccept));
