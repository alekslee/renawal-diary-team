import { reduxForm } from 'redux-form';
import { injectIntl } from 'react-intl';
import { ALLOWED_COUNTRIES } from 'lib/allowedCountries';
import Form from './form';

const validate = ({ email, displayName, location, password, passwordConfirmation }) => {
  const errors = {};
  if (!email) errors.email = 'Required';
  if (!displayName) errors.displayName = 'Required';
  if (!password) errors.password = 'Required';
  if (!passwordConfirmation) errors.passwordConfirmation = 'Required';
  if (!errors.passwordConfirmation && password !== passwordConfirmation)
    errors.passwordConfirmation = 'Passwords doesn\'t match';
  if (!(location && location.countryCode && location.lat && location.lng)) errors.location = 'Required';
  if (!errors.location && !ALLOWED_COUNTRIES.includes(location.countryCode.toLowerCase()))
    errors.location = 'Country not supported';

  return errors;
};

export default reduxForm({ form: 'invitationAccept', validate })(injectIntl(Form));
