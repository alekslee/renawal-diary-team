import React from 'react';
import { Field } from 'redux-form';
import { intlShape } from 'react-intl';
import { func } from 'prop-types';

import FieldWithErrors from 'components/FieldWithErrors';
import LocationField from 'components/LocationField';
import I18n from 'components/I18n';
import { defaultMessages } from 'locales/default';

const propTypes = {
  intl: intlShape.isRequired,
  handleSubmit: func.isRequired
};

const Form = ({ handleSubmit, intl: { formatMessage } }) => {
  return (
    <form autoComplete='off' onSubmit={ handleSubmit } >
      <div className='input-field'>
        <Field
          component={ FieldWithErrors }
          type='text'
          name='displayName'
          autoComplete='off'
          placeholder={ formatMessage(defaultMessages.displayName) }
        />
      </div>
      <div className='input-field'>
        <Field
          component={ LocationField }
          type='text'
          name='location'
          autoComplete='off'
          placeholder={ formatMessage(defaultMessages.location) }
        />
      </div>
      <div className='input-field'>
        {/* workaround for disabling autocomplete */}
        <input
          style={ { position: 'fixed', width: 0, height: 0, border: 'none' } }
          type='text'
          name='fakeusernameremembered'
        />
        <Field
          component={ FieldWithErrors }
          type='password'
          name='password'
          autoComplete='off'
          placeholder={ formatMessage(defaultMessages.password) }
        />
      </div>
      <div className='input-field'>
        <Field
          component={ FieldWithErrors }
          type='password'
          name='passwordConfirmation'
          autoComplete='off'
          placeholder={ formatMessage(defaultMessages.passwordConfirmation) }
        />
      </div>
      <button type='submit' className='btn auth-btn' >
        <I18n text={ defaultMessages.acceptInvitation } />
      </button>
    </form>
  );
};
Form.propTypes = propTypes;

export default Form;
