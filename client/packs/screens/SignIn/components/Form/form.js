import React from 'react';
import { Field } from 'redux-form';
import { Link } from 'react-router-dom';
import { intlShape } from 'react-intl';
import { defaultMessages } from 'locales/default';
import { func, string } from 'prop-types';

import { paths } from 'layouts/constants';
import FieldWithErrors from 'components/FieldWithErrors';

const propTypes = {
  handleSubmit: func.isRequired,
  intl: intlShape.isRequired,
  countryCode: string.isRequired
};

const Form = ({ handleSubmit, intl: { formatMessage }, countryCode }) => {
  return (
    <form onSubmit={ handleSubmit } >
      <div className='input-field'>
        <Field
          name='email'
          component={ FieldWithErrors }
          type='text'
          placeholder={ formatMessage(defaultMessages.email) }
        />
      </div>
      <div className='input-field'>
        <Field
          name='password'
          component={ FieldWithErrors }
          type='password'
          placeholder={ formatMessage(defaultMessages.password) }
        />
      </div>
      <Link to={ paths.USERS_FORGOT_PASSWORD.replace(':country', countryCode) } className='forgot-text' >
        { formatMessage(defaultMessages.forgotPassword) }
      </Link>
      <button type='submit' className='btn auth-btn' >{ formatMessage(defaultMessages.login) }</button>
    </form>
  );
};
Form.propTypes = propTypes;

export default Form;
