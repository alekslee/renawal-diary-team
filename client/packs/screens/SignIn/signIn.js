import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { defaultMessages } from 'locales/default';
import { string, func, object } from 'prop-types';
import { toastr } from 'lib/helpers';

import SignUpModalLink from 'components/SignUpModalLink';
import { oauthSigninUrls } from './constants';
import { Form } from './components';
import I18n from 'components/I18n';
import { paths } from 'layouts/constants';

import GoogleImage from 'images/svg/google.svg';
import FacebookImage from 'images/svg/facebook.svg';
import TwitterImage from 'images/svg/twitt.svg';
import LinkedinImage from 'images/svg/linkedin.svg';

const propTypes = {
  history: object.isRequired,
  signInDispatch: func.isRequired
};

class SignIn extends Component {
  submitHandler = ({ email, password }) => {
    const { history, signInDispatch } = this.props;

    const callback = ({ country }) => {
      history.push(paths.ROOT.replace(':country', country.isoA2Code));
      toastr.success('', { component: <I18n text={ defaultMessages.deviseSessionsSignedIn } /> });
    };
    signInDispatch({ email, password, callback });
  };

  render() {
    const { submitHandler } = this;

    return (
      <section className='auth-container'>
        <div className='auth-block login-block'>
          <h3 className='auth-title'>
            <I18n text={ defaultMessages.login } />
          </h3>
          <Form onSubmit={ submitHandler } />
          <div className='sign-up-text'>
            <I18n text={ defaultMessages.dontHaveAnAccount } />
            <SignUpModalLink>
              <I18n text={ defaultMessages.signUp } />
            </SignUpModalLink>
          </div>
          <div className='or-block'>
            <div className='dash' />
            <span>
              <I18n text={ defaultMessages.or } />
            </span>
            <div className='dash' />
          </div>
          <div className='social-block'>
            <a href={ oauthSigninUrls.GOOGLE } className='btn btn-circle google-background'>
              <img src={ GoogleImage } alt='Google' className='google-img' />
            </a>
            <a href={ oauthSigninUrls.FACEBOOK } className='btn btn-circle facebook-background'>
              <img src={ FacebookImage } alt='Facebook' className='facebook-img' />
            </a>
            <a href={ oauthSigninUrls.TWITTER } className='btn btn-circle twitter-background'>
              <img src={ TwitterImage } alt='Twitter' className='twitt-img' />
            </a>
            <a href={ oauthSigninUrls.LINKEDIN } className='btn btn-circle linkedin-background'>
              <img src={ LinkedinImage } alt='Linkedin' className='linkedin-img' />
            </a>
          </div>
        </div>
      </section>
    );
  }
};
SignIn.propTypes = propTypes;

export default SignIn;
