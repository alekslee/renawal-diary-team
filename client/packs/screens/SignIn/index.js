import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import SignIn from './signIn';

import { actions } from 'core/currentUser';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    signInDispatch: actions.signIn
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SignIn));
