import React, { Component } from 'react';
import { func, object } from 'prop-types';
import { Lightbox } from "react-modal-image";
import { Form, NotesList } from './components';

const propTypes = {
  category: object.isRequired,
  fetchNoteListDispatch: func.isRequired,
};

class MyNotes extends Component {

  state = {
    notes: [],
    openImage: null,
  }

  openLightbox = (file) => {
    this.setState({ openImage: file });
  }

  closeLightbox = (file) => {
    this.setState({ openImage: null });
  }

  handleDownload = ({ url, filename, isNew, file }) => {
    const a = document.createElement("a");
          a.setAttribute("download", filename);

    if (isNew) {
      const b = new Blob([file]);
      a.href = URL.createObjectURL(b);
      a.click();
    } else {
      fetch(url).then(t =>
        t.blob().then((b)=>{
          a.href = URL.createObjectURL(b);
          a.click();
        })
      );
    }
  }

  componentDidMount = () => {
    this.fetchNotes();
  }

  fetchNotes = (year) => {
    const { category, fetchNoteListDispatch } = this.props;

    const callback = (notes) => {
      this.setState({ notes });
    }
    fetchNoteListDispatch({ categoryId: category.id, year, callback });
  }

  render() {
    const {
      fetchNotes,
      openLightbox,
      closeLightbox,
      handleDownload,
      state: { notes, openImage },
      props: {
        history,
        countryCode,
        change,
        category,
        currentUser,
        formAttributes,
        fetchNoteListDispatch,
        createNoteDispatch }
    } = this;

    return (
      <div className="notes-wrapper">
        <div className="notes-editor-container">
          <div className="notes-header">My notes</div>
          <div className="notes-editor">
            <Form
              { ...{ countryCode, history, change, category, currentUser, createNoteDispatch, fetchNotes, openLightbox, handleDownload, formAttributes }}
            />
          </div>
        </div>

        <NotesList
          { ...{ notes, fetchNotes, category, openLightbox, handleDownload }}
        />

        { openImage &&
          <Lightbox
            medium={ openImage.url }
            large={ openImage.url }
            alt={ openImage.filename }
            onClose={ closeLightbox }
          />
        }
      </div>
    );
  }
}

MyNotes.propTypes = propTypes;

export default MyNotes;
