import React, { Component, Fragment } from 'react';
import { object, func } from 'prop-types';
import { Field } from 'redux-form';

import DocumentIcon from 'images/icons/Document';

const propTypes = {};

class AttachedDocument extends Component {

  render() {
    const {
      props: { file, index, isNew, removeFile, openLightbox, handleDownload }
    } = this;

    let filename = file.filename;
    let filetype = file.contentType;
    let url = file.url;

    if (isNew) {
      filename = file.name;
      filetype = file.type;
      url = URL.createObjectURL(file);
    }

    const isImage = filetype.match(/image/);
    const removeIcon = removeFile ? (
          <span
            className="delete-file"
            onClick={(e) => {
              e.stopPropagation();
              removeFile(index);
            }}
          >
            X
          </span>
    ) : null;

    return (
      <Fragment >
        { isImage ?
        <div className="image-box">
          { removeIcon }
          <img
            onClick={() => openLightbox && openLightbox({ url, filename }) }
            src={ url }
            alt="image"
          />
        </div>
        :
        <div className="uploaded-file">
          <div className="image-box"
            onClick={() => handleDownload && handleDownload({ url, filename, isNew, file }) }
          >
            { removeIcon }
            <DocumentIcon />
          </div>
          <span
            onClick={() => handleDownload && handleDownload({ url, filename, isNew, file }) }
          > { filename } </span>
        </div> }
      </Fragment>
    );
  }
}

AttachedDocument.propTypes = propTypes;

export default AttachedDocument;
