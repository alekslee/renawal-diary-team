import React, { Component, Fragment } from 'react';
import { array, func } from 'prop-types';

import moment from 'moment';
import AttachedDocument from './attachedDocument';

const propTypes = {
  notes: array.isRequired,
  fetchNotes: func.isRequired,
};

class MyNotes extends Component {

  state = {
    isShort: true,
    year: moment().year(),
    showMoreBodies: {},
  }

  showMoreHandler = () => {
    this.setState({ isShort: false });
  }

  showLessHandler = () => {
    this.setState({ isShort: true });
  }

  showMoreBodyHandle = (id) => {
    const { showMoreBodies } = this.state;
    showMoreBodies[id] = true;
    this.setState({ showMoreBodies });
  }

  changeYear = (type) => {
    let year = this.state.year;

    if( type === 'next' ) {
      year = moment(year, 'YYYY').add(1, 'year').year();
    } else {
      year = moment(year, 'YYYY').subtract(1, 'year').year();
    }
    this.setState({ year });

    this.props.fetchNotes(`${year}`);
  }

  render() {
    const { showMoreHandler,
            showLessHandler,
            showMoreBodyHandle,
            changeYear,
            props: { notes,
              closeLightbox,
              openLightbox,
              handleDownload },
            state: { isShort, showMoreBodies, year }
          } = this;

    const list = isShort ? notes.slice(0, 3) : notes;

    const splitByN = (fullText) => {
      const textArrs = (fullText || '').trim().split(/\n|\r/);

      return textArrs.map(((text, ind) => (
        <Fragment key={ind}>
          { text }
          { ind !== (textArrs.length - 1) && <br /> }
        </Fragment>
      )))
    }

    return (
      <div className="notes-container">
        { list.length > 0 &&
        <div className="notes-list-header">
          <div className="notes-count">
            { `${notes.length} Notes` }
          </div>
          <div className="year-switcher">
            <span className="arrow prev" onClick={() => changeYear('prev') }/>
            <span className="year"> { year } </span>
            <span className="arrow next" onClick={() => changeYear('next') } />
          </div>
        </div>
        }

        <div>
          { list.map(({ id, title, body, files, createdAt }) => (
            <div
              key={ id }
              className="note-item"
            >
              <div className="note-header">

                <span className="title">
                  { title }
                </span>

                <span className="date">
                  { createdAt && moment(createdAt).fromNow() || 'Unknown' }
                </span>
              </div>

              <div className="note-body">
                { showMoreBodies[id] ? (
                  <p> { splitByN(body) } </p>
                  ) : (
                  <p>
                    { splitByN((body || '').slice(0, 100)).slice(0, 1)}

                    { ((body || '').length > 100 || splitByN(body || '').length > 3) &&
                      <span>
                        ... &nbsp;
                        <span
                          className="see-more-text"
                          onClick={ () => showMoreBodyHandle(id) }
                        >
                            See more
                        </span>
                      </span>
                     }
                  </p>
                  )
                 }

                { files.length > 0 &&
                  <div className="attached-files">
                    { files.map((file, index) =>
                      <AttachedDocument key={index}
                        file={ file }
                        openLightbox={ openLightbox }
                        handleDownload={ handleDownload }
                      />
                    )}
                  </div> }
              </div>
            </div>
          ))}

          { notes.length > 3 &&
            ( isShort ?
              <div className='see-more' >
                <div className="show-more-wrap" onClick={ showMoreHandler }>
                  <span>
                    Show more
                    {/*<span className="show-more-icon"/>*/}
                  </span>
                </div>
              </div>
              :
              <div className='see-more' >
                <div className="show-more-wrap" onClick={ showLessHandler }>
                  <span>
                    Show less
                    {/*<span className="show-less-icon" />*/}
                  </span>
                </div>
              </div>
            )
          }
        </div>

      </div>
    );
  }
}

MyNotes.propTypes = propTypes;

export default MyNotes;
