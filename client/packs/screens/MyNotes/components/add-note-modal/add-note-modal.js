import React, { Component } from 'react';
import { bool, func, object } from 'prop-types';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Form } from '../../components';
import { Lightbox } from "react-modal-image";

import Logo from 'images/calendar.svg';

const propTypes = {
  isModalOpen: bool.isRequired,
  toggle: func.isRequired,
  category: object.isRequired,
  changePickCategoriesStepDispatch: func.isRequired,
};

class AddNoteModal extends Component {

  state = {
    notes: [],
    openImage: null,
  }

  openLightbox = (file) => {
    this.setState({ openImage: file });
  }

  closeLightbox = (file) => {
    this.setState({ openImage: null });
  }

  componentDidMount = () => {
    console.log('todo:67 uncoment note button');
    this.mounted = true;
    // this.changePickCategoriesStep();
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  changePickCategoriesStep = () => {
    const {currentUser, changePickCategoriesStepDispatch} = this.props;

    if (!currentUser.id) {
      changePickCategoriesStepDispatch({ pickCategoriesStep: false });
    }
  }

  render() {
    const {
      openLightbox,
      closeLightbox,
      handleDownload,
      state: { notes, openImage },
      props: {
        history,
        countryCode,
        change,
        category,
        currentUser,
        formAttributes,
        createNoteDispatch,
        isModalOpen,
        toggle }
    } = this;

    if (true) {
      return null;
    }

    if (!isModalOpen) {
      return null;
    }

    return (
      <Modal
        isOpen={ isModalOpen }
        toggle={ () => toggle() }
        className={ `add-note-modal ${ category.root.enName }` }
      >
        <ModalHeader toggle={ () => toggle() }>
          Add new note
        </ModalHeader>

        <ModalBody>
          <div className="notes-wrapper">
            <div className="notes-editor-container">
              <div className="notes-header">My notes</div>
              <div className="notes-editor">
                <Form
                  saveCallback={() => {
                    toggle('isModalOpen')
                  }}
                  { ...{ countryCode, history, change, category, currentUser, createNoteDispatch, fetchNotes, openLightbox, formAttributes }}
                />
              </div>
            </div>

            { openImage &&
              <Lightbox
                medium={ openImage.url }
                large={ openImage.url }
                alt={ openImage.filename }
                onClose={ closeLightbox }
              />
            }
          </div>
        </ModalBody>

      </Modal>
    );
  }
}

AddNoteModal.propTypes = propTypes;

export default AddNoteModal;
