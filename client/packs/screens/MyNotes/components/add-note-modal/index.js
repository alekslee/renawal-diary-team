import NoteModal from './add-note-modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, getFormValues } from 'redux-form';
import { actions } from 'core/notes';
import { selectors as categorySelectors } from 'core/category';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { withRouter } from 'react-router-dom';
import { actions as signUpActions } from 'core/signUp';

const mapStateToProps = (state, props) => ({
  formAttributes: getFormValues(props.form)(state),
  category: categorySelectors.getCategory(state),
  currentUser: currentUserSelectors.getCurrentUser(state),
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    createNoteDispatch: actions.createNote,
    fetchNoteListDispatch: actions.fetchList,
    changePickCategoriesStepDispatch: signUpActions.changePickCategoriesStep,
  }, dispatsh)
);

export default reduxForm({ form: 'noteForm' })(
  connect(mapStateToProps, mapDispatchToProps)(withRouter(NoteModal))
);
