import React, { Component, Fragment } from 'react';
import { object, func, string } from 'prop-types';
import { Field } from 'redux-form';
import FieldWithErrors from 'components/FieldWithErrors';
import { toastr } from 'lib/helpers';
import { paths } from 'layouts/constants';

import UploadFile from 'images/icons/UploadFile';
import UploadImage from 'images/icons/UploadImage';
import SignInModalLink from 'components/SignInModalLink';
import AttachedDocument from './attachedDocument';

const propTypes = {
  category: object,
  currentUser: object,
  formAttributes: object,
  createNoteDispatch: func.isRequired,
  fetchNotes: func.isRequired,
  saveCallback: func,
  countryCode: string.isRequired,
  history: object.isRequired,
};

class NoteForm extends Component {

  state = {
    files: []
  }

  onFileAdd = (ev) => {
    let { files } = this.state;
    files = [...files].concat( Array.from(ev.target.files) );
    files = files.sort((a,b) => a.type.match(/image/) ? -1 : 1 )
    this.setState({ files });
  }

  removeFile = (index) => {
    let { files } = this.state;
    delete files[index];
    files = files.filter(f => f);
    this.setState({ files });
  }

  render() {
    const {
      removeFile,
      state: { files, loading },
      props: { category,
        history,
        countryCode,
        change,
        fetchNotes,
        currentUser,
        formAttributes,
        saveCallback,
        createNoteDispatch,
        closeLightbox,
        openLightbox,
        handleDownload }
    } = this;

    const submitCreate = (afterCallback=null) => {

      let callback = (resp) => {

        if (afterCallback) {
          afterCallback();
        } else {
          fetchNotes();
          change('title', null);
          change('body', null);
          this.setState({ loading: false, files: [] });
        }
        saveCallback && saveCallback();
        toastr.success('Saved!');
      };


      const errorCallback = (res) => {
        if (afterCallback) {
          afterCallback();
        } else {
          this.setState({ loading: false });
        }
      }

      const { title, body } = (formAttributes || {});
      const categoryId = category.id;
      this.setState({ loading: true });
      createNoteDispatch({ categoryId, title, body, files, callback, errorCallback });
    }

    return (
      <Fragment>
        <form>

          <Field
            name="title"
            className="reminder-input"
            placeholder="Enter your title.."
            component={ FieldWithErrors }
          />

          <Field
            name="body"
            placeholder="Write your note here…"
            componentType="textarea"
            component={ FieldWithErrors }
          />

          { files.length > 0 && (
            <div className="attached-files --new">
              { files.map((file, index) => (
                <AttachedDocument isNew key={index}
                  { ...{ index, file, removeFile, handleDownload, openLightbox } }
                />
              ))}
            </div>
          )}

          <div className="form-footer" >

            <div className="files-box">
              <label className="file-uploader">
                <UploadImage />
                <input
                  className="hidden"
                  type="file"
                  name="picture"
                  id='picture'
                  multiple
                  onChange={ this.onFileAdd.bind(this) }
                />
              </label>
              <label className="file-uploader">
                <UploadFile />
                <input
                  className="hidden"
                  type="file"
                  name="atach"
                  id='atach'
                  multiple
                  onChange={ this.onFileAdd.bind(this) }
                />
              </label>
            </div>
            { currentUser.id ?
              <button
                className="btn"
                type="button"
                onClick={() => submitCreate()}
                disable={`${loading}`}
              >
                { loading && <span className="spinner-border spinner-border-sm mr-3" role="status" aria-hidden="true" /> }

                Save
              </button> :
              <SignInModalLink
                onSignUpSuccess={() => {
                  submitCreate(() => {
                    history.push(
                      paths.MY_NOTES.replace(':country', countryCode).replace(':slag', category.slag)
                    );
                  });
                }}
              >
                <button
                  className="btn"
                  type="button"
                  onClick={(e) => {
                    if (!(formAttributes || {}).title && files.length == 0) {
                      toastr.error('Error!', 'Title or attachments cant be blank');
                      e.stopPropagation();
                    }
                  }}
                  disable={`${loading}`}
                >
                  { loading && <span className="spinner-border spinner-border-sm mr-3" role="status" aria-hidden="true"></span> }

                  Save
                </button>
              </SignInModalLink>
            }
          </div>

        </form>

      </Fragment>
    );
  }
}

NoteForm.propTypes = propTypes;

export default NoteForm;
