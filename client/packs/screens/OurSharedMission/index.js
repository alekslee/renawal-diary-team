
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import OurSharedMission from './ourSharedMission';

import { actions as categoryActions } from 'core/category';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchCategoriesDispatch: categoryActions.fetchCategories,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(OurSharedMission);