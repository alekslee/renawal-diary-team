import React, { Fragment, Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import {} from 'prop-types';

import { defaultMessages } from 'locales/default';
import I18n from 'components/I18n';
import CopyOrShare from 'components/CopyOrShare';
import SocialShareButtons from 'components/SocialShareButtons';

const propTypes = {};

class OurSharedMission extends Component {

  componentDidMount() {
    this.props.fetchCategoriesDispatch({ countryCode: this.props.countryCode });
  }

  render() {

    return (
      <Fragment>
        <section className='mission-header'>
          <Container>
            <h1 className='mission-title'>
              <I18n text={ defaultMessages.ourSharedMission } />
            </h1>
            <p>
              <I18n text={ defaultMessages.weWantEveryCustomerInTheWorldToPurchaseFromBusinesses } />
            </p>
          </Container>
        </section>
        <section className='mission-content'>
          <Container>
            <Row>
              <Col lg='6'>
                <div className='mission-text'>
                  <h3>
                    <I18n text={ defaultMessages.howCanWeAchieveThis } />
                  </h3>
                  <p>
                    <I18n text={ defaultMessages.weCanOnlyAchieveThisThroughTheCoOperation } />
                  </p>
                  <p>
                    <I18n text={ defaultMessages.unlikeSearchEnginesWeBelieveBusinesses } />
                  </p>
                  <p>
                    <I18n text={ defaultMessages.yourDecisionsAndInsightsAreSharedOnYourGroupLeaderboard } />
                  </p>
                  <p>
                    <I18n text={ defaultMessages.ifYouWouldLikeToCreateAMoreTransparentWorld } />
                  </p>
                </div>
              </Col>
              <Col lg='6'>
                <div className='mission-invite'>
                  <div className='mission-invite-inner main-social copy-white'>
                    <h3 className='social-title'>
                      <I18n text={ defaultMessages.inviteTheWorld } />
                    </h3>
                    <SocialShareButtons />
                    <CopyOrShare />
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </section>
      </Fragment>
    );
  }
};
OurSharedMission.propTypes = propTypes;

export default OurSharedMission;
