import React, { Component } from 'react';
import { toastr } from 'lib/helpers';
import { intlShape } from 'react-intl';
import { object, func, string } from 'prop-types';

import { defaultMessages } from 'locales/default';
import I18n from 'components/I18n';
import Loader from 'components/Loader';
import { Form } from './components';
import { paths } from 'layouts/constants';

const propTypes = {
  intl: intlShape.isRequired,
  history: object.isRequired,
  resetPasswordDispatch: func.isRequired,
  countryCode: string.isRequired
};

class ForgotPassword extends Component {
  state = {
    inProcess: false
  };

  submitHandler = ({ email }) => {
    const { history, resetPasswordDispatch, countryCode, intl: { formatMessage } } = this.props;

    const callback = () => {
      this.setState({ inProcess: false });
      history.push(paths.USERS_SIGN_IN.replace(':country', countryCode));
      toastr.success(formatMessage(defaultMessages.devisePasswordsSendInstructions));
    };

    const errorCallback = () => {
      this.setState({ inProcess: false });
    };

    this.setState({ inProcess: true });
    resetPasswordDispatch({ email, callback, errorCallback });
  };

  render() {
    const {
      submitHandler,
      state: { inProcess }
    } = this;

    return (
      <div>
        { inProcess && <Loader /> }
        <section className='auth-container'>
          <div className='auth-block forgot-password'>
            <h3 className='auth-title'>
              <I18n text={ defaultMessages.forgotPasswordPageTitle } />
            </h3>
            <p className='forgot-password-text'>
              <I18n text={ defaultMessages.forgotPasswordPageP1 } />
            </p>
            <Form onSubmit={ submitHandler } />
          </div>
        </section>
      </div>
    );
  }
};
ForgotPassword.propTypes = propTypes;

export default ForgotPassword;
