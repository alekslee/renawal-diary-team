import React from 'react';
import { Field } from 'redux-form';
import { intlShape } from 'react-intl';
import { Link } from 'react-router-dom';
import { func, string } from 'prop-types';

import { defaultMessages } from 'locales/default';
import I18n from 'components/I18n';
import { paths } from 'layouts/constants';

const propTypes = {
  intl: intlShape.isRequired,
  handleSubmit: func.isRequired,
  countryCode: string.isRequired
};

const Form = ({ handleSubmit, countryCode, intl: { formatMessage } }) => {
  return (
    <form onSubmit={ handleSubmit }>
      <div className='input-field'>
        <Field
          type='text'
          name='email'
          component='input'
          autoComplete='email'
          placeholder={ formatMessage(defaultMessages.forgotPasswordPageEmailPlaceholder) }
          autoFocus
        />
      </div>
      <button type='submit' className='btn auth-btn' >{ formatMessage(defaultMessages.forgotPasswordPageSend) }</button>
      <div className='forgot-text'>
        <I18n text={ defaultMessages.forgotPasswordPageRemeberPassword } />
        &nbsp;
        <Link to={ paths.USERS_SIGN_IN.replace(':country', countryCode) } >
          <I18n text={ defaultMessages.forgotPasswordPageLogin } />
        </Link>
      </div>
    </form>
  );
};
Form.propTypes = propTypes;

export default Form;
