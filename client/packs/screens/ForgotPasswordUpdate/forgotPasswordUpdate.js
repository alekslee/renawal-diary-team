import React, { Component } from 'react';
import { intlShape } from 'react-intl';
import { shape, string, object, func } from 'prop-types';
import { toastr } from 'lib/helpers';
import queryString from 'query-string';

import { defaultMessages } from 'locales/default';
import I18n from 'components/I18n';
import Loader from 'components/Loader';
import { Form } from './components';
import { paths } from 'layouts/constants';

const propTypes = {
  history: object.isRequired,
  location: object.isRequired,
  countryCode: string.isRequired,
  changeUserResetedPasswordDispatch: func.isRequired,
  intl: intlShape.isRequired
};

class ForgotPasswordUpdate extends Component {
  state = {
    inProcess: false
  };

  submitHandler = ({ password, passwordConfirmation }) => {
    const {
      props: {
        location,
        history,
        countryCode,
        changeUserResetedPasswordDispatch,
        intl: { formatMessage }
      }
    } = this;
    const callback = () => {
      this.setState({ inProcess: false });
      history.push(paths.ROOT.replace(':country', countryCode));
      toastr.success(formatMessage(defaultMessages.devisePasswordsUpdated));
    };

    const errorCallback = () => {
      this.setState({ inProcess: false });
    };

    this.setState({ inProcess: true });
    const resetPasswordToken = queryString.parse(location.search).reset_password_token || '';
    changeUserResetedPasswordDispatch({ password, passwordConfirmation, resetPasswordToken, callback, errorCallback });
  };

  render() {
    const {
      submitHandler,
      state: { inProcess }
    } = this;

    return (
      <div>
        { inProcess && <Loader /> }
        <section className='auth-container'>
          <div className='auth-block forgot-password'>
            <h3 className='auth-title'>
              <I18n text={ defaultMessages.forgotPasswordUpdatePageTitle } />
            </h3>
            <Form onSubmit={ submitHandler } />
          </div>
        </section>
      </div>
    );
  }
};
ForgotPasswordUpdate.propTypes = propTypes;

export default ForgotPasswordUpdate;
