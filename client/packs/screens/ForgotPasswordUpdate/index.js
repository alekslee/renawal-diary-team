import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import ForgotPasswordUpdate from './forgotPasswordUpdate';

import { actions } from 'core/currentUser';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    changeUserResetedPasswordDispatch: actions.changeUserResetedPassword
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(ForgotPasswordUpdate)));
