import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';
import { injectIntl } from 'react-intl';
import Form from './form';

import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
  }, dispatsh)
);

const validate = ({ password, passwordConfirmation }) => {
  const errors = {};
  if (!password) errors.password = 'Required';
  if (!passwordConfirmation) errors.passwordConfirmation = 'Required';
  if (passwordConfirmation !== password) errors.passwordConfirmation = 'Password doesn\'t match';
  return errors;
};

export default reduxForm({
  form: 'forgotPasswordUpdate',
  validate
})(
  connect(mapStateToProps, mapDispatchToProps)(injectIntl(Form))
);
