import React from 'react';
import { Field } from 'redux-form';
import { intlShape } from 'react-intl';
import { Link } from 'react-router-dom';
import { func, string } from 'prop-types';

import { defaultMessages } from 'locales/default';
import FieldWithErrors from 'components/FieldWithErrors';
import I18n from 'components/I18n';
import { paths } from 'layouts/constants';

const propTypes = {
  intl: intlShape.isRequired,
  handleSubmit: func.isRequired,
  countryCode: string.isRequired
};

const Form = ({ handleSubmit, countryCode, intl: { formatMessage } }) => {
  return (
    <form onSubmit={ handleSubmit } >
      <div className='input-field'>
        <Field
          component={ FieldWithErrors }
          type='password'
          name='password'
          autoComplete='off'
          placeholder={ formatMessage(defaultMessages.forgotPasswordUpdatePageNewPassword) }
        />
      </div>
      <div className='input-field'>
        <Field
          component={ FieldWithErrors }
          type='password'
          name='passwordConfirmation'
          autoComplete='off'
          placeholder={ formatMessage(defaultMessages.forgotPasswordUpdatePageConfirmPassword) }
        />
      </div>
      <button type='submit' className='btn auth-btn' >
        <I18n text={ defaultMessages.forgotPasswordUpdatePageButton } />
      </button>
      <div className='forgot-text'>
        <I18n text={ defaultMessages.forgotPasswordUpdatePageRememberPassword } />
        &nbsp;
        <Link to={ paths.USERS_SIGN_IN.replace(':country', countryCode) } >
          <I18n text={ defaultMessages.forgotPasswordUpdatePageLogin } />
        </Link>
      </div>
    </form>
  );
};
Form.propTypes = propTypes;

export default Form;
