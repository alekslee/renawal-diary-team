import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { stopSubmit } from 'redux-form';
import ResendUnlock from './resendUnlock';

import { selectors, actions } from 'core/currentUser';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  resendUnlockInstructionsFormErrors: selectors.getCurrentUserResendUnlockFormErrors(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    resendCurrentUserUnlockInstructionsDispatch: actions.resendCurrentUserUnlockInstructions,
    setCurrentUserDispatch: actions.setCurrentUser,
    setFormErrorDispatch: error => stopSubmit('resendUnlock', { email: error }),
    setCurrentUserUnlockInstructionsErrorsDispatch: actions.setCurrentUserUnlockInstructionsErrors
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ResendUnlock));
