import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { toastr } from 'lib/helpers';
import { object, string, func, array } from 'prop-types';

import { paths } from 'layouts/constants';
import Loader from 'components/Loader';
import I18n from 'components/I18n';
import { Form } from './components';
import { defaultMessages } from 'locales/default';

const propTypes = {
  currentUser: object,
  countryCode: string.isRequired,
  history: object.isRequired,
  resendUnlockInstructionsFormErrors: array,
  resendCurrentUserUnlockInstructionsDispatch: func.isRequired,
  setFormErrorDispatch: func.isRequired,
  setCurrentUserDispatch: func.isRequired,
  setCurrentUserUnlockInstructionsErrorsDispatch: func.isRequired
};

class ResendUnlock extends Component {
  state = {
    inProcess: false
  };

  componentWillUnmount() {
    const { setCurrentUserDispatch, setCurrentUserUnlockInstructionsErrorsDispatch } = this.props;
    setCurrentUserDispatch({ currentUser: {} });
    setCurrentUserUnlockInstructionsErrorsDispatch({});
  }

  submitHandler = ({ email }) => {
    const {
      history,
      currentUser,
      countryCode,
      setFormErrorDispatch,
      resendUnlockInstructionsFormErrors,
      resendCurrentUserUnlockInstructionsDispatch
    } = this.props;

    if (
      resendUnlockInstructionsFormErrors
      && resendUnlockInstructionsFormErrors.length
      && currentUser.email === email
    ) {
      return setFormErrorDispatch(resendUnlockInstructionsFormErrors[0]);
    }

    const callback = () => {
      if (currentUser.id) {
        history.push(paths.ROOT.replace(':country', countryCode));
      } else {
        history.push(paths.USERS_SIGN_IN.replace(':country', countryCode));
      }
      toastr.success('', { component: <I18n text={ defaultMessages.deviseUnlocksSendInstructions } /> });
    };

    const errorCallback = () => {
      this.setState({ inProcess: false });
    };

    this.setState({ inProcess: true });
    resendCurrentUserUnlockInstructionsDispatch({ email, callback, errorCallback });
  };

  render() {
    const {
      submitHandler,
      state: { inProcess },
      props: { currentUser }
    } = this;

    return (
      <section className='auth-container'>
        { inProcess && <Loader /> }
        <div className='auth-block resend-unlock' >
          <h3 className='auth-title' >
            <I18n text={ defaultMessages.resendUnlockInstructions } />
          </h3>
          <Form initialValues={ { email: currentUser.email } } onSubmit={ submitHandler } />
        </div>
      </section>
    );
  }
};
ResendUnlock.propTypes = propTypes;

export default ResendUnlock;
