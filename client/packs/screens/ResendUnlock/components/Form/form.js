import React from 'react';
import { Field } from 'redux-form';
import { func } from 'prop-types';
import { intlShape } from 'react-intl';

import FieldWithErrors from 'components/FieldWithErrors';
import I18n from 'components/I18n';
import { defaultMessages } from 'locales/default';

const propTypes = {
  handleSubmit: func.isRequired,
  intl: intlShape.isRequired
};

const Form = ({ handleSubmit, intl: { formatMessage } }) => {
  return (
    <form onSubmit={ handleSubmit } >
      <div className='input-field' >
        <Field
          name='email'
          component={ FieldWithErrors }
          type='text'
          placeholder={ formatMessage(defaultMessages.email) }
        />
      </div>
      <button type='submit' className='btn auth-btn'>
        <I18n text={ defaultMessages.resendUnlockInstructions } />
      </button>
    </form>
  );
};
Form.propTypes = propTypes;

export default Form;
