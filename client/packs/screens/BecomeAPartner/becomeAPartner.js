import React, { Fragment, Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { toastr } from 'lib/helpers';
import { intlShape } from 'react-intl';
import { func } from 'prop-types';

import I18n from 'components/I18n';
import Loader from 'components/Loader';
import { defaultMessages } from 'locales/default';
import { Form } from './components';

import HeartImage from 'images/svg/heart.svg';

const propTypes = {
  intl: intlShape.isRequired,
  becomeAPartnerSendMailDispatch: func.isRequired,
  clearFormDispatch: func.isRequired
};

class BecomeAPartner extends Component {
  state = {
    inProcess: false
  };

  componentDidMount() {
    this.props.fetchCategoriesDispatch({ countryCode: this.props.countryCode });
  }

  submitHandler = ({ name, email, body }) => {
    const { becomeAPartnerSendMailDispatch, clearFormDispatch, intl: { formatMessage } } = this.props;

    const callback = () => {
      toastr.success(formatMessage(defaultMessages.becomeAPartnerPageSuccessfullyEmailSend));
      clearFormDispatch('becomeAPartnerSendMail');
      this.setState({ inProcess: false });
    };
    const errorCallback = () => {
      this.setState({ inProcess: false });
    };

    this.setState({ inProcess: true });
    becomeAPartnerSendMailDispatch({ name, email, body, callback, errorCallback });
  };

  render() {
    const {
      submitHandler,
      state: { inProcess }
    } = this;

    return (
      <Fragment>
        <section className='partner-header'>
          { inProcess && <Loader /> }
          <Container>
            <Row>
              <Col lg='6'>
                <h2 className='partner-title'>
                  <I18n text={ defaultMessages.theRenewalDiaryCommunity } />
                  <br />
                  <img src={ HeartImage } alt='heart' className='heart' />
                  <I18n text={ defaultMessages.greatBusinesses } />
                </h2>
              </Col>
            </Row>
          </Container>
        </section>
        <section className='partner-content'>
          <Container>
            <Row>
              <Col lg='5'>
                <p className='partner-text'>
                  <I18n text={ defaultMessages.becomeAPartnerPageP1 } />
                </p>
                <p className='partner-text'>
                  <I18n text={ defaultMessages.becomeAPartnerPageP2 } />
                </p>
              </Col>
              <Col lg='7'>
                <div className='partner-form-container'>
                  <Form
                    initialValues={ { name: '', email: '', body: '' } }
                    ref={ node => this.form = node }
                    onSubmit={ submitHandler }
                  />
                </div>
              </Col>
            </Row>
          </Container>
        </section>
      </Fragment>
    );
  }
};
BecomeAPartner.propTypes = propTypes;

export default BecomeAPartner;
