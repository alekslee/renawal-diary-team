import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { injectIntl } from 'react-intl';
import { reset } from 'redux-form';
import BecomeAPartner from './becomeAPartner';

import { actions as categoryActions } from 'core/category';
import { actions } from 'core/becomeAPartnerScreen';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchCategoriesDispatch: categoryActions.fetchCategories,
    becomeAPartnerSendMailDispatch: actions.becomeAPartnerSendMail,
    clearFormDispatch: reset
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(BecomeAPartner));