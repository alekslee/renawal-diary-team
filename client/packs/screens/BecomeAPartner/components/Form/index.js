import { reduxForm } from 'redux-form';
import Form from './form';

const validate = ({ email, body }) => {
  const errors = {};
  if (!email) errors.email = 'Required';
  if (!body) errors.body = 'Required';
  return errors;
};

export default reduxForm({ form: 'becomeAPartnerSendMail', validate })(Form);
