import React from 'react';
import { Field } from 'redux-form';
import { func } from 'prop-types';

import { defaultMessages } from 'locales/default';
import I18n from 'components/I18n';
import FieldWithErrors from 'components/FieldWithErrors';

const propTypes = {
  handleSubmit: func.isRequired,
};

const Form = ({ handleSubmit }) => {
  return (
    <form className='partner-form' onSubmit={ handleSubmit } >
      <h3 className='partner-form-title'>
        <I18n text={ defaultMessages.becomeAPartnerPageBecomeAPartner } />
      </h3>
      <p className='partner-form-text'>
        <I18n text={ defaultMessages.becomeAPartnerPageP3 } />
      </p>
      <div className='input-group'>
        <Field
          type='text'
          component={ FieldWithErrors }
          name='name'
          placeholder='Name'
        />
      </div>
      <div className='input-group'>
        <Field
          type='text'
          component={ FieldWithErrors }
          name='email'
          placeholder='Email'
        />
      </div>
      <div className='input-group textarea-input'>
        <label>
          <I18n text={ defaultMessages.becomeAPartnerPageYourMessage } />:
        </label>
        <Field
          component={ FieldWithErrors }
          componentType='textarea'
          name='body'
        />
      </div>
      <button type='submit' className='btn send-button'>
        <I18n text={ defaultMessages.becomeAPartnerPageSend } />
      </button>
    </form>
  );
};
Form.propTypes = propTypes;

export default Form;
