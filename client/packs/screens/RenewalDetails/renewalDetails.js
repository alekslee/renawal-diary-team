import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { toastr } from 'lib/helpers';
import classNames from 'classnames';
import { func, object, shape, string, bool } from 'prop-types';

import { Form, Header } from './components';
import BenefitsInfoBlock from 'components/BenefitsInfoBlock';
import { paths } from 'layouts/constants';
import { retrieveFormInitialValues } from 'lib/helpers/renewalDetails';

const propTypes = {
  category: shape({
    id: string.isRequired
  }).isRequired,
  renewalDetail: object,
  isRenewalDetailExists: bool.isRequired,
  currentUser: object.isRequired,
  history: object.isRequired,
  countryCode: string.isRequired,
  criteriaQuestionsOptions: object,
  pricesQuestionsOptions: object,
  updateRenewalDetailDispatch: func.isRequired,
  createRenewalDetailDispatch: func.isRequired,
  fetchRenewalDetailScreenDataDispatch: func.isRequired,
  setRenewalDetailDispatch: func.isRequired,
  setRenewalDetailFormOptionsDispatch: func.isRequired
};

class RenewalDetails extends Component {
  state = {
    isLoading: true,
    inProcess: false
  };

  componentDidMount() {
    const { fetchRenewalDetailScreenDataDispatch, category } = this.props;

    const callback = () => this.setState({ isLoading: false });
    fetchRenewalDetailScreenDataDispatch({ categoryId: category.id, callback, errorCallback: callback });
  };

  componentWillUnmount() {
    const { setRenewalDetailDispatch, setRenewalDetailFormOptionsDispatch } = this.props;
    setRenewalDetailDispatch({ renewalDetail: {} });
    setRenewalDetailFormOptionsDispatch({ formOptions: {} });
  };

  submitHandler = fields => {
    const {
      updateRenewalDetailDispatch,
      createRenewalDetailDispatch,
      category,
      countryCode,
      history,
      isRenewalDetailExists
    } = this.props;

    const callback = () => {
      this.setState({ inProcess: false });
      if (isRenewalDetailExists) {
        toastr.success('Renewal details succesfully updated!');
      } else {
        toastr.success('Renewal details succesfully created!');
      }
      history.push(paths.LEADERBOARD.replace(':country', countryCode).replace(':slag', category.slag));
    };

    const errorCallback = () => {
      this.setState({ inProcess: false });
    };

    let params;
    if (fields.usingProvider) {
      params = {
        ...fields,
        criteriaQuestions: JSON.stringify(fields.criteriaQuestions),
        pricesQuestions: JSON.stringify(fields.pricesQuestions)
      };
    } else {
      params = {
        usingProvider: fields.usingProvider,
        criteriaQuestions: JSON.stringify(fields.criteriaQuestions)
      };
    }

    this.setState({ inProcess: true });
    const attributes = { params, callback, errorCallback };
    if (isRenewalDetailExists) {
      updateRenewalDetailDispatch(attributes);
    } else {
      createRenewalDetailDispatch(attributes);
    }
  };

  retrieveInitialValues = () => {
    const {
      isRenewalDetailExists,
      renewalDetail,
      criteriaQuestionsOptions,
      pricesQuestionsOptions,
      currentUser,
      category
    } = this.props;

    return retrieveFormInitialValues({
      isRenewalDetailExists,
      renewalDetail,
      criteriaQuestionsOptions,
      pricesQuestionsOptions,
      currentUser,
      category
    });
  };

  render() {
    const {
      submitHandler,
      retrieveInitialValues,
      state: { inProcess, isLoading },
      props: { renewalDetail, isRenewalDetailExists }
    } = this;


    const isDisabledForm = renewalDetail.year && renewalDetail.year !== new Date().getFullYear();
    return (
      <div className='details-wrapper'>
        <div className='reminder-main renewal-details'>
          <Header />
          <div className={ classNames('reminder-main-body', { 'disabled': isDisabledForm }) }>
            <Form
              initialValues={ retrieveInitialValues() }
              disabled={ isDisabledForm }
              isRenewalDetailExists={ isRenewalDetailExists }
              enableReinitialize
              onSubmit={ submitHandler }
            />
          </div>
        </div>
        <BenefitsInfoBlock />
      </div>
    );
  };
};
RenewalDetails.propTypes = propTypes;

export default RenewalDetails;
