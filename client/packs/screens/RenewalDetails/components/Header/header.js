import React, { Component } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { bool, object, string, number, array, func } from 'prop-types';

import DropdownComponent from 'components/DropdownComponent';
import Loader from 'components/Loader';

const propTypes = {
  isRenewalDetailExists: bool.isRequired,
  renewalDetailYear: number.isRequired,
  renewalDetailPossibleYears: array.isRequired,
  category: object.isRequired,
  countryCode: string.isRequired,
  fetchRenewalDetailDispatch: func.isRequired
};

class Header extends Component {
  state = {
    inProcess: false
  };

  loadRenewalDetailPerYear = year => {
    const { fetchRenewalDetailDispatch, category } = this.props;

    const callback = () => this.setState({ inProcess: false });

    this.setState({ inProcess: true });
    fetchRenewalDetailDispatch({ categoryId: category.id, year, callback, errorCallback: callback });
  };

  render() {
    const {
      loadRenewalDetailPerYear,
      state: { inProcess },
      props: { isRenewalDetailExists, category, countryCode, renewalDetailYear, renewalDetailPossibleYears }
    } = this;

    return (
      <div className='reminder-main-header'>
        { inProcess && <Loader /> }
        <h3>My { category.name } Details ({ countryCode.toUpperCase() })</h3>
        <div className='header-dropdowns'>
          <DropdownComponent
            className='year-dropdown'
            options={ renewalDetailPossibleYears }
            selected={ renewalDetailYear }
            onChange={ loadRenewalDetailPerYear }
          />
          {
            isRenewalDetailExists &&
              <DropdownComponent
                className='details-dropdown'
                options={ [`${category.name} policy 1`, `${category.name} policy 2`] }
                selected={ `${category.name} policy 1` }
                onChange={ () => {} }
              />
          }
        </div>
      </div>
    );
  };
};
Header.propTypes = propTypes;

export default Header;
