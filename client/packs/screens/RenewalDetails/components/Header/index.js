import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from './header';

import { selectors, actions } from 'core/renewalDetail';
import { selectors as categorySelectors } from 'core/category';
import { selectors as currentCountrySelectors } from 'core/currentCountry';

const mapStateToProps = state => ({
  isRenewalDetailExists: selectors.getIsRenewalDetailExists(state),
  renewalDetailYear: selectors.getRenewalDetailYear(state),
  renewalDetailPossibleYears: selectors.getRenewalDetailPossibleYears(state),
  category: categorySelectors.getCategory(state),
  countryCode: currentCountrySelectors.getCurrentCountryCode(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchRenewalDetailDispatch: actions.fetchRenewalDetail
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(Header);
