import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import { toastr } from 'lib/helpers';
import { string, func, bool } from 'prop-types';

import { Edit, New } from './components';

const propTypes = {
  handleSubmit: func.isRequired,
  change: func.isRequired,
  invalid: bool.isRequired,
  submitFailed: bool,
  dirty: bool.isRequired,
  form: string,
  disabled: bool,
  isRenewalDetailExists: bool.isRequired
};

const Form = ({ change, dirty, form, invalid, submitFailed, handleSubmit, disabled, isRenewalDetailExists }) => {
  const handleFormSubmit = e => {
    handleSubmit(e);
    if (invalid) toastr.error('Form is invalid. Please fix errors');
  };

  return (
    <form onSubmit={ handleFormSubmit } >
      {
        isRenewalDetailExists ?
          <Edit { ...{ change, submitFailed, dirty, form, disabled } } />
          :
          <New { ...{ change, submitFailed, dirty, form, disabled } } />
      }
    </form>
  );
};
Form.propTypes = propTypes;

export default Form;
