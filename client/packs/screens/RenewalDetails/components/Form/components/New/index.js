import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getFormSyncErrors, setSubmitFailed, formValueSelector } from 'redux-form';
import New from './new';

import { selectors } from 'core/renewalDetail';

const mapStateToProps = (state, props) => ({
  syncErrors: getFormSyncErrors(props.form)(state),
  criteriaQuestionsOptions: selectors.getCriteriaQuestionsOptions(state),
  usingProvider: formValueSelector(props.form)(state, 'usingProvider')
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setSubmitFailedDispatch: setSubmitFailed
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(New);
