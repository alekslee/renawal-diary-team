import React from 'react';
import { object, number, bool, func } from 'prop-types';

import { STEP_NAMES, NOT_USING_PROVIDER_STEP_NAMES } from './constants';

const propTypes = {
  criteriaQuestionsOptions: object.isRequired,
  currentStep: number.isRequired,
  usingProvider: bool,
  toStep: func.isRequired
};

const StepingHeader = ({ criteriaQuestionsOptions, currentStep, usingProvider, toStep }) => {
  let stepNames = [...usingProvider ? STEP_NAMES : NOT_USING_PROVIDER_STEP_NAMES];
  if (Object.keys(criteriaQuestionsOptions).length === 0) stepNames.splice(1, 1); // 1 - it's index of criteria step

  return (
    <div className='steps-wrapper'>
      <div className={ `steps-container active-${ currentStep }` }>
        {
          stepNames.map((name, index) => (
            <div key={ name } className={ `step step-${ index }` } onClick={ () => toStep(index) } >
              <div className='step-circle'>
                <div className='step-backdrop' />
                <div className='step-num'>{ index + 1 }</div>
                <div className='step-line'>
                  <div className='step-line-inner' />
                </div>
              </div>
              <span className='step-name'>{ name }</span>
            </div>
          ))
        }
      </div>
      <div className='steps-status'>
        <span>Complete to see a personalized leaderboard and crowd insights!</span>
        <span className='complete'>{ Math.round(currentStep * 100 / stepNames.length) }% Complete</span>
      </div>
    </div>
  );
};
StepingHeader.propTypes = propTypes;

export default StepingHeader;
