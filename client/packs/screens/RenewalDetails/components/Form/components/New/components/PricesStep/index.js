import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PricesStep from './pricesStep';

import { selectors, actions } from 'core/renewalDetail';

const mapStateToProps = state => ({
  pricesQuestionsOptions: selectors.getPricesQuestionsOptions(state)
});

export default connect(mapStateToProps, null)(PricesStep);
