import React from 'react';
import { string, bool, func } from 'prop-types';

import ProviderDetailsFormData from 'components/ProviderDetailsFormData';

const propTypes = {
  form: string.isRequired,
  dirty: bool,
  disabled: bool,
  submitFailed: bool,
  change: func.isRequired,
  toNextStep: func.isRequired
};

const ProviderDetailsStep = ({ form, dirty, disabled, submitFailed, change, toNextStep }) => {
  return (
    <div>
      <ProviderDetailsFormData { ...{ form, dirty, disabled, submitFailed, change } } withUsingProvider />
      <div className='steps-buttons'>
        <span className='next-button' onClick={ toNextStep } >Next</span>
      </div>
    </div>
  );
};
ProviderDetailsStep.propTypes = propTypes;

export default ProviderDetailsStep;
