export const STEP_NAMES = ['Provider Details', 'My Criteria', 'Price Details', 'Service Rating'];
export const NOT_USING_PROVIDER_STEP_NAMES = [STEP_NAMES[0], STEP_NAMES[1]];
