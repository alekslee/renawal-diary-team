import React from 'react';
import { string, bool, object, func } from 'prop-types';

import CriteriaQuestionsFormData from 'components/CriteriaQuestionsFormData';

const propTypes = {
  form: string.isRequired,
  disabled: bool,
  usingProvider: bool,
  criteriaQuestionsOptions: object.isRequired,
  toNextStep: func.isRequired,
  toPrevStep: func.isRequired
};

const CriteriasStep = ({ form, disabled, usingProvider, criteriaQuestionsOptions, toNextStep, toPrevStep }) => {
  return (
    <div>
      <CriteriaQuestionsFormData
        { ...{ form, disabled } }
        namePrefix='criteriaQuestions'
        criteriaOptions={ criteriaQuestionsOptions }
      />
      <div className='steps-buttons'>
        <span className='back-button' onClick={ toPrevStep } >Back</span>
        {
          usingProvider ?
            <span className='next-button' onClick={ toNextStep } >Next</span>
            :
            <button className='leaderboard-button' type='submit'>See Leaderboard</button>
        }
      </div>
    </div>
  );
};
CriteriasStep.propTypes = propTypes;

export default CriteriasStep;
