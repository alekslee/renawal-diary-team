import React from 'react';
import { string, bool, object, func } from 'prop-types';

import CriteriaQuestionsFormData from 'components/CriteriaQuestionsFormData';

const propTypes = {
  form: string.isRequired,
  disabled: bool,
  pricesQuestionsOptions: object.isRequired,
  toNextStep: func.isRequired,
  toPrevStep: func.isRequired
};

const PricesStep = ({ form, disabled, pricesQuestionsOptions, toNextStep, toPrevStep }) => {
  return (
    <div>
      <CriteriaQuestionsFormData
        { ...{ form, disabled } }
        namePrefix='pricesQuestions'
        criteriaOptions={ pricesQuestionsOptions }
      />
      <div className='steps-buttons'>
        <span className='back-button' onClick={ toPrevStep } >Back</span>
        <span className='next-button' onClick={ toNextStep } >Next</span>
      </div>
    </div>
  );
};
PricesStep.propTypes = propTypes;

export default PricesStep;
