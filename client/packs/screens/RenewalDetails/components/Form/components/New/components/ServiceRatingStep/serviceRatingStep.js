import React from 'react';
import { object, string, bool, func } from 'prop-types';

import RatingFormData from 'components/RatingFormData';
import SignUpModalLink from 'components/SignUpModalLink';

const propTypes = {
  formAttributes: object.isRequired,
  currentUser: object.isRequired,
  category: object.isRequired,
  form: string.isRequired,
  disabled: bool,
  change: func.isRequired,
  toPrevStep: func.isRequired,
  createRenewalDetailDispatch: func.isRequired
};

const ServiceRatingStep = ({
  formAttributes, currentUser, category, form, disabled, change, toPrevStep, createRenewalDetailDispatch
}) => {
  const createRenewalDetail = () => {
    let params;
    if (formAttributes.usingProvider) {
      params = {
        ...formAttributes,
        criteriaQuestions: JSON.stringify(formAttributes.criteriaQuestions),
        pricesQuestions: JSON.stringify(formAttributes.pricesQuestions)
      };
    } else {
      params = {
        usingProvider: formAttributes.usingProvider,
        criteriaQuestions: JSON.stringify(formAttributes.criteriaQuestions)
      };
    }

    createRenewalDetailDispatch({ params, categoryId: category.id });
  };

  return (
    <div>
      <RatingFormData { ...{ form, disabled, change } } />
      <div className='steps-buttons'>
        <span className='back-button' onClick={ toPrevStep } >Back</span>
        {
          currentUser.id ?
            <button className='leaderboard-button' type='submit'>See Leaderboard</button>
            :
            <SignUpModalLink className='leaderboard-button' onSignUpSuccess={ createRenewalDetail }>
              See Leaderboard
            </SignUpModalLink>
        }
      </div>
    </div>
  );
};
ServiceRatingStep.propTypes = propTypes;

export default ServiceRatingStep;
