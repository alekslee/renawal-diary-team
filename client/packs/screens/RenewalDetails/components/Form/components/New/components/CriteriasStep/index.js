import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formValueSelector } from 'redux-form';
import CriteriasStep from './criteriasStep';

import { selectors, actions } from 'core/renewalDetail';

const mapStateToProps = (state, props) => ({
  criteriaQuestionsOptions: selectors.getCriteriaQuestionsOptions(state),
  usingProvider: formValueSelector(props.form)(state, 'usingProvider')
});

export default connect(mapStateToProps, null)(CriteriasStep);
