import { ProviderDetailsStep, CriteriasStep, PricesStep, ServiceRatingStep } from './components';

export const STEPS = [ProviderDetailsStep, CriteriasStep, PricesStep, ServiceRatingStep];
export const NOT_USING_PROVIDER_STEPS = [STEPS[0], STEPS[1]];
