import React, { Component } from 'react';
import { object, string, bool, func } from 'prop-types';

import { STEPS, NOT_USING_PROVIDER_STEPS } from './constants';
import { StepingHeader } from './components';

const propTypes = {
  syncErrors: object.isRequired,
  criteriaQuestionsOptions: object.isRequired,
  form: string.isRequired,
  dirty: bool,
  disabled: bool,
  usingProvider: bool,
  submitFailed: bool,
  setSubmitFailedDispatch: func.isRequired,
  change: func.isRequired
};

class New extends Component {
  state = {
    currentStep: 0
  };

  toNextStep = () => {
    const { currentStep } = this.state;

    this.toStep(currentStep + 1);
  };

  toPrevStep = () => {
    const { currentStep } = this.state;

    this.toStep(currentStep - 1);
  };

  toStep = step => {
    const { setSubmitFailedDispatch, form, syncErrors } = this.props;
    const lastStep = this.retrieveSteps().length - 1;

    if (step < 0 || step > lastStep) return;
    // works only if errors could be only in first step. For now it's enought. For later - maybe needs to be changed
    if (Object.keys(syncErrors).length) return setSubmitFailedDispatch(form);

    this.setState({ currentStep: step });
  };

  retrieveSteps = () => {
    const { criteriaQuestionsOptions, usingProvider } = this.props;
    let steps = [...usingProvider ? STEPS : NOT_USING_PROVIDER_STEPS];
    if (Object.keys(criteriaQuestionsOptions).length === 0) steps.splice(1, 1); // 1 - it's index of criteria step
    return steps;
  };

  render() {
    const {
      toNextStep,
      toPrevStep,
      toStep,
      retrieveSteps,
      state: { currentStep },
      props: { criteriaQuestionsOptions, form, dirty, disabled, usingProvider, submitFailed, change }
    } = this;

    const StepComponent = retrieveSteps()[currentStep];

    return (
      <div>
        <StepingHeader { ...{ criteriaQuestionsOptions, currentStep, usingProvider, toStep } } />
        <StepComponent
          { ...{ form, dirty, disabled, submitFailed, change, toNextStep, toPrevStep } }
        />
      </div>
    );
  }
};
New.propTypes = propTypes;

export default New;
