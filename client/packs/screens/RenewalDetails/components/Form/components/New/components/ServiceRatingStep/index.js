import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getFormValues } from 'redux-form';
import ServiceRatingStep from './serviceRatingStep';

import { actions } from 'core/renewalDetail';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { selectors as categorySelectors } from 'core/category';

const mapStateToProps = (state, props) => ({
  formAttributes: getFormValues(props.form)(state),
  currentUser: currentUserSelectors.getCurrentUser(state),
  category: categorySelectors.getCategory(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    createRenewalDetailDispatch: actions.createRenewalDetail
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(ServiceRatingStep);
