import React, { Fragment } from 'react';
import { object, bool, func, string } from 'prop-types';

import ProviderDetailsFormSection from 'components/ProviderDetailsFormSection';
import CriteriaQuestionsFormSection from 'components/CriteriaQuestionsFormSection';
import RatingFormSection from 'components/RatingFormSection';

const propTypes = {
  change: func.isRequired,
  submitFailed: bool,
  dirty: bool.isRequired,
  form: string,
  disabled: bool,
  usingProvider: bool,
  criteriaQuestionsOptions: object.isRequired,
  pricesQuestionsOptions: object.isRequired
};

const Edit = ({
  change,
  submitFailed,
  dirty,
  form,
  disabled,
  usingProvider,
  criteriaQuestionsOptions,
  pricesQuestionsOptions
}) => {
  return (
    <Fragment>
      <ProviderDetailsFormSection { ...{ change, submitFailed, dirty, form, disabled } } withUsingProvider />
      <CriteriaQuestionsFormSection
        { ...{ form, disabled } }
        criteriaOptions={ criteriaQuestionsOptions }
        title='My criteria (This enables us to match people like you)'
        namePrefix='criteriaQuestions'
      />
      {
        usingProvider &&
          <Fragment>
            <CriteriaQuestionsFormSection
              { ...{ form, disabled } }
              criteriaOptions={ pricesQuestionsOptions }
              title='What did you pay? (Optional) (Compare your prices)'
              namePrefix='pricesQuestions'
            />
            <RatingFormSection { ...{ change, form, disabled } } />
          </Fragment>
      }
      <div className='renewal-details-btn'>
        <button type='submit' className='btn reminder-btn small-btn' >Go to the Group Leaderboard</button>
      </div>
    </Fragment>
  );
};
Edit.propTypes = propTypes;

export default Edit;
