import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formValueSelector } from 'redux-form';
import Edit from './edit';

import { selectors, actions } from 'core/renewalDetail';

const mapStateToProps = state => ({
  usingProvider: formValueSelector('renewalDetailsForm')(state, 'usingProvider'),
  criteriaQuestionsOptions: selectors.getCriteriaQuestionsOptions(state),
  pricesQuestionsOptions: selectors.getPricesQuestionsOptions(state)
});

export default connect(mapStateToProps, null)(Edit);
