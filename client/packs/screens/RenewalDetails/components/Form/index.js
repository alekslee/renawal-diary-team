import { reduxForm, formValueSelector } from 'redux-form';
import Form from './form';

const validate = ({ lastBuy, currentProvider, buyingType, buyingTypeCompany, checkedPlaces }) => {
  const errors = {};
  if (!lastBuy) errors.lastBuy = 'Required';
  if (!currentProvider || !currentProvider.label) errors.currentProvider = 'Required';
  if (!buyingType) errors.buyingType = 'Required';
  if (buyingType !== 'provider' && (!buyingTypeCompany || !buyingTypeCompany.label)) {
    errors.buyingTypeCompany = 'Required';
  }
  if (!checkedPlaces) errors.checkedPlaces = 'Required';
  return errors;
};

export default reduxForm({ form: 'renewalDetailsForm', validate })(Form);
