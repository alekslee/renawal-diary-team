import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import RenewalDetails from './renewalDetails';

import { selectors as categorySelectors } from 'core/category';
import { selectors as currentCountrySelectors } from 'core/currentCountry';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { selectors, actions } from 'core/renewalDetail';

const mapStateToProps = state => ({
  category: categorySelectors.getCategory(state),
  countryCode: currentCountrySelectors.getCurrentCountryCode(state),
  currentUser: currentUserSelectors.getCurrentUser(state),
  renewalDetail: selectors.getRenewalDetail(state),
  isRenewalDetailExists: selectors.getIsRenewalDetailExists(state),
  criteriaQuestionsOptions: selectors.getCriteriaQuestionsOptions(state),
  pricesQuestionsOptions: selectors.getPricesQuestionsOptions(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchRenewalDetailScreenDataDispatch: actions.fetchRenewalDetailScreenData,
    createRenewalDetailDispatch: actions.createRenewalDetail,
    setRenewalDetailDispatch: actions.setRenewalDetail,
    updateRenewalDetailDispatch: actions.updateRenewalDetail,
    setRenewalDetailFormOptionsDispatch: actions.setRenewalDetailFormOptions
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(RenewalDetails));
