import React, { Fragment } from 'react';
import Select from 'react-select';
import { object, string, array, bool } from 'prop-types';

import NumberInput from 'components/NumberInput';
import { CURRENCY_OPTIONS, SYMBOLS_AFTER_DOT } from './constants';

const propTypes = {
  input: object.isRequired,
  meta: object.isRequired,
  currencySymbol: string.isRequired,
  disabled: bool,
  withDot: bool
};

const defaultProps = {
  withDot: false,
  disabled: false
};

const PriceField = ({ input, meta: { touched, error }, currencySymbol, disabled, withDot }) => {
  const onFocusHandler = () => input.onChange('');

  return (
    <Fragment>
      <div className='price-input'>
        <NumberInput
          value={ input.value }
          symbolsAfterDot={ withDot ? SYMBOLS_AFTER_DOT : 0 }
          inputProps={ { onFocus: onFocusHandler } }
          disabled={ disabled }
          onChange={ input.onChange }
        />
        <span>{ currencySymbol }</span>
      </div>
      { touched && (error && <span>{ error }</span>) }
    </Fragment>
  );
};
PriceField.propTypes = propTypes;
PriceField.defaultProps = defaultProps;

export default PriceField;
