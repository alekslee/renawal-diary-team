import React, { Component } from 'react';
import { UnauthorizedModalsContext } from 'layouts/UnAuthorizedApp/context';
import { string, any } from 'prop-types';

const propTypes = {
  className: string,
  children: any
};

class ForgotPasswordModalLink extends Component {
  render() {
    const { className, children } = this.props;
    const { toggleForgotPasswordModal } = this.context;

    return (
      <a { ...{ className } } onClick={ toggleForgotPasswordModal } >
        { children }
      </a>
    );
  }
};
ForgotPasswordModalLink.propTypes = propTypes;
ForgotPasswordModalLink.contextType = UnauthorizedModalsContext;

export default ForgotPasswordModalLink;
