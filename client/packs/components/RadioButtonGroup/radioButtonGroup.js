import React, { Fragment } from 'react';
import { string, func, array, any } from 'prop-types';

import { DEFAULT_OPTIONS } from './constants';

const propTypes = {
  name: string.isRequired,
  onChange: func,
  value: any,
  options: array.isRequired
};
const defaultProps = {
  onChange: () => {}
};

const RadioButtonGroup = ({ name, onChange, options, value }) => {
  return (
    <Fragment>
      {
        options.map(option => {
          option = { ...DEFAULT_OPTIONS, ...option };

          return (
            <label
              key={ option.value }
              className='radio-button'
              style={ option.visible ? {} : { display: 'none' } }
            >
              { option.title && <h5>{ option.title }</h5> }
              <input
                type='radio'
                name={ name }
                value={ option.value }
                checked={ value === option.value }
                disabled={ option.disabled }
                onChange={ () => onChange(option.value) }
              />
              <div>
                <span />
                { option.label }
              </div>
            </label>
          );
        })
      }
    </Fragment>
  );
};
RadioButtonGroup.propTypes = propTypes;
RadioButtonGroup.defaultProps = defaultProps;

export default RadioButtonGroup;
