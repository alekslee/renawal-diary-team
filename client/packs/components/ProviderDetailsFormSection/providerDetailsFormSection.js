import React, { Fragment, Component } from 'react';
import { bool, func, string } from 'prop-types';

import FormSlider from 'components/FormSlider';
import ProviderDetailsFormData from 'components/ProviderDetailsFormData';

const propTypes = {
  change: func.isRequired,
  disabled: bool,
  dirty: bool.isRequired,
  withUsingProvider: bool,
  submitFailed: bool,
  form: string.isRequired
};

const defaultProps = {
  withUsingProvider: false,
  disabled: false
};

const ProviderDetailsFormSection = ({ form, dirty, disabled, submitFailed, withUsingProvider, change }) => {
  return (
    <FormSlider title='My Choice' formIcon='ProviderDetails'  sliderOpen >
      <ProviderDetailsFormData { ...{ form, dirty, disabled, submitFailed, withUsingProvider, change } } />
    </FormSlider>
  );
};
ProviderDetailsFormSection.propTypes = propTypes;
ProviderDetailsFormSection.defaultProps = defaultProps;

export default ProviderDetailsFormSection;
