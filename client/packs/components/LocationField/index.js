import { connect } from 'react-redux';
import LocationField from './locationField';
import { selectors } from 'core/company';

const mapStateToProps = state => ({
  companyListAddressFilter: selectors.getCompanyListAddressFilter(state)
});

export default connect(mapStateToProps)(LocationField);