import React, { Component, Fragment } from 'react';
import { object, string, bool, shape, func } from 'prop-types';
import cx from 'classnames';

const propTypes = {
  input: shape({
    onChange: func.isRequired
  }).isRequired,
  onSelect: func,
  placeholder: string,
  disabled: bool,
  meta: shape({
    touched: bool,
    error: string,
  }).isRequired,
  companyListAddressFilter: object,
  country: object
};

const defaultProps = {
  disabled: false
};

class LocationField extends Component {
  constructor(props) {
    super(props);

    if (props.meta.initial) {
      const { address, lat, lng, countryCode, city, state, postalCode } = props.meta.initial;
      this.state = { address, lat, lng, countryCode, city, state, postalCode };
    } else if (props.country) {
      const { country } = this.props;
      this.state = {
        address: country.name,
        countryCode: country.isoA2Code.toLocaleLowerCase()
      };
    } else {
      this.state = {
        address: '',
        lat: null,
        lng: null,
        countryCode: null,
        city: null,
        state: null,
        postalCode: null,
      };
    }
  }

  componentDidMount() {
    if (!window.google) return;
    this.searchBox = new google.maps.places.Autocomplete(this.locationNode);
    this.searchBox.addListener('place_changed', this.onPlacesChanged);
    if ( this.props.country ) this.updateLocation();
  };

  componentWillUnmount() {
    if (!window.google) return;
    google.maps.event.clearInstanceListeners(this.searchBox);
    const pac = document.querySelector('.pac-container');
    pac && pac.remove();
  };

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.companyListAddressFilter) {
      return { address: nextProps.companyListAddressFilter.value };
    } else {
      return null
    }
  }

  focusHandler = () => {
    this.setState({ address: '', lat: null, lng: null, countryCode: '', city: '', postalCode: '', state: '' });
    this.updateLocation();
  };

  changeHandler = ({ target: { value } }) => {
    this.setState({ address: value });
    this.updateLocation();
  };

  blurHandler = () => {
    this.updateLocation();
  };

  onPlacesChanged = () => {
    const { geometry: { location }, formatted_address, address_components } = this.searchBox.getPlace();
    const countryCode = this.getCountryCode(address_components);
    const city = this.getCity(address_components);
    const postalCode = this.getPostalCode(address_components);
    const state = this.getState(address_components);

    this.setState({
      address: formatted_address,
      lat: location.lat(),
      lng: location.lng(),
      countryCode,
      city,
      state,
      postalCode,
    });
    this.updateLocation('places-changed');
  };

  getCountryCode = addressComponents => {
    const country = addressComponents.find(component => component.types.includes('country'));
    return country && country.short_name;
  };

  getCity = addressComponents => {
    const city = addressComponents.find(component => component.types.includes('locality'));
    return city && city.short_name;
  };

  getState = addressComponents => {
    const city = addressComponents.find(component => component.types.includes('administrative_area_level_1'));
    return city && city.long_name;
  };

  getPostalCode = addressComponents => {
    const postalCode = addressComponents.find(component => component.types.includes('postal_code'));
    return postalCode && postalCode.short_name;
  };

  updateLocation = (type=null) => {
    const {
      state: { address, lat, lng, countryCode, city, state, postalCode },
      props: { input: { onChange }, onSelect }
    } = this;

    if (onSelect && type === 'places-changed') {
      onSelect({ address, lat, lng, countryCode, city, state, postalCode });
    }

    onChange({ address, lat, lng, countryCode, city, state, postalCode });
  }

  render() {
    const {
      focusHandler,
      changeHandler,
      blurHandler,
      state: { address },
      props: { input, placeholder, disabled, meta: { touched, error }, companyListAddressFilter }
    } = this;

    return (
      <Fragment>
        <input
          { ...{ ...input, disabled, value: address, onChange: changeHandler, onBlur: blurHandler } }
          className={cx('reminder-input', { 'error-input': touched && error })}
          placeholder={ placeholder }
          ref={ node => this.locationNode = node }
          type='text'
          onFocus={ focusHandler }
        />
        { touched && (error && <span className="error-hint">{ error }</span>) }
      </Fragment>
    );
  }
};
LocationField.propTypes = propTypes;
LocationField.defaultProps = defaultProps;

export default LocationField;
