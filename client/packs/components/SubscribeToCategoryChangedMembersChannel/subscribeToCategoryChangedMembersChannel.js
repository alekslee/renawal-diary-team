import React, { Component, Fragment } from 'react';
import { any, func } from 'prop-types';

import { deleteSubscription } from 'lib/utils';
import { subscriptionIds } from 'core/category/constants';

const propTypes = {
  children: any.isRequired,
  subscribeToCategoryCounterChannelDispatch: func.isRequired,
  changeCategoryUsersCountDispatch: func.isRequired
};

class SubscribeToCategoryChangedMembersChannel extends Component {
  componentDidMount() {
    const {
      subscribeToCategoryCounterChannelDispatch,
      changeCategoryUsersCountDispatch
    } = this.props;

    const onReceive = ({ categoryChangedMembers }) => {
      changeCategoryUsersCountDispatch({ usersCount: categoryChangedMembers.usersCount });
    };
    subscribeToCategoryCounterChannelDispatch({ onReceive });
  };

  componentWillUnmount() {
    deleteSubscription(subscriptionIds.CATEGORY_COUNTER_CHANNEL);
  };

  render() {
    const { subscribeToCategoryCounterChannelDispatch, children } = this.props;

    return (
      <Fragment>
        { children }
      </Fragment>
    );
  }
};
SubscribeToCategoryChangedMembersChannel.propTypes = propTypes;

export default SubscribeToCategoryChangedMembersChannel;
