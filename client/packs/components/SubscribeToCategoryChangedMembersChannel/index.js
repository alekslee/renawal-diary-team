import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SubscribeToCategoryChangedMembersChannel from './subscribeToCategoryChangedMembersChannel';

import { selectors, actions } from 'core/category';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    subscribeToCategoryCounterChannelDispatch: actions.subscribeToCategoryCounterChannel,
    changeCategoryUsersCountDispatch: actions.changeCategoryUsersCount
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(SubscribeToCategoryChangedMembersChannel);
