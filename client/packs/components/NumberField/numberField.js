import React, { Fragment } from 'react';
import { object, number, bool } from 'prop-types';

import NumberInput from 'components/NumberInput';

const propTypes = {
  input: object.isRequired,
  meta: object.isRequired,
  minValue: number,
  disabled: bool,
  symbolsAfterDot: number
};

const NumberField = ({ input, disabled, meta: { touched, error }, minValue, symbolsAfterDot }) => {
  const onFocusHandler = () => input.onChange('');

  return (
    <Fragment>
      <NumberInput
        { ...{ minValue, symbolsAfterDot, disabled } }
        inputProps={ { onFocus: onFocusHandler } }
        value={ input.value || null }
        onChange={ input.onChange }
      />
      { touched && (error && <span>{ error }</span>) }
    </Fragment>
  );
};
NumberField.propTypes = propTypes;

export default NumberField;
