import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import reviewDetailsModal from './reviewDetailsModal';
import { reduxForm, getFormValues } from 'redux-form';
import { injectIntl } from 'react-intl';

import { selectors, actions } from 'core/company';
import { actions as feedbackActions } from 'core/feedback';
import { selectors as detailSelectors, actions as detailActions } from 'core/renewalDetail';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { selectors as categorySelectors } from 'core/category';

const mapStateToProps = (state, props) => ({
  currentUser: currentUserSelectors.getCurrentUser(state),
  criteriaQuestionsOptions: detailSelectors.getCriteriaQuestionsOptions(state),
  formAttributes: getFormValues('reviewDetailsForm')(state),
  category: categorySelectors.getCategory(state),
  providersOptions: detailSelectors.getProvidersOptions(state),
  buyingTypeOptions: detailSelectors.makeGetBuyingTypeOptions()(state, props),
  countriesSelectOptions: selectors.getCountrySelectOptions(state),
  personalizeFilterStatus: selectors.getPersonalizeFilterStatus(state),
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setCompanyListFiltersDispatch: actions.setCompanyListFilters,
    createCategoryPersonalizeFilterDispatch: actions.createCategoryPersonalizeFilter,
    createFeedbackDispatch: feedbackActions.createFeedback,
    fetchCompaniesListCriteriaQuestionsFilterDispatch: actions.fetchCompaniesListCriteriaQuestionsFilter,
  }, dispatsh)
);

export default reduxForm({ form: 'reviewDetailsForm' })(
  connect(mapStateToProps, mapDispatchToProps)(injectIntl(reviewDetailsModal))
);

