import React, { Fragment, Component } from 'react';
import { object, string, bool, array, func } from 'prop-types';
import Loader from 'components/Loader';
import SignUpModalLink from 'components/SignUpModalLink';
import SignInModalLink from 'components/SignInModalLink';
import { CriteriaBlock, TogglePersonalFilter } from '../../../packs/screens/Leaderboard/components/PersonalizeYourResults/components';
import RadioButtonField from 'components/RadioButtonField';
import SelectAndAutocompleteInput from 'components/SelectAndAutocompleteInput';
import RatingFormData from 'components/RatingFormData';
import { InsightsSection } from 'screens/Vote/components/Form/components';
import { Field } from 'redux-form';
import { toastr } from 'lib/helpers';
import { defaultMessages } from 'locales/default';
import I18n from 'components/I18n';

import { TYPES_CONVERTER } from 'components/CriteriaQuestionsFormData/constants';
import { LAST_BUY_OPTIONS, CHECKED_PLACES_OPTIONS } from 'core/renewalDetail/constants';
import { USING_PROVIDER_OPTIONS, BUYING_TYPE_OPTIONS, PLEASE_NAME_TEXT } from 'components/ProviderDetailsFormData/constants';

import Close from 'images/svg/close-dark.svg';
import cx from 'classnames';
import { Row, Col, Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

const propTypes = {
  criteriaQuestionsOptions: object,
  // countriesSelectOptions: array.isRequired,
  providersOptions: array.isRequired,
  category: object.isRequired,
  toggle: func.isRequired,
  currentUser: object.isRequired,
  form: string.isRequired,
  isOpen: bool.isRequired,
  personalizeFilterStatus: bool,
};

const ALLSTEPS = [
  'criteria1',
  'criteria2',
  'provider',
  // 'standart',
  // 'rating',
  'smart-reminder',
];

class ReviewDetailsModal extends Component {

  state = {
    windowSize: window.innerWidth,
    hasProvider: null,
    currentStep: 0,
    steps: ALLSTEPS,
  }

  componentDidMount() {
    this.setWindowSize();
    const { criteriaQuestionsOptions } = this.props;
    const isCriteria = criteriaQuestionsOptions && Object.keys(criteriaQuestionsOptions).length !== 0;
    this.setState({ steps: (isCriteria ? ALLSTEPS : ALLSTEPS.slice(2)) });
    window.addEventListener('resize', this.setWindowSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setWindowSize);
  };

  setWindowSize = () => {
    this.setState({ windowSize: window.innerWidth });
  }

  toggleFilter = (questions) => {
    const {
      props: { toggle, createCategoryPersonalizeFilterDispatch, fetchCompaniesListCriteriaQuestionsFilterDispatch, currentUser, category },
    } = this;

    const categoryId = category.id;
    const criteriaQuestions = JSON.stringify((questions || {}).criteriaQuestions);

    if (currentUser.id) {
      createCategoryPersonalizeFilterDispatch({ categoryId, criteriaQuestions });
    } else {
      fetchCompaniesListCriteriaQuestionsFilterDispatch({ criteriaQuestions });
    }

    toggle();
  }

  handleResetCategoryPersonalizeFilter = () => {
    this.toggleFilter({});
  };

  handleCreateCategoryPersonalizeFilter = () => {
    const {
      props: { formAttributes, category, setCompanyListFiltersDispatch, fetchRenewalDetailScreenDataDispatch, createFeedbackDispatch },
      state: { hasProvider },
    } = this;

    if (formAttributes.location) {
      const companyListCountryFilter = { value: formAttributes.location.countryCode, label: formAttributes.location.countryCode };
      const companyListCityFilter = { value: formAttributes.location.city, label: formAttributes.location.city };
      const companyListAddressFilter = { value: formAttributes.location.address };

      setCompanyListFiltersDispatch({
        companyListCountryFilter,
        companyListCityFilter,
        companyListAddressFilter
      });
    }

    if (hasProvider === true) {
      const {
        lastBuy,
        buyingType,
        checkedPlaces,
        currentProvider,
        buyingTypeCompany,
        currentProviderRate,
        currentProviderRateComment,
        buyingTypeCompanyRate,
        buyingTypeCompanyRateComment,
        criteriaQuestions,
        pricesQuestions,
        insightsComment,
        triggeredAt,
        createSmartReminder
      } = (formAttributes || {});

      const callback = () => {
        toastr.success("Success");
        fetchRenewalDetailScreenDataDispatch({ categoryId: category.id });
      }

      createFeedbackDispatch({
        callback,
        params: {
          renewalDetailAttributes: {
            lastBuy,
            buyingType,
            checkedPlaces,
            currentProvider,
            buyingTypeCompany,
            currentProviderRate,
            currentProviderRateComment,
            buyingTypeCompanyRate,
            buyingTypeCompanyRateComment,
            criteriaQuestions: JSON.stringify(criteriaQuestions),
            pricesQuestions: JSON.stringify(pricesQuestions)
          },
          triggeredAt,
          createSmartReminder,
          insightsComment: insightsComment, //&& draftToMarkdown(convertToRaw(insightsComment.getCurrentContent())),
          categoryId: category.id
        }
      });
    }

    this.toggleFilter(formAttributes);
  }

  render() {
    const syncErrors = {};
    const {
      handleCreateCategoryPersonalizeFilter,
      handleResetCategoryPersonalizeFilter,
      props: { criteriaQuestionsOptions, personalizeFilterStatus, formAttributes, buyingTypeOptions, providersOptions, toggle, currentUser, change, form, dirty, invalid, isOpen, category },
      state: { steps, windowSize, hasProvider, currentStep }
    } = this;

    const {
      lastBuy,
      buyingType,
      checkedPlaces,
      currentProvider,
      buyingTypeCompany,
      currentProviderRate,
      currentProviderRateComment,
      buyingTypeCompanyRate,
      buyingTypeCompanyRateComment,
    } = (formAttributes || {});

    const stepComponents = {
      criteria1: (
       <CriteriaBlock
          splitParts
          namePrefix="criteriaQuestions"
          part={0}
         { ...{ criteriaQuestionsOptions, form } }
       />
      ),
      criteria2: (
       <CriteriaBlock
          splitParts
          namePrefix="criteriaQuestions"
          part={1}
         { ...{ criteriaQuestionsOptions, form } }
       />
      ),
      provider: (
        <Fragment>
            <Fragment>
              <div
                className="top-part"
                ref={(ref) => this.providerContainer = ref}
              >

                <div
                  className="top-part-item"
                >
                  <h3>Do you have an existing provider? </h3>

                  <div className="radio-container grid-4">
                    <Field
                      name="isProvider"
                      onChange={ value => {
                        if (value) {
                          this.setState({ hasProvider: value });
                        } else {
                          this.setState({ currentStep: currentStep + 1 });
                        }
                      } }
                      options={ [{ value: true, label: 'Yes' }, { value: false, label: 'No' } ] }
                      component={ RadioButtonField }
                    />
                  </div>
                </div>

                { hasProvider === true &&
                  <Fragment>
                    <div className='top-part-item'>
                      {/* How did you buy? I bought: */}
                      <div className={ cx('reminder-form-item') }>
                        <h3>
                          <I18n text={ defaultMessages.voteFormBuyingTypeTitle } />
                          &nbsp;
                          <span>
                            <I18n text={ defaultMessages.voteFormBuyingTypeSubtitle } />:
                          </span>
                        </h3>
                        <div className='radio-container grid-2'>
                          <Field
                            name='buyingType'
                            options={ BUYING_TYPE_OPTIONS }
                            component={ RadioButtonField }
                          />
                        </div>
                      </div>

                      <h3>Search current provider </h3>
                      <div className="grid-1">
                        <Field
                          name='currentProvider'
                          component={ SelectAndAutocompleteInput }
                          selectOnMenuOpen={() => {
                            setTimeout(() => {
                              this.providerContainer.scrollTop += 200
                            }, 100)
                          }}
                          wrapperClass='grid-1 provider'
                          selectClass='reminder-select'
                          inputClass='reminder-input'
                          prefixClass='Select'
                          selectPlaceholder='Search current provider'
                          items={ providersOptions }
                        />

                        {/* <a onClick={() => toggle('isNewProviderModalOpen', 'provider')} className="add-new-provider">
                        + Add new Provider
                      </a>*/}
                      </div>
                    </div>

                  </Fragment>
                }
              </div>
            </Fragment>
        </Fragment>
      ),

      'smart-reminder': (
        <InsightsSection
          withoutInsightsComment
          createCheckbox
          { ...{ change, form } }
        />
      ),
    };

    const categoryName = category && category.root && category.root.enName;

    return (
      <Fragment >
        <div className="filterPopUp">

          <Modal
            isOpen={ isOpen }
            toggle={ () => toggle() }
            className={
              `filter-modal ${categoryName}`
            }
          >
            <ModalHeader
              toggle={ () => toggle() }
              charCode='x'
            >
              My Details
            </ModalHeader>


            <ModalBody>

              {!currentUser.id &&
                <div className="modal-filter-login">
                  <span className="login-info">Already a member?</span>
                  <SignInModalLink className='login' >
                    Login
                  </SignInModalLink>
                </div>
              }

              <div className="filter-progress">
                <div
                  className="progress"
                  style={ {width: `${((currentStep + 1) * 100)/(steps.length)}%`, height: '2px'} }
                />
              </div>

              <div className="modal-switcher">
                <span className="switcher-title">
                  Save My Preferences
                </span>

                <TogglePersonalFilter />
              </div>

              <div>
                { stepComponents[steps[currentStep]] }
              </div>

            </ModalBody>

            <ModalFooter>
              <Fragment>

                { ( currentStep !== (steps.length - 1) && hasProvider !== false) &&
                <Button
                  className="btn next"
                  onClick={ () => {
                    this.setState({ currentStep: currentStep + 1 });
                  } }
                >
                    Next
                </Button>
                }

              </Fragment>

              { (currentStep === (steps.length - 1) || hasProvider === false) && (
                currentUser.id ?
                <Button
                  className="btn next"
                  onClick={ () => {
                    handleCreateCategoryPersonalizeFilter();
                  } }
                >
                  Apply
                </Button> :
                <SignInModalLink
                  className="btn next btn-secondary"
                  onSignUpSuccess={() => {
                    handleCreateCategoryPersonalizeFilter();
                  }}
                >
                  Apply
                </SignInModalLink>
              )}
            </ModalFooter>


          </Modal>
        </div>
      </Fragment>
    );
  };
};

ReviewDetailsModal.propTypes = propTypes;

export default ReviewDetailsModal;

// { currentStep === 0 ?
//      <Button
//        className="btn cancel"
//        onClick={ () => {
//          if ( hasProvider === false ) {
//              this.setState({ hasProvider: null });
//              this.props.change('isProvider', null);
//          } else {
//            toggle()
//          }

//        } }
//      >
//        Cancel
//      </Button> :
//      <Button
//        className="btn cancel"
//        onClick={ () => {
//          this.setState({ currentStep: currentStep - 1 });
//        } }
//      >
//        Back
//      </Button>
//      }

// standart: (
//   <Fragment>
//     <div className="top-part">
//       <div className={ cx('reminder-form-item', { 'required': syncErrors.lastBuy }) }>
//         <h3>When did you sign up with your current provider?</h3>
//         <div className='radio-container grid-2 m-grid-2'>
//           <Field
//             name='lastBuy'
//             options={ LAST_BUY_OPTIONS }
//             component={ RadioButtonField }
//           />
//         </div>
//       </div>

//       { lastBuy &&
//         <div className={ cx('reminder-form-item top-part-item mt-3', { 'required': syncErrors.buyingType }) }>
//           <h3>
//             <I18n text={ defaultMessages.voteFormBuyingTypeTitle } />
//             <span>
//             <I18n text={ defaultMessages.voteFormBuyingTypeSubtitle } />:
//           </span>
//           </h3>
//           <div className='radio-container grid-2'>
//             <Field
//               name='buyingType'
//               options={ BUYING_TYPE_OPTIONS }
//               component={ RadioButtonField }
//             />
//           </div>
//         </div>
//       }

//       { buyingType && buyingType !== 'provider' &&
//         <div className='reminder-form-item'>
//           <h3>Please name { PLEASE_NAME_TEXT[buyingType] }</h3>
//           <div className='grid-1' >
//             <Field
//               name='buyingTypeCompany'
//               component={ SelectAndAutocompleteInput }
//               wrapperClass='grid-1 provider'
//               selectClass='reminder-select'
//               inputClass='reminder-input'
//               prefixClass='Select'
//               selectPlaceholder={`Search current ${PLEASE_NAME_TEXT[buyingType]}`}
//               // selectPlaceholder={<I18n text={ defaultMessages.voteFormBuyingTypeInputPlaceholder } />}
//               items={ buyingTypeOptions }
//             />
//           </div>
//         </div>
//       }

//       { buyingTypeCompany &&
//         <div className={ cx('reminder-form-item', { 'required': syncErrors.checkedPlaces }) }>
//           <h3>How many places / sites do you usually check?</h3>
//           <div className='radio-container grid-3'>
//             <Field
//               name='checkedPlaces'
//               options={ CHECKED_PLACES_OPTIONS }
//               component={ RadioButtonField }
//             />
//           </div>
//         </div>
//       }
//     </div>
//   </Fragment>
// ),
// rating: (
//   <RatingFormData
//     { ...{ change, form, dirty, invalid } }
//   />
// ),
