import CategoryElement from './categoryElement';
import { injectIntl } from 'react-intl';

export default injectIntl(CategoryElement);
