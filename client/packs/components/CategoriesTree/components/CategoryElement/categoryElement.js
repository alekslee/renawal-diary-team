import React, { Component } from 'react';
import { shape, string, array, number, func } from 'prop-types';

import { LeafCategoryElement } from './components';
import I18n from 'components/I18n';
import { defaultMessages } from 'locales/default';

const propTypes = {
  category: shape({
    name: string.isRequired,
    code: string.isRequired,
    slag: string.isRequired,
    enName: string.isRequired,
    leafChildren: array.isRequired,
    usersForAllDescendantsAndAllCountriesCount: number.isRequired
  }).isRequired,
  index: number.isRequired,
  selectCategoryDispatch: func.isRequired
};

class CategoryElement extends Component {
  componentDidMount() {
  };

  render() {
    const {
      props: {
        category: {
          name,
          enName,
          leafChildren,
          usersForAllDescendantsAndAllCountriesCount
        },
        index,
        selectCategoryDispatch
      }
    } = this;

    return (
      <div className={ `group-box ${enName}` }>
        <div className="-title category-name" >
          { name }
        </div>
        <div className='-subtitle'>
          <span className='amount category-members-counter'>{ usersForAllDescendantsAndAllCountriesCount }</span>
          <br />
          <I18n text={ defaultMessages.members } />
        </div>
        <div className='scroll-group-container'>
          <ul>
            {
              leafChildren.map((category, leafIndex) => (
                <LeafCategoryElement
                  key={ category.id }
                  { ...{ category, selectCategoryDispatch } }
                  parentIndex={ index }
                  index={ leafIndex }
                />
              ))
            }
          </ul>
        </div>
      </div>
    );
  };
};
CategoryElement.propTypes = propTypes;

export default CategoryElement;
