import React from 'react';
import { shape, string, number, bool, func } from 'prop-types';

const propTypes = {
  category: shape({
    id: string.isRequired,
    name: string.isRequired,
    code: string.isRequired,
    enName: string.isRequired,
    selected: bool
  }).isRequired,
  parentIndex: number.isRequired,
  index: number.isRequired,
  selectCategoryDispatch: func.isRequired
};

const LeafCategoryElement = ({
  parentIndex,
  index,
  category: { id, name, enName, selected },
  selectCategoryDispatch
}) => {
  return (
    <li className='d-flex justify-content-between align-items-center'>
      <span className='category-name'>
        <label className='red-checkbox right'>
          { name }
          <input
            type='checkbox'
            className='category-checkbox'
            name={ enName }
            checked={ selected }
            value={ id }
            onChange={ () => selectCategoryDispatch({ parentIndex, index, selected: !selected }) }
          />
          <span className='checkmark' />
        </label>
      </span>
    </li>
  );
};
LeafCategoryElement.propTypes = propTypes;

export default LeafCategoryElement;
