import React from 'react';
import { array, func } from 'prop-types';

import { CategoryElement } from './components';

const propTypes = {
  categories: array.isRequired,
  selectCategoryDispatch: func.isRequired
};

const CategoriesTree = ({ categories, selectCategoryDispatch }) => {
  return (
    <div id='categories-tree' className='groups-block'>
      {
        categories.map((category, index) => (
          <CategoryElement key={ category.id } { ...{ category, index, selectCategoryDispatch } } />
        ))
      }
    </div>
  );
};
CategoriesTree.propTypes = propTypes;

export default CategoriesTree;
