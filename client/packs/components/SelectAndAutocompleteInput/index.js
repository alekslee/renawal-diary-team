import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formValueSelector } from 'redux-form';
import SelectAndAutocompleteInput from './selectAndAutocompleteInput';

const mapStateToProps = (state, props) => ({
  fieldValue: formValueSelector(props.meta.form)(state, props.input.name)
});

export default connect(mapStateToProps, null)(SelectAndAutocompleteInput);
