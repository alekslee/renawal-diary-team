import React, { Component } from 'react';
import Select from 'react-select';
import { shape, func, object, string, bool, array, any } from 'prop-types';

import Autocomplete from 'components/Autocomplete';

const propTypes = {
  input: shape({
    onChange: func.isRequired
  }).isRequired,
  meta: object.isRequired,
  selectOnMenuOpen: func,
  wrapperClass: string,
  selectClass: string,
  inputClass: string,
  prefixClass: string,
  inputPlaceholder: string,
  selectPlaceholder: string,
  disabled: bool,
  fieldValue: any,
  items: array.isRequired
};

class SelectAndAutocompleteInput extends Component {
  constructor(props) {
    super(props);

    const selectValue = props.meta.initial || props.input.value || '';
    this.state = { selectValue, inputValue: '' };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const {
      state: { selectValue, inputValue },
      props: { fieldValue }
    } = this;

    if (!nextProps.fieldValue && fieldValue) {
      this.setState({ selectValue: '', inputValue: '' });
    } else if (
      nextProps.fieldValue && fieldValue !== nextProps.fieldValue &&
      nextProps.fieldValue !== selectValue && nextProps.fieldValue !== inputValue
    ) {
      this.setState({ selectValue: '', inputValue: nextProps.fieldValue });
    };
  };

  changeSelectHandler = selectValue => {
    this.setState({ selectValue, inputValue: '' }, this.updateValue);
  };

  changeInputHandler = inputValue => {
    if (inputValue && !inputValue.label) inputValue = {};
    this.setState({ selectValue: '', inputValue }, this.updateValue);
  };

  updateValue = () => {
    const {
      state: { selectValue, inputValue },
      props: { input: { onChange } }
    } = this;
    onChange(selectValue || inputValue);
  };

  render() {
    const {
      changeSelectHandler,
      changeInputHandler,
      state: { selectValue, inputValue },
      props: {
        items,
        wrapperClass,
        selectOnMenuOpen,
        selectClass,
        inputClass,
        prefixClass,
        inputPlaceholder,
        disabled,
        selectPlaceholder
      }
    } = this;

    return (
      <div className={ wrapperClass } >
        <Select
          className={ selectClass }
          onMenuOpen={ selectOnMenuOpen }
          classNamePrefix={ prefixClass }
          value={ selectValue }
          options={ items }
          placeholder={ selectPlaceholder }
          isDisabled={ disabled }
          onChange={ changeSelectHandler }
        />
{/*        <p className='text-gray'>or</p>
        <Autocomplete
          { ...{ items } }
          className={ inputClass }
          inputProps={ { disabled } }
          placeholder={ inputPlaceholder }
          value={ inputValue.label }
          onChange={ changeInputHandler }
        />*/}
      </div>
    );
  }
};
SelectAndAutocompleteInput.propTypes = propTypes;

export default SelectAndAutocompleteInput;
