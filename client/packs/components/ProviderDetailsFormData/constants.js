import React from 'react';
import I18n from 'components/I18n';
import { defaultMessages } from 'locales/default';

export const USING_PROVIDER_OPTIONS = [
  {
    value: true,
    label: 'Yes'
  },
  {
    value: false,
    label: 'No'
  }
];

export const BUYING_TYPE_OPTIONS = [
  {
    value: 'provider',
    label: <I18n text={ defaultMessages.voteFormBuyingTypeProvider } />
  },
  {
    value: 'comparsion_site',
    label: <I18n text={ defaultMessages.voteFormBuyingTypeComparsionSite } />
  },
  {
    value: 'broker',
    label: <I18n text={ defaultMessages.voteFormBuyingTypeBroker } />
  },
  {
    value: 'other',
    label: <I18n text={ defaultMessages.voteFormBuyingTypeOther } />
  }
];

export const PLEASE_NAME_TEXT = {
  comparsion_site: 'the Price Comparsion Site',
  broker: 'the broker',
  other: 'the other source'
};
