import React from 'react';
import { shape, object, array, string, bool, func } from 'prop-types';

import AutocompleteField from 'components/AutocompleteField';

const propTypes = {
  input: shape({
    onChange: func.isRequired
  }).isRequired,
  meta: object.isRequired,
  items: array.isRequired,
  className: string,
  placeholder: string,
  buyingType: string,
  disabled: bool,
  setBuyingTypeDispatch: func.isRequired
};

const BuyingTypeCompanyField = ({
  input, meta, items, className, placeholder, buyingType, disabled, setBuyingTypeDispatch
}) => {
  const onChangeHandler = company => {
    if (company.companyType && company.companyType !== buyingType) {
      setBuyingTypeDispatch(meta.form, company.companyType);
    }
  };

  return (
    <AutocompleteField { ...{ input, meta, items, className, placeholder, disabled } } onChange={ onChangeHandler } />
  );
};
BuyingTypeCompanyField.propTypes = propTypes;

export default BuyingTypeCompanyField;
