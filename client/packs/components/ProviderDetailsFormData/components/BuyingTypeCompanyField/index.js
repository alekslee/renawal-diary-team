import { connect } from 'react-redux';
import { change, formValueSelector } from 'redux-form';
import { bindActionCreators } from 'redux';
import BuyingTypeCompanyField from './buyingTypeCompanyField';

const mapStateToProps = (state, props) => ({
  buyingType: formValueSelector(props.meta.form)(state, 'buyingType')
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setBuyingTypeDispatch: (form, buyingType) => change(form, 'buyingType', buyingType)
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(BuyingTypeCompanyField);
