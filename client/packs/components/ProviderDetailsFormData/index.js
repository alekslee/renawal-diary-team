import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formValueSelector, getFormSyncErrors, isDirty } from 'redux-form';
import { injectIntl } from 'react-intl';
import ProviderDetailsFormData from './providerDetailsFormData';

import { selectors } from 'core/renewalDetail';

const makeMapStateToProps = () => {
  const buyingTypeOptions = selectors.makeGetBuyingTypeOptions();
  const mapStateToProps = (state, props) => {
    const syncErrors = getFormSyncErrors(props.form)(state);
    const dirty = isDirty(props.form)(state, Object.keys(syncErrors));
    return {
      providersOptions: selectors.getProvidersOptions(state),
      mixOptions: selectors.getMixTypeOptions(state),
      buyingTypeOptions: buyingTypeOptions(state, props),
      buyingType: formValueSelector(props.form)(state, 'buyingType'),
      currentProvider: formValueSelector(props.form)(state, 'currentProvider'),
      buyingTypeCompany: formValueSelector(props.form)(state, 'buyingTypeCompany'),
      usingProvider: formValueSelector(props.form)(state, 'usingProvider'),
      syncErrors: (props.submitFailed || dirty) ? syncErrors : {}
    };
  };
  return mapStateToProps;
};

export default connect(makeMapStateToProps, null)(injectIntl(ProviderDetailsFormData));
