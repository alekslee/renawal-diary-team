import React, { Fragment, Component } from 'react';
import { intlShape } from 'react-intl';
import { Field } from 'redux-form';
import classNames from 'classnames';
import { string, bool, object, array, func } from 'prop-types';

import I18n from 'components/I18n';
import FormSlider from 'components/FormSlider';
import SelectAndAutocompleteInput from 'components/SelectAndAutocompleteInput';
import RadioButtonField from 'components/RadioButtonField';
import { defaultMessages } from 'locales/default';
import { USING_PROVIDER_OPTIONS, BUYING_TYPE_OPTIONS, PLEASE_NAME_TEXT } from './constants';
import { LAST_BUY_OPTIONS, CHECKED_PLACES_OPTIONS } from 'core/renewalDetail/constants';
import { BuyingTypeCompanyField } from './components';

const propTypes = {
  intl: intlShape.isRequired,
  toggle: func.isRequired,
  buyingType: string,
  currentProvider: object,
  buyingTypeCompany: object,
  buyingTypeOptions: array.isRequired,
  mixOptions: array.isRequired,
  providersOptions: array.isRequired,
  usingProvider: bool,
  change: func.isRequired,
  disabled: bool,
  dirty: bool.isRequired,
  withUsingProvider: bool,
  syncErrors: object
};

const defaultProps = {
  withUsingProvider: false,
  disabled: false
};

class ProviderDetailsFormData extends Component {
  state = {
    hidenBuyingTypeCompany: !this.props.buyingType || this.props.buyingType === 'provider'
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { buyingType, buyingTypeCompany, change, dirty, currentProvider } = this.props;

    if (nextProps.buyingType === 'provider') this.setState({ hidenBuyingTypeCompany: true });

    if (
      nextProps.dirty && buyingType !== nextProps.buyingType &&
      nextProps.buyingTypeCompany && nextProps.buyingTypeCompany.companyType !== nextProps.buyingType
    ) {
      change('buyingTypeCompany', {});
    } else if (nextProps.buyingType !== 'provider') {
      this.setState({ hidenBuyingTypeCompany: false });
    }
  };

  render() {
    const {
      state: { hidenBuyingTypeCompany },
      props: {
        intl: { formatMessage },
        toggle,
        mixOptions,
        disabled,
        withUsingProvider,
        usingProvider,
        buyingType,
        buyingTypeOptions,
        providersOptions,
        syncErrors,
      }
    } = this;

    return (
      <Fragment>
        {
          withUsingProvider &&
            <div className='reminder-form-item'>
              <h3>Do you have an existing provider?</h3>
              <div className='radio-container grid-4'>
                <Field
                  { ...{ disabled } }
                  name='usingProvider'
                  options={ USING_PROVIDER_OPTIONS }
                  component={ RadioButtonField }
                />
              </div>
            </div>
        }
        {
          (!withUsingProvider || usingProvider) &&
            <Fragment>
              <div className={ classNames('reminder-form-item', { 'required': syncErrors.lastBuy }) }>
                <h3>When did you sign up with your current provider?</h3>
                <div className='radio-container grid-3 m-grid-2'>
                  <Field
                    { ...{ disabled } }
                    name='lastBuy'
                    options={ LAST_BUY_OPTIONS }
                    component={ RadioButtonField }
                  />
                </div>
              </div>
              <div className={ classNames('reminder-form-item', { 'required': syncErrors.buyingType }) }>
                {/*How did you buy? I bought:*/}
                <h3>
                  <I18n text={ defaultMessages.voteFormBuyingTypeTitle } />
                  <span>
                    <I18n text={ defaultMessages.voteFormBuyingTypeSubtitle } />:
                  </span>
                </h3>
                <div className='radio-container grid-2'>
                  <Field
                    { ...{ disabled } }
                    name='buyingType'
                    options={ BUYING_TYPE_OPTIONS }
                    component={ RadioButtonField }
                  />
                </div>
              </div>
              <div
                className={
                  classNames(
                    'reminder-form-item', { 'required': syncErrors.buyingTypeCompany || syncErrors.currentProvider }
                  )
                }
              >
                <div className='buying-company' style={ hidenBuyingTypeCompany ? { display: 'none' } : {} } >
                  <h3>Please name { PLEASE_NAME_TEXT[buyingType] }</h3>
                  <div className='grid-2' >
                    <Field
                      { ...{ disabled } }
                      name='buyingTypeCompany'
                      component={ BuyingTypeCompanyField }
                      placeholder={ formatMessage(defaultMessages.voteFormBuyingTypeInputPlaceholder) }
                      className='reminder-input'
                      items={ buyingTypeOptions }
                    />

                    { buyingType !== 'other' &&
                      <span onClick={() => toggle('isNewProviderModalOpen', buyingType)} className="add-new-provider">
                        + Add new {`${PLEASE_NAME_TEXT[buyingType]}`}
                      </span>
                    }

                  </div>
                </div>
                <div style={ !hidenBuyingTypeCompany ? { display: 'none' } : {} }>
                  <h3>Search current provider </h3>

                  <div className="grid-2">
                    <Field
                      { ...{ disabled } }
                      name='currentProvider'
                      component={ SelectAndAutocompleteInput }
                      wrapperClass='grid-1 provider'
                      selectClass='reminder-select'
                      inputClass='reminder-input'
                      prefixClass='Select'
                      selectPlaceholder='Search current provider'
                      inputPlaceholder='Select provider by name'
                      items={ providersOptions }
                    />

                    <span onClick={() => toggle('isNewProviderModalOpen', 'provider')} className="add-new-provider">
                      + Add new Provider
                    </span>
                  </div>
                </div>

              </div>
              <div className={ classNames('reminder-form-item', { 'required': syncErrors.checkedPlaces }) }>
                <h3>How many places / sites do you usually check?</h3>
                <div className='radio-container grid-3'>
                  <Field
                    { ...{ disabled } }
                    name='checkedPlaces'
                    options={ CHECKED_PLACES_OPTIONS }
                    component={ RadioButtonField }
                  />
                </div>
              </div>
            </Fragment>
        }
      </Fragment>
    );
  }
};
ProviderDetailsFormData.propTypes = propTypes;

export default ProviderDetailsFormData;
