import React from 'react';
import { string } from 'prop-types';

const propTypes = {
  rank: string.isRequired,
  code: string.isRequired,
  usersCount: string.isRequired
};

const CountrySliderElement = ({ rank, code, usersCount }) => {
  return (
    <div className='flag-div'>
      <span className='-num'>{ rank }</span>
      <div className='flag-image'>
        <img src={ `/images/flags/${code}.png` } />
      </div>
      <span className='-num country-members-counter'>{ usersCount }</span>
    </div>
  );
};
CountrySliderElement.propTypes = propTypes;

export default CountrySliderElement;
