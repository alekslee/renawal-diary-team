import React, { Fragment } from 'react';
import { object, any, array, bool } from 'prop-types';

import RadioButtonGroup from 'components/RadioButtonGroup';

const propTypes = {
  input: object.isRequired,
  meta: object.isRequired,
  options: array.isRequired,
  disabled: bool
};

const defaultProps = {
  disabled: false
};

const RadioButtonField = ({ input: { name, value, onChange }, options, disabled }) => {
  if (disabled) options = options.map(option => ({ ...option, disabled: true }));
  return (
    <Fragment>
      <RadioButtonGroup
        { ...{ name, options, onChange, value } }
      />
    </Fragment>
  );
};
RadioButtonField.propTypes = propTypes;
RadioButtonField.defaultProps = defaultProps;

export default RadioButtonField;
