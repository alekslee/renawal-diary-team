import React, { Component } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { array, any, func, string } from 'prop-types';

const propTypes = {
  options: array.isRequired,
  selected: any.isRequired,
  onChange: func.isRequired,
  className: string
};

class DropdownComponent extends Component {
  state = {
    isOpen: false
  }

  toggle = () => this.setState({ isOpen: !this.state.isOpen });

  render() {
    const {
      toggle,
      state: { isOpen },
      props: { options, onChange, selected, className }
    } = this;

    const filteredOptions = options.filter(option => option !== selected);

    return (
      <Dropdown { ...{ toggle, isOpen } } className={ className } >
        <DropdownToggle color='white' className='reminder-dropdown-toggle' >
          { selected }
        </DropdownToggle>
        <DropdownMenu>
          {
            filteredOptions.map(option => (
              <DropdownItem key={ option } onClick={ () => onChange(option) } >
                { option }
              </DropdownItem>
            ))
          }
        </DropdownMenu>
      </Dropdown>
    );
  }
};
DropdownComponent.propTypes = propTypes;

export default DropdownComponent;
