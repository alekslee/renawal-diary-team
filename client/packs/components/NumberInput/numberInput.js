import React, { Fragment } from 'react';
import Select from 'react-select';
import { object, string, bool, number, oneOfType, func } from 'prop-types';

const propTypes = {
  value: oneOfType([number, string]),
  inputProps: object,
  minValue: number,
  symbolsAfterDot: number,
  disabled: bool,
  onChange: func.isRequired
};

const defaultProps = {
  inputProps: {},
  symbolsAfterDot: null,
  value: null,
  minValue: null,
  disabled: false
};

const NumberInput = ({ inputProps, minValue, disabled, symbolsAfterDot, value, onChange }) => {
  const stringToNumber = str => {
    const parsedNumber = parseInt(str.replace(/[.,]/g, ''));
    if (minValue !== null) {
      return parsedNumber < minValue ? minValue : parsedNumber;
    }
    return parsedNumber;
  };

  const numberToString = number => {
    if (number !== 0 && !number) return '';
    if (number === 0) return symbolsAfterDot ? '0.00' : '0';

    const str = (symbolsAfterDot ? number / 100.0 : number).toString();

    const symbolsBeforeFirstDivider = (str.length - (symbolsAfterDot || 0)) % 3;
    let reg;
    if (symbolsBeforeFirstDivider) {
      reg = new RegExp(`(^\\d{${symbolsBeforeFirstDivider}}|(?:.)\\d{3})(?=[^.])`, 'g');
    } else {
      reg = new RegExp('(\\d{3})(?=[^.])', 'g');
    };
    return str.replace(reg, '$1,');
  };

  const onChangeHandler = ({ target: { value } }) => {
    onChange(stringToNumber(value));
  };

  return (
    <input
      type='text'
      className='reminder-input'
      { ...{ ...inputProps, disabled, value: numberToString(value) } }
      onChange={ onChangeHandler }
      onBlur={ onChangeHandler }
    />
  );
};
NumberInput.propTypes = propTypes;
NumberInput.defaultProps = defaultProps;

export default NumberInput;
