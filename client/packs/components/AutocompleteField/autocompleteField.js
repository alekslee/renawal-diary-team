import React, { Component } from 'react';
import { object, array, string, shape, func, any, bool } from 'prop-types';

import Autocomplete from 'components/Autocomplete';

const propTypes = {
  input: shape({
    onChange: func.isRequired
  }).isRequired,
  meta: object.isRequired,
  items: array.isRequired,
  className: string,
  placeholder: string,
  disabled: bool,
  fieldValue: any,
  onChange: func
};

const AutocompleteField = ({
  input, disabled, items, className, placeholder, fieldValue, onChange
}) => {
  const onFocusHandler = () => input.onChange('');

  const onChangeHandler = value => {
    if (value && !value.label) value = {};
    input.onChange(value);
    if (onChange) onChange(value);
  };

  return (
    <Autocomplete
      { ...{ items, className, placeholder } }
      inputProps={ { disabled, onFocus: onFocusHandler } }
      value={ fieldValue && fieldValue.label }
      onChange={ onChangeHandler }
    />
  );
};
AutocompleteField.propTypes = propTypes;

export default AutocompleteField;
