import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import { bindActionCreators } from 'redux';
import AutocompleteField from './autocompleteField';

const mapStateToProps = (state, props) => ({
  fieldValue: formValueSelector(props.meta.form)(state, props.input.name)
});

export default connect(mapStateToProps, null)(AutocompleteField);
