import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import { number, object, string, bool, func } from 'prop-types';
import { Row, Col } from 'reactstrap';
import cx from 'classnames';

import RadioButtonGroup from 'components/RadioButtonGroup';
import RadioButtonField from 'components/RadioButtonField';

import { RateField, StarRateField } from './components';
import { LABELS_COMPANY_TYPES, selectedRadioStates } from './constants';

const propTypes = {
  currentProviderRate: number,
  buyingTypeCompanyRate: number,
  buyingTypeCompany: object,
  formAttributes: object,
  buyingType: string,
  currentProvider: object,
  disabled: bool,
  change: func.isRequired
};

class RatingFormData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedRadio: this.retrieveSelectedRatio(props)
    };
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      this.props.currentProvider !== nextProps.currentProvider ||
      this.props.buyingTypeCompany !== nextProps.buyingTypeCompany
    ) {
      this.setState({ selectedRadio: this.retrieveSelectedRatio(nextProps) });
    }

    if (this.props.buyingType !== nextProps.buyingType) this.props.change('buyingTypeCompanyRate', null);
  };

  retrieveSelectedRatio = ({ currentProvider, buyingTypeCompany }) => {
    if (!currentProvider && buyingTypeCompany && buyingTypeCompany.value) {
      return selectedRadioStates.BUYING_TYPE_COMPANY;
    }
    if (currentProvider && currentProvider.value) return selectedRadioStates.CURRENT_PROVIDER;
    return {};
  };

  onChangeRadioHandler = value => {
    this.setState({ selectedRadio: value });
  };

  // retrieveRadionButtonOptions = () => {
  //   const {
  //     buyingTypeCompany,
  //     buyingType,
  //     currentProvider,
  //     currentProviderRate,
  //     buyingTypeCompanyRate
  //   } = this.props;

  //   const isVisibleBuyingTypeRate = buyingTypeCompany && buyingTypeCompany.label && buyingType !== 'provider';

  //   return [
  //     {
  //       value: selectedRadioStates.CURRENT_PROVIDER,
  //       disabled: !currentProvider,
  //       visible: !!currentProvider && !!currentProvider.label,
  //       title: 'Provider',
  //       label: (
  //         <Fragment>
  //           { currentProvider && currentProvider.label }
  //           <div className='rate-text'>
  //             { currentProviderRate }/10
  //           </div>
  //         </Fragment>
  //       )
  //     },
  //     {
  //       value: selectedRadioStates.BUYING_TYPE_COMPANY,
  //       disabled: !buyingTypeCompany,
  //       visible: isVisibleBuyingTypeRate,
  //       title: LABELS_COMPANY_TYPES[buyingType],
  //       label: (
  //         <Fragment>
  //           { buyingTypeCompany && buyingTypeCompany.label }
  //           <div className='rate-text'>
  //             { buyingTypeCompanyRate }/10
  //           </div>
  //         </Fragment>
  //       )
  //     }
  //   ];
  // };

  changeRateColumn = newRateColumn => {
    const {
      buyingTypeCompany,
      buyingType,
    } = this.props;

    const isVisibleBuyingTypeRate = buyingTypeCompany && buyingTypeCompany.label && buyingType !== 'provider';
    if (newRateColumn === selectedRadioStates.BUYING_TYPE_COMPANY && isVisibleBuyingTypeRate) {
      this.setState({ selectedRadio: selectedRadioStates.BUYING_TYPE_COMPANY });
    } else if (newRateColumn === selectedRadioStates.CURRENT_PROVIDER) {
      this.setState({ selectedRadio: selectedRadioStates.CURRENT_PROVIDER });
    }
  };

  RenderTenRating = ({ onTrigger, namePrefix, hide }) => {
    const {
      retrieveRadionButtonOptions,
      changeRateColumn,
      state: { selectedRadio },
      props: { disabled },
    } = this;

    return (
      <div style={ hide ? {} : { display: 'none' } } >

        <Field
          component={ RateField }
          name={`${namePrefix}Rate`}
          onTrigger={ () => {
            //onTrigger
          } }
        />

        <div className="d-flex justify-content-between rate-help-title" style={{ width: 327 }}>
          <h5 className="d-inline">Not at all Likely</h5>
          <h5 className="d-inline">Extremely Likely</h5>
        </div>

        <br/>

       {/* <div className='textarea-field'>
          <Field
            component='input'
            className='reminder-input'
            placeholder='Title'
            disabled={ disabled }
            name={`${namePrefix}RateTitle`}
          />
        </div>

        <br/>

        <div className='textarea-field'>
          <Field
            component='textarea'
            className='reminder-input'
            placeholder='Review'
            disabled={ disabled }
            name={`${namePrefix}RateComment`}
          />
        </div>*/}
      </div>
    );
  }

  render() {
    const {
      onChangeRadioHandler,
      retrieveRadionButtonOptions,
      changeRateColumn,
      state: { selectedRadio },
      props: { disabled, formAttributes },
      RenderTenRating,
    } = this;
    // const radioButtonOptions = retrieveRadionButtonOptions();

    const  fAttributes = formAttributes || {};
    const  provider = fAttributes.currentProvider;
    const  company = fAttributes.buyingTypeCompany;

    return (
      <div className="top-part">
        <div className='reminder-form-item'>
          <h3>How likely are you to recommend this business to a friend or colleague?</h3>

          <Row>
            { provider &&
              <Col xs={ 12 } onClick={ () => onChangeRadioHandler(selectedRadioStates.CURRENT_PROVIDER) }>
                <h2 className="rating-title mb-2">
                  { provider.name }
                </h2>
              </Col>
            }

            { company &&
              <Col xs={ 12 } onClick={ () => onChangeRadioHandler(selectedRadioStates.BUYING_TYPE_COMPANY) }>
                <h2 className="rating-title mb-2">
                 { company.name }
                </h2>
              </Col>
            }
          </Row>

          <br/>
            <p> Rate your experience with this businness</p>

          <br/>

          <div className='rating-block-container'>
            <p> PRICE </p>
            <Field
              component={ StarRateField }
              name='priceRate'
              disabled={ disabled }
            />
            <h5>Tap a start to rate</h5>
          </div>

          <div className='rating-block-container'>
            <p> SERVICE </p>
            <Field
              component={ StarRateField }
              name='serviceRate'
              disabled={ disabled }
            />
            <h5>Tap a start to rate</h5>
          </div>

          <h3> Did you submit a claim with business? </h3>
          <div className="radio-container grid-4">
            <Field
              name="claimWithBussiness"
              options={[{ value: true, label: 'Yes'}, { value: false, label: 'No'}]}
              disabled={ disabled }
              component={ RadioButtonField }
            />
          </div>

          <br/>

          <div className='rating-block-container'>
            <p> CLAIMS </p>
            <Field
              component={ StarRateField }
              name='claimRate'
              disabled={ disabled }
            />
            <h5>Tap a start to rate</h5>
          </div>

          <div className='rating-block-container'>
            <RenderTenRating
              namePrefix="currentProvider"
              // onTrigger={() => changeRateColumn(selectedRadioStates.BUYING_TYPE_COMPANY) }
              hide={selectedRadio === selectedRadioStates.CURRENT_PROVIDER}
            />

            <RenderTenRating
              namePrefix="buyingTypeCompany"
              // onTrigger={() => changeRateColumn(selectedRadioStates.CURRENT_PROVIDER) }
              hide={selectedRadio === selectedRadioStates.BUYING_TYPE_COMPANY}
            />
          </div>

        </div>
      </div>
    );
  }
};
RatingFormData.propTypes = propTypes;

export default RatingFormData;
