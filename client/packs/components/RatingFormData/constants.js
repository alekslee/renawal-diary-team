export const LABELS_COMPANY_TYPES = {
  provider: 'Provider',
  broker: 'Broker',
  comparsion_site: 'Price Comparison Site',
  other: 'Other Source'
};

export const selectedRadioStates = {
  CURRENT_PROVIDER: 'currentProvider',
  BUYING_TYPE_COMPANY: 'buyingTypeCompany',
};
