import React from 'react';
import { object, bool, func } from 'prop-types';

import cx from 'classnames';
import StarIcon from 'images/icons/StarIcon';
import ReactStars from 'react-stars'

const propTypes = {
  input: object.isRequired,
  disabled: bool,
};

const StarRateField = ({ input: { value, onChange }, disabled, onTrigger }) => {
  value = value || undefined;

  const onClickHandler = (i) => {
    if (disabled) return;
    onChange(i);
  };

  return (
    <div className="rate-block">
      <div className="rate-icon-container">

        <ReactStars
          className="rate-icon-parent"
          value={value}
          count={5}
          onChange={onClickHandler}
          size={25}
          color1="#e3e5ea"
          color2="#efce4a"
        />

        <span className="rating-value">
          { value }
        </span>
      </div>
    </div>
  );
};
StarRateField.propTypes = propTypes;

export default StarRateField;
