import React from 'react';
import { object, bool, func } from 'prop-types';

import { Rate } from './components';

const propTypes = {
  input: object.isRequired,
  disabled: bool,
  onTrigger: func
};

const RateField = ({ input: { value, onChange }, disabled, onTrigger }) => {
  value = value || undefined;
  return (
    <Rate { ...{ onChange, value, disabled, onTrigger } } />
  );
};
RateField.propTypes = propTypes;

export default RateField;
