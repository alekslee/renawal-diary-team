import React, { Component } from 'react';
import classNames from 'classnames';
import { number, func, bool } from 'prop-types';

import RatingBadIcon from 'images/icons/RatingBadIcon';
import RatingNormalIcon from 'images/icons/RatingNormalIcon';
import RatingGoodIcon from 'images/icons/RatingGoodIcon';

const propTypes = {
  value: number,
  disabled: bool,
  onChange: func.isRequired,
  onTrigger: func
};
const defaultProps = {
  disabled: false,
  onTrigger: () => {}
};

class Rate extends Component {

  generateRatings = () => {
    const {
      props: { value, onChange, disabled, onTrigger }
    } = this;

    const onClickHandler = i => {
      if (disabled) return;

      onChange(i);
      onTrigger();
    };

    let ratings = [];
    for (let i = 1; i <= 10; i++) {
      let ratingIcon;

      if (i <= 6) {
        ratingIcon = <RatingBadIcon />;
      } else if (i >= 9) {
        ratingIcon = <RatingGoodIcon />;
      } else {
        ratingIcon = <RatingNormalIcon />;
      }
      ratings.push(
        <div
          key={ i }
          className={ classNames(
            'rate-icon', { 'active' : i <= value }
          ) }
          onClick={ () => onClickHandler(i) }
        >
          { ratingIcon }
          <span className='rate-value'>{ i }</span>
        </div>
      );
    };
    return ratings;
  };

  render() {
    const {
      generateRatings,
      props: { value }
    } = this;

    const rateVal = parseInt(value) || 0;

    return (
      <div className='rate-block'>
        <div className='rate-icon-container'>
          { generateRatings() }
        </div>
        <div className='rate-progress-bar' >
          {/*<hr*/}
          {/*  className={ classNames(*/}
          {/*    'active-bar',*/}
          {/*    { 'hidden': !rateVal  },*/}
          {/*    { 'full': rateVal === 10 }*/}
          {/*  ) }*/}
          {/*  style={ { width: `calc(5px + ${(rateVal - 1) * 33}px)` } }*/}
          {/*/>*/}
          <hr className='default-bar' />
          <span className='dash dash-first' />
          <span className='dash dash-second' />
        </div>
      </div>
    );
  }
};
Rate.propTypes = propTypes;
Rate.defaultProps = defaultProps;

export default Rate;
