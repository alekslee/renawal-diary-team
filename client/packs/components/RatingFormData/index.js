import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formValueSelector, getFormValues } from 'redux-form';
import RatingFormData from './ratingFormData';

const mapStateToProps = (state, props) => ({
  formAttributes: getFormValues(props.form)(state),
  currentProviderRate: formValueSelector(props.form)(state, 'currentProviderRate'),
  currentProvider: formValueSelector(props.form)(state, 'currentProvider'),
  buyingTypeCompany: formValueSelector(props.form)(state, 'buyingTypeCompany'),
  buyingType: formValueSelector(props.form)(state, 'buyingType'),
  buyingTypeCompanyRate: formValueSelector(props.form)(state, 'buyingTypeCompanyRate'),
});

export default connect(mapStateToProps, null)(RatingFormData);
