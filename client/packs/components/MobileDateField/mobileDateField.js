import React from 'react';
import { object, string } from 'prop-types';
import moment from 'moment';

const propTypes = {
  input: object.isRequired,
  className: string
};

const MobileDateField = ({ input, className, name }) => {
  return (
    <div className='react-datepicker-wrapper mobile-date'>
      <div className='react-datepicker__input-container '>
        <input
          type='date'
          value={ input.value }
          onChange={ input.onChange }
          placeholder={ input.value!='' ? moment(input.value).format('LL') : 'Select date' }
          {...{ className, name }}
        />
      </div>
    </div>
  );
}

MobileDateField.propTypes = propTypes;

export default MobileDateField;
