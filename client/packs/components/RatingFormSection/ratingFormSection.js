import React, { Component, Fragment } from 'react';
import { object, string, bool, func } from 'prop-types';

import FormSlider from 'components/FormSlider';
import RatingFormData from 'components/RatingFormData';

const propTypes = {
  buyingTypeCompany: object,
  currentProvider: object,
  disabled: bool,
  form: string.isRequired,
  change: func.isRequired
};

const RatingFormSection = ({ currentProvider, buyingTypeCompany, disabled, form, change }) => {
  const isVisible = !!(currentProvider || buyingTypeCompany);
  if (!isVisible) return <Fragment />;

  return (
    <FormSlider title='My Customer Satisfaction Ratings: (Help other members)' formIcon="CustomerSatisfaction">
      <RatingFormData { ...{ change, disabled, form } } />
    </FormSlider>
  );
};
RatingFormSection.propTypes = propTypes;

export default RatingFormSection;
