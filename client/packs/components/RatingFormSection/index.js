import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formValueSelector } from 'redux-form';
import RatingFormSection from './ratingFormSection';

const mapStateToProps = (state, props) => ({
  currentProvider: formValueSelector(props.form)(state, 'currentProvider'),
  buyingTypeCompany: formValueSelector(props.form)(state, 'buyingTypeCompany')
});

export default connect(mapStateToProps, null)(RatingFormSection);
