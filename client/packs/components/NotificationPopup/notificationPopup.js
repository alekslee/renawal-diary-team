import React, { Fragment } from 'react';
import { object } from 'prop-types';
import moment from 'moment';

const propTypes = {
  // icon: object,
  // title: object,
  // message: object,
  // remove: func,
}

const NotificationPopup = (props) => {
  const { icon, title, message, remove } = props;

  return(
    <Fragment>
      <div
        className='close'
        onClick={ () => remove() }
      />
      <div className='notify-title'>{ title }</div>
      <div className='notify-body'>{ message }</div>
    </Fragment>
  );
};

NotificationPopup.propTypes = propTypes;

export default NotificationPopup;