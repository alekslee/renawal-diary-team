import React from 'react';
import {} from 'prop-types';

const propTypes = {};

const BenefitsInfoBlock = (props) => {
  return (
    <div className='info-sidebar'>
      <div className='info-sidebar-header'>
        <p>Benefits</p>
      </div>
      <div className='info-sidebar-content'>
        <div>
          <h3>Best value & service:</h3>
          <p>We find the businesses offering the best value and customer service</p>
        </div>
        <div>
          <h3>Save time and money:</h3>
          <p>We can save more time and money</p>
        </div>
        <div>
          <h3>Benchmarked Insights:</h3>
          <p>We can benchmark our prices & insights with each other</p>
        </div>
        <div>
          <h3>Support:</h3>
          <p>we can support each other via Q&A</p>
        </div>
        <div>
          <h3>Group Offers:</h3>
          <p>we can leverage our numbers to activate group offers</p>
        </div>
        <div>
          <h3>Free Draw:</h3>
          <p>
            Vote & we will enter you into a free monthly draw to pay your contract for a year!
            * Proof of purchase required.
          </p>
        </div>
      </div>
    </div>
  );
};

BenefitsInfoBlock.propTypes = propTypes;

export default BenefitsInfoBlock;
