import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { injectIntl } from 'react-intl';
import { withRouter } from 'react-router-dom';
import Header from './header';

import { selectors } from 'core/currentCountry';
import { selectors as categorySelectors } from 'core/category';
import { selectors as currentUserSelectors } from 'core/currentUser';
import { actions as categoryActions } from 'core/category';

const mapStateToProps = state => ({
  category: categorySelectors.getCategory(state),
  categories: categorySelectors.getCategories(state),
  flattenCategories: categorySelectors.getFlattenCategories(state),
  categoriesOptions: categorySelectors.getCategoriesOptions(state),
  countryCode: selectors.getCurrentCountryCode(state),
  currentUser: currentUserSelectors.getCurrentUser(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setCategoryDispatch: categoryActions.setCategory,
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(Header)));
