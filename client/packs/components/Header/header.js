import React, { Component, Fragment } from 'react';
import { any, string, object, array, func } from 'prop-types';
import { NavLink, Link } from 'react-router-dom';
import { defaultMessages } from 'locales/default';
import { intlShape } from 'react-intl';
import { paths } from 'layouts/constants';
import Select from 'react-select';
import cx from 'classnames';

import { Navbar, NavbarBrand, NavbarToggler, Collapse } from 'reactstrap';
import I18n from 'components/I18n';

import PartnerIcon from 'images/icons/PartnerIcon';
import GlobalMission from 'images/icons/GlobalMission';
import Close from 'images/svg/close';
import Logo from 'images/icons/Logo';
import MyNotesIcon from 'images/icons/MyNotesIcon';
import LeaderboardsIcon from 'images/icons/LeaderboardsIcon';
import VoteIcon from 'images/icons/VoteIcon';
import CalendarAdd from 'images/svg/calendar-add';

const propTypes = {
  children: any,
  currentUser: object,
  category: object,
  categories: array,
  flattenCategories: array,
  categoriesOptions: array,
  setCategoryDispatch: func,
  countryCode: string.isRequired,
  intl: intlShape.isRequired,
  history: object.isRequired,
};

class Header extends Component {
  state = {
    isOpen: false,
    isOpenCategories: false,
  };

  toggle = key => this.setState({ [key]: !this.state[key] });

  CollapseCategories = ({ slag }) => {
    const {
      toggle,
      state: { isOpenCategories },
      props: {
        history, countryCode, setCategoryDispatch, categoriesOptions, flattenCategories,
        intl: { formatMessage },
      }
    } = this;

    return (
      <Collapse isOpen={ isOpenCategories } navbar >
        <ul className='nav navbar-nav'>
          <div className='left-links'>
            { categoriesOptions.map(({ label, options }, ind1) => (
              <Fragment key={ind1}>
                <div className="pl-4 py-4">
                  { label }
                </div>

                { options.map(({ value, label }, ind2) => (
                  <div
                    key={ind2}
                    onClick={ev => {
                      ev.preventDefault();
                      const category = flattenCategories.find(({ id }) => +id === +value);
                      // category && setCategoryDispatch({ category });
                      category && history.push(paths.LEADERBOARD.replace(':country', countryCode).replace(':slag', category.slag));
                      toggle('isOpen');
                    }}
                    className="nav-link global-link hide-desktop pl-5"
                  >
                    { label }
                  </div>
                 ))}
              </Fragment>
            )) }
          </div>
        </ul>
        </Collapse>
    );
  }

  CollapseLinks = ({ slag }) => {
    const {
      toggle,
      CollapseCategories,
      state: { isOpen, isOpenCategories },
      props: {
        countryCode, children, categoriesOptions, flattenCategories,
        category,
        intl: { formatMessage },
      }
    } = this;

    return (
      <Collapse isOpen={ isOpen } navbar >
        <ul className='nav navbar-nav'>
          <div className='left-links'>
            <NavLink
              onClick={() => toggle('isOpen') }
              to={ paths.LEADERBOARD.replace(':country', countryCode).replace(':slag', slag).concat('?open-reminders=true') }
              className='nav-link global-link hide-desktop'
            >
              <img src={ CalendarAdd } alt="Reminders"/>
              My reminders
            </NavLink>
            <div
              onClick={(ev) => {
                ev.preventDefault()
                toggle('isOpenCategories')
              } }
              className='nav-link global-link hide-desktop categories-selector'
            >
              <div className={cx({ "open-categories": isOpenCategories })}>
                Select Categories
              </div>

              <CollapseCategories slag={slag} />

            </div>
            <NavLink
              onClick={() => toggle('isOpen') }
              to={ paths.VOTE.replace(':country', countryCode).replace(':slag', slag) }
              className='nav-link global-link hide-desktop'
            >
              <VoteIcon />
              Have your say!
            </NavLink>
            <NavLink
              onClick={() => toggle('isOpen') }
              to={ paths.LEADERBOARD.replace(':country', countryCode).replace(':slag', slag) }
              className='nav-link global-link hide-desktop'
            >
              <LeaderboardsIcon />
              <I18n text={ defaultMessages.leaderboard } />
            </NavLink>
            <NavLink
              onClick={() => toggle('isOpen') }
              to={ paths.MY_NOTES.replace(':country', countryCode).replace(':slag', slag) }
              className='nav-link global-link hide-desktop'
            >
              <MyNotesIcon />
              <I18n text={ defaultMessages.myNotes } />
            </NavLink>

            <NavLink
              onClick={() => toggle('isOpen') }
              to={ paths.OUR_SHARED_MISSION.replace(':country', countryCode) }
              className='nav-link global-link'
            >
              <GlobalMission />
              { formatMessage(defaultMessages.ourSharedMission) }
            </NavLink>
            <NavLink
              onClick={() => toggle('isOpen') }
              to={ paths.BECOME_A_PARTNER.replace(':country', countryCode) }
              className='nav-link partner-link'
            >
              <PartnerIcon />
              { formatMessage(defaultMessages.becomeAPartner) }
            </NavLink>
          </div>
          <div className='right-links'>
            { children }
          </div>
        </ul>
        </Collapse>
    );
  }
  render() {
    const {
      CollapseLinks,
      toggle,
      state: { isOpen },
      props: { children, countryCode, currentUser, categories, category, intl: { formatMessage } }
    } = this;

    const slag = (category || {}).slag || (((categories[0] || {}).leafChildren || {})[0] || {}).slag;

    return (
      <header>
        <div className={ cx('container', {'active': isOpen }) }>
          <Navbar className='navbar-expand-lg navbar-dark'>
            <NavLink
              to={ paths.ROOT.replace(':country', countryCode) }
              className='nav-link navbar-brand partner-link'
            >
              <Logo />
              <span>Renewal Diary</span>
            </NavLink>

            <NavbarToggler onClick={ () => toggle('isOpen') } >
              {!isOpen ?
                <svg
                  className='navbar-custom-toggler-icon' focusable='false' height='30'
                  viewBox='0 0 30 30' width='30' xmlns='http://www.w3.org/2000/svg'
                >
                  <title>
                  Menu
                  </title>
                  <path
                    d='M4 7h42M4 15h42M4 23h42' stroke='currentColor' strokeLinecap='round'
                    strokeMiterlimit='10' strokeWidth='2'
                  />
                </svg> : <img src={ Close } alt="Close" />}
            </NavbarToggler>

            { slag &&
              <CollapseLinks slag={slag}/> }

          </Navbar>
        </div>
      </header>
    );
  }
};

Header.propTypes = propTypes;

export default Header;
