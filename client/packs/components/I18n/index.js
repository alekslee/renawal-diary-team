import I18n from './i18n';
import { injectIntl } from 'react-intl';

export default injectIntl(I18n);
