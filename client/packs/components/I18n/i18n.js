import React, { Fragment } from 'react';
import { intlShape } from 'react-intl';
import { shape, string, object } from 'prop-types';

const propTypes = {
  intl: intlShape.isRequired,
  text: shape({
    id: string.isRequired,
    defaultMessage: string.isRequired
  }).isRequired,
  variables: object
};
const defaultProps = {
  variables: {}
};

const I18n = ({ intl: { formatMessage }, text: { id, defaultMessage }, variables }) => {
  return (
    <Fragment>{ formatMessage({ id, defaultMessage }, variables) }</Fragment>
  );
};
I18n.propTypes = propTypes;
I18n.defaultProps = defaultProps;

export default I18n;
