import React from 'react';
import cx from 'classnames';
import { object, string, shape, bool } from 'prop-types';

const propTypes = {
  input: object.isRequired,
  placeholder: string,
  type: string,
  className: string,
  disabled: bool,
  meta: shape({
    touched: bool,
    error: string
  }).isRequired,
  componentType: string
};

const defaultProps = {
  componentType: 'input',
  disabled: false
};

const FieldWithErrors = ({
  input, placeholder, onKeyPress, disabled, type, className, meta: { touched, error }, componentType
}) => {
  const onFocusHandler = () => input.onChange('');

  const inputProps = { ...input,
    onFocus: onFocusHandler,
    className: cx(className, { 'error-input': touched && error, disabled: disabled }),
    onKeyPress, placeholder,
    disabled, type,
  };
  return (
    <div>
      {
        componentType === 'textarea' ?
          <textarea { ...inputProps } />
          :
          <input { ...inputProps } />
      }
      { touched && (error && <span className="error-hint">{ error }</span>) }
    </div>
  );
};
FieldWithErrors.propTypes = propTypes;
FieldWithErrors.defaultProps = defaultProps;

export default FieldWithErrors;
