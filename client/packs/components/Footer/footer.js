import React from 'react';
import { defaultMessages } from 'locales/default';
import I18n from 'components/I18n';
import { EmailShareButton } from 'react-share';
import {} from 'prop-types';

const propTypes = {};

const Footer = (props) => {
  return (
    <footer className='text-center'>
      © Renewal Diary.&nbsp;
      <I18n text={ defaultMessages.allRightsReserved } />
      {/*<a href='mailto:hello@renewaldiary.com'> hello@renewaldiary.com</a>*/}
    </footer>
  );
};
Footer.propTypes = propTypes;

export default Footer;
