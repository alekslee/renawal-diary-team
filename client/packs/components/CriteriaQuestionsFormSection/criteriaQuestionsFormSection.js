import React from 'react';
import { object, string, bool } from 'prop-types';

import FormSlider from 'components/FormSlider';
import CriteriaQuestionsFormData from 'components/CriteriaQuestionsFormData';

const propTypes = {
  criteriaOptions: object.isRequired,
  title: string.isRequired,
  formIcon: string,
  namePrefix: string.isRequired,
  disabled: bool,
  form: string
};

const CriteriaQuestionsFormSection = ({ criteriaOptions, disabled, title, namePrefix, form, formIcon }) => {
  return (
    <FormSlider title={ title } formIcon={ formIcon } >
      <CriteriaQuestionsFormData { ...{ criteriaOptions, disabled, namePrefix, form } } />
    </FormSlider>
  );
};
CriteriaQuestionsFormSection.propTypes = propTypes;

export default CriteriaQuestionsFormSection;
