import React, { Component } from 'react';
import { UnauthorizedModalsContext } from 'layouts/UnAuthorizedApp/context';
import { string, any, func } from 'prop-types';

const propTypes = {
  className: string,
  children: any,
  onSignUpSuccess: func,
};

class SignInModalLink extends Component {
  render() {
    const { className, children, onSignUpSuccess } = this.props;
    const { toggleSignInModal } = this.context;

    return (
      <a { ...{ className } } onClick={ () => toggleSignInModal(onSignUpSuccess) } >
        { children }
      </a>
    );
  }
};
SignInModalLink.propTypes = propTypes;
SignInModalLink.contextType = UnauthorizedModalsContext;

export default SignInModalLink;
