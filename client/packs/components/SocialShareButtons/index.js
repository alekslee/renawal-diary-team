import SocialShareButtons from './socialShareButtons';
import { injectIntl } from 'react-intl';

export default injectIntl(SocialShareButtons);
