import React from 'react';
import { intlShape } from 'react-intl';
import { EmailShareButton, FacebookShareButton, TwitterShareButton, LinkedinShareButton } from 'react-share';
import { bool } from 'prop-types';

import { defaultMessages } from 'locales/default';
import { BASE_APP_URL } from 'core/constants';
import EmailImage from 'images/svg/mail.svg';
import FacebookImage from 'images/svg/facebook.svg';
import TwitterImage from 'images/svg/twitt.svg';
import LinkedinImage from 'images/svg/linkedin.svg';

const propTypes = {
  intl: intlShape.isRequired,
  withEmail: bool
};

const defaultProps = {
  withEmail: false
};

const SocialShareButtons = ({ intl: { formatMessage }, withEmail }) => {
  const inviteMessage = formatMessage(defaultMessages.inviteMessage);

  return (
    <div className='choose-social'>
      {
        withEmail &&
          <EmailShareButton
            className='social-link email'
            url={ BASE_APP_URL }
            subject={ formatMessage(defaultMessages.inviteMailSubject) }
            body={ `${inviteMessage} ${BASE_APP_URL}` }
          >
            <img src={ EmailImage } alt='email' />
            { formatMessage(defaultMessages.email) }
          </EmailShareButton>
      }
      <FacebookShareButton url={ BASE_APP_URL } quote={ inviteMessage } className='social-link facebook' >
        <img src={ FacebookImage } alt='facebook' />
        Facebook
      </FacebookShareButton>
      <TwitterShareButton url={ BASE_APP_URL } title={ inviteMessage } className='social-link twitter'>
        <img src={ TwitterImage } alt='twitter' />
        Twitter
      </TwitterShareButton>
      <LinkedinShareButton url={ BASE_APP_URL } title={ inviteMessage } className='social-link linkedin' >
        <img src={ LinkedinImage } alt='linkedin' />
        Linkedin
      </LinkedinShareButton>
    </div>
  );
};
SocialShareButtons.defaultProps = defaultProps;
SocialShareButtons.propTypes = propTypes;

export default SocialShareButtons;
