import React, { Fragment } from 'react';
import { Field } from 'redux-form';
import { bool, string } from 'prop-types';

import PriceField from 'components/PriceField';

const propTypes = {
  havingSavingMetre: bool,
  namePrefix: string.isRequired,
  disabled: bool,
  name: string.isRequired
};

const SavingMetrePriceCriteria = ({ havingSavingMetre, disabled, namePrefix, name }) => {
  return (
    <div className='reminder-form-item grid-4'>
      {
        havingSavingMetre ?
          <Fragment>
            <div>
              <h5>Day time</h5>
              <Field
                name={ `${namePrefix}[${name}][day_time]` }
                currencySymbol='c'
                component={ PriceField }
                disabled={ disabled }
                withDot
              />
            </div>
            <div>
              <h5>Night time</h5>
              <Field
                name={ `${namePrefix}[${name}][night_time]` }
                currencySymbol='c'
                component={ PriceField }
                disabled={ disabled }
                withDot
              />
            </div>
          </Fragment>
          :
          <div>
            <h5>Price</h5>
            <Field
              name={ `${namePrefix}[${name}][price]` }
              currencySymbol='c'
              component={ PriceField }
              disabled={ disabled }
              withDot
            />
          </div>
      }
      <span className='hint'>(Optional : For members that wish to benchmark their prices)</span>
    </div>
  );
};
SavingMetrePriceCriteria.propTypes = propTypes;

export default SavingMetrePriceCriteria;
