import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CarMakeCriteria from './carMakeCriteria';

import { selectors, actions } from 'core/renewalDetail';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchRenewalDetailCriteriasQuestionsCarModelsOptionsDispatch:
      actions.fetchRenewalDetailCriteriasQuestionsCarModelsOptions
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(CarMakeCriteria);
