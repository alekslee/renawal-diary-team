import React from 'react';
import { Field } from 'redux-form';
import { object, bool, string } from 'prop-types';

import FieldWithErrors from 'components/FieldWithErrors';

const propTypes = {
  question: object.isRequired,
  name: string.isRequired,
  disabled: bool,
  namePrefix: string.isRequired
};

const TextareaCriteria = ({ question, name, namePrefix, disabled }) => {
  return (
    <div className='reminder-form-item'>
      <h5>{ question.title }</h5>
      <div>
        <Field
          name={ `${namePrefix}[${name}]` }
          type='text'
          className='reminder-input'
          disabled={ disabled }
          component={ FieldWithErrors }
          componentType='textarea'
        />
      </div>
    </div>
  );
};
TextareaCriteria.propTypes = propTypes;

export default TextareaCriteria;
