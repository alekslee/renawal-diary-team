import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import LocationCriteria from './locationCriteria';

import { selectors } from 'core/category';
import { actions as renewalDetailActions } from 'core/renewalDetail';

const mapStateToProps = state => ({
  category: selectors.getCategory(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchRenewalDetailFormAutocompleteOptionsDispatch: renewalDetailActions.fetchRenewalDetailFormAutocompleteOptions
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(LocationCriteria);
