import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formValueSelector } from 'redux-form';
import MotorBikeModelCriteria from './motorBikeModelCriteria';

import { selectors, actions } from 'core/renewalDetail';

const mapStateToProps = (state, props) => ({
  make: formValueSelector(props.form)(state, 'criteriaQuestions[make]')
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptionsDispatch:
      actions.fetchRenewalDetailCriteriasQuestionsMotorBikeEngineSizeOptions,
    fetchRenewalDetailCriteriasQuestionsCarTrimOptionsDispatch:
      actions.fetchRenewalDetailCriteriasQuestionsCarTrimOptions
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(MotorBikeModelCriteria);
