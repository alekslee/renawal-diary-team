import React, { Fragment, Component } from 'react';
import { object, string, bool, func } from 'prop-types';

import SelectCriteria from 'components/CriteriaQuestionsFormData/components/SelectCriteria';
import Loader from 'components/Loader';

const propTypes = {
  question: object.isRequired,
  name: string.isRequired,
  disabled: bool,
  namePrefix: string.isRequired,
  fetchMobileModelsOptionsDispatch: func.isRequired
};

class MobileMakeCriteria extends Component {
  state = {
    inProcess: false
  };

  onChangeHandler = value => {
    const { fetchMobileModelsOptionsDispatch } = this.props;

    const callback = () => this.setState({ inProcess: false });

    this.setState({ inProcess: true });
    fetchMobileModelsOptionsDispatch({ make: value, callback, errorCallback: callback });
  };

  render() {
    const {
      onChangeHandler,
      state: { inProcess },
      props: { question, name, disabled, namePrefix }
    } = this;

    return (
      <Fragment>
        <SelectCriteria { ...{ question, name, disabled, namePrefix } } onChange={ onChangeHandler } />
      </Fragment>
    );
  }
};
MobileMakeCriteria.propTypes = propTypes;

export default MobileMakeCriteria;
