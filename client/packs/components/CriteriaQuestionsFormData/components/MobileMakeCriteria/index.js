import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MobileMakeCriteria from './MobileMakeCriteria';

import { selectors, actions } from 'core/renewalDetail';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchMobileModelsOptionsDispatch:
      actions.fetchMobileModelsOptions
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(MobileMakeCriteria);
