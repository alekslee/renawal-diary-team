import React, { Fragment } from 'react';
import { object, string, bool } from 'prop-types';

import SelectCriteria from 'components/CriteriaQuestionsFormData/components/SelectCriteria';

const propTypes = {
  question: object.isRequired,
  typeOfCoverRequired: string,
  name: string.isRequired,
  namePrefix: string.isRequired,
  disabled: bool
};

const ChildrenCountCriteria = ({ question, typeOfCoverRequired, name, namePrefix, disabled }) => {
  if (typeOfCoverRequired !== 'family') return <Fragment />;

  return (
    <SelectCriteria { ...{ question, name, disabled, namePrefix } } />
  );
};
ChildrenCountCriteria.propTypes = propTypes;

export default ChildrenCountCriteria;
