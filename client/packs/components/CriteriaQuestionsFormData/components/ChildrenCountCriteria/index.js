import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formValueSelector } from 'redux-form';
import ChildrenCountCriteria from './childrenCountCriteria';

const mapStateToProps = (state, props) => ({
  typeOfCoverRequired: formValueSelector(props.form)(state, `${props.namePrefix}[type_of_cover_required]`)
});

export default connect(mapStateToProps, null)(ChildrenCountCriteria);
