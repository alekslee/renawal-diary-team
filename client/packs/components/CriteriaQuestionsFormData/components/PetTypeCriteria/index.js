import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PetTypeCriteria from './petTypeCriteria';

import { selectors, actions } from 'core/renewalDetail';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchRenewalDetailCriteriasQuestionsPetBreedOptionsDispatch:
      actions.fetchRenewalDetailCriteriasQuestionsPetBreedOptions
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(PetTypeCriteria);
