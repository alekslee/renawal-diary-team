import React from 'react';
import { Field } from 'redux-form';
import { object, string, bool, number, oneOfType } from 'prop-types';

import NumberField from 'components/NumberField';

const propTypes = {
  question: object.isRequired,
  name: string.isRequired,
  disabled: bool,
  value: oneOfType([string, number]),
  namePrefix: string.isRequired
};

const NumberCriteria = ({ question, value, name, disabled, namePrefix }) => {
  let subject;
  if (value === 1) {
    subject = question.subjectSingular;
  } else if (value > 1) {
    subject = question.subjectPlural;
  };

  return (
    <div className='reminder-form-item'>
      <h5>{ question.title }</h5>
      <div className='grid-4'>
        <Field
          name={ `${namePrefix}[${name}]` }
          disabled={ disabled }
          minValue={ question.minValue }
          symbolsAfterDot={ question.symbolsAfterDot }
          component={ NumberField }
        />
        { subject }
      </div>
    </div>
  );
};
NumberCriteria.propTypes = propTypes;

export default NumberCriteria;
