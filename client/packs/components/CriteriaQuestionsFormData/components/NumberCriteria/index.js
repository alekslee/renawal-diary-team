import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import { bindActionCreators } from 'redux';
import NumberCriteria from './numberCriteria';

const mapStateToProps = (state, props) => ({
  value: formValueSelector(props.form)(state, `${props.namePrefix}[${props.name}]`)
});

export default connect(mapStateToProps, null)(NumberCriteria);
