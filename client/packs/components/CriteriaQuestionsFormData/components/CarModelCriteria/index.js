import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formValueSelector } from 'redux-form';
import CarModelCriteria from './carModelCriteria';

import { selectors, actions } from 'core/renewalDetail';

const mapStateToProps = (state, props) => ({
  make: formValueSelector(props.form)(state, 'criteriaQuestions[make]')
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    fetchRenewalDetailCriteriasQuestionsCarTrimOptionsDispatch:
      actions.fetchRenewalDetailCriteriasQuestionsCarTrimOptions
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(CarModelCriteria);
