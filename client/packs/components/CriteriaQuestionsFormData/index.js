import { connect } from 'react-redux';
import CriteriaQuestionsFormData from './criteriaQuestionsFormData';

import { selectors as categorySelectors } from 'core/category';

const mapStateToProps = state => ({
  category: categorySelectors.getCategory(state)
});

export default connect(mapStateToProps, null)(CriteriaQuestionsFormData);
