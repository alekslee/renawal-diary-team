import React, { Fragment } from 'react';
import { object, string, bool } from 'prop-types';

import { TYPES_CONVERTER } from './constants';

const propTypes = {
  criteriaOptions: object.isRequired,
  category: object.isRequired,
  namePrefix: string.isRequired,
  disabled: bool,
  form: string
};

const CriteriaQuestionsFormData = ({ criteriaOptions, category, disabled, namePrefix, form }) => {
  if (Object.keys(criteriaOptions).length === 0) return <Fragment />;

  let questions = [];
  let question, ReactElement;

  for (let key in criteriaOptions) {
    if (key !== 'subheader') {
      question = criteriaOptions[key];
      ReactElement = TYPES_CONVERTER[question.type];
      if (ReactElement) {
        questions.push(<ReactElement { ...{ key, name: key, question, namePrefix, form, disabled } } />);
      }
    }
  };

  const subheader = criteriaOptions.subheader && criteriaOptions.subheader.replace(':category_name', category.name);

  return (
    <Fragment >
      { !!criteriaOptions.subheader && <h3 className='header-subtitle'>{ subheader }</h3> }
      { questions }
    </Fragment>
  );
};
CriteriaQuestionsFormData.propTypes = propTypes;

export default CriteriaQuestionsFormData;
