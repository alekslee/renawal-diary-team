import CopyOrShare from './copyOrShare';
import { injectIntl } from 'react-intl';

export default injectIntl(CopyOrShare);

