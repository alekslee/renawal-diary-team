import React, { Component } from 'react';
import { intlShape } from 'react-intl';
import Clipboard from 'react-clipboard.js';
import { Tooltip } from 'reactstrap';
import { EmailShareButton } from 'react-share';
import { object, bool } from 'prop-types';

import { defaultMessages } from 'locales/default';
import { BASE_APP_URL } from 'core/constants';

const propTypes = {
  intl: intlShape.isRequired,
  modalTooltip: bool
};

class CopyOrShare extends Component {
  state = {
    isOpenCopiedMessageTooltip: false
  };

  toggleTooltip = () => {
    const { isOpenCopiedMessageTooltip } = this.state;
    if (!isOpenCopiedMessageTooltip) setTimeout(this.toggleTooltip, 3000);
    this.setState({ isOpenCopiedMessageTooltip: !isOpenCopiedMessageTooltip });
  };

  render() {
    const {
      toggleTooltip,
      state: { isOpenCopiedMessageTooltip },
      props: { intl: { formatMessage }, modalTooltip }
    } = this;
    const inviteMessage = formatMessage(defaultMessages.inviteMessage);

    return (
      <div className='public-link' >
        <div className='copy'>
          { formatMessage(defaultMessages.copyLink) }
          <span id={ modalTooltip ? 'modal-cliapboardIcon' : 'clipboardIcon' } >
            <Clipboard data-clipboard-text={ `${inviteMessage} ${BASE_APP_URL}` } className='copy-icon' />
          </span>
        </div>
        <Tooltip
          isOpen={ isOpenCopiedMessageTooltip }
          toggle={ toggleTooltip }
          trigger='click'
          placement='bottom'
          className='copied-message'
          target={ modalTooltip ? 'modal-cliapboardIcon' : 'clipboardIcon' }
        >
          <div className='check' />
          <div className='message' >
            <p>{ formatMessage(defaultMessages.copiedToClipboard) }</p>
            <span>{ BASE_APP_URL }</span>
          </div>
        </Tooltip>
        { formatMessage(defaultMessages.orShareThisWebsite) }
        &nbsp;
        <EmailShareButton
          className='email-link'
          url={ BASE_APP_URL }
          subject={ formatMessage(defaultMessages.inviteMailSubject) }
          body={ `${inviteMessage} ${BASE_APP_URL}` }
        >
          { formatMessage(defaultMessages.email) }
        </EmailShareButton>
      </div>
    );
  }
};

CopyOrShare.propTypes = propTypes;

export default CopyOrShare;
