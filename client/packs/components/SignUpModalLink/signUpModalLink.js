import React, { Component } from 'react';
import { UnauthorizedModalsContext } from 'layouts/UnAuthorizedApp/context';
import { string, any, func } from 'prop-types';

const propTypes = {
  className: string,
  children: any,
  onSignUpSuccess: func
};

class SignUpModalLink extends Component {
  render() {
    const { className, children, onSignUpSuccess } = this.props;
    const { toggleSignUpModal } = this.context;

    return (
      <a { ...{ className } } onClick={ () => toggleSignUpModal(onSignUpSuccess) } >
        { children }
      </a>
    );
  }
};
SignUpModalLink.propTypes = propTypes;
SignUpModalLink.contextType = UnauthorizedModalsContext;

export default SignUpModalLink;
