import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FormSlider from './formSlider';

import { selectors, actions } from 'core/formSlider';

const mapStateToProps = state => ({
  activeSliderID: selectors.getActiveSliderId(state)
});

const mapDispatchToProps = dispatsh => (
  bindActionCreators({
    setActiveSliderIdDispatch: actions.setActiveSliderId
  }, dispatsh)
);

export default connect(mapStateToProps, mapDispatchToProps)(FormSlider);
