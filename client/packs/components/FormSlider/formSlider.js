import React, { Component } from 'react';
import { bool, string, any, func, number } from 'prop-types';
import { ICONS } from './constants';
import cx from 'classnames';
import AnimateHeight from 'react-animate-height';

const propTypes = {
  sliderOpen: bool,
  children: any,
  activeSliderID: number,
  title: string.isRequired,
  formIcon: string,
  setActiveSliderIdDispatch: func.isRequired
};

const defaultProps = {
  sliderOpen: false
};

let slidersCount = 0;

class FormSlider extends Component {
  constructor(props) {
    super(props);

    this.sliderID = ++slidersCount;
  };

  componentDidMount() {
    const { setActiveSliderIdDispatch, sliderOpen } = this.props;
    if (sliderOpen) setActiveSliderIdDispatch({ activeSliderID: this.sliderID });
  };

  componentWillUnmount() {
    const { setActiveSliderIdDispatch } = this.props;

    setActiveSliderIdDispatch({ activeSliderID: null });
  }

  toggleSlider = () => {
    const {
      sliderID,
      props: { setActiveSliderIdDispatch, activeSliderID }
    } = this;

    if (activeSliderID === sliderID) {
      setActiveSliderIdDispatch({ activeSliderID: null });
    } else {
      setActiveSliderIdDispatch({ activeSliderID: sliderID });
    }
  }

  render() {
    const {
      toggleSlider,
      props: { title, children, activeSliderID, formIcon }
    } = this;

    const isCurrentSliderOpen = this.sliderID === activeSliderID;

    return (
      <div>
        <div className={ cx('form-slider', { active: isCurrentSliderOpen }) } onClick={ toggleSlider } >
          <h3 className='form-slider-title'>
            <span className="form-slider-icon">
              {formIcon && ICONS[formIcon]}
            </span>
            <span>{ title }</span>
          </h3>
          <div>
            {/*<span className='points'>+ 5 points</span>*/}
            <span className='toggle-button'>
              {
                isCurrentSliderOpen ?
                  <span>&ndash;</span>
                  :
                  <span>+</span>
              }
            </span>
          </div>
        </div>
        <AnimateHeight
          duration={ 250 }
          height={ isCurrentSliderOpen ? 'auto' : 0 }
        >
          { children }
        </AnimateHeight>
      </div>
    );
  };
};
FormSlider.propTypes = propTypes;
FormSlider.defaultProps = defaultProps;

export default FormSlider;
