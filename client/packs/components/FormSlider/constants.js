import React from 'react';
import ProviderDetails from 'images/icons/ProviderDetails';
import PriceDetails from 'images/icons/PriceDetails/PriceDetails';
import MyCriteria from 'images/icons/MyCriteria/MyCriteria';
import MyInsights from 'images/icons/MyInsights';
import CustomerSatisfaction from 'images/icons/CustomerSatisfaction';
import Like from 'images/icons/Like';


export const ICONS = {
  ProviderDetails: <ProviderDetails />,
  PriceDetails: <PriceDetails />,
  MyCriteria: <MyCriteria />,
  MyInsights: <MyInsights />,
  CustomerSatisfaction: <CustomerSatisfaction />,
  Like: <Like />,
};