import React, { Component } from 'react';
import { func, object, string } from 'prop-types';
import { deleteSubscription } from 'lib/utils';

const propTypes = {
  rootCategoryId: string,
  votesCounter: object.isRequired,
  subscribeToVoteCountsDispatch: func.isRequired,
  fetchVotesCountDispatch: func.isRequired,
};

class VotesCounter extends Component {

  state = {
    votesCounter: null
  }

  componentDidMount() {
    const { rootCategoryId } = this.props;

    this.fetchAndSubscribe(rootCategoryId);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { rootCategoryId, votesCounter } = nextProps;
    const { rootCategoryId: oldRootCategoryId, votesCounter: oldVotesCounter } = this.props;

    if (rootCategoryId && oldRootCategoryId && rootCategoryId !== oldRootCategoryId) {
      deleteSubscription(`${oldRootCategoryId}-votes-count`);
      this.fetchAndSubscribe(rootCategoryId);
    }
  }

  componentWillUnmount() {
    const { rootCategoryId } = this.props;
    deleteSubscription(`${rootCategoryId}-votes-count`);
  };

  fetchAndSubscribe = (rootCategoryId) => {
    const { fetchVotesCountDispatch, subscribeToVoteCountsDispatch } = this.props;

    const callback = (votesCount) => {
      this.setState({ votesCounter: votesCount })
    };

    const onReceive = ({ votesCount }) => {
      this.setState({ votesCounter: votesCount })
    }

    subscribeToVoteCountsDispatch({ period: 'today', rootCategoryId, onReceive });
    fetchVotesCountDispatch({ period: 'today', rootCategoryId, callback, errorCallback: callback});
  }

  render() {
    const { votesCounter } = this.state;

    return (
      <div className='home-members'>
        <strong>
          { (votesCounter || {}).value || 0 }
        </strong>
        &nbsp;
        Votes to date
      </div>
    );
  };
}

VotesCounter.propTypes = propTypes;

export default VotesCounter;
