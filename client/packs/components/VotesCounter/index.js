import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { selectors as categorySelectors } from 'core/category';
import { actions, selectors } from 'core/votes';
import VotesCounter from './votesCounter';

const mapStateToProps = state => ({
  votesCounter: selectors.getVotesCount(state),
  rootCategoryId: ((categorySelectors.getCategory(state) || {}).root || {}).id,
});

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    fetchVotesCountDispatch: actions.fetchVotesCount,
    subscribeToVoteCountsDispatch: actions.subscribeToVoteCounts,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(VotesCounter);
