import React from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { object } from 'prop-types';

const propTypes = {
  input: object.isRequired,
  meta: object.isRequired
};

const EditorField = ({ input: { value, onChange }, meta: { initial } }) => {
  return (
    <Editor
      toolbarClassName='editor-toolbar'
      wrapperClassName='editor-container'
      editorClassName='editor-body'
      editorState={ value || initial }
      toolbar={ { image: { uploadEnabled: true } } }
      onEditorStateChange={ onChange }
    />
  );
};
EditorField.propTypes = propTypes;

export default EditorField;
