import React, { Fragment, Component } from 'react';
import { Link } from 'react-router-dom';
import { object, string, any } from 'prop-types';

import SignUpModalLink from 'components/SignUpModalLink';

const propTypes = {
  currentUser: object.isRequired,
  to: string.isRequired,
  className: string,
  linkComponent: any,
  children: any
};
const defaultProps = {
  linkComponent: Link
};

class UserSignedLink extends Component {
  render() {
    const { currentUser, to, className, linkComponent, children } = this.props;
    const LinkComponent = linkComponent;

    return (
      <Fragment>
        {
          currentUser.id ?
            <LinkComponent { ...{ to, className } } >
              { children }
            </LinkComponent>
            :
            <SignUpModalLink { ...{ className } } >
              { children }
            </SignUpModalLink>
        }
      </Fragment>
    );
  }
};
UserSignedLink.propTypes = propTypes;
UserSignedLink.defaultProps = defaultProps;

export default UserSignedLink;
