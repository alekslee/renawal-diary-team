import { connect } from 'react-redux';
import UserSignedLink from './userSignedLink';

import { selectors } from 'core/currentUser';

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state)
});

export default connect(mapStateToProps, null)(UserSignedLink);
