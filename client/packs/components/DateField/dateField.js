import React from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import { object, string } from 'prop-types';

const propTypes = {
  input: object.isRequired,
  className: string
};

const DateField = ({ input, className, name, disabled }) => {
  return (
    <DatePicker
      selected={ input.value || new Date() }
      dateFormat='MMMM d, yyyy'
      minDate={ new Date() }
      onChange={ input.onChange }
      { ...{ className, name, disabled } }
    />
  );
};
DateField.propTypes = propTypes;

export default DateField;
