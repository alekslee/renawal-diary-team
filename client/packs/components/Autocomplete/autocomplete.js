import React from 'react';
import ReactAutocomplete from 'react-autocomplete';
import { array, object, func, string } from 'prop-types';

const propTypes = {
  items: array.isRequired,
  className: string,
  placeholder: string,
  value: string,
  inputProps: object,
  onChange: func.isRequired,
  onSelect: func,
};
const defaultProps = {
  className: '',
  placeholder: '',
  inputProps: {}
};

const Autocomplete = ({ inputProps, items, className, placeholder, value, onChange, onSelect }) => {
  const shouldItemRender = (item, value) => {
    const regExp = new RegExp(value, 'i');
    return !!item.label.match(regExp);
  };

  return (
    <ReactAutocomplete
      getItemValue={ (item) => item.value }
      renderItem={ (item, isHighlighted) => (
        <div key={ item.value } style={ { background: isHighlighted ? 'lightgray' : 'white' } } >
          { item.label }
        </div>
      ) }
      { ...{ items, shouldItemRender, value } }
      inputProps={ { ...inputProps, placeholder, className } }
      wrapperStyle={ { position: 'relative' } }
      onChange={ ({ target: { value } }) => onChange({ value: undefined, label: value }) }
      onSelect={ (_value, item) => onSelect ? onSelect(item) : onChange(item) }
    />
  );
};
Autocomplete.propTypes = propTypes;
Autocomplete.defaultProps = defaultProps;

export default Autocomplete;
