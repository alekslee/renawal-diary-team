import React from 'react';

export default () => {
  return(
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="8"
      height="14"
      viewBox="0 0 8 14"
    >
      <path d="M1.799 7.082l5.42 5.58-.679.699L.78 7.43l-.339-.349.34-.35L6.54.804l.678.7z" />
    </svg>
  );
};
