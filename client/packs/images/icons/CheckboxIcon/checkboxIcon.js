import React from 'react';

export default () => {
  return(
    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="11" viewBox="0 0 14 11">
      <path d="M1.568 4.97L-.02 6.559l4.234 4.234 9.526-8.996L12.152.208 4.214 7.617z" />
    </svg>
  );
};
