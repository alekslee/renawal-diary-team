import React from 'react';

export default () => (
    <svg
      xmlns="http://www.w3.org/2000/svg" width="21" height="19"
      viewBox="0 0 21 19"
      className="myInsights"
    >
      <g fill="#6D7387" fillRule="evenodd">
        <rect
          width="2.165" height="1" x="3.316"
          y="5.588" rx=".5"
        />
        <rect
          width="11.053" height="1" x="6.564"
          y="5.588" rx=".5"
        />
        <rect
          width="2.165" height="1" x="3.316"
          y="8.941" rx=".5"
        />
        <rect
          width="11.053" height="1" x="6.564"
          y="8.941" rx=".5"
        />
        <path fillRule="nonzero" d="M17.85 0H3.15A3.162 3.162 0 0 0 0 3.167v9.148a3.162 3.162 0 0 0 3.15 3.166h.973l-.616 3.097a.353.353 0 0 0 .134.35.348.348 0 0 0 .372.03l6.573-3.477h7.264A3.162 3.162 0 0 0 21 12.315V3.167A3.162 3.162 0 0 0 17.85 0zm2.45 12.315a2.46 2.46 0 0 1-2.45 2.463H10.5a.348.348 0 0 0-.163.04l-6 3.175.556-2.793a.353.353 0 0 0-.343-.422h-1.4A2.46 2.46 0 0 1 .7 12.315V3.167A2.46 2.46 0 0 1 3.15.704h14.7a2.46 2.46 0 0 1 2.45 2.463v9.148z" />
      </g>
    </svg>
);
