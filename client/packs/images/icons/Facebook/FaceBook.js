import React from 'react';

export default () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg" width="8" height="15"
      viewBox="0 0 8 15"
    >
      <path fill="#AABACC" fillRule="evenodd" d="M4.725 14.232V7.74h2.146l.321-2.53H4.725V3.595c0-.732.2-1.232 1.235-1.232h1.319V.1A17.29 17.29 0 0 0 5.357 0C3.454 0 2.152 1.179 2.152 3.345V5.21H0v2.53h2.152v6.492h2.573z" />
    </svg>

  );
};