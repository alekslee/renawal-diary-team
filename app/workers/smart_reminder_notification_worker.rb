class SmartReminderNotificationWorker
  include Sidekiq::Worker

  def perform(smart_reminder_id)
    smart_reminder = SmartReminder.find(smart_reminder_id)
    smart_reminder.set_status
    smart_reminder.notification_job_id = SmartReminders::RunNotificationWorker.run!(smart_reminder: smart_reminder)
    smart_reminder.save
    send_mail(smart_reminder)
  end

  private

  def send_mail(smart_reminder)
    if smart_reminder.status.amber?
      SmartReminderNotificationMailer.with(smart_reminder_id: smart_reminder.id).green_to_amber.deliver_later
    elsif smart_reminder.status.red?
      SmartReminderNotificationMailer.with(smart_reminder_id: smart_reminder.id).amber_to_red.deliver_later
    elsif smart_reminder.status.completed?
      SmartReminderNotificationMailer.with(smart_reminder_id: smart_reminder.id).date_becomes.deliver_later
    end
  end
end
