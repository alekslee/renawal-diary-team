class MigrateToS3Worker
  include Sidekiq::Worker

  def perform
    Companies::MigrateToS3.run!(from: :local, to: :amazon)
  end
end
