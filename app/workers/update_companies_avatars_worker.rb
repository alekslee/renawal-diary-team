class UpdateCompaniesAvatarsWorker
  include Sidekiq::Worker

  def perform
    Companies::AttachAvatars.run!(should_update: first_run?)
  end

  private

  def first_run?
    Sidekiq::Workers.new.find { |w| w[-1]['payload']['jid'] == jid }[-1]['payload']['retry_count'].zero? rescue true
  end
end
