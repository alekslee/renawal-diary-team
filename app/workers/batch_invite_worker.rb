# frozen_string_literal: true

class BatchInviteWorker
  include Sidekiq::Worker

  sidekiq_options failures: true

  def perform(emails, message, inviter_id)
    inviter = inviter_id.present? ? User.find_by(id: inviter_id) : nil

    emails.each do |email|
      User.invite!({ email: email }, inviter, message: message)
    end
  end
end
