class ParseAndSyncCompaniesWorker
  include Sidekiq::Worker

  def perform
    Companies::Parser::Synchronize.run!
  end

end
