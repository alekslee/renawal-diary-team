# frozen_string_literal: true

class  Companies::AttachAvatar::RetrieveAvatarUrl::ParseFaviconUrl < ActiveInteraction::Base
  API_URL = 'http://favicongrabber.com/api/grab/'
  GOOGLE_FAVICON_URL = 'https://www.google.com/s2/favicons?domain='

  object :company

  def execute
    data = fetch_data(company)
    icon_url = find_icon_url(data)
    icon_url ||= LinkThumbnailer.generate(company.url).favicon || GOOGLE_FAVICON_URL + company.url
    icon_url
  rescue StandardError
    nil
  end

  private

  def fetch_data(company)
    domain = URI.parse(company.url).host
    uri = URI(API_URL + domain)
    uri.query = { pretty: true }.to_query

    io = open(uri)
    Oj.load(io.read)
  end

  def find_icon_url(data)
    icons = data['icons']
    icon = icons.select { |ico| ico['sizes'] }&.max_by { |key| key['sizes'] }
    icon ||= icons.find(&:presence)
    icon['src']
  end
end
