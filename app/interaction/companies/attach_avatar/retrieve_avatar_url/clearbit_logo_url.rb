# frozen_string_literal: true

class Companies::AttachAvatar::RetrieveAvatarUrl::ClearbitLogoUrl < ActiveInteraction::Base
  API_URL = 'https://logo.clearbit.com/'

  object :company

  def execute
    company_logo(company)
  rescue OpenURI::HTTPError
    nil
  end

  private

  def company_logo(company)
    domain = URI.parse(company.url).host
    uri = URI(API_URL + domain)
    open(uri).read && uri.to_s
  end
end
