# frozen_string_literal: true

class Companies::AttachAvatar::LoadIo < ActiveInteraction::Base
  DEFAULT_AVATAR = Rails.public_path.join('images').join('default-company-avatar.svg')

  string :url, default: nil

  def execute
    if url.present?
      io = URI.parse(url).open
      [io, io.content_type]
    else
      default_avatar
    end
  rescue StandardError
    default_avatar
  end

  private

  def default_avatar
    [File.open(DEFAULT_AVATAR), Rack::Mime::MIME_TYPES[File.extname(DEFAULT_AVATAR)]]
  end
end