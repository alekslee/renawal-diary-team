# frozen_string_literal: true

class Companies::AttachAvatar::RetrieveAvatarUrl < ActiveInteraction::Base
  object :company

  def execute
    return nil if company.url.blank?
    compose(ClearbitLogoUrl, company: company) ||
    compose(ParseFaviconUrl, company: company)
  end
end
