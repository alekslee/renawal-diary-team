# frozen_string_literal: true

class Companies::AttachAvatar::GenerateName < ActiveInteraction::Base
  string :content_type
  string :company_code

  def execute
    extension = Rack::Mime::MIME_TYPES.invert[content_type]
    "#{company_code}#{extension}"
  end
end