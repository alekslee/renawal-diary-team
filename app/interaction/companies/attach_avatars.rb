# frozen_string_literal: true

class Companies::AttachAvatars < ActiveInteraction::Base
  boolean :should_update, default: false

  QUALITY_IMAGE_SIZE = { width: 68, height: 68 }.freeze

  def execute
    Company.all.each do |company|
      next if !should_update && company.avatar.attached? && is_quality_avatar?(company)

      compose(Companies::AttachAvatar, company: company)
    end
  end

  private

  def is_quality_avatar?(company)
    image_url = company.avatar.try(:service_url)
    return false unless image_url

    image_size = open(image_url, 'rb') { |f| ImageSize.new(f) }
    image_size.width > QUALITY_IMAGE_SIZE[:width] &&
      image_size.height > QUALITY_IMAGE_SIZE[:height]
  end
end