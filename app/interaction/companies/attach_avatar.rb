# frozen_string_literal: true

require 'open-uri'

class Companies::AttachAvatar < ActiveInteraction::Base
  object :company

  def execute
    avatar_url = compose(Companies::AttachAvatar::RetrieveAvatarUrl, company: company)
    io, content_type = compose(LoadIo, url: avatar_url)
    filename = compose(GenerateName, content_type: content_type, company_code: company.code)
    company.avatar.attach(io: io, filename: filename)
    io.close
  end
end
