# frozen_string_literal: true

class Companies::List::FilterByPast7Days < ActiveInteraction::Base

  def execute
    <<-SQL.squish
      AND renewal_details.created_at > '#{7.days.ago.to_s(:db)}'
    SQL
  end
end
