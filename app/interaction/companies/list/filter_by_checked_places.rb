# frozen_string_literal: true

class Companies::List::FilterByCheckedPlaces < ActiveInteraction::Base
  symbol :checked_places

  # Not push, because push modifying current array
  validates_inclusion_of :checked_places, in: RenewalDetail.checked_places.values + [:two_places_or_more]

  def execute
    <<-SQL.squish
      AND renewal_details.checked_places #{retrieve_checked_places_clause}
    SQL
  end

  private

  def retrieve_checked_places_clause
    case checked_places
    when :two_places_or_more
      'IN (\'two_places\', \'three_places_or_more\')'
    else
      "= '#{checked_places}'"
    end
  end
end
