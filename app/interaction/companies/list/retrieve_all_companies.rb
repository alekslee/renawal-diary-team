# frozen_string_literal: true

class Companies::List::RetrieveAllCompanies < ActiveInteraction::Base
  object :user, default: nil
  object :current_country, class: Country
  integer :category_id

  def execute
    country = user ? user.country : current_country
    category = compose(Categories::FindCountryLeafCategory, country: country, category_id: category_id)
    # category_ids = category.root.self_and_descendants.select(:id)
    # Category.where("code = :code OR id = :id", id: category.id, code: category.root.code).select(:id)

    Company.where(category: category.self_or_depends_categories, status: :approved)
  end
end
