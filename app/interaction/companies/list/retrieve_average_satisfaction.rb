# frozen_string_literal: true

class Companies::List::RetrieveAverageSatisfaction < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation
  symbol :company_type, default: nil

  def execute
    companies.group(:id).average(average_query)
  end

  private

  def average_query
    <<-SQL.squish
      CASE WHEN renewal_details.buying_type = 'provider' OR companies.company_type = 'provider'
        THEN renewal_details.current_provider_rate
        ELSE renewal_details.buying_type_company_rate
      END
    SQL
  end
end
