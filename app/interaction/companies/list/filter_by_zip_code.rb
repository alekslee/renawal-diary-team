# frozen_string_literal: true

class Companies::List::FilterByZipCode < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation
  string :zip_code
  symbol :country_code

  def execute
    companies
      .left_joins({ current_renewal_details: { feedback: :user } })
      .where(generate_sql)
  end

  def generate_sql
    map_sql.join(' OR ')
  end

  def map_sql
    cities = load_cities_names.presence || [['undefined', 'undefined']]
    ap cities
    cities.map do |city, state|
      city_like = "%#{ ActiveRecord::Base.sanitize_sql_like(city.downcase) }%"
      state_like = "%#{ ActiveRecord::Base.sanitize_sql_like(state.downcase) }%"

      <<-SQL.squish
        LOWER(((renewal_details.criteria_questions->>'location')::jsonb->>'city')::text) ILIKE '#{city_like}' OR
        LOWER(((renewal_details.criteria_questions->>'location')::jsonb->>'state')::text) ILIKE '#{state_like}' OR
        LOWER(((renewal_details.criteria_questions->>'location')::jsonb->>'address')::text) ILIKE '#{city_like}' OR
        LOWER(((renewal_details.criteria_questions->>'location')::jsonb->>'address')::text) ILIKE '#{state_like}'
      SQL
    end
  end

  private

  def load_cities_names
    res1 = compose(
      Countries::LoadCitiesByZipCode, country_code: country_code, zip_code: zip_code, api: 'ezcmd'
    ).map{|s| [ s['place_name'], s['state'] ] }

    res2 = compose(
      Countries::LoadCitiesByZipCode, country_code: country_code, zip_code: zip_code, api: 'google'
    ).map do |s|
      address = s.data['address']
      [ address['county'].gsub(/city| of /i,'').squish, address['state'] || '-']
    end

    res1.concat(res2)
  end

  def method_name
    Geocoder.search("#{country_code} #{zip_code}").map{ |s|s.data['address']['county']}
  end
end
