# frozen_string_literal: true

class Companies::List::FilterByCity < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation
  string :city

  def execute
    companies.instance_eval(&generate_query)
  end

  def generate_query
    query = city.downcase

    proc do
      left_joins({ current_renewal_details: { feedback: :user } })
      .where(
        <<-SQL.squish,
          LOWER(((renewal_details.criteria_questions->>'location')::jsonb->>'city')::text) ILIKE :like_query OR
          LOWER(((renewal_details.criteria_questions->>'location')::jsonb->>'state')::text) ILIKE :like_query OR
          LOWER(((renewal_details.criteria_questions->>'location')::jsonb->>'address')::text) ILIKE :like_query
        SQL
        like_query: "%#{ sanitize_sql_like(query) }%",
      )
    end
  end
end
