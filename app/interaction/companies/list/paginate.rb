# frozen_string_literal: true

class Companies::List::Paginate < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation
  integer :page, default: 1
  integer :per_page, default: 10

  validates_numericality_of :page, greater_than_or_equal_to: 1

  def execute
    offset = retrieve_offset
    limit = retrieve_limit(offset)
    companies_list = companies[offset...limit]
    add_rank(companies_list, offset)
  end

  private

  def retrieve_offset
    (page - 1) * per_page
  end

  def retrieve_limit(offset)
    offset + per_page
  end

  def add_rank(companies, offset)
    return [] unless companies

    companies.each_with_index.map do |company, index|
      company.rank = offset + index + 1
      company
    end
  end
end
