# frozen_string_literal: true

class Companies::List::RetrieveAveragePrice < ActiveInteraction::Base
  object :category, class: Category

  def execute
    q = category.renewal_details
                .where(condition_sql)

    yearly = q.where(select_period_sql, period: 'per_year')
    average_yearly_price = yearly.count == 0 ? nil : ((yearly.sum(value_sql) / 12) / yearly.count)

    monthly = q.where(select_period_sql, period: 'per_month')
    average_monthly_price = monthly.count == 0 ? nil : (monthly.sum(value_sql) / monthly.count)

    avg = [ average_yearly_price, average_monthly_price ].compact
    avg.size == 0 ? 0 : avg.sum / avg.size
  end

  def condition_sql
    <<-SQL.squish
      ((renewal_details.prices_questions->>'price')::jsonb->>'value')::int > 0
    SQL
  end

  def select_period_sql
    <<-SQL.squish
      ((renewal_details.prices_questions->>'price')::jsonb->>'period')::text = :period
    SQL
  end

  def value_sql
    <<-SQL.squish
      ((renewal_details.prices_questions->>'price')::jsonb->>'value')::int
    SQL
  end

end
