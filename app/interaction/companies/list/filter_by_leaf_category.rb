# frozen_string_literal: true

class Companies::List::FilterByLeafCategory < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation
  object :category, class: Category

  def execute
    # companies.where(renewal_details: { category_id: [category_id, nil] })

    companies.where(category: category.self_and_ancestors )
  end

end
