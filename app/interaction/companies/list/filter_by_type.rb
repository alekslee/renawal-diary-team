# frozen_string_literal: true

class Companies::List::FilterByType < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation
  symbol :company_type

  validates_inclusion_of :company_type, in: Company.company_type.values

  def execute
    #TODO fix all types
    sites = %i{ comparison_site comparsion_site }
    type = company_type.in?(sites) ? sites : company_type
    companies.where(company_type: type)
  end
end
