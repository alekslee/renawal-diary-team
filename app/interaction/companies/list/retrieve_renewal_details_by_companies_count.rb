# frozen_string_literal: true

class Companies::List::RetrieveRenewalDetailsByCompaniesCount < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation

  def execute
    companies.count('renewal_details.id')
  end
end
