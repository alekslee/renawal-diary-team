# frozen_string_literal: true

class Companies::List::Order < ActiveInteraction::Base

  QUERIES = {
    insights: 'COUNT(renewal_details.id)',
    best_value: <<-SQL.squish.freeze,
      COALESCE(
        AVG(
          CASE WHEN renewal_details.buying_type = 'provider' OR companies.company_type = 'provider'
            THEN renewal_details.current_provider_rate
            ELSE renewal_details.buying_type_company_rate
          END
        ),
        0
      )
    SQL
    best_service: <<-SQL.squish,
      COUNT(renewal_details.id)
    SQL
    sheet_index: 'companies.sheet_index',
    satisfection: <<-SQL.squish.freeze
    COALESCE(
      AVG(
        CASE WHEN renewal_details.buying_type = 'provider' OR companies.company_type = 'provider'
          THEN renewal_details.current_provider_rate
          ELSE renewal_details.buying_type_company_rate
        END
      ),
      0
    )
  SQL
  }.freeze

  object :companies, class: ActiveRecord::Relation
  symbol :order_direction, default: :desc
  symbol :order_column, default: :best_value

  validates_inclusion_of :order_column, in: %i[
    best_value
    best_service
    insights
    satisfection
    sheet_index
  ]

  # R * (v / (v + m)) + (m / (v + m)) * C
  # R = average for the company (mean) = (Rating)
  # v = number of votes for the company = (votes)
  # m = minimum votes required to be listed in the Top 250 (currently 25000)
  # C = the mean vote across the whole report (currently 7.0)
  def execute
    companies.group(:id).order("#{QUERIES[order_column]} #{order_direction}, companies.id ASC")
  end

  # Now we comment it, because we osing ordering by insights count

  # private
  #
  # def query
  #   column = retrieve_column
  #   avg_of_companies = retrieve_average_by(column)
  #   average_of_company = "COALESCE(CAST(AVG(renewal_details.#{column}) as float), CAST(0 as float))"
  #   number_of_votes = <<-SQL
  #     CASE COUNT(renewal_details.id)
  #       WHEN 0 THEN #{COEFFICIENT}
  #       ELSE CAST(COUNT(renewal_details.id) as float)
  #     END
  #   SQL
  #   part_one = "#{average_of_company} * (#{number_of_votes} / (#{number_of_votes} + #{COEFFICIENT}))"
  #   part_two = "(#{COEFFICIENT} / (#{number_of_votes} + #{COEFFICIENT}) * #{avg_of_companies})"
  #   "(#{part_one} + #{part_two}) DESC"
  # end
  #
  # def retrieve_column
  #   company_type.eql?(:provider) ? 'current_provider_rate' : 'buying_type_company_rate'
  # end
  #
  # def retrieve_average_by(column)
  #   companies.average("renewal_details.#{column}") || 0
  # end
end
