# frozen_string_literal: true

class Companies::List::FilterByLastBuy < ActiveInteraction::Base
  symbol :last_buy

  validates_inclusion_of :last_buy, in: RenewalDetail::LAST_BUY_DATE_VALUES

  def execute
    <<-SQL.squish
      AND renewal_details.last_buy_date BETWEEN
        '#{RenewalDetail::LAST_BUY_DATE_VALUES[last_buy].call.strftime('%Y-%m-%d')}'
        AND
        '#{Time.now.strftime('%Y-%m-%d')}'
    SQL
  end
end
