# frozen_string_literal: true

class Companies::List::FilterByCriteriaQuestions < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation
  object :criteria_questions, class: Hash

  def execute
    query = "(#{get_query})"
    companies.where(query, merge_questions)
  end

  def get_query
    map_query.join(' OR ')
  end

  def merge_questions
    criteria_questions.reduce({}) do |memo, (key, value)|
      if value.is_a?(Hash)
        value.each do |_key, _value|
          memo.merge!({ "#{key}_#{_key}".to_sym => _value })
        end
        memo
      else
        memo.merge({ key => value })
      end
    end
  end

  def map_query
    criteria_questions.reduce([]) do |memo, (key, value)|
      if value.is_a?(Hash)
        value.each do |_key, _value|
          memo.push "(#{ question_deep_sql(key, _key, _value) })"
        end
        memo
      else
        memo.push "(#{ question_sql(key, value) })"
      end
    end
  end

  def question_sql(key, value)
    type = cast_type(value)
    <<-SQL.squish
      (renewal_details.criteria_questions->>'#{key}')::#{type} = :#{key}
    SQL
  end

  def question_deep_sql(parent, key, value)
    type = cast_type(value)
    <<-SQL.squish
      ((renewal_details.criteria_questions->>'#{parent}')::jsonb->>'#{key}')::#{type} = :#{parent}_#{key}
    SQL
  end

  def cast_type(value)
    case value.class.to_s
    when 'Float'
      'float'
    when 'FalseClass', 'TrueClass'
      'boolean'
    when 'Integer'
      'int'
    else
      'text'
    end
  end
end
