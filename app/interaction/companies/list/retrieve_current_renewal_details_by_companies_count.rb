# frozen_string_literal: true

class Companies::List::RetrieveCurrentRenewalDetailsByCompaniesCount < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation

  def execute
    companies.where(renewal_details: { buying_type: :provider }).count('renewal_details.id')
  end
end
