# frozen_string_literal: true

class Companies::List::FilterByKeyWord < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation
  string :key_word, default: nil

  # just filter by name provider brokers comparison site
  def execute
    companies.instance_eval(&generate_query)
  end

  def generate_query
    query = key_word.downcase

    proc do
      where(
        <<-SQL.squish,
          LOWER(companies.name) ILIKE :like_query OR
          companies.company_type = :query OR
          companies.code = :query
        SQL
        like_query: "%#{ sanitize_sql_like(query) }%",
        query: query
      )
    end
  end
end
