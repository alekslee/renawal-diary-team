# frozen_string_literal: true

class Companies::List::JoinsRenewalDetailsTable < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation
  string :filter_by_checked_places_query, default: nil
  string :filter_by_last_buy_query, default: nil
  string :filter_by_past_7_days, default: nil

  def execute
    companies.joins(joins_query)
  end

  private

  def joins_query
    <<-SQL.squish
      LEFT OUTER JOIN renewal_details ON
        (renewal_details.current_provider_id = companies.id OR
        renewal_details.buying_type_company_id = companies.id)
        #{filter_by_checked_places_query}
        #{filter_by_last_buy_query}
        #{filter_by_past_7_days}
    SQL
  end
end
