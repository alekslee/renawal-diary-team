# frozen_string_literal: true

class Companies::List::RetrieveTotalCount < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation

  def execute
    companies.distinct.count
  end
end
