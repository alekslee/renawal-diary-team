# frozen_string_literal: true

class Companies::List::FilterByCountries < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation
  symbol :country_code

  validates_inclusion_of :country_code, in: AVAILABLE_COUNTRIES

  def execute
    companies.left_joins(:country)
             .where(sql_country_code, country_code: country_code)

  end

  def sql_country_code
      # OR
      # LOWER(((renewal_details.criteria_questions->>'location')::jsonb->>'countryCode')::text) = :country_code
    <<-SQL.squish
      countries.iso_a2_code = :country_code
    SQL
  end
end
