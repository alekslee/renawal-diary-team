# frozen_string_literal: true

class Companies::List::RetrieveRecommendedSource < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation

  def execute
    compose(Companies::List::Order, companies: companies, order_column: 'satisfection').first
  end
end
