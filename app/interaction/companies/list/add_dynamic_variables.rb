# frozen_string_literal: true

class Companies::List::AddDynamicVariables < ActiveInteraction::Base
  array :companies
  hash :renewal_details_count, strip: false
  hash :average_satisfaction, strip: false
  hash :average_rating, strip: false
  hash :current_renewal_details_count, strip: false

  def execute
    total_renewal_details_count = retrieve_total_renewal_details_count
    companies.map do |company|
      company.members = calculate_percent(company, total_renewal_details_count)
      company.insights = renewal_details_count[company.id]
      company.satisfaction = calculate_satisfaction(company)
      company.rating = calculate_rating(company)
      company.direct = calculate_direct(company)
      company
    end
  end

  private

  def retrieve_total_renewal_details_count
    renewal_details_count.values.sum
  end

  def calculate_percent(company, total_renewal_details_count)
    (100 * renewal_details_count[company.id].to_f / total_renewal_details_count).round(1)
  end

  def calculate_rating(company)
    average_rating[company.id]&.round(1)
  end

  def calculate_satisfaction(company)
    average_satisfaction[company.id]&.round(1)
  end

  def calculate_direct(company)
    return false unless company.company_type.provider?

    direct_count = current_renewal_details_count[company.id]
    return false if direct_count.nil?

    indirect_count = renewal_details_count[company.id] - direct_count
    direct_count > indirect_count
  end
end
