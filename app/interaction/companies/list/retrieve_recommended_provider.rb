# frozen_string_literal: true

class Companies::List::RetrieveRecommendedProvider < ActiveInteraction::Base
  object :companies, class: ActiveRecord::Relation

  def execute
    providers = compose(Companies::List::FilterByType, companies: companies, company_type: :provider)
    compose(Companies::List::Order, companies: providers, order_column: 'satisfection').first
  end
end
