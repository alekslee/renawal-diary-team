# frozen_string_literal: true

class Companies::MigrateToS3 < ActiveInteraction::Base
  symbol :from
  symbol :to

  def execute
    migrate(from, to)
  end

  private

  def migrate(from, to)
    config_file = Pathname.new(Rails.root.join('config/storage.yml'))
    configs = YAML.load(ERB.new(config_file.read).result) || {}

    from_service = ActiveStorage::Service.configure from, configs
    to_service   = ActiveStorage::Service.configure to, configs

    ActiveStorage::Blob.service = from_service

    ActiveStorage::Blob.find_each do |blob|
      blob.open do |tf|
        checksum = blob.checksum
        to_service.upload(blob.key, tf, checksum: checksum)
      end
    end
  end
end
