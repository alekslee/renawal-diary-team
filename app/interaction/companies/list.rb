# frozen_string_literal: true

class Companies::List < ActiveInteraction::Base
  integer :category_id
  integer :page, :per_page, default: nil
  symbol :company_type, :last_buy, :checked_places,
         :country_code, :order_column, :order_direction, default: nil

  string :autocompleate, default: nil
  string :key_word, default: nil
  string :zip_code, default: nil
  string :city, default: nil
  string :address, default: nil
  object :criteria_questions, class: Hash, default: nil
  object :user, default: nil
  object :current_country, class: Country

  # rubocop:disable Metrics/AbcSize
  def execute
    companies = compose(RetrieveAllCompanies, inputs)

    if country_code.present? && city.blank? && zip_code.blank?
      companies = compose(FilterByCountries, companies: companies, country_code: country_code)
    end

    # companies = compose(FilterByLeafCategory, category: category, companies: companies) if category

    companies = compose(FilterByKeyWord, key_word: key_word, companies: companies) if key_word.present?

    if autocompleate == 'true'
      return [ companies.limit(10) ]
    end

    companies = compose(FilterByZipCode, zip_code: zip_code, country_code: country_code, companies: companies) if country_code.present? && zip_code.present?

    companies = compose(FilterByCity, companies: companies, city: city) if city

    filter_by_checked_places_query = checked_places && compose(FilterByCheckedPlaces, checked_places: checked_places)
    filter_by_last_buy_query = last_buy && compose(FilterByLastBuy, last_buy: last_buy)
    filter_by_past_7_days = compose(FilterByPast7Days)

    companies = compose(
      JoinsRenewalDetailsTable,
      companies: companies,
      filter_by_checked_places_query: filter_by_checked_places_query,
      filter_by_last_buy_query: filter_by_last_buy_query,
      filter_by_past_7_days: filter_by_past_7_days
    )

    unless key_word.present?
      questions = criteria_questions.presence || user&.default_criteria_questions(category_id: category.id).presence
      companies = compose(
        FilterByCriteriaQuestions,
        companies: companies, criteria_questions: questions,
      ) if questions
    end

    recommended_source = compose(RetrieveRecommendedSource, companies: companies)

    recommended_provider = compose(RetrieveRecommendedProvider, companies: companies)

    average_price = compose(RetrieveAveragePrice, category: category)

    companies = compose(FilterByType, companies: companies, company_type: company_type) if company_type

    total_count = compose(RetrieveTotalCount, companies: companies)

    average_satisfaction = compose(RetrieveAverageSatisfaction, companies: companies, company_type: company_type)
    average_rating = get_average_rating(companies)

    companies = compose(Order, inputs.merge(companies: companies))

    renewal_details_count = compose(RetrieveRenewalDetailsByCompaniesCount, companies: companies)

    current_renewal_details_count = compose(RetrieveCurrentRenewalDetailsByCompaniesCount, companies: companies)

    companies = compose(Paginate, companies: companies, page: page, per_page: per_page)

    companies = compose(
      AddDynamicVariables,
      companies: companies,
      renewal_details_count: renewal_details_count,
      average_satisfaction: average_satisfaction,
      average_rating: average_rating,
      current_renewal_details_count: current_renewal_details_count
    )

    [companies, total_count, recommended_source, recommended_provider, average_price]
  end

  private

  def get_average_rating(companies)
    average_query = <<-SQL.squish
      (renewal_details.price_rate + renewal_details.service_rate + renewal_details.claim_rate) / 3
    SQL

    companies.group(:id).average(average_query)
  end

  def category
    @category ||= Category.find_by(id: category_id)
  end

  # rubocop:enable Metrics/AbcSize
end
