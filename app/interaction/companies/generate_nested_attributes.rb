# frozen_string_literal: true

class Companies::GenerateNestedAttributes < ActiveInteraction::Base
  hash :attributes, strip: false
  object :category

  def execute
    return {} if attributes.empty?

    root_category = category.root
    attributes.each_with_object({}) do |(key, val), hash|
      company = category.root.companies.find_by(val)
      company ||= compose(Companies::Create, val.merge(category: root_category))
      hash[key.to_s.sub('_attributes', '_id')] = company.id
    end
  end
end
