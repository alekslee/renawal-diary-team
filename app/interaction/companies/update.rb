# frozen_string_literal: true

class Companies::Update < ActiveInteraction::Base
  string :name, default: nil
  string :url, default: nil
  string :twitter, default: nil

  integer :sheet_index, default: 0
  symbol :status, default: nil

  object :company

  def execute
    company.assign_attributes(inputs.compact.except(:company))
    errors.merge!(company.errors) unless company.save
    company
  end
end
