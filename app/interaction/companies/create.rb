# frozen_string_literal: true

class Companies::Create < ActiveInteraction::Base
  string :name
  string :url, default: nil
  string :twitter, default: nil
  string :company_type

  integer :sheet_index, default: 0
  symbol :status, default: nil
  boolean :with_validation, default: false

  object :category

  def execute
    company = category.companies.new(inputs.except(:category))
    errors.merge!(company.errors) unless company.save
    company
  end
end
