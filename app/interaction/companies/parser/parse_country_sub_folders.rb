# frozen_string_literal: true

class Companies::Parser::ParseCountrySubFolders < ActiveInteraction::Base
  object :country
  object :country_folder, class: GoogleDrive::Collection

  def execute
    country_folder.subfolders.each do |folder|
      root_category = country.root_categories.to_a.find { |c| c.code == folder.name }

      if root_category.nil?
        ap "Do not found root category with code: '#{folder.name}'"
        next
      end

      descendants  = root_category.descendants.to_a

      folder.spreadsheets.each do |spreadsheet|
        puts "#{country.code}: #{folder.name}: #{spreadsheet.name}".green
        category = descendants.find { |c| c.code == spreadsheet.name }

        ap "#{country.code}:#{folder.name}: #{root_category.code}: #{spreadsheet.name}"

        if category.nil?
          ap "Do not found category with code: '#{spreadsheet.name}'"
          next
        end

        if category
          compose(Companies::Parser::ParseSpreadsheet, spreadsheet: spreadsheet, category: category)
        else
          errors.add(:category, "with code: '#{spreadsheet.name}' not found")
        end
      end
    end

  end
end
