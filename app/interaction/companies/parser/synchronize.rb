# frozen_string_literal: true

class Companies::Parser::Synchronize < ActiveInteraction::Base
  CONFIG_PATH = Rails.root.join('config', 'google_drive_config.json').freeze
  ROOT_FOLDER_ID = ENV['ROOT_FOLDER_ID']

  def execute
    session = GoogleDrive::Session.from_config(CONFIG_PATH.to_s)
    folder = session.folder_by_id(ROOT_FOLDER_ID)
    compose(Companies::Parser::ParseRootFolder, folder: folder)
  end
end
