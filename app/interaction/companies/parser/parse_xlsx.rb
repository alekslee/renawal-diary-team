# frozen_string_literal: true

class Companies::Parser::ParseXlsx < ActiveInteraction::Base
  object :xlsx, class: Roo::Excelx
  object :category

  def execute
    xlsx.each_with_pagename do |type, sheet|
      next if row_empty?(sheet, 1) || row_empty?(sheet, 2)

      compose(Companies::Parser::ParseSheet, company_type: type, sheet: sheet, category: category)
    end
  end

  private

  def row_empty?(sheet, i)
    (1..4).to_a.all? do |j|

      sheet.cell(i,j).nil?
    end
  end
end
