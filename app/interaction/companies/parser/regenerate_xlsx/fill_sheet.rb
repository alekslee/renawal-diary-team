# frozen_string_literal: true

class Companies::Parser::RegenerateXlsx::FillSheet < ActiveInteraction::Base
  object :sheet, class: Axlsx::Worksheet
  array :companies

  def execute
    companies.each do |company|
      sheet.add_row [company.name, company.url, company.twitter, company.code]
    end
  end
end
