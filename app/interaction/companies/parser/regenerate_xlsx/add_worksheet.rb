# frozen_string_literal: true

class Companies::Parser::RegenerateXlsx::AddWorksheet < ActiveInteraction::Base
  object :package, class: Axlsx::Package
  string :sheet_name
  array :companies

  def execute
    package.workbook.add_worksheet do |sheet|
      sheet.name = sheet_name
      sheet.add_row Company::XLSX_HEADERS.values
      compose(Companies::Parser::RegenerateXlsx::FillSheet, sheet: sheet, companies: companies)
    end
  end
end
