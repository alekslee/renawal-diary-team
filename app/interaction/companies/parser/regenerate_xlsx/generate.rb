# frozen_string_literal: true

class Companies::Parser::RegenerateXlsx::Generate < ActiveInteraction::Base
  string :path
  object :category
  array  :sheets

  def execute
    Axlsx::Package.new do |package|
      compose(Companies::Parser::RegenerateXlsx::FillPackage, category: category, package: package, sheets: sheets)
      package.serialize(path)
    end
  end
end
