# frozen_string_literal: true

class Companies::Parser::RegenerateXlsx::FillPackage < ActiveInteraction::Base
  object :package, class: Axlsx::Package
  object :category
  array :sheets

  def execute
    group = category.reload.companies.group_by(&:company_type)

    sheets.each do |sheet_name|
      companies = group.dig(sheet_name)&.sort_by(&:sheet_index) || []

      compose(
        Companies::Parser::RegenerateXlsx::AddWorksheet,
        package: package,
        sheet_name: sheet_name,
        companies: companies,
      )
    end

  end
end
