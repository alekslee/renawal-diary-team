# frozen_string_literal: true

class Companies::Parser::UpdateOrCreateCompany < ActiveInteraction::Base
  string :name
  string :url, default: nil
  string :twitter, default: nil
  string :company_type
  string :code, default: nil
  integer :sheet_index
  symbol :status, default: nil

  object :category

  def execute
    company = retrieve_company_by_sheet_code || retrieve_company_by_generate_code
    return company if (company && company_attributes_doesnt_change?(company)) || doesnt_inputs?

    category.companies_changed = true
    return compose(Companies::Update, inputs.merge(company: company)) if company

    compose(Companies::Create, inputs)
  end

  private

  def retrieve_company_by_sheet_code
    code && category.reload.companies.to_a.find { |c| c.code == code && c.company_type == company_type }
  end

  def retrieve_company_by_generate_code
    _code = Company.generate_code(name)
    category.companies.to_a.find { |c| c.code == _code && c.company_type == company_type }
  end

  def company_attributes_doesnt_change?(company)
    return false unless company
    attributes = inputs.compact.slice(:name, :url, :twitter, :sheet_index).stringify_keys
    attributes == company.attributes.slice(*attributes.keys)
  end

  def doesnt_inputs?
    inputs.slice(:name, :url, :twitter).values.any?(&:blank?)
  end
end
