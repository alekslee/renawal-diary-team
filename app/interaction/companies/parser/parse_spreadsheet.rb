# frozen_string_literal: true

class Companies::Parser::ParseSpreadsheet < ActiveInteraction::Base
  object :spreadsheet, class: GoogleDrive::Spreadsheet
  object :category

  def execute
    path = compose(Companies::Parser::RetrieveTmpSpreadsheetPath, spreadsheet_id: spreadsheet.id)
    spreadsheet.export_as_file(path)
    xlsx = Roo::Spreadsheet.open(path)

    ApplicationRecord.transaction do
      puts "run Companies::Parser::ParseXlsx: #{category.id}, #{category.code}".green
      compose(Companies::Parser::ParseXlsx, xlsx: xlsx, category: category)

      # companies can be changed within ParseXlsx interaction if spreadsheet on GoogleDrive was changed
      if category.companies_changed?
        puts "run Companies::Parser::UpdateSpreadsheetOnGoogle: #{category.id}, #{category.code}".green
        compose(Companies::Parser::UpdateSpreadsheetOnGoogle, category: category, spreadsheet: spreadsheet, path: path)
      end

      FileUtils.rm(path)
    end

  end
end
