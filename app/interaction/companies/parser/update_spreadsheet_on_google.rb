# frozen_string_literal: true

class Companies::Parser::UpdateSpreadsheetOnGoogle < ActiveInteraction::Base
  string :path
  object :category, class: Category
  object :spreadsheet, class: GoogleDrive::Spreadsheet

  def execute
    # spreadsheet.update_from_file(path)

    if spreadsheet.name != category.code
      ap "wrong category:#{category.id} '#{category.code}', expected '#{spreadsheet.name}'"
      return
    end

    requests = compose(
      Companies::Parser::GoogleSpreadsheetsRequests::BatchUpdate::Generate,
      category: category,
      spreadsheet: spreadsheet
    )

    spreadsheet.batch_update(requests)
    # FileUtils.rm(path)
  end

end
