# frozen_string_literal: true

class Companies::Parser::ParseSheet < ActiveInteraction::Base
  string :company_type
  object :sheet, class: Roo::Excelx
  object :category

  # validates_inclusion_of :company_type, in: Company.company_type.values

  def execute
    unless company_type.in? Company.company_type.values
      ap "Company type '#{company_type}' is not included in the list #{Company.company_type.values.join(',')}"
      return
    end

    compose(Companies::Parser::RemoveDuplicatesCompanies, company_type: company_type, category: category)
    compose(Companies::Parser::DestroyCompanies, company_type: company_type, category: category, collections: collections.to_a)

    collections.each_with_index do |data, index|
      next if index.zero? # skip headers
      data[:name] = data[:name].to_s
      data[:code] = data[:code].to_s
      data[:url] = data[:url] || '-'
      data[:twitter] = data[:twitter] || '-'

      #to google sheet index
      data[:sheet_index] = (index + 1)
      compose(Companies::Parser::UpdateOrCreateCompany, data.merge(inputs).merge(status: :approved))
    end
  end

  def collections
    sheet.each(retrieve_parse_options)
  end

  private

  # for case when spreadsheets is createing first time and doesn't contains code cell yet
  def retrieve_parse_options
    Company::XLSX_HEADERS.reduce({}) do |memo, (_key, value)|
      memo[_key] = value if value = find_key(_key, value)
      memo
    end
  end

  def find_key(_key, value)
    @header_row = sheet.row(1)

    @header_row.find do |title|
      sanitize_title = title.to_s.squish.downcase

      sanitize_title == value || (sanitize_title == 'website' && _key == :url)
    end
  end
end
