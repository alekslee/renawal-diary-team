# frozen_string_literal: true

class Companies::Parser::ParseRootFolder < ActiveInteraction::Base
  object :folder, class: GoogleDrive::Collection

  CONTRY_CODE_REPLACE = {
    'uk' => 'gb'
  }

  def execute
    countries = Country.all.includes(root_categories: :companies).to_a
    folder.subfolders.each do |country_folder|
      folder_name = CONTRY_CODE_REPLACE[country_folder.name] || country_folder.name
      country = countries.find { |c| c.iso_a2_code == folder_name }

      if folder_name.in? ['gb', 'us']
        next
      end

      if country
        compose(Companies::Parser::ParseCountryFolder, country: country, country_folder: country_folder)
        compose(Companies::Parser::ParseCountrySubFolders, country: country, country_folder: country_folder)
      else
        errors.add(:country, "with code: '#{folder_name}' not found")
      end
    end
  end
end
