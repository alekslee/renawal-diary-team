class Companies::Parser::GoogleSpreadsheetsRequests::BatchUpdate::SheetMatrix < ActiveInteraction::Base
  array :companies
  object :sheet, class: GoogleDrive::Worksheet

  def execute
    @company_hash = companies.index_by(&:sheet_index)
    sheet_matrix = get_sheet_matrix(sheet)

    # HEADERS
    cells_matrix = [ get_headers ]

    sheet_matrix.each_with_index do |row, index|
      google_row_index = index + 2
      cells_matrix << get_col(
        @company_hash[google_row_index], row, google_row_index
      )
    end

    cells_matrix
  end

  def get_col(company, row, google_row_index)
    if company.nil?
      unless company = @company_hash.values.find { |c| c.name == row[:name] }
        ap "company not found #{sheet.inspect} row:#{row.inspect}"

        return { values: [
          text_row('')
        ]}
      end
    end

    code = Company.generate_code(company.name)

    if row[:code].present? && code != row[:code]
      ap "row:#{google_row_index} code changed from #{row[:code]} => #{code}"
    end

    # if code != row[:code]
    { values: [
      text_row(code)
    ]}
  end

  def get_headers
    { values: [
      text_row('code'),
      text_row('alias'),
    ]}
  end

  def text_row(text)
    { user_entered_value: { string_value: text }}
  end

  def get_sheet_matrix(sheet)
    sheet_rows = sheet.rows

    headers = sheet_rows.first
    sheet_rows.drop(1).map do |row|
      headers.each_with_index.reduce({}) do |memo, (key, index)|
        memo[key.to_s.downcase.to_sym] = row[index]
        memo
      end
    end
  end
end