class Companies::Parser::GoogleSpreadsheetsRequests::BatchUpdate::Generate < ActiveInteraction::Base
  object :category, class: Category
  object :spreadsheet, class: GoogleDrive::Spreadsheet

  def execute
    group = category.reload.companies.group_by(&:company_type)

    spreadsheet.worksheets.map do |sheet|
      companies = group.dig(sheet.title)&.sort_by(&:sheet_index) || []

      if companies.blank?
        ap "#{sheet.title} not found in {#{group.keys.join(',')}}"
      end

      cells_matrix = compose(
        Companies::Parser::GoogleSpreadsheetsRequests::BatchUpdate::SheetMatrix,
        companies: companies,
        sheet: sheet
      )

      #{ name: 0, url: 1, twitter: 2, code: 3, alias: 4 }
      offset = {
        sheet_id: sheet.gid.to_i,
        column_index: 3, # code column
        row_index: 0 # with header( row[0] )
      }

      request = { update_cells: {
        rows: cells_matrix,
        start: offset,
        fields: "*"
      }}

      request
    end
  end

end
