# frozen_string_literal: true

class Companies::Parser::RetrieveTmpSpreadsheetPath < ActiveInteraction::Base
  TMP_DIRNAME = Rails.root.join('tmp', 'google_companies_parser').to_s.freeze

  string :spreadsheet_id

  def execute
    FileUtils.mkdir_p(TMP_DIRNAME) unless File.directory?(TMP_DIRNAME)
    "#{TMP_DIRNAME}/#{spreadsheet_id}.xlsx"
  end
end
