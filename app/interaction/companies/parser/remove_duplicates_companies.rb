# frozen_string_literal: true

class Companies::Parser::RemoveDuplicatesCompanies < ActiveInteraction::Base
  string :company_type
  object :category, class: Category

  def execute
    group_index = types_companies.uniq(&:code).index_by(&:id)

    companies_to_delete = types_companies.reduce([]) do |array, company|
      if group_index[company.id].nil?
        array.push(company)
      end
      array
    end

    companies_to_delete.map(&:destroy)
  end

  private

  def types_companies
    category.reload.companies.to_a.select{ |c| c.company_type == company_type }
  end

end
