# frozen_string_literal: true

class Companies::Parser::ParseCountryFolder < ActiveInteraction::Base
  object :country
  object :country_folder, class: GoogleDrive::Collection

  def execute

    country_folder.spreadsheets.each do |spreadsheet|
      category = country.root_categories.to_a.find { |c| c.code == spreadsheet.name }
      puts "#{country.code}: #{spreadsheet.name}".green

      unless spreadsheet.name.in? %w{ insurance broadband energy subscriptions business }
        puts "TODO: #{spreadsheet.name}".blue
        next
      end

      if category
        compose(Companies::Parser::ParseSpreadsheet, spreadsheet: spreadsheet, category: category)
      else
        errors.add(:category, "with code: '#{spreadsheet.name}' not found")
      end
    end
  end
end
