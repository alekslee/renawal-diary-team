# frozen_string_literal: true

class Companies::Parser::DestroyCompanies < ActiveInteraction::Base
  object :collections, class: Array
  string :company_type
  object :category, class: Category

  def execute
    @indexed_collections = collections.drop(1).index_by do |data|
      data[:code].to_s.presence || Company.generate_code(data[:name])
    end

    companies_to_delete.map(&:destroy)
  end

  def companies_to_delete
    types_companies.reduce([]) do |array, company|
      c_code = company.code.to_s

      if @indexed_collections[c_code].nil?
        array.push(company)
      end

      array
    end
  end

  private

  def types_companies
    category.reload.companies.to_a.select{ |c| c.company_type == company_type }
  end

end
