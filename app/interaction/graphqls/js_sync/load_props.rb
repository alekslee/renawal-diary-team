# frozen_string_literal: true

module Graphqls::JsSync
  class LoadProps < ActiveInteraction::Base
    API_FILE_NAME = 'api.js'
    QUERIES_FOLDER_NAME = 'queries'

    object :core_path, class: Pathname
    string :js_action_name
    object :controller, class: ApplicationController
    object :user, default: nil
    object :country
    array :variables, default: []

    def execute
      file_api_data = ReadFile.run! pathname: core_path.join(API_FILE_NAME)
      query_names = LoadQueryNames.run! data: file_api_data, queries_folder_name: QUERIES_FOLDER_NAME
      queries = LoadQueries.run! query_names: query_names, queries_folder_path: core_path.join(QUERIES_FOLDER_NAME)
      attrs = LoadQueryAttributes.run! data: file_api_data, js_action_name: js_action_name
      attrs = LoadVariables.run! variables: variables, attrs: attrs
      parsed_attrs = ReplaceQueryNamesWithQueries.run! queries: queries, attributes: attrs
      queries_params = RubyHashFromString.run! string: parsed_attrs
      data = compose(
        Graphqls::MultipleExecute,
        context: { current_user: user, controller: controller, current_country: country },
        queries: queries_params[:queries]
      ).as_json

      access_queries = attrs.scan(/(?<=query:) \w+/).map(&:squish)

      compose(TransformGraphqlKeysToReduxKeys, data: data, access_queries: access_queries, queries: queries)
    end
  end
end
