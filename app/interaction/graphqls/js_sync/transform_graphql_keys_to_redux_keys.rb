# frozen_string_literal: true

class Graphqls::JsSync::TransformGraphqlKeysToReduxKeys < ActiveInteraction::Base
  array :data
  hash :queries, strip: false
  array :access_queries

  def execute
    access_queries.reduce({}) do |memo, action_query_name|

      graphql = retrieve_graphql_key( queries[action_query_name] )

      data.find do |element|
        if el = element['data'][graphql]
          memo[action_query_name] = el
        end
      end

      memo
    end
  end

  def retrieve_graphql_key(query)
    query.match(/(query|mutation|subscription)\s(\w+)(\([^{}]+\))?\s*\{/m)[2]
  end
end