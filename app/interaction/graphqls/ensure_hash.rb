# frozen_string_literal: true

class Graphqls::EnsureHash < ActiveInteraction::Base
  object :ambiguous_param, class: Object

  def execute
    case ambiguous_param
    when String
      if ambiguous_param.present?
        compose(Graphqls::EnsureHash, ambiguous_param: JSON.parse(ambiguous_param))
      else
        {}
      end
    when Hash, ActionController::Parameters
      ambiguous_param
    when nil
      {}
    else
      errors.add(:unexpected_parameter, ambiguous_param.to_s)
    end
  end
end
