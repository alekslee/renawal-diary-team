# frozen_string_literal: true

class Graphqls::RetrieveVariables < ActiveInteraction::Base
  hash :params, strip: false

  def execute
    variables = compose(Graphqls::EnsureHash, ambiguous_param: params[:variables])
    files = compose(Graphqls::RetrieveVariablesFromMultipartParams, inputs)
    variables.deep_merge!(files) if files
    variables
  end
end
