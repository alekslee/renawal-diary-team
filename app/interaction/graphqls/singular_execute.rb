# frozen_string_literal: true

class Graphqls::SingularExecute < ActiveInteraction::Base
  hash :context, strip: false do
    object :current_user, class: User, default: nil
    object :controller, class: ApplicationController, default: nil
    object :channel, class: ApplicationCable::Channel, default: nil
    object :current_country, class: Country
  end
  hash :params, strip: false do
    string :query, default: nil
  end

  def execute
    variables = compose(Graphqls::RetrieveVariables, params: params)
    RenewaldiaryWebappSchema.execute(params[:query], variables: variables, context: context)
  end
end
