# frozen_string_literal: true

class Countries::Create < ActiveInteraction::Base
  string :en_name, :iso_a2_code, :default_lang_code
  array :available_lang_codes

  def execute
    country = Country.new(inputs)
    errors.merge!(country.errors) unless country.save
    country
  end
end
