# frozen_string_literal: true

class Countries::LoadCitiesByZipCode < ActiveInteraction::Base
  symbol :country_code
  string :zip_code
  string :api, default: 'google'
  EZCMD_FINDZIP_URL = "https://ezcmd.com/apps/api_geo_postal_codes/nearby_locations_by_zip_code"

  def execute

    case api
    when 'google'
      execute_google
    when 'ezcmd'
      execute_ezcmd
    end

  end

  def execute_google
    Geocoder.search("#{country_code} #{zip_code}")
  end

  def execute_ezcmd
    responce = fetch_zip_codes(zip_code, country_code)

    if responce['success']
      responce['search_results']
    else
      ap responce
      []
    end
  end

  private

  def fetch_zip_codes(zip_code, country_code)
    url = URI(EZCMD_FINDZIP_URL)
    url.path = Pathname(url.path).join(ENV['EZCMD_API_KEY'], ENV['EZCMD_USER_ID']).to_s
    url.query = {
      zip_code: zip_code.to_s,
      country_code: country_code.to_s.upcase,
      within: 1
    }.to_query

    io = open(url)
    Oj.load(io.read) || {} # rescue {}
  end

end
