# frozen_string_literal: true

# TODO: check if we need enable batching for this
class Countries::WithRank < ActiveInteraction::Base
  RANK_ORDER = %w[nz za in au ca gb us].freeze
  MULTIPLIER = RANK_ORDER.count + 2 # counting from 1 and 1 more than max counting number

  object :prepended_country, class: Country, default: nil

  def execute
    countries = prepended_country ? retrieve_and_sort_countries_with_prepended : retrieve_and_sort_countries
    countries.map.with_index do |country, index|
      country.assign_attributes(rank: index + 1)
      country
    end
  end

  private

  def retrieve_and_sort_countries
    Country.all.sort_by do |country|
      retrieve_priority(country)
    end
  end

  def retrieve_and_sort_countries_with_prepended
    Country.where.not(id: prepended_country.id).sort_by do |country|
      retrieve_priority(country)
    end.prepend(prepended_country)
  end

  def retrieve_priority(country)
    country_priority_index = RANK_ORDER.index(country.code)
    addition = country_priority_index ? country_priority_index + 1 : 0
    -(country.users_count * MULTIPLIER + addition)
  end
end
