# frozen_string_literal: true

class Countries::LoadCities < ActiveInteraction::Base
  string :country_code

  def execute
    cities = CS.states(country_code.to_sym).reduce([]) do |arr, (code, _)|
      arr.concat CS.cities(code, country_code.to_sym)
    end.uniq
  end

end
