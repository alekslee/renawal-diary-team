# frozen_string_literal: true

class Countries::LoadRegions < ActiveInteraction::Base
  string :country_code

  def execute
    CS.states(country_code.to_sym)
  end

end
