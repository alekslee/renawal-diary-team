# frozen_string_literal: true

class Countries::LoadCategories < ActiveInteraction::Base
  object :country
  hash :categories, strip: false

  def execute
    categories.each do |code, data|
      load_category(code, data)
    end
  end

  private

  def load_category(code, data)
    category = country.categories.find_by(code: code, en_name: data[:name])
    category ||= compose(Categories::Create, country: country, code: code, en_name: data[:name])
    compose(Categories::LoadChildren, category: category, children: data[:categories]) if data[:categories].present?
  end
end
