# frozen_string_literal: true

class Countries::SetupFromYML < ActiveInteraction::Base
  def execute
    countries = compose(LoadYamlFile, path: Rails.root.join('config', 'country_categories.yml').to_s)[:countries]
    countries.each do |country_code, data|
      load_country(country_code, data)
    end
  end

  private

  def load_country(country_code, data)
    country = Country.find_by_iso_a2_code(country_code)
    country ||= compose(Countries::Create, data.merge(iso_a2_code: country_code.to_s))
    compose(Countries::LoadCategories, country: country, categories: data[:categories]) if data[:categories].present?
  end
end
