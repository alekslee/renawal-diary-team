# frozen_string_literal: true

class Categories::FindCountryLeafCategory < ActiveInteraction::Base
  object :country
  integer :category_id

  def execute
    category = country.leaf_categories.find_by_id(category_id)
    errors.add(:category, 'doesn\'t exists') unless category
    category
  end
end
