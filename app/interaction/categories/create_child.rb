# frozen_string_literal: true

class Categories::CreateChild < ActiveInteraction::Base
  object :category
  symbol :code
  string :en_name

  def execute
    child_category = category.children.new(child_attributes)
    errors.merge!(child_category.errors) unless child_category.save
    child_category
  end

  private

  def child_attributes
    inputs.except(:category).merge(country_id: category.country_id)
  end
end
