# frozen_string_literal: true

class Categories::ToggleSubscribe < ActiveInteraction::Base
  integer :category_id
  object :user
  boolean :selected

  def execute
    category = Category.find(category_id)

    if selected
      user.users_categories.find_or_create_by(category: category)
      category.parent.users_categories.find_or_create_by(user: user)
    else
      user.users_categories.where(category: category).destroy_all
      category.parent.users_categories.where(user: user).destroy_all
    end

    Categories::BroadcastCategoryChangedMembers.run!(category: category)
  end
end
