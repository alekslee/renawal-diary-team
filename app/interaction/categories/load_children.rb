# frozen_string_literal: true

class Categories::LoadChildren < ActiveInteraction::Base
  object :category
  hash :children, strip: false

  def execute
    children.each do |code, data|
      load_category(code, data)
    end
  end

  def load_category(code, data)
    new_category = Category.unscoped.find_by(parent_id: category.id, code: code, en_name: data[:name], country_id: category.country_id)
    new_category ||= compose(Categories::CreateChild, category: category, code: code, en_name: data[:name])
    compose(Categories::LoadChildren, category: new_category, children: data[:categories]) if data[:categories].present?
  end
end
