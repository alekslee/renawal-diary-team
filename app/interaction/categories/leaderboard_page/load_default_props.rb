# frozen_string_literal: true

class Categories::LeaderboardPage::LoadDefaultProps < ActiveInteraction::Base
  LAYOUT_DATA_CORE_PATH = Rails.root.join('client', 'packs', 'core', 'category').freeze
  SCREEN_DATA_CORE_PATH = Rails.root.join('client', 'packs', 'core', 'company').freeze
  FETCH_LAYOUT_DATA_ACTION_NAME = 'fetchSmartReminderLayoutData'
  FETCH_SCREEN_DATA_ACTION_NAME = 'fetchLeaderboardScreenData'
  FIRST_COMPANIES_PER_PAGE = 3
  DEFAULT_CHECKED_PLACES_FILTER = 'two_places_or_more'
  DEFAULT_LAST_BUY_FILTER = 'this_week'
  COMPANY_TYPE_OPTIONS = %w{ all provider broker comparsion_site }

  object :user, default: nil
  object :controller, class: ApplicationController
  object :country
  string :category_id

  def execute
    by_code = category_id.to_s.underscore.delete_prefix(Category.slag_prefix)

    q = Category
    if user
      q = q.where(country_id: user&.country)
    else
      q = q.where(country_id: country.id)
    end

    category = q.find_by("categories.id = :by_id OR categories.code = :by_code", by_id: category_id.to_i, by_code: by_code)
    cat_id = category&.id

    is_energy_broad_band_category = ['broadband', 'energy'].include?(category.root.en_name)

    layout_props = compose(
      Graphqls::JsSync::LoadProps,
      core_path: LAYOUT_DATA_CORE_PATH,
      js_action_name: FETCH_LAYOUT_DATA_ACTION_NAME,
      controller: controller,
      user: user,
      country: country,
      variables: [{ id: cat_id.to_s }, { categoryId: cat_id.to_s }]
    )
    category = layout_props['fetchCategory']

    screen_props = compose(
      Graphqls::JsSync::LoadProps,
      core_path: SCREEN_DATA_CORE_PATH,
      js_action_name: FETCH_SCREEN_DATA_ACTION_NAME,
      controller: controller,
      user: user,
      country: country,
      variables: [{
        categoryId: cat_id.to_s,
        criteriaQuestions: nil,
        checkedPlaces: DEFAULT_CHECKED_PLACES_FILTER,
        companyType: is_energy_broad_band_category ? COMPANY_TYPE_OPTIONS[1] : COMPANY_TYPE_OPTIONS[0],
        countryCode: country.iso_a2_code,
        lastBuy: DEFAULT_LAST_BUY_FILTER,
        perPage: FIRST_COMPANIES_PER_PAGE
      }, {
        categoryId: cat_id.to_s,
      }]
    )

    {
      category: { object: category },
      company: {
        list: screen_props['fetchCompaniesList']&.[]('companies'),
        totalCount: screen_props['fetchCompaniesList']&.[]('totalCount'),
        recommendedSource: screen_props['fetchCompaniesList']&.[]('recommendedSource'),
        recommendedProvider: screen_props['fetchCompaniesList']&.[]('recommendedProvider'),
        averagePrice: screen_props['fetchCompaniesList']&.[]('averagePrice'),
        personalizeFilters: screen_props['personalizeFilters']
      },
      smartReminder: {
        list: layout_props['fetchSmartReminders'],
        customList: layout_props['fetchCustomSmartReminders'],
      }
    }
  end
end
