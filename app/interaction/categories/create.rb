# frozen_string_literal: true

class Categories::Create < ActiveInteraction::Base
  object :country
  symbol :code
  string :en_name

  def execute
    category = country.categories.new(inputs.except(:country))
    errors.merge!(category.errors) unless category.save
    category
  end
end
