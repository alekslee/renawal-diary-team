# frozen_string_literal: true

class Categories::FetchCategories < ActiveInteraction::Base
  string :country_code

  def execute
    country_id = Country.find_by_code(country_code.downcase)&.id
    excluded_category_ids = JSON.parse($redis&.hget('cache-exclude-category-ids', country_id) || '[]')

    q = Category
      .where(country_id: country_id)

    if excluded_category_ids.present?
      q = q
        .where.not(id: excluded_category_ids)
    end

    q.order(:id)
  end
end