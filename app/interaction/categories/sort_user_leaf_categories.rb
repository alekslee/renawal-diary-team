# frozen_string_literal: true

class Categories::SortUserLeafCategories < ActiveInteraction::Base
  object :user
  array :categories do
    object class: Category
  end

  def execute
    categories_ids = user.categories.leaf.ids
    categories.sort_by { |c| categories_ids.include?(c.id) ? -1 : 0 }
  end
end
