# frozen_string_literal: true

class Categories::BroadcastCategoriesChangedMembers < ActiveInteraction::Base
  array :categories

  def execute
    RenewaldiaryWebappSchema.subscriptions.trigger('categoriesChangedMembers', {}, categories.to_a)
  end
end
