# frozen_string_literal: true

class Categories::BroadcastCategoryChangedMembers < ActiveInteraction::Base
  object :category

  def execute
    RenewaldiaryWebappSchema.subscriptions.trigger('categoryChangedMembers', {}, category)
  end
end
