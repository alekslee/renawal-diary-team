# frozen_string_literal: true

class Categories::FindUsersLeafCategory < ActiveInteraction::Base
  object :user
  integer :category_id

  def execute
    category = user.country_leaf_categories.find_by_id(category_id)
    errors.add(:category, 'doesn\'t exists') unless category
    category
  end
end
