# frozen_string_literal: true

class Categories::VotePage::LoadDefaultProps < ActiveInteraction::Base
  LAYOUT_DATA_CORE_PATH = Rails.root.join('client', 'packs', 'core', 'category').freeze
  FETCH_LAYOUT_DATA_ACTION_NAME = 'fetchSmartReminderLayoutData'

  object :user, default: nil
  object :controller, class: ApplicationController
  object :country
  string :category_id

  def execute
    by_code = category_id.to_s.underscore.delete_prefix(Category.slag_prefix)

    cat_id = Category
      .where(country_id: country)
      .find_by("id = :by_id OR code = :by_code", by_id: category_id.to_i, by_code: by_code)&.id

    layout_props = compose(
      Graphqls::JsSync::LoadProps,
      core_path: LAYOUT_DATA_CORE_PATH,
      js_action_name: FETCH_LAYOUT_DATA_ACTION_NAME,
      controller: controller,
      user: user,
      country: country,
      variables: [{ id: cat_id.to_s }, { categoryId: cat_id.to_s }]
    )
    category = layout_props['fetchCategory']

    {
      category: { object: category },
      smartReminder: {
        list: layout_props['fetchSmartReminders'],
        customList: layout_props['fetchCustomSmartReminders'],
      }
    }
  end
end
