# frozen_string_literal: true

class Mobiles::AddMakes < ActiveInteraction::Base
  MAKES = Mobiles::ParseMakes.run.result

  def execute
    Mobile.transaction do
      MAKES.each do |make|
        Mobile.create!(make: make, model: 'Default')
      end
    end
  end
end
