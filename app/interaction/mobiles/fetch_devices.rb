# frozen_string_literal: true

class Mobiles::FetchDevices < ActiveInteraction::Base
  API_URL = 'https://fonoapi.freshpixl.com/v1/getdevice'

  string :device

  def execute
    prepera_results( fetch_devices(device) )
  end

  def prepera_results(result)
    if result.is_a?(Hash)
      ap result
      # raise StandardError, result
      return []
    end

    result
  end

  private

  def fetch_devices(val)
    uri = URI(API_URL)
    uri.query = { token: ENV['FONOAPI_TOKEN'],
                  device: val.gsub(' ', '+') }.to_query

    io = open(uri)
    Oj.load(io.read)
  end
end
