# frozen_string_literal: true

class Mobiles::Criterias::RetrieveMakeOptions < ActiveInteraction::Base

  def execute
    makes = fetch_makes

    makes.each_with_object([]) do |mobile, array|
      next if mobile.make.nil?

      array.push(value: mobile.make, label: mobile.make)
    end
  end

  private

  def fetch_makes
    Mobile.select(:make).distinct
  end
end
