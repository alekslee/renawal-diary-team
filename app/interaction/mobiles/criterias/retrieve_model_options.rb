# frozen_string_literal: true

class Mobiles::Criterias::RetrieveModelOptions < ActiveInteraction::Base
  string :make, default: nil

  def execute
    devices = make ? compose(Mobiles::FetchDevices, device: make) : []
    devices.map { |device| { label: device['DeviceName'], value: device['DeviceName'] } }
  end
end
