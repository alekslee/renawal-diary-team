# frozen_string_literal: true

class Mobiles::ParseMakes < ActiveInteraction::Base
  PATH_TO_FILE = Rails.root.join('config', 'mobiles_data', 'phone_dataset.csv')

  def execute
    makes = []

    CSV.foreach(PATH_TO_FILE, header_converters: :symbol, headers: true, liberal_parsing: true) do |row|
      make = row[:brand].tr("'", '')
      makes << make unless makes.include?(make)
    end

    makes
  end
end
