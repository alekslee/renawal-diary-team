# frozen_string_literal: true

class SmartReminders::RunNotificationWorker::RetrieveTimePerformed < ActiveInteraction::Base
  object :smart_reminder

  def execute
    SmartReminder::WORKER_DATE_TRIGGERED[smart_reminder.status.to_sym].call(smart_reminder.triggered_at)
  end
end
