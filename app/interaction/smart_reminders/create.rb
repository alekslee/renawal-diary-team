# frozen_string_literal: true

class SmartReminders::Create < ActiveInteraction::Base
  integer :category_id, default: nil
  string :reminder_type
  date :triggered_at
  string :alias_name, default: nil
  object :user

  def execute
    smart_reminder = user.smart_reminders.new(inputs.except(:user))
    smart_reminder.category = default_category unless smart_reminder.category
    errors.merge!(smart_reminder.errors) unless smart_reminder.save
    smart_reminder
  end

  private

  def default_category
    user.country.categories.find_by(code: 'other')
  end
end
