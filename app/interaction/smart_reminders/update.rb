# frozen_string_literal: true

class SmartReminders::Update < ActiveInteraction::Base
  integer :id
  integer :category_id, default: nil
  string :reminder_type
  date :triggered_at
  string :alias_name, default: nil
  object :user

  def execute
    smart_reminder = compose(SmartReminders::Find, inputs)
    smart_reminder.assign_attributes(inputs.except(:id, :user))
    errors.merge!(smart_reminder.errors) unless smart_reminder.save
    smart_reminder
  end
end
