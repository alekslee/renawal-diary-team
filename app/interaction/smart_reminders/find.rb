# frozen_string_literal: true

class SmartReminders::Find < ActiveInteraction::Base
  integer :id
  object :user

  def execute
    smart_reminder = user.smart_reminders.find_by_id(id)
    errors.add(:smart_reminder, 'doesn\'t exists') unless smart_reminder
    smart_reminder
  end
end
