# frozen_string_literal: true

class SmartReminders::DestroyMulti < ActiveInteraction::Base
  object :user
  array :ids

  def execute
    user.smart_reminders.where(id: ids).destroy_all
  end
end
