# frozen_string_literal: true

class SmartReminders::RunNotificationWorker < ActiveInteraction::Base
  object :smart_reminder

  def execute
    return if smart_reminder.status.completed?

    time_performed = compose(RetrieveTimePerformed, inputs)
    SmartReminderNotificationWorker.perform_at(time_performed, smart_reminder.id)
  end
end
