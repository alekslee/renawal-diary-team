# frozen_string_literal: true

class Feedbacks::BroadcastVotesCount < ActiveInteraction::Base

  integer :category_id, default: nil
  integer :root_category_id, default: nil
  symbol :period, default: :today

  def execute
    count = compose(Feedbacks::VotesCount, { period: period, root_category_id: root_category_id })

    RenewaldiaryWebappSchema.subscriptions.trigger('votesCount', { period: period.to_s, root_category_id: root_category_id }, count)
  end

end
