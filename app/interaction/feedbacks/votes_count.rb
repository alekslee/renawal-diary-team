# frozen_string_literal: true

class Feedbacks::VotesCount < ActiveInteraction::Base
  VOTE_PERIODS = { today: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day }

  integer :category_id, default: nil
  integer :root_category_id, default: nil
  symbol :period, default: nil

  def execute
    q = Feedback.all

    if category || root_category
      q = q.joins(:renewal_detail)
    end

    if root_category
      q.where!(renewal_details: { category_id: root_category.self_and_descendants })
    end

    if category
      q.where!(renewal_details: { category_id: category.id })
    end
    # TODO need discuss
    # if period
    #   q.where!(created_at: VOTE_PERIODS[period])
    # end

    q.count
  end

  private

  def root_category
    @root_category ||= Category.find_by(id: root_category_id) if root_category_id
  end

  def category
    @category ||= Category.find_by(id: category_id) if root_category_id
  end

end
