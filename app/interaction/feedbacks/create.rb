# frozen_string_literal: true

class Feedbacks::Create < ActiveInteraction::Base
  hash :renewal_detail_attributes do
    string :buying_type, default: nil
    string :last_buy, default: nil
    string :checked_places, default: nil
    integer :current_provider_id, default: nil
    integer :buying_type_company_id, default: nil
    integer :current_provider_rate, default: nil
    string :current_provider_rate_comment, default: nil
    integer :buying_type_company_rate, default: nil
    string :buying_type_company_rate_comment, default: nil
    string :criteria_questions, default: nil
    string :prices_questions, default: nil

    float :price_rate, default: nil
    float :service_rate, default: nil
    float :claim_rate, default: nil
    boolean :claim_with_bussiness, default: nil

    hash :current_provider_attributes, :buying_type_company_attributes, default: nil do
      string :name
      string :company_type
    end
  end

  integer :category_id
  integer :year
  string :insights_comment, default: nil
  object :user

  def execute
    compose(Categories::ToggleSubscribe, inputs.merge(selected: true))
    category = compose(Categories::FindUsersLeafCategory, inputs)

    nested_attrs = compose(Companies::GenerateNestedAttributes, attributes: companies_attributes, category: category)

    attributes =  {
      renewal_detail_attributes: renewal_detail_attributes.except(
        :current_provider_attributes, :buying_type_company_attributes
      ).compact.merge(nested_attrs).merge(
        category_id: category_id,
        user_id: user.id,
        using_provider: true
      ),
      insights_comment: insights_comment
    }

    # feedback = user.feedbacks.joins(:renewal_detail).where(renewal_details: { category_id: category_id }).first

    # if feedback
    #   feedback.update(attributes)
    # else
      feedback = user.feedbacks.new(attributes)
    # end

    if feedback.save
      compose(Feedbacks::BroadcastVotesCount, root_category_id: category.root&.id)
    else
      add_feedback_errors(feedback)
    end
    feedback
  end

  private

  # we need to write it because we have bug with nested attributes errors
  def add_feedback_errors(feedback)
    feedback.errors.messages.each do |key, value|
      errors.add(key, value.first)
    end
  end

  def companies_attributes
    inputs[:renewal_detail_attributes].slice(:current_provider_attributes, :buying_type_company_attributes).compact
  end
end
