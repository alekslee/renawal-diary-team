class Users::ResendUnlockInstruction < ActiveInteraction::Base
  string :email

  def execute
    user = User.send_unlock_instructions(inputs)
    errors.merge!(user.errors) if user.errors.present?
    user
  end
end
