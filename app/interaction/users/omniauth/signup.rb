# frozen_string_literal: true

class Users::Omniauth::Signup < ActiveInteraction::Base
  string :email
  boolean :email_confirmed
  string :google_uid, :facebook_uid, :twitter_uid, :linkedin_uid, default: nil

  def execute
    user = User.find_by(uid_attr) || User.find_by_email(email)
    if user
      errors.add(:user, 'already exist. Please try to sign in.')
    else
      User.new(inputs.except(:email_confirmed).merge(confirmed_at_attr))
    end
  end

  private

  def uid_attr
    @uid_attr ||= inputs.slice(:google_uid, :facebook_uid, :twitter_uid, :linkedin_uid).compact
  end

  def confirmed_at_attr
    { confirmed_at: email_confirmed? ? Time.current : nil }
  end
end
