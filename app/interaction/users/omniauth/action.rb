# frozen_string_literal: true

class Users::Omniauth::Action < ActiveInteraction::Base
  object :auth, class: OmniAuth::AuthHash

  symbol :type

  validates_inclusion_of :type, in: %i[login signup]

  def execute
    attributes = compose(User::PROVIDERS_ATTRIBUTES[auth.provider], inputs)
    case type
    when :login
      compose(Users::Omniauth::Login, attributes)
    when :signup
      compose(Users::Omniauth::Signup, attributes)
    end
  end
end
