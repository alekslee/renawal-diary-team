# frozen_string_literal: true

class Users::Omniauth::AttributesFromGoogle < ActiveInteraction::Base
  object :auth, class: OmniAuth::AuthHash

  def execute
    {
      email: auth.info.email,
      google_uid: auth.uid,
      email_confirmed: auth.extra.id_info.email_verified
    }
  end
end
