# frozen_string_literal: true

class Users::Omniauth::Login < ActiveInteraction::Base
  string :email
  boolean :email_confirmed
  string :google_uid, :facebook_uid, :twitter_uid, :linkedin_uid, default: nil

  def execute
    user = User.find_by(uid_attr)
    return user if user

    user = User.find_by_email(email)
    if user && email_confirmed?
      compose(Users::Update, uid_attr.merge(user: user))
    elsif user
      errors.add(:email, 'unconfirmed. Please confirm your email address in the omniauth provder.')
    else
      errors.add(:user, 'doesn\'t exist. Please register at first')
    end
  end

  private

  def uid_attr
    @uid_attr ||= inputs.slice(:google_uid, :facebook_uid, :twitter_uid, :linkedin_uid).compact
  end
end
