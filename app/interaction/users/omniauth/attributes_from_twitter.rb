# frozen_string_literal: true

class Users::Omniauth::AttributesFromTwitter < ActiveInteraction::Base
  object :auth, class: OmniAuth::AuthHash

  def execute
    {
      email: auth.info.email,
      google_uid: auth.uid,
      email_confirmed: true # TODO: check with vulnerabilities
    }
  end
end
