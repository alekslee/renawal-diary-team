class Users::ResendConfirmationInstruction < ActiveInteraction::Base
  string :email

  def execute
    user = User.send_confirmation_instructions(inputs)
    errors.merge!(user.errors) if user.errors.present?
    user
  end
end
