class Users::AcceptInvitation < ActiveInteraction::Base
  string :invitation_token
  string :display_name
  string :password
  float :lat
  float :lng

  object :controller, class: ApplicationController

  def execute
    user = User.accept_invitation!(inputs.except(:controller))
    if user.errors.present?
      errors.merge!(user.errors)
    else
      controller.sign_in(:user, user)
    end
    user
  end
end
