# frozen_string_literal: true

class Users::GetUserForSignIn < ActiveInteraction::Base
  string :email

  def execute
    user = User.find_by_email(email)
    if user && !user.active_for_authentication?
      if user.locked_at
        errors.add(:your_account, 'is locked')
      else
        errors.add(:your_account, 'is not confirmed. You have to confirm your email address before continuing')
      end
    elsif user.nil?
      errors.add(:bad_credentials, '. Invalid email or password')
    end
    user
  end
end
