# frozen_string_literal: true

class Users::ResetPassword < ActiveInteraction::Base
  string :email

  def execute
    user = User.send_reset_password_instructions(inputs)
    errors.merge!(user.errors) if user.errors.present?
  end
end
