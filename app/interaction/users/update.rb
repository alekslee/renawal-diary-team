# frozen_string_literal: true

class Users::Update < ActiveInteraction::Base
  string :display_name, default: nil
  file :avatar, default: nil
  string :google_uid, :facebook_uid, :twitter_uid, :linkedin_uid, default: nil
  array :users_categories_attributes, default: nil do
    hash do
      integer :id, default: nil
      integer :category_id
      boolean :_destroy, default: false
    end
  end

  object :user

  def execute
    user.assign_attributes(inputs.except(:user).compact)
    if user.save
      compose(Categories::BroadcastCategoriesChangedMembers, categories: user.country.categories.roots)
    else
      errors.merge!(user.errors)
    end
    user
  end
end
