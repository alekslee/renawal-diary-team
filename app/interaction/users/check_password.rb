# frozen_string_literal: true

class Users::CheckPassword < ActiveInteraction::Base
  object :user
  string :password

  def execute
    is_valid_password = user.valid_password?(password)
    if !user.valid_for_authentication? { is_valid_password } || !is_valid_password
      errors.add(:bad_credentials, '. Invalid email or password')
    end
    is_valid_password
  end
end
