# frozen_string_literal: true

class Users::ChangeUsersResetedPassword < ActiveInteraction::Base
  string :password
  string :password_confirmation
  string :reset_password_token
  object :controller, class: ApplicationController

  def execute
    user = User.reset_password_by_token(inputs)
    if user.errors.empty?
      user.unlock_access! if user.access_locked?
      controller.sign_in(:user, user)
    else
      errors.merge!(user.errors)
    end
    user
  end
end
