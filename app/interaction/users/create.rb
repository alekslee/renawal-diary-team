# frozen_string_literal: true

class Users::Create < ActiveInteraction::Base
  string :email
  string :display_name
  string :password
  float :lat
  float :lng
  array :users_categories_attributes do
    hash do
      integer :category_id
    end
  end

  string :facebook_uid, :twitter_uid, :linkedin_uid, :google_uid, default: nil

  object :controller, class: ApplicationController, default: nil

  def execute
    user = User.new(user_params)
    if !user.save
      errors.merge!(user.errors)
    elsif controller
      controller.sign_in(:user, user)
      compose(Categories::BroadcastCategoriesChangedMembers, categories: user.country.categories.roots)
    end
    user
  end

  private

  # TODO: system with confirmation needs refactoring. We can have vulnerabilities with current confirmations system
  def user_params
    params = inputs.except(:controller).merge(uid: SecureRandom.uuid)
    params[:confirmed_at] = Time.now if facebook_uid || twitter_uid || linkedin_uid || google_uid
    params
  end
end
