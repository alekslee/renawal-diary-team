# frozen_string_literal: true

class RenewalDetails::Criterias::RetrieveBaseMakeOptions < ActiveInteraction::Base
  def execute
    makes = fetch_makes

    makes.each_with_object([]) do |car, array|
      next if car.make.nil?

      array.push(value: car.make, label: car.make)
    end
  end

  private

  def fetch_makes
    Car.select(:make).distinct
  end
end
