# frozen_string_literal: true

class RenewalDetails::Criterias::AddCarSpecifications < ActiveInteraction::Base
  PATH_TO_FILE = Rails.root.join('config', 'motor_bike_criteria', 'specification_value.csv')
  TRIMS = RenewalDetails::Criterias::ParseCarTrim.run.result
  SPECIFICATIONS = RenewalDetails::Criterias::ParseCarSpecification.run.result

  def execute
    CSV.foreach(PATH_TO_FILE, header_converters: :symbol, headers: true, liberal_parsing: true).each_slice(500) do |array|
      CarSpecification.transaction do
        array.each do |row|
          per_row(row)
        end
      end
    end
  end

  private

  def per_row(row)
    uid_car_specification = row[:id_car_specification].tr("'", '').to_i
    uid_car_specification_value = row[:id_car_specification_value].tr("'", '').to_i
    uid_car_trim = row[:id_car_trim].tr("'", '').to_i
    value = row[:value].tr("'", '')
    trim = TRIMS[:"#{uid_car_trim}"]
    name = SPECIFICATIONS[:"#{uid_car_specification}"]
    unit = row[:unit]&.tr("'", '')

    CarSpecification.create!(
      uid_car_specification: uid_car_specification,
      uid_car_specification_value: uid_car_specification_value,
      uid_car_trim: uid_car_trim,
      value: value,
      trim: trim,
      name: name,
      unit: unit
    )
  end

end
