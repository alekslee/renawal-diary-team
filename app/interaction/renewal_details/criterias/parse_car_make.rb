# frozen_string_literal: true

class RenewalDetails::Criterias::ParseCarMake < ActiveInteraction::Base
  PATH_TO_FILE = Rails.root.join('config', 'motor_bike_criteria', 'make.csv')

  def execute
    makes = {}

    CSV.foreach(PATH_TO_FILE, header_converters: :symbol, headers: true, liberal_parsing: true) do |row|
      id_car_make = row[:id_car_make].tr("'", '').to_i
      name = row[:name].tr("'", '')
      makes.merge!("#{id_car_make}": name)
    end
    makes
  end
end
