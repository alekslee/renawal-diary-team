# frozen_string_literal: true

class RenewalDetails::Criterias::RetrievePetBreedOptions < ActiveInteraction::Base
  PET_BREED_FILE_PATH = Rails.root.join('config', 'pet_breeds.yml')

  object :renewal_detail, default: nil
  string :value, default: nil

  def execute
    animal_type = value || (renewal_detail && renewal_detail.criteria_questions['animal_type'])
    return [] if animal_type.nil?

    breeds = LoadYamlFile.run!(path: PET_BREED_FILE_PATH.to_s)[animal_type.to_sym] || []
    breeds.map do |breed|
      { label: breed, value: breed }
    end
  end
end
