# frozen_string_literal: true

class RenewalDetails::Criterias::UpdateCarType < ActiveInteraction::Base
  def execute
    Car.where(car_type: nil).update_all(car_type: 10)
  end
end
