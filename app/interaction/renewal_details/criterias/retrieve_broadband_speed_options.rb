# frozen_string_literal: true

class RenewalDetails::Criterias::RetrieveBroadbandSpeedOptions < ActiveInteraction::Base
  MIN_MBPS = 5
  MAX_MBPS = 200
  STEP = 5
  LAST_OPTIONS = [{ value: '1gb', label: '1 GB' }, { value: '2gb', label: '2 GB' }].freeze

  def execute
    (MIN_MBPS..MAX_MBPS).step(STEP).map do |number|
      { value: "#{number}mb", label: "#{number} mbs/mps" }
    end + LAST_OPTIONS
  end
end
