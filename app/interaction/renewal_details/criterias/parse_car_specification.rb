# frozen_string_literal: true

class RenewalDetails::Criterias::ParseCarSpecification < ActiveInteraction::Base
  PATH_TO_FILE = Rails.root.join('config', 'motor_bike_criteria', 'specification.csv')

  def execute
    specifications = {}

    CSV.foreach(PATH_TO_FILE, header_converters: :symbol, headers: true, liberal_parsing: true) do |row|
      id_car_specification = row[:id_car_specification].tr("'", '').to_i
      name = row[:name].tr("'", '')
      specifications.merge!("#{id_car_specification}": name)
    end
    specifications
  end
end
