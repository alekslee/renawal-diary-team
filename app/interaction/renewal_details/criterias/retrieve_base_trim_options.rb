# frozen_string_literal: true

class RenewalDetails::Criterias::RetrieveBaseTrimOptions < ActiveInteraction::Base
  object :renewal_detail, default: nil
  string :value, :make, default: nil

  def execute
    make_name = retrieve_make_name
    model_name = retrieve_model_name
    return [] if make_name.nil? || model_name.nil?

    Car.where(make: make_name, model: model_name).select(:trim).distinct.each_with_object([]) do |car, array|
      next if car.trim.nil?

      array.push(value: car.trim, label: car.trim)
    end
  end

  private

  def retrieve_model_name
    value || (renewal_detail && renewal_detail.criteria_questions&.dig('model'))
  end

  def retrieve_make_name
    make || (renewal_detail && renewal_detail.criteria_questions&.dig('make'))
  end
end
