# frozen_string_literal: true

class RenewalDetails::Criterias::RetrieveMotorBikeEngineSizeOptions < ActiveInteraction::Base
  string :make, default: nil
  string :model, default: nil

  def execute
    CarSpecification.find_by_sql(engine_size_query).map do |engine_size|
      { label: engine_size.value, value: engine_size.value }
    end
  end

  private

  def engine_size_query
    <<-SQL.squish
      SELECT DISTINCT car_specifications.value
        FROM car_specifications
        LEFT JOIN cars
        ON car_specifications.trim = cars.trim
        WHERE cars.make = ('#{make}')
        AND cars.model = ('#{model}')
        AND car_specifications.name = 'Displacement'
    SQL
  end
end
