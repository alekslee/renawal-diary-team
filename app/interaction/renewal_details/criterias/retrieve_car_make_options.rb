# frozen_string_literal: true

class RenewalDetails::Criterias::RetrieveCarMakeOptions < RenewalDetails::Criterias::RetrieveBaseMakeOptions
  private

  def fetch_makes
    Car.car.select(:make).distinct
  end
end
