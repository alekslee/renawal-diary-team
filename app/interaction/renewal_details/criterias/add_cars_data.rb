# frozen_string_literal: true

class RenewalDetails::Criterias::AddCarsData < ActiveInteraction::Base
  PATH_TO_FILE = Rails.root.join('config', 'motor_bike_criteria', 'trim.csv')
  MODELS = RenewalDetails::Criterias::ParseCarModel.run.result
  MAKES = RenewalDetails::Criterias::ParseCarMake.run.result

  def execute
    Car.transaction do
      CSV.foreach(PATH_TO_FILE, header_converters: :symbol, headers: true, liberal_parsing: true) do |row|
        per_row(row)
      end
    end
  end

  private

  def per_row(row)
    id_model = row[:id_car_model].tr("'", '').to_i
    id_make =  MODELS[:makes][:"#{id_model}"]
    model = MODELS[:names][:"#{id_model}"]
    make = MAKES[:"#{id_make}"]
    trim = row[:name].tr("'", '')
    car_type = row[:id_car_type].tr("'", '').to_i
    year = row[:start_production_year].tr("'", '').to_i

    Car.create!(
      model: model,
      make: make,
      trim: trim,
      car_type: car_type,
      year: year
    )
  end
end
