# frozen_string_literal: true

class RenewalDetails::Criterias::RetrieveMotorBikeMakeOptions < RenewalDetails::Criterias::RetrieveBaseMakeOptions
  private

  def fetch_makes
    Car.motor_bike.select(:make).distinct
  end
end
