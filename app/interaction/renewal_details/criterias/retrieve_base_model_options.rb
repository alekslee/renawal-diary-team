# frozen_string_literal: true

class RenewalDetails::Criterias::RetrieveBaseModelOptions < ActiveInteraction::Base
  object :renewal_detail, default: nil
  string :value, default: nil

  def execute
    make_name = value || (renewal_detail && renewal_detail.criteria_questions&.dig('make'))
    return [] if make_name.nil?

    Car.where(make: make_name).select(:model).distinct.each_with_object([]) do |car, array|
      next if car.model.nil?

      array.push(value: car.model, label: car.model)
    end
  end
end
