# frozen_string_literal: true

class RenewalDetails::Criterias::ParseCarTrim < ActiveInteraction::Base
  PATH_TO_FILE = Rails.root.join('config', 'motor_bike_criteria', 'trim.csv')

  def execute
    trims = {}

    CSV.foreach(PATH_TO_FILE, header_converters: :symbol, headers: true, liberal_parsing: true) do |row|
      id_car_trim = row[:id_car_trim].tr("'", '').to_i
      name = row[:name].tr("'", '')
      trims.merge!("#{id_car_trim}": name)
    end
    trims
  end
end
