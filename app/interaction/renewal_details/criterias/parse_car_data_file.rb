# frozen_string_literal: true

class RenewalDetails::Criterias::ParseCarDataFile < ActiveInteraction::Base
  PATH_TO_FILE = Rails.root.join('config', 'car_data.csv')

  def execute
    CSV.read(PATH_TO_FILE, headers: true)
  end
end
