# frozen_string_literal: true

class RenewalDetails::Criterias::ParseCarModel < ActiveInteraction::Base
  PATH_TO_FILE = Rails.root.join('config', 'motor_bike_criteria', 'model.csv')

  def execute
    models = {
      names: {},
      makes: {}
    }

    CSV.foreach(PATH_TO_FILE, header_converters: :symbol, headers: true, liberal_parsing: true) do |row|
      id_car_model = row[:id_car_model].tr("'", '').to_i
      id_car_make = row[:id_car_make].tr("'", '').to_i
      name = row[:name].tr("'", '')
      models[:names].merge!("#{id_car_model}": name)
      models[:makes].merge!("#{id_car_model}": id_car_make)
    end
    models
  end
end
