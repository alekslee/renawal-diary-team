# frozen_string_literal: true

class RenewalDetails::RetrievePricesQuestionsOptions < ActiveInteraction::Base
  object :renewal_detail, default: nil
  object :category, default: nil

  def execute
    category_obj = category || renewal_detail.category

    data = compose(LoadYamlFile, path: 'config/criteria_questions_default.yml')
    root_category_data = data[category_obj.root.code.to_sym] || data[:default]
    root_category_data[:prices_questions]
  end

  private

  def present_inputs
    return if renewal_detail || category

    errors.add(:one_of_inputs, 'should be present')
  end
end
