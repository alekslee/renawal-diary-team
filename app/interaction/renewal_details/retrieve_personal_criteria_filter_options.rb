# frozen_string_literal: true

class RenewalDetails::RetrievePersonalCriteriaFilterOptions < ActiveInteraction::Base
  object :renewal_detail, default: nil
  object :category, default: nil

  def execute
    category_obj = category || renewal_detail.category

    merge_criterias(category_obj).each_with_object({}) do |(key, opts), hash|
      hash[key] = opts.except(:options_interaction)
      next if opts[:options_interaction].nil?

      hash[key][:options] = compose(
        opts[:options_interaction].constantize,
        renewal_detail: renewal_detail, value: opts[:default_parent_option_value]
      )
    end
  end

  def merge_criterias(category_obj)
    data = load_chema(category_obj, 'config/criteria_questions_default.yml')
    key_data = load_chema(category_obj, 'config/criteria_personal_options.yml')

    key_data.reduce({}) do |memo, (key, value)|
      key, value = key.shift if key.is_a?(Hash)
      memo.merge(key => (data[key] || {}).deep_merge(value || {}))
    end
  end

  def load_chema(category_obj, path)
    data = compose(LoadYamlFile, path: path)
    root_category_data = data[category_obj.root.code.to_sym] || data[:default]
    puts category_obj.code.to_sym
    root_category_data[category_obj.code.to_sym] || {}
  end

  private

  def present_inputs
    return if renewal_detail || category

    errors.add(:one_of_inputs, 'should be present')
  end
end
