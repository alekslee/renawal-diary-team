# frozen_string_literal: true

class RenewalDetails::Find < ActiveInteraction::Base
  integer :renewal_detail_id
  object :user

  def execute
    renewal_detail = user.renewal_details.find_by_id(renewal_detail_id)
    errors.add(:renewal_detail, 'doesn\'t exists') unless renewal_detail
    renewal_detail
  end
end
