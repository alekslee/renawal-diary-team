# frozen_string_literal: true

class RenewalDetails::Update::ResetRenewalDetail < ActiveInteraction::Base
  RESETED_RENEWAL_DETAILS_ATTRIBUTES = {
    checked_places: nil,
    buying_type: nil,
    current_provider_id: nil,
    buying_type_company_id: nil,
    last_buy_date: nil,
    using_provider: false,
    prices_questions: nil
  }.freeze

  object :renewal_detail
  object :criteria_questions, class: Hash

  def execute
    errors.merge!(renewal_detail.errors) unless renewal_detail.update(
      RESETED_RENEWAL_DETAILS_ATTRIBUTES.merge(criteria_questions: criteria_questions)
    )
    renewal_detail
  end
end
