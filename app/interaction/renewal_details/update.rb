# frozen_string_literal: true

class RenewalDetails::Update < ActiveInteraction::Base
  string :checked_places, :buying_type, default: nil
  symbol :last_buy, default: nil
  boolean :using_provider, default: true
  integer :current_provider_id, :buying_type_company_id, default: nil
  integer :current_provider_rate, :buying_type_company_rate, default: nil
  string :current_provider_rate_comment, :buying_type_company_rate_comment, default: nil
  object :criteria_questions, class: Hash, default: nil
  string :prices_questions, default: nil
  hash :current_provider_attributes, :buying_type_company_attributes, default: nil do
    string :name
    string :company_type
  end

  integer :renewal_detail_id
  object :user

  def execute
    renewal_detail = compose(RenewalDetails::Find, inputs)
    if using_provider
      nested_attrs = compose(
        Companies::GenerateNestedAttributes,
        attributes: companies_attributes,
        category: renewal_detail.category
      )
      renewal_detail.assign_attributes(
        inputs.except(:user, :renewal_detail_id, :current_provider_attributes, :buying_type_company_attributes)
              .compact.merge(nested_attrs)
      )
      add_feedback_errors(renewal_detail) unless renewal_detail.save
    else
      renewal_detail = compose(
        ResetRenewalDetail,
        renewal_detail: renewal_detail, criteria_questions: criteria_questions
      )
    end
    renewal_detail
  end

  private

  # we need to write it because we have bug with nested attributes errors
  def add_feedback_errors(renewal_detail)
    renewal_detail.errors.messages.each do |key, value|
      errors.add(key, value.first)
    end
  end

  def companies_attributes
    inputs.slice(:current_provider_attributes, :buying_type_company_attributes).compact
  end
end
