# frozen_string_literal: true

class RenewalDetails::FindByCategory < ActiveInteraction::Base
  object :user
  integer :category_id
  integer :year

  def execute
    user.renewal_details.find_by(inputs.except(:user))
  end
end
