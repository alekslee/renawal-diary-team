# frozen_string_literal: true

class RenewalDetails::Create < ActiveInteraction::Base
  boolean :using_provider
  string :buying_type, default: nil
  string :checked_places, default: nil
  symbol :last_buy, default: nil
  integer :current_provider_id, :buying_type_company_id, default: nil
  integer :current_provider_rate, :buying_type_company_rate, default: nil
  string :current_provider_rate_comment, :buying_type_company_rate_comment, default: nil
  object :criteria_questions, class: Hash, default: nil
  string :prices_questions, default: nil
  hash :current_provider_attributes, :buying_type_company_attributes, default: nil do
    string :name
    string :company_type
  end

  integer :category_id
  object :user

  def execute
    compose(Categories::ToggleSubscribe, inputs.merge(selected: true))
    category = compose(Categories::FindUsersLeafCategory, inputs)

    if using_provider
      nested_attrs = compose(Companies::GenerateNestedAttributes, attributes: companies_attributes, category: category)
      renewal_detail = user.renewal_details.new(
        inputs.except(:user, :current_provider_attributes, :buying_type_company_attributes).compact.merge(nested_attrs)
      )
    else
      renewal_detail = user.renewal_details.new(
        using_provider: using_provider, category_id: category.id, criteria_questions: criteria_questions
      )
    end
    add_feedback_errors(renewal_detail) unless renewal_detail.save
    renewal_detail
  end

  private

  # we need to write it because we have bug with nested attributes errors
  def add_feedback_errors(renewal_detail)
    renewal_detail.errors.messages.each do |key, value|
      errors.add(key, value.first)
    end
  end

  def companies_attributes
    inputs.slice(:current_provider_attributes, :buying_type_company_attributes).compact
  end
end
