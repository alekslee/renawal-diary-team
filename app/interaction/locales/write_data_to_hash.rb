# frozen_string_literal: true

# TODO: later refactoring and writing tests, because don't have time to the deadline
class Locales::WriteDataToHash < ActiveInteraction::Base
  string :path
  string :lang
  array :keys
  array :values

  def execute
    hash = compose(LoadYamlFile, path: Rails.root.join('config', 'locales', path).to_s)
    keys.each_with_index do |key, index|
      h = key.split('-').reverse.inject(values[index].text) { |value, key_l| { key_l.to_sym => value } }
      hash[lang.to_sym].deep_merge!(h)
    end
    hash
  end
end
