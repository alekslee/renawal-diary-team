# frozen_string_literal: true

# TODO: later refactoring and writing tests, because don't have time to the deadline
class Locales::UpdateBackup < ActiveInteraction::Base
  string :prefix, default: nil

  def execute
    file_name = prefix.nil? ? 'en.yml' : "#{prefix}.en.yml"
    backup_file_name = prefix.nil? ? 'backup_en.yml' : "backup_#{prefix}.en.yml"
    FileUtils.cp(
      Rails.root.join('config', 'locales', file_name),
      Rails.root.join('config', 'locales', backup_file_name)
    )
  end
end
