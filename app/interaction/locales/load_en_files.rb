# frozen_string_literal: true

# TODO: later refactoring and writing tests, because don't have time to the deadline
class Locales::LoadEnFiles < ActiveInteraction::Base
  string :prefix, default: nil

  def execute
    file_name = prefix.nil? ? 'en.yml' : "#{prefix}.en.yml"
    backup_file_name = prefix.nil? ? 'backup_en.yml' : "backup_#{prefix}.en.yml"

    en_file = compose(LoadYamlFile, path: Rails.root.join('config', 'locales', file_name).to_s)[:en]
    backup_en_file = compose(LoadYamlFile, path: Rails.root.join('config', 'locales', backup_file_name).to_s)[:en]
    [en_file, backup_en_file]
  end
end
