# frozen_string_literal: true

# TODO: later refactoring and writing tests, because don't have time to the deadline
class Locales::GenerateFilesIfNotExists < ActiveInteraction::Base
  hash :paths, strip: false

  def execute
    paths.each do |code, path|
      next if File.exist?(path)

      data = { code => {} }.as_json.to_yaml
      File.open(path, 'w+') do |f|
        f.write(data)
      end
    end
  end
end
