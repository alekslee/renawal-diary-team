# frozen_string_literal: true

# TODO: later refactoring and writing tests, because don't have time to the deadline
class Locales::RetrieveDataForTranslate < ActiveInteraction::Base
  hash :en_file, strip: false
  hash :backup_en_file, strip: false

  def execute
    @data = { keys: [], values: [] }
    format_data(en_file)
    @data
  end

  private

  def format_data(data, path = [], key = nil)
    path << key unless key.nil?
    data.each do |key_l, value|
      if value.is_a? Hash
        format_data(value, Array.new(path), key_l)
      elsif value.is_a? String
        path << key_l
        write_data(path, value)
        path.pop
      end
    end
  end

  # TODO: Fix rubocop guard clause
  # rubocop:disable Style/GuardClause
  def write_data(path, value)
    if backup_en_file.dig(*path).nil? || value != backup_en_file.dig(*path)
      @data[:keys].push(path.join('-'))
      @data[:values] << value
    end
  end
  # rubocop:enable Style/GuardClause
end
