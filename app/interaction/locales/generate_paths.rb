# frozen_string_literal: true

# TODO: later refactoring and writing tests, because don't have time to the deadline
class Locales::GeneratePaths < ActiveInteraction::Base
  string :prefix, default: nil

  def execute
    countries_codes = compose(::Locales::RetrieveCountriesCodes)
    countries_codes.each_with_object({}) do |k, res|
      file_name = prefix.nil? ? "#{k}.yml" : "#{prefix}.#{k}.yml"
      res[k] = Rails.root.join('config', 'locales', file_name).to_s
    end
  end
end
