# frozen_string_literal: true

# TODO: later refactoring and writing tests, because don't have time to the deadline
class Locales::TranslateLocales < ActiveInteraction::Base
  hash :paths, strip: false
  hash :data_for_translate, strip: false
  object :google_translate, class: Google::Cloud::Translate::Api

  def execute
    paths.each do |lang, path|
      source = data_for_translate[:values]
      next if source.empty?

      translates = google_translate.translate(source, from: 'en', to: lang)
      translates = translates.is_a?(Array) ? translates : [translates]
      data = compose(
        ::Locales::WriteDataToHash,
        path: path,
        keys: data_for_translate[:keys],
        values: translates,
        lang: lang
      )
      compose(::Locales::WriteToFile, path: path, data: data)
    end
  end
end
