# frozen_string_literal: true

# TODO: later refactoring and writing tests, because don't have time to the deadline
class Locales::Translate < ActiveInteraction::Base
  string :prefix, default: nil

  def execute
    paths = compose(::Locales::GeneratePaths, prefix: prefix)
    compose(::Locales::GenerateFilesIfNotExists, paths: paths)
    google_translate = compose(::Locales::ConnectToGoogleTranslate)
    en_file, backup_en_file = compose(::Locales::LoadEnFiles, prefix: prefix)
    data_for_translate = compose(::Locales::RetrieveDataForTranslate, en_file: en_file, backup_en_file: backup_en_file)
    compose(
      ::Locales::TranslateLocales,
      paths: paths,
      google_translate: google_translate,
      data_for_translate: data_for_translate
    )
    compose(::Locales::UpdateBackup, prefix: prefix)
  end
end
