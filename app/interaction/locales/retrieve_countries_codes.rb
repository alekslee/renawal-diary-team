# frozen_string_literal: true

# TODO: later refactoring and writing tests, because don't have time to the deadline
class Locales::RetrieveCountriesCodes < ActiveInteraction::Base
  def execute
    path = Rails.root.join('config', 'country_categories.yml').to_s
    country_categories = compose(LoadYamlFile, path: path)
    countries_codes = country_categories[:countries].map { |_k, v| v[:default_lang_code] }
    countries_codes.uniq - ['en']
  end
end
