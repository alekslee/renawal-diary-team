# frozen_string_literal: true

require 'google/cloud/translate'

# TODO: later refactoring and writing tests, because don't have time to the deadline
class Locales::ConnectToGoogleTranslate < ActiveInteraction::Base
  CREDENTIALS = {
    project_id: 'renewal-diary-1541699672148',
    credentials: Rails.root.join('config', 'Renewal Diary 1-7e08513fa07e.json').to_s
  }.freeze

  def execute
    Google::Cloud::Translate.new(CREDENTIALS)
  end
end
