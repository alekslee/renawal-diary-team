# frozen_string_literal: true

# TODO: later refactoring and writing tests, because don't have time to the deadline
class Locales::WriteToFile < ActiveInteraction::Base
  string :path
  hash :data, strip: false

  def execute
    File.open(path, 'w+') do |f|
      f.write(data.as_json.to_yaml)
    end
  end
end
