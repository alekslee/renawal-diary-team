# frozen_string_literal: true

class RubyHashFromString < ActiveInteraction::Base
  string :string

  # rubocop:disable Security/Eval, Lint/RescueException
  def execute
    eval(string)
  rescue Exception => e
    raise e if Rails.env.development?

    Rails.logger.error(e.message)
    {}
  end
  # rubocop:enable Security/Eval, Lint/RescueException
end
