# frozen_string_literal: true

class PersonalizeFilters::Create < ActiveInteraction::Base
  object :criteria_questions, class: Hash, default: nil

  integer :category_id
  object :user
  boolean :is_active

  def execute
    category = compose(Categories::FindUsersLeafCategory, inputs)

    personalize_filter = category.personalize_filters
                                 .find_or_create_by(user_id: user.id)

    personalize_filter.criteria_questions = criteria_questions || personalize_filter.criteria_questions
    personalize_filter.is_active = is_active
    personalize_filter.save
    personalize_filter
  end

end
