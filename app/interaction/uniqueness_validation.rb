# frozen_string_literal: true

class UniquenessValidation < ActiveInteraction::Base
  symbol :field_name, :field_value

  validates_inclusion_of :field_name, in: %i[display_name email]

  def execute
    !User.where(field_name => field_value).exists?
  end
end
