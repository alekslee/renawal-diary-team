# frozen_string_literal: true

class LoadYamlFile < ActiveInteraction::Base
  string :path

  def execute
    YAML.load_file(path).deep_symbolize_keys
  rescue Errno::ENOENT, Psych::Exception => e
    errors.add(:load_yaml_file, e.message)
  end
end
