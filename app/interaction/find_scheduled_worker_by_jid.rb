# frozen_string_literal: true

class FindScheduledWorkerByJid < ActiveInteraction::Base
  string :jid

  def execute
    Sidekiq::ScheduledSet.new.find do |job|
      job.jid == jid
    end
  end
end
