# frozen_string_literal: true

class CriteriaQuestionsSerializer
  class << self

    def load(hash)
      hash.is_a?(String) ? Oj.load(hash) : hash
    end

    alias_method :dump, :load

  end
end
