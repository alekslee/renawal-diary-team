# frozen_string_literal: true

class PricesQuestionsSerializer
  def self.dump(hash)
    hash.is_a?(String) ? hash : Oj.dump(hash)
  end

  def self.load(hash)
    Oj.load(hash || '{}')
  end
end
