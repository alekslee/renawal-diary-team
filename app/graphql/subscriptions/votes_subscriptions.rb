# frozen_string_literal: true

module Subscriptions::VotesSubscriptions
  extend ActiveSupport::Concern

  included do
    field :votes_count, Types::VoteType, null: true do
      description 'Retrieve today\'s votes'
      argument :period, String, required: false
      argument :root_category_id, Integer, required: false
    end
  end

  #TODO only broadcast send this data
  # trigger('votesCount', { period: nil, root_category_id: nil}, count )
  def votes_count(period: nil, root_category_id: nil); end

end
