# frozen_string_literal: true

module Subscriptions::CategorySubscriptions
  extend ActiveSupport::Concern

  included do
    field :categories_changed_members, [Types::CategoryType], null: false,
        description: 'A user subscribes or unsubscribes to the categories'

    field :category_changed_members, Types::CategoryType, null: false,
        description: 'A user subscribes or unsubscribes to the category'
  end

  def categories_changed_members
    object
  end

  def category_changed_members
    object
  end
end
