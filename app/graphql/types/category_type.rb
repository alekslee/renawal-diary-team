# frozen_string_literal: true

class Types::CategoryType < Types::BaseObject
  field :id, ID, null: false
  field :name, String, null: false
  field :en_name, String, null: false
  field :country_id, Integer, null: false
  field :parent_id, Integer, null: true
  field :depth, Integer, null: false
  field :code, String, null: false
  field :slag, String, null: false
  field :users_categories_count, Integer, null: false
  field :users_count, Integer, null: false
  field :root, Types::CategoryType, null: true
  field :provider_companies, [Types::CompanyType], null: false
  field :broker_companies, [Types::CompanyType], null: false
  field :comparsion_site_companies, [Types::CompanyType], null: false

  field :users_for_all_descendants_and_all_countries_count, Integer, null: false
  field :leaf_children, [Types::CategoryType], null: false
  field :sorted_leaf_children, [Types::CategoryType], null: false
  field :selected, Boolean, null: false
  field :current_users_category_id, ID, null: true
  field :leaf_current_user_children, [Types::CategoryType], null: false
  field :renewal_detail, Types::RenewalDetailType, null: true
  field :renewal_detail_possible_years, [Integer], null: false

  def users_for_all_descendants_and_all_countries_count
    Loaders::CategoriesDescendantsLoader.for(where: { children_count: 0 }).load(object).then do |descendants|
      leaf_categories = Category.where(children_count: 0, code: descendants.pluck(:code))
      UsersCategory.where(category_id: leaf_categories.pluck(:id)).count
    end
  end

  def leaf_children
    Loaders::CategoriesDescendantsLoader.for(where: { children_count: 0 })
      .load(object)
      .then do |categories|

      categories_with_companies = if object.code == 'subscriptions'
        categories.joins(:companies).uniq
      else
        categories.joins("INNER JOIN companies ON (companies.category_id = categories.id OR companies.category_id = '#{object.id}')").uniq
      end

      categories_with_companies.blank? ? [categories.first] : categories_with_companies
    end
  end

  def sorted_leaf_children
    Loaders::CategoriesDescendantsLoader
      .for(where: { children_count: 0 })
      .load(object)
      .then do |categories|

      if context[:current_user]
        c = Categories::SortUserLeafCategories.run!(user: context[:current_user], categories: categories)
        categories = Category.where(id: c.map(&:id))
      end

      categories_with_companies = if object.code == 'subscriptions'
        categories.joins(:companies).uniq
      else
        categories.joins("INNER JOIN companies ON (companies.category_id = categories.id OR companies.category_id = '#{object.id}')").uniq
      end

      categories_with_companies.blank? ? [categories.first] : categories_with_companies
    end
  end

  def selected
    return false unless context[:current_user]

    Loaders::AssociationLoader.for(User, :categories).load(context[:current_user]).then do |categories|
      categories.map(&:id).include?(object.id)
    end
  end

  def current_users_category_id
    return nil unless context[:current_user]

    Loaders::AssociationLoader.for(Category, :users_categories).load(object).then do |users_categories|
      users_category = users_categories.to_a.find do |uc|
        uc.user_id == context[:current_user].id
      end
      users_category&.id
    end
  end

  def leaf_current_user_children
    return [] if context[:current_user].nil?

    category_ids = context[:current_user].category_ids
    Loaders::CategoriesDescendantsLoader.for(where: { children_count: 0, id: category_ids }).load(object)
  end

  def renewal_detail
    return nil if context[:current_user].nil?

    Loaders::Record.for(RenewalDetail, column: :category_id, where: { user_id: context[:current_user].id })
                   .load(object.id)
  end

  def renewal_detail_possible_years
    object.renewal_details.pluck(:year).push(Date.today.year).uniq.compact
  end
end
