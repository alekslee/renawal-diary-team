# frozen_string_literal: true

class Types::CountrySliderType < Types::BaseObject
  field :current_country_slider_index, Integer, null: false
  field :countries_with_rank, [Types::CountryType], null: false
end
