# frozen_string_literal: true

class Types::RenewalDetailType < Types::BaseObject
  field :id, ID, null: false
  field :using_provider, Boolean, null: true
  field :category_id, ID, null: false
  field :category, Types::CategoryType, null: false
  field :last_buy, String, null: true
  field :buying_type, String, null: true
  field :checked_places, String, null: true
  field :current_provider_id, ID, null: true
  field :current_provider, Types::CompanyType, null: true
  field :buying_type_company_id, ID, null: true
  field :buying_type_company, Types::CompanyType, null: true
  field :current_provider_rate, Integer, null: true
  field :current_provider_rate_comment, String, null: true
  field :buying_type_company_rate, Integer, null: true
  field :buying_type_company_rate_comment, String, null: true
  field :year, Integer, null: false

  field :criteria_questions, String, null: true
  field :prices_questions, String, null: true
  field :provider_companies, [Types::CompanyType], null: true
  field :broker_companies, [Types::CompanyType], null: true
  field :comparsion_site_companies, [Types::CompanyType], null: true

  def criteria_questions
    object.criteria_questions.to_json
  end

  def prices_questions
    object.prices_questions.to_json
  end

  def provider_companies
    object.category.provider_companies
  end

  def broker_companies
    object.category.broker_companies
  end

  def comparsion_site_companies
    object.category.comparsion_site_companies
  end
end
