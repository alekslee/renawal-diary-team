# frozen_string_literal: true

class Types::CompaniesResponseType < Types::BaseObject
  field :companies, [Types::CompanyType], null: false
  field :total_count, Integer, null: true
  field :recommended_provider, Types::CompanyType, null: true
  field :recommended_source, Types::CompanyType, null: true
  field :average_price, Integer, null: true
  field :errors, [String], null: false
end
