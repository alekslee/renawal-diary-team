# frozen_string_literal: true

class Types::CitySelectOptionsType < Types::BaseObject
  field :id, ID, null: false
  field :value, String, null: false
  field :label, String, null: false
  field :zip_code, String, null: false
end
