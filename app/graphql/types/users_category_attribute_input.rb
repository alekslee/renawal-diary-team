# frozen_string_literal: true

class Types::UsersCategoryAttributeInput < Types::BaseInputObject
  description 'Attributes for creating and updating users categories'
  argument :id, ID, required: false
  argument :category_id, Integer, required: true
  argument :_destroy, Boolean, required: false
end
