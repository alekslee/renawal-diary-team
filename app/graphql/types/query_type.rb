# frozen_string_literal: true

class Types::QueryType < Types::BaseObject
  include Queries::UserQueries
  include Queries::CountryQueries
  include Queries::CategoryQueries
  include Queries::CompanyQueries
  include Queries::RenewalDetailQueries
  include Queries::SmartReminderQueries
  include Queries::VoteQueries
  include Queries::NoteQueries
end
