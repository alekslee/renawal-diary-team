# frozen_string_literal: true

class Types::SmartReminderType < Types::BaseObject
  field :id, ID, null: false
  field :category_id, ID, null: true
  field :reminder_type, String, null: false
  field :renewal_detail_id, ID, null: true
  field :category, Types::CategoryType, null: false
  field :name, String, null: false
  field :triggered_at, GraphQL::Types::ISO8601DateTime, null: false
  field :alias_name, String, null: true
  field :status, String, null: false
end
