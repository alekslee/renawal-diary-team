# frozen_string_literal: true

class Types::MutationType < Types::BaseObject

  #user
  field :sign_in_user, mutation: Mutations::SignInUser
  field :log_out_user, mutation: Mutations::LogOutUser
  field :update_user, mutation: Mutations::UpdateUser
  field :change_users_reseted_password, mutation: Mutations::ChangeUsersResetedPassword
  field :delete_user, mutation: Mutations::User::Delete

  field :create_note, mutation: Mutations::CreateNote
  field :invite_users, mutation: Mutations::InviteUsers
  field :category_subscribe, mutation: Mutations::CategorySubscribe
  field :become_a_partner_send_mail, mutation: Mutations::BecomeAPartnerSendMail
  field :user_reset_password, mutation: Mutations::UserResetPassword
  field :create_user, mutation: Mutations::CreateUser
  field :user_resend_confirmation_instruction, mutation: Mutations::UserResendConfirmationInstruction
  field :user_resend_unlock_instruction, mutation: Mutations::UserResendUnlockInstruction
  field :user_accept_invitation, mutation: Mutations::UserAcceptInvitation
  field :create_feedback, mutation: Mutations::CreateFeedback
  field :create_renewal_detail, mutation: Mutations::CreateRenewalDetail
  field :update_renewal_detail, mutation: Mutations::UpdateRenewalDetail
  field :uniqueness_validation, mutation: Mutations::UniquenessValidation
  field :create_smart_reminder, mutation: Mutations::CreateSmartReminder
  field :create_personalize_filter, mutation: Mutations::CreatePersonalizeFilter
  field :update_smart_reminder, mutation: Mutations::UpdateSmartReminder

  field :create_company, mutation: Mutations::Company::Create
end
