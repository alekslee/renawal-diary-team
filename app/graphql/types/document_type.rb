# frozen_string_literal: true

class Types::DocumentType < Types::BaseObject
  field :url, String, null: false
  field :content_type, String, null: false
  field :filename, String, null: false

  def url
    Rails.application.routes.url_helpers.rails_blob_path( object, only_path: true )
  end
end
