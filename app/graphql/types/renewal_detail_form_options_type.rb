# frozen_string_literal: true

class Types::RenewalDetailFormOptionsType < Types::BaseObject
  field :personal_criteria_filter_options, String, null: false
  field :criteria_questions_options, String, null: false
  field :prices_questions_options, String, null: false
  field :renewal_detail_possible_years, [Integer], null: false
  field :provider_companies, [Types::CompanyType], null: true
  field :broker_companies, [Types::CompanyType], null: true
  field :comparsion_site_companies, [Types::CompanyType], null: true

  def personal_criteria_filter_options
    inputs = { object.class.to_s.underscore => object }
    RenewalDetails::RetrievePersonalCriteriaFilterOptions.run!(inputs).to_json
  end

  def criteria_questions_options
    inputs = { object.class.to_s.underscore => object }
    RenewalDetails::RetrieveCriteriaQuestionsOptions.run!(inputs).to_json
  end

  def prices_questions_options
    inputs = { object.class.to_s.underscore => object }
    RenewalDetails::RetrievePricesQuestionsOptions.run!(inputs).to_json
  end

  def renewal_detail_possible_years
    category.renewal_details.pluck(:year).push(Date.today.year).uniq.compact
  end

  def provider_companies
    category.provider_companies.where(status: 'approved')
  end

  def broker_companies
    category.broker_companies.where(status: 'approved')
  end

  def comparsion_site_companies
    category.comparsion_site_companies.where(status: 'approved')
  end

  private

  def category
    @category ||= object.is_a?(Category) ? object : object.category
  end
end
