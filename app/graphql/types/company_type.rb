# frozen_string_literal: true

class Types::CompanyType < Types::BaseObject
  DEFAULT_AVATAR_PATH = '/images/default-company-avatar.svg'

  field :id, ID, null: false
  field :name, String, null: false
  field :url, String, null: true
  field :twitter, String, null: true
  field :company_type, String, null: false
  field :members, Float, null: true
  field :insights, Integer, null: true
  field :satisfaction, Float, null: true
  field :rank, Integer, null: true
  field :rating, Float, null: true
  field :direct, Boolean, null: true
  field :avatar_url, String, null: true

  def avatar_url
    # object.avatar.try(:service_url) || DEFAULT_AVATAR_PATH
    if object.avatar.attached?
      Rails.application.routes.url_helpers.rails_blob_path(object.avatar, only_path: true)
    else
      DEFAULT_AVATAR_PATH
    end
  end
end
