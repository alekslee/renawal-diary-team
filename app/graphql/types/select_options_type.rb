# frozen_string_literal: true

class Types::SelectOptionsType < Types::BaseObject
  field :label, String, null: true
  field :value, String, null: true
end
