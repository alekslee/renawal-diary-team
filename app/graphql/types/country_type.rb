# frozen_string_literal: true

class Types::CountryType < Types::BaseObject
  field :id, ID, null: false
  field :en_name, String, null: false
  field :iso_a2_code, String, null: false
  field :code, String, null: false
  field :default_lang_code, String, null: false
  field :users_count, Integer, null: true
  field :rank, Integer, null: true
  field :root_categories, [Types::CategoryType], null: false
  field :name, String, null: false
  field :currency, String, null: false

  def root_categories
    Loaders::AssociationLoader.for(Country, :root_categories).load(object)
  end
end
