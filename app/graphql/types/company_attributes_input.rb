# frozen_string_literal: true

class Types::CompanyAttributesInput < Types::BaseInputObject
  description 'Attributes for creating companies'
  argument :name, String, required: true
  argument :company_type, String, required: true
end
