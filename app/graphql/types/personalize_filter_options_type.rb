# frozen_string_literal: true

class Types::PersonalizeFilterOptionsType < Types::BaseObject
  field :personal_criteria_filter_options, String, null: false
  field :criteria_questions, String, null: false
  field :is_active, Boolean, null: false

  def personal_criteria_filter_options
    inputs = { object.class.to_s.underscore => object }
    RenewalDetails::RetrievePersonalCriteriaFilterOptions.run!(inputs).to_json
  end

  def criteria_questions
    (get_personalize_filter&.criteria_questions || {}).to_json
  end

  def is_active
    get_personalize_filter&.is_active.nil? ? true : get_personalize_filter.is_active
  end

  private

  def get_personalize_filter
    category.personalize_filters.find_by(user_id: context[:current_user]&.id)
  end

  def category
    @category ||= object.is_a?(Category) ? object : object.category
  end
end
