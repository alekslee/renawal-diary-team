# frozen_string_literal: true

class Types::FeedbackType < Types::BaseObject
  field :id, ID, null: false
  field :renewal_detail_id, ID, null: false
  field :renewal_detail, Types::RenewalDetailType, null: false
end
