# frozen_string_literal: true

class Types::NoteType < Types::BaseObject

  field :id, Integer, null: false
  field :title, String, null: true
  field :body, String, null: true
  field :created_at, GraphQL::Types::ISO8601DateTime, null: false
  field :files, [Types::DocumentType], null: true


end
