# frozen_string_literal: true

class Types::PriceAttributesInput < Types::BaseInputObject
  description 'Attributes for creating prices'
  argument :cents, Integer, required: true
  argument :currency, String, required: true
  argument :name, String, required: true
end
