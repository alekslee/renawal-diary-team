# frozen_string_literal: true

class Types::UserType < Types::BaseObject
  field :id, ID, null: false
  field :uid, String, null: false
  field :display_name, String, null: false
  field :bonus_points, Integer, null: false
  field :email, String, null: false
  field :avatar, Types::ImageType, null: false
  field :address, String, null: true
  field :lat, Float, null: true
  field :lng, Float, null: true
  field :country_code, String, null: true

  field :country, Types::CountryType, null: true
  field :root_categories, [Types::CategoryType], null: false
  field :country_root_categories, [Types::CategoryType], null: false

  def country
    Loaders::AssociationLoader.for(User, :country).load(object)
  end

  def root_categories
    Loaders::AssociationLoader.for(User, :root_categories).load(object)
  end

  def country_root_categories
    Loaders::AssociationLoader.for(Country, :root_categories).load(object.country)
  end
end
