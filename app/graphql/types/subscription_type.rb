# frozen_string_literal: true

class Types::SubscriptionType < GraphQL::Schema::Object
  include Subscriptions::CategorySubscriptions
  include Subscriptions::VotesSubscriptions
end
