# frozen_string_literal: true

class Types::RenewalDetailsAttributesInput < Types::BaseInputObject
  description 'Attributes for creating and updating renewal details'

  argument :id, ID, required: false
  argument :buying_type, String, required: false
  argument :last_buy, String, required: false
  argument :checked_places, String, required: false
  argument :current_provider_id, ID, required: false
  argument :buying_type_company_id, ID, required: false
  argument :current_provider_rate, Integer, required: false
  argument :current_provider_rate_comment, String, required: false
  argument :buying_type_company_rate, Integer, required: false
  argument :buying_type_company_rate_comment, String, required: false
  argument :criteria_questions, String, required: false
  argument :prices_questions, String, required: false
  argument :current_provider_attributes, Types::CompanyAttributesInput, required: false
  argument :buying_type_company_attributes, Types::CompanyAttributesInput, required: false
  argument :price_rate, Float, required: false
  argument :service_rate, Float, required: false
  argument :claim_rate, Float, required: false
  argument :claim_with_bussiness, Boolean, required: false

end
