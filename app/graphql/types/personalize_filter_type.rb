# frozen_string_literal: true

class Types::PersonalizeFilterType < Types::BaseObject
  field :id, ID, null: false
  field :criteria_questions, String, null: false
  field :is_active, Boolean, null: true

  def criteria_questions
    object.criteria_questions.to_json
  end

end
