# frozen_string_literal: true

class Types::VoteType < Types::BaseObject
  field :value, Integer, null: true

  def value
    object
  end
end
