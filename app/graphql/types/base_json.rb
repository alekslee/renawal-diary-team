# frozen_string_literal: true

class Types::BaseJson < GraphQL::Types::JSON

  def self.coerce_result(value, _context)
    value.reduce({}) {|m, (k,v)| m.merge(k.to_s.camelcase(:lower) => v) }
  end

end
