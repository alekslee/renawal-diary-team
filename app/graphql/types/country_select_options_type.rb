# frozen_string_literal: true

class Types::CountrySelectOptionsType < Types::BaseObject
  field :id, ID, null: false
  field :value, String, null: false
  field :label, String, null: false
end
