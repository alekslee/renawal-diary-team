# frozen_string_literal: true

class Mutations::Company::Create < Mutations::BaseAuthMutation
  field :company, Types::CompanyType, null: true
  field :errors, Types::BaseJson, null: false

  argument :name, String, required: true
  argument :url, String, required: true
  argument :category_id, String, required: true
  argument :country_code, String, required: true
  argument :company_modal_type, String, required: true

  def resolve(opts)
    category = Category.find(opts[:category_id])

    atts = opts.except(:category_id, :company_modal_type, :country_code)
    atts = atts.merge({
      twitter: '-',
      company_type: opts[:company_modal_type],
      category: category,
      with_validation: true,
    })

    company = Companies::Create.run(atts).result
    if company.valid?
      SystemMailer
        .with(company: company)
        .company_approve
        .deliver_later

      {
        company: company,
        errors: []
      }
    else
      {
        company: nil,
        errors: company.errors.messages
      }
    end
  end
end
