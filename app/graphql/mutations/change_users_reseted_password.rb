# frozen_string_literal: true

class Mutations::ChangeUsersResetedPassword < Mutations::BaseNoAuthMutation
  field :errors, [String], null: false
  field :user, Types::UserType, null: true

  argument :password, String, required: true
  argument :password_confirmation, String, required: true
  argument :reset_password_token, String, required: true

  def resolve(password:, password_confirmation:, reset_password_token:)
    interactor = Users::ChangeUsersResetedPassword.run(
      password: password,
      password_confirmation: password_confirmation,
      reset_password_token: reset_password_token,
      controller: context[:controller]
    )
    if interactor.valid?
      user = interactor.result
      {
        user: user,
        errors: []
      }
    else
      {
        user: nil,
        errors: interactor.errors.full_messages
      }
    end
  end
end
