# frozen_string_literal: true

class Mutations::CreateUser < Mutations::BaseNoAuthMutation
  field :user, Types::UserType, null: true
  field :errors, [String], null: false

  argument :email, String, required: true
  argument :password, String, required: true
  argument :display_name, String, required: true
  argument :lat, Float, required: true
  argument :lng, Float, required: true
  argument :users_categories_attributes, [Types::UsersCategoryAttributeInput], required: true

  argument :google_uid, String, required: false
  argument :facebook_uid, String, required: false
  argument :twitter_uid, String, required: false
  argument :linkedin_uid, String, required: false

  def resolve(
    email:, password:, display_name:, lat:, lng:, users_categories_attributes:,
    google_uid: nil, facebook_uid: nil, twitter_uid: nil, linkedin_uid: nil
  )
    interactor = Users::Create.run(
      email: email, password: password, display_name: display_name, lat: lat, lng: lng,
      google_uid: google_uid, facebook_uid: facebook_uid, twitter_uid: twitter_uid, linkedin_uid: linkedin_uid,
      users_categories_attributes: users_categories_attributes.map(&:to_h),
      controller: context[:controller]
    )

    if interactor.valid?
      {
        user: interactor.result,
        errors: []
      }
    else
      {
        user: nil,
        errors: interactor.errors.full_messages
      }
    end
  end
end
