# frozen_string_literal: true

class Mutations::CreateNote < Mutations::BaseAuthMutation
  field :note, Types::NoteType, null: true
  field :errors, [String], null: false

  argument :files, [Types::FileType], required: false
  argument :title, String, required: false
  argument :body, String, required: false
  argument :category_id, String, required: true

  def resolve(category_id:, title: nil, body: nil, files: nil)

    note = Note.new(title: title,
      user_id: context[:current_user].id,
      category_id: category_id,
      body: body,
    )

    if files
      note.files = files
    end

    if note.valid?
      note.save

      {
        note: note,
        errors: []
      }
    else
      {
        note: nil,
        errors: note.errors.full_messages
      }
    end
  end
end
