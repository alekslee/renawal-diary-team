# frozen_string_literal: true

class Mutations::CreateRenewalDetail < Mutations::BaseAuthMutation
  field :renewal_detail, Types::RenewalDetailType, null: true
  field :errors, [String], null: false

  argument :category_id, ID, required: true

  argument :using_provider, Boolean, required: true
  argument :buying_type, String, required: false
  argument :last_buy, String, required: false
  argument :checked_places, String, required: false
  argument :current_provider_id, ID, required: false
  argument :buying_type_company_id, ID, required: false
  argument :criteria_questions, String, required: false
  argument :prices_questions, String, required: false
  argument :current_provider_rate, Integer, required: false
  argument :current_provider_rate_comment, String, required: false
  argument :buying_type_company_rate, Integer, required: false
  argument :buying_type_company_rate_comment, String, required: false
  argument :current_provider_attributes, Types::CompanyAttributesInput, required: false
  argument :buying_type_company_attributes, Types::CompanyAttributesInput, required: false

  def resolve(
    category_id:, using_provider:, buying_type: nil, last_buy: nil, checked_places: nil, current_provider_id: nil,
    buying_type_company_id: nil, criteria_questions: nil, prices_questions: nil, current_provider_rate: nil,
    current_provider_rate_comment: nil, buying_type_company_rate: nil, buying_type_company_rate_comment: nil,
    current_provider_attributes: nil,
    buying_type_company_attributes: nil
  )
    interactor = RenewalDetails::Create.run(
      user: context[:current_user], category_id: category_id, using_provider: using_provider, last_buy: last_buy,
      buying_type: buying_type, checked_places: checked_places, current_provider_id: current_provider_id,
      buying_type_company_id: buying_type_company_id,
      criteria_questions: parse_criteria_questions(criteria_questions),
      prices_questions: prices_questions,
      current_provider_rate: current_provider_rate, current_provider_rate_comment: current_provider_rate_comment,
      buying_type_company_rate: buying_type_company_rate,
      buying_type_company_rate_comment: buying_type_company_rate_comment,
      current_provider_attributes: current_provider_attributes&.to_h,
      buying_type_company_attributes: buying_type_company_attributes&.to_h
    )

    if interactor.valid?
      {
        renewal_detail: interactor.result,
        errors: []
      }
    else
      {
        renewal_detail: nil,
        errors: interactor.errors.full_messages
      }
    end
  end

  private

  def parse_criteria_questions(criteria_questions)
    criteria_questions ? JSON.parse(criteria_questions, symbolize_names: true) : nil
  end
end
