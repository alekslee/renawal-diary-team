# frozen_string_literal: true

class Mutations::UniquenessValidation < Mutations::BaseMutation
  field :is_valid, Boolean, null: false

  argument :field_name, String, required: true
  argument :field_value, String, required: true

  def resolve(field_name:, field_value:)
    { is_valid: ::UniquenessValidation.run(field_name: field_name.underscore, field_value: field_value).result }
  end
end
