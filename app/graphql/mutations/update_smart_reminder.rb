# frozen_string_literal: true

class Mutations::UpdateSmartReminder < Mutations::BaseAuthMutation
  field :smart_reminder, Types::SmartReminderType, null: true
  field :errors, [String], null: false

  argument :id, ID, required: true
  argument :category_id, ID, required: false
  argument :reminder_type, String, required: true
  argument :triggered_at, String, required: true
  argument :alias_name, String, required: false

  def resolve(id:, category_id: nil, reminder_type: nil, triggered_at:, alias_name: nil)
    interactor = SmartReminders::Update.run(
      user: context[:current_user],
      id: id,
      category_id: category_id,
      reminder_type: reminder_type,
      triggered_at: triggered_at,
      alias_name: alias_name,
    )

    if interactor.valid?
      {
        smart_reminder: interactor.result,
        errors: []
      }
    else
      {
        smart_reminder: nil,
        errors: interactor.errors.full_messages
      }
    end
  end
end
