# frozen_string_literal: true

class Mutations::CreateSmartReminder < Mutations::BaseAuthMutation
  field :smart_reminder, Types::SmartReminderType, null: true
  field :errors, Types::BaseJson, null: false

  argument :category_id, ID, required: false
  argument :reminder_type, String, required: true
  argument :triggered_at, String, required: true
  argument :alias_name, String, required: false

  def resolve(category_id: nil, triggered_at:, alias_name: nil, reminder_type:)
    interactor = SmartReminders::Create.run(
      user: context[:current_user],
      category_id: category_id,
      reminder_type: reminder_type,
      triggered_at: triggered_at,
      alias_name: alias_name,
    )

    if interactor.valid?
      {
        smart_reminder: interactor.result,
        errors: {}
      }
    else
      {
        smart_reminder: nil,
        errors: interactor.errors.messages
      }
    end
  end
end
