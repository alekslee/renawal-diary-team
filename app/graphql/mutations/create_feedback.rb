# frozen_string_literal: true

class Mutations::CreateFeedback < Mutations::BaseAuthMutation
  field :feedback, Types::FeedbackType, null: true
  field :errors, [String], null: false

  argument :category_id, ID, required: true
  argument :year, Integer, required: true

  argument :create_smart_reminder, Boolean, required: false
  argument :triggered_at, String, required: false

  argument :renewal_detail_attributes, Types::RenewalDetailsAttributesInput, required: true
  argument :insights_comment, String, required: false

  def resolve(
    renewal_detail_attributes:,
    category_id:,
    year:,
    insights_comment: nil,
    triggered_at: nil,
    create_smart_reminder: nil
  )
    @category_id = category_id

    interactor = Feedbacks::Create.run(
      user: context[:current_user],
      category_id: category.id,
      year: year,
      renewal_detail_attributes: renewal_detail_attributes&.to_h,
      insights_comment: insights_comment
    )

    if create_smart_reminder
      category_feedback_count = context[:current_user].category_feedbacks(category.id).count
      reminder_interactor = SmartReminders::Create.run(
        user: context[:current_user],
        category_id: category.id,
        reminder_type: 'select',
        triggered_at: triggered_at,
        alias_name: "#{category.name} #{category_feedback_count}",
      )
    end

    if interactor.valid?
      {
        feedback: interactor.result,
        errors: []
      }
    else
      {
        feedback: nil,
        errors: interactor.errors.full_messages
      }
    end
  end

  def category
    @category ||= Category.find(@category_id)
  end

end
