class Mutations::UserAcceptInvitation < Mutations::BaseNoAuthMutation
  field :errors, [String], null: false
  field :user, Types::UserType, null: false

  argument :invitation_token, String, required: true
  argument :display_name, String, required: true
  argument :password, String, required: true
  argument :lat, Float, required: true
  argument :lng, Float, required: true

  def resolve(invitation_token:, display_name:, password:, lat:, lng:)
    interactor = Users::AcceptInvitation.run(
      invitation_token: invitation_token,
      display_name: display_name,
      password: password,
      lat: lat,
      lng: lng,
      controller: context[:controller]
    )
    if interactor.valid?
      {
        user: interactor.result.reload,
        errors: []
      }
    else
      {
        user: nil,
        errors: interactor.errors.full_messages
      }
    end
  end
end
