# frozen_string_literal: true

class Mutations::UpdateUser < Mutations::BaseAuthMutation
  field :user, Types::UserType, null: true
  field :errors, [String], null: false

  argument :display_name, String, required: false
  argument :avatar, Types::FileType, required: false
  argument :users_categories_attributes, [Types::UsersCategoryAttributeInput], required: false

  def resolve(display_name: nil, avatar: nil, users_categories_attributes: nil)
    interactor = Users::Update.run(
      user: context[:current_user],
      display_name: display_name,
      avatar: avatar,
      users_categories_attributes: users_categories_attributes&.map(&:to_h)
    )

    if interactor.valid?
      {
        # should be reload because of bug in the association loader after update categories
        user: interactor.result.reload,
        errors: []
      }
    else
      {
        user: nil,
        errors: interactor.errors.full_messages
      }
    end
  end
end
