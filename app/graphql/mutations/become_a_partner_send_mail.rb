# frozen_string_literal: true

class Mutations::BecomeAPartnerSendMail < Mutations::BaseMutation
  field :status, String, null: false

  argument :name, String, required: false
  argument :email, String, required: false
  argument :body, String, required: false

  def resolve(name: nil, email: nil, body: nil)
    PartnerMailer.with(name: name, email: email, body: body).become_a_partner.deliver_later
    { status: :success }
  end
end
