# frozen_string_literal: true

class Mutations::CreatePersonalizeFilter < Mutations::BaseAuthMutation
  field :personalize_filter, Types::PersonalizeFilterType, null: true
  field :errors, [String], null: false

  argument :category_id, ID, required: true
  argument :criteria_questions, String, required: false
  argument :is_active, Boolean, required: false

  def resolve(category_id:, criteria_questions: nil, is_active: nil)
    interactor = PersonalizeFilters::Create.run(
      user: context[:current_user], category_id: category_id,
      criteria_questions: parse_criteria(criteria_questions),
      is_active: is_active
    )

    if interactor.valid?
      {
        personalize_filter: interactor.result,
        errors: []
      }
    else
      {
        personalize_filter: nil,
        errors: interactor.errors.full_messages
      }
    end
  end

  private

  def parse_criteria(criteria_questions)
    criteria_questions ? JSON.parse(criteria_questions, symbolize_names: true) : nil
  end

end
