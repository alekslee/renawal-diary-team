# frozen_string_literal: true

class Mutations::SignInUser < Mutations::BaseNoAuthMutation
  field :user, Types::UserType, null: true
  field :token, String, null: true
  field :client_id, String, null: true
  field :errors, [String], null: false

  argument :email, String, required: false
  argument :password, String, required: false
  argument :type, String, required: false

  def resolve(email: nil, password: nil, type: 'api')
    interactor = Users::SignIn.run(email: email, password: password, controller: context[:controller], type: type)

    if interactor.valid?
      user, client_id, token = interactor.result
      {
        user: user,
        token: token,
        client_id: client_id,
        errors: []
      }
    else
      {
        user: nil,
        token: nil,
        client_id: nil,
        errors: interactor.errors.full_messages
      }
    end
  end
end
