# frozen_string_literal: true

class Mutations::InviteUsers < Mutations::BaseAuthMutation
  field :status, String, null: false

  argument :message, String, required: true
  argument :emails, [String], required: true

  def resolve(message:, emails:)
    BatchInviteWorker.perform_async(emails, message, context[:current_user].id)
    { status: :success }
  end
end
