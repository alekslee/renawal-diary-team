# frozen_string_literal: true

class Mutations::DestroySmartReminders < Mutations::BaseAuthMutation
  field :errors, [String], null: false

  argument :ids, [String], required: true

  def resolve(ids:)
    interactor = SmartReminders::DestroyMulti.run(user: context[:current_user], ids: ids)

    if interactor.valid?
      { errors: [] }
    else
      { errors: interactor.errors.full_messages }
    end
  end
end
