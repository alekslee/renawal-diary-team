# frozen_string_literal: true

class Mutations::LogOutUser < Mutations::BaseAuthMutation
  field :errors, [String], null: false

  argument :client_id, String, required: false

  def resolve(client_id: nil)
    interactor = Users::LogOut.run(
      controller: context[:controller],
      user: context[:current_user],
      client_id: client_id
    )

    if interactor.valid?
      interactor.result
      {
        errors: []
      }
    else
      {
        errors: interactor.errors.full_messages
      }
    end
  end
end
