class Mutations::UserResendConfirmationInstruction < Mutations::BaseMutation
  field :errors, [String], null: false
  field :email, String, null: false

  argument :email, String, required: true

  def resolve(email:)
    interactor = Users::ResendConfirmationInstruction.run(email: email)
    if interactor.valid?
      {
        email: email,
        errors: []
      }
    else
      {
        email: email,
        errors: interactor.errors.full_messages
      }
    end
  end
end
