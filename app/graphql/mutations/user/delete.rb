# frozen_string_literal: true

class Mutations::User::Delete < Mutations::BaseAuthMutation
  field :errors, [String], null: false

  def resolve
    user = context[:current_user]
    User.transaction do
      user.users_categories.delete_all!
      user.users_countries.delete_all!
      user.feedbacks.delete_all!
      user.notes.delete_all!
      user.personalize_filters.delete_all!
      user.renewal_details.delete_all!
      user.smart_reminders.delete_all!
      user.destroy && user.destroy
    end

    {
      errors: []
    }
  rescue => e
    {
      errors: e.message
    }
  end
end
