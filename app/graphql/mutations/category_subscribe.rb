# frozen_string_literal: true

class Mutations::CategorySubscribe < Mutations::BaseAuthMutation
  field :category, Types::CategoryType, null: true
  field :errors, [String], null: false

  argument :category_id, String, required: true
  argument :selected, String, required: true

  def resolve(category_id:, selected:)
    interactor = Categories::ToggleSubscribe.run(
      user: context[:current_user],
      category_id: category_id,
      selected: selected == 'true',
    )
    @category_id = category_id

    if interactor.valid?
      {
        # should be reload because of bug in the association loader after update categories
        category: category.reload,
        errors: []
      }
    else
      {
        category: nil,
        errors: interactor.errors.full_messages
      }
    end
  end

  def category
    @category ||= Category.find_by id: @category_id
  end
end
