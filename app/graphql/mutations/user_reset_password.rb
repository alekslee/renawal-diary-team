# frozen_string_literal: true

class Mutations::UserResetPassword < Mutations::BaseNoAuthMutation
  field :errors, [String], null: false

  argument :email, String, required: true

  def resolve(email:)
    interactor = Users::ResetPassword.run(email: email)
    if interactor.valid?
      {
        errors: []
      }
    else
      {
        errors: interactor.errors.full_messages
      }
    end
  end
end
