# frozen_string_literal: true

class Loaders::LeafCategoriesByCodeLoader < GraphQL::Batch::Loader
  def perform(codes)
    codes.each { |code| fulfill(code, Category.where(code: code, children_count: 0)) }
  end
end
