# frozen_string_literal: true

class Loaders::CategoriesDescendantsLoader < GraphQL::Batch::Loader
  def initialize(where: nil)
    @where = where
  end

  def cache_key(category)
    category.object_id
  end

  def perform(categories)
    categories.each { |category| fulfill(category, load_descendants(category)) }
  end

  private

  def load_descendants(category)
    descendants = category.descendants
    descendants = descendants.where(@where) if @where
    descendants
  end
end
