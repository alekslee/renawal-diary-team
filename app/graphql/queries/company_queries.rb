# frozen_string_literal: true

module Queries::CompanyQueries
  extend ActiveSupport::Concern

  included do
    field :category_companies, Types::CompaniesResponseType, null: false do
      description 'Retrieve companies for category'
      argument :category_id, String, required: true
      argument :company_type, String, required: false
      argument :last_buy, String, required: false
      argument :checked_places, String, required: false
      argument :criteria_questions, String, required: false
      argument :key_word, String, required: false
      argument :zip_code, String, required: false
      argument :city, String, required: false
      argument :address, String, required: false
      argument :autocompleate, String, required: false
      argument :country_code, String, required: false
      argument :page, Integer, required: false
      argument :per_page, Integer, required: false
      argument :order_column, String, required: false
      argument :order_direction, String, required: false
    end
  end

  def category_companies(
    category_id:,
    company_type: nil,
    last_buy: nil,
    checked_places: nil,
    country_code: nil,
    criteria_questions: nil,
    key_word: nil,
    zip_code: nil,
    city: nil,
    address: nil,
    autocompleate: nil,
    page: nil,
    per_page: nil,
    order_column: nil,
    order_direction: nil
  )
    interaction = Companies::List.run(
      user: context[:current_user],
      current_country: context[:current_country],
      category_id: category_id,
      company_type: company_type,
      last_buy: last_buy,
      checked_places: checked_places,
      criteria_questions: parse_criteria(criteria_questions),
      key_word: key_word,
      zip_code: zip_code,
      city: city,
      address: address,
      autocompleate: autocompleate,
      country_code: country_code,
      page: page,
      per_page: per_page,
      order_column: order_column,
      order_direction: order_direction
    )

    if interaction.valid?
      companies, total_count, recommended_source, recommended_provider, average_price = interaction.result
      {
        companies: companies,
        total_count: total_count,
        recommended_source: recommended_source,
        recommended_provider: recommended_provider,
        average_price: average_price,
        errors: []
      }
    else
      {
        companies: [],
        total_count: nil,
        recommended_source: nil,
        recommended_provider: nil,
        average_price: nil,
        errors: interaction.errors.full_messages
      }
    end
  end

  private

  def parse_criteria(criteria_questions)
    criteria_questions ? JSON.parse(criteria_questions, symbolize_names: true) : nil
  end

end
