# frozen_string_literal: true

module Queries::VoteQueries
  extend ActiveSupport::Concern

  included do
    field :votes_count, Types::VoteType, null: true do
      description 'Retrieve today\'s votes'
      argument :period, String, required: false
      argument :root_category_id, Integer, required: false
    end

    def votes_count(period: nil, root_category_id: nil)
      Feedbacks::VotesCount.run!(
        period: period&.to_sym,
        root_category_id: root_category_id,
      )
    end
  end
end
