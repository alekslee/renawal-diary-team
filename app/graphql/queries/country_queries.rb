# frozen_string_literal: true

module Queries::CountryQueries
  extend ActiveSupport::Concern

  included do
    field :countries_for_slider, Types::CountrySliderType, null: false do
      description 'Retrieve all countries with countries rank'
    end

    field :countries_select_options, [Types::CountrySelectOptionsType], null: false do
      description 'Retrieve all countries for select'
    end

    field :cities_select_options, [Types::CitySelectOptionsType], null: false do
      argument :country_code, String, required: true
      argument :key_word, String, required: false
      description 'Sities for country'
    end

    field :regions_select_options, [Types::CountrySelectOptionsType], null: false do
      argument :country_code, String, required: true
      argument :key_word, String, required: false
      description 'Sities for country'
    end

    field :personalize_filters, Types::PersonalizeFilterOptionsType, null: false do
      argument :category_id, String, required: true
      description 'Retrieve filter'
    end

  end

  def countries_for_slider
    countries = Countries::WithRank.run!
    index = countries.pluck(:id).index(context[:current_country].id)
    Struct.new(:countries_with_rank, :current_country_slider_index).new(countries, index)
  end

  def countries_select_options
    select_option_class = Struct.new(:value, :label, :id)
    Country.all.map { |country| select_option_class.new(country.iso_a2_code, country.name, country.id) }
  end

  def regions_select_options(country_code:, key_word: nil)
    select_option_class = Struct.new(:value, :label, :id)
    regions = Countries::LoadRegions.run!(country_code: country_code)

    regions.each_with_index.map { |(_, name), index| select_option_class.new(name, name, index) }
  end

  def cities_select_options(country_code:, key_word: nil)
    select_option_class = Struct.new(:value, :label, :zip_code, :id)

    cities = Countries::LoadCities.run!(country_code: country_code)
    cities.each_with_index.map { |name, index| select_option_class.new(name, name, 'TODO: zip_code', index) }
  end

  def personalize_filters(category_id:)
    Category.find_by_id(category_id)
  end

end
