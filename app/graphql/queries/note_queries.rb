# frozen_string_literal: true

module Queries::NoteQueries
  extend ActiveSupport::Concern

  included do
    field :note_list, [Types::NoteType], null: false do
      description 'Retrieve all notes'
      argument :category_id, String, required: true
      argument :year, String, required: false
    end
  end

  def note_list(category_id:, year: nil)
    return [] unless context[:current_user]

    q = context[:current_user].notes

    q = q.where(category_id: category_id)

    if year
      date = Date.parse("#{year}-01-01")
      q = q.where(created_at: date..date.end_of_year)
    end

    q = q.includes(files_attachments: :blob)
         .order(created_at: :desc)
  end

end
