# frozen_string_literal: true

module Queries::CategoryQueries
  extend ActiveSupport::Concern

  included do
    field :country_categories, [Types::CategoryType], null: false do
      description 'Retrieve categories for current country'
      argument :country_code, String, required: true
    end

    field :user_country_categories, [Types::CategoryType], null: false do
      description 'Retrieve categories for current user country'
    end

    field :current_user_categories, [Types::CategoryType], null: false do
      description 'Retrieve categories for current user'

      def visible?(context)
        super && context[:current_user].present?
      end
    end

    field :category, Types::CategoryType, null: true do
      description 'Retrieve category'
      argument :id, String, required: true
    end

    field :category_by_country, Types::CategoryType, null: true do
      description 'Retrieve category by country'
      argument :category_code, String, required: true
      argument :category_depth, Integer, required: true
      argument :country_code, String, required: true

      def visible?(context)
        super && context[:current_user].present?
      end
    end
  end

  def country_categories(country_code:)
    Categories::FetchCategories.run(country_code: country_code).result
  end

  def user_country_categories
    Categories::FetchCategories.run(country_code: retrieve_country.code).result
  end

  def current_user_categories
    Loaders::AssociationLoader.for(User, :root_categories).load(context[:current_user])
  end

  def category(id:)
    retrieve_country.categories.find_by_id(id)
  end

  def category_by_country(country_code:, category_code:, category_depth:)
    Country.find_by_iso_a2_code(country_code.downcase)&.categories&.find_by(code: category_code, depth: category_depth)
  end

  private

  def retrieve_country
    context[:current_user] ? context[:current_user].country : context[:current_country]
  end
end
