# frozen_string_literal: true

module Queries::UserQueries
  extend ActiveSupport::Concern

  included do
    field :all_users, [Types::UserType], null: false do
      description 'Retrieve all users'
    end
    field :my_country_users, [Types::UserType], null: false do
      description 'Retrieve all users of my Country'
    end
  end

  def all_users
    Loaders::Record.for(User).load_many(User.approved.pluck(:id))
  end

  def my_country_users
    ids = User.all.joins(:countries).where(countries: { id: context[:current_user].country_ids }).pluck(:id).uniq
    Loaders::Record.for(User).load_many(ids)
  end
end
