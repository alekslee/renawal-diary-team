# frozen_string_literal: true

module Queries::RenewalDetailQueries
  extend ActiveSupport::Concern

  included do
    field :renewal_detail, Types::RenewalDetailType, null: true do
      argument :category_id, String, required: true
      argument :year, Integer, required: false
    end

    field :renewal_detail_form_options, Types::RenewalDetailFormOptionsType, null: true do
      argument :category_id, String, required: true
      argument :year, Integer, required: false
    end

    field :car_model_options, [Types::SelectOptionsType], null: false do
      argument :make, String, required: true
    end

    field :car_trim_options, [Types::SelectOptionsType], null: false do
      argument :make, String, required: true
      argument :model, String, required: true
    end

    field :pet_breed_options, [Types::SelectOptionsType], null: false do
      argument :pet_type, String, required: true
    end

    field :motor_bike_engine_size_options, [Types::SelectOptionsType], null: false do
      argument :make, String, required: true
      argument :model, String, required: true
    end

    field :mobile_model_options, [Types::SelectOptionsType], null: false do
      argument :make, String, required: true
    end
  end

  def renewal_detail(category_id:, year: nil)
    return if context[:current_user].nil?

    context[:current_user].renewal_details.where(category_id: category_id, year: year || Date.today.year).order(:updated_at).last
  end

  def renewal_detail_form_options(category_id:, year: nil)
    renewal_detail(category_id: category_id, year: year) || Category.find_by_id(category_id)
  end

  def car_model_options(make:)
    RenewalDetails::Criterias::RetrieveCarModelOptions.run(value: make).result
  end

  def car_trim_options(make:, model:)
    RenewalDetails::Criterias::RetrieveCarTrimOptions.run(make: make, value: model).result
  end

  def motor_bike_engine_size_options(make:, model:)
    RenewalDetails::Criterias::RetrieveMotorBikeEngineSizeOptions.run(make: make, model: model).result
  end

  def mobile_model_options(make:)
    Mobiles::Criterias::RetrieveModelOptions.run(make: make).result
  end

  def pet_breed_options(pet_type:)
    RenewalDetails::Criterias::RetrievePetBreedOptions.run(value: pet_type).result
  end
end
