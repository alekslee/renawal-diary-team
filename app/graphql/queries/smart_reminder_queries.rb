# frozen_string_literal: true

module Queries::SmartReminderQueries
  extend ActiveSupport::Concern

  included do
    field :smart_reminders, [Types::SmartReminderType], null: false do
      argument :category_id, String, required: true
    end
  end

  def smart_reminders(category_id:)
    return [] if context[:current_user].nil?

    q = context[:current_user].smart_reminders

    q = q.where(category_id: category_id)
         .or(q.where(reminder_type: 'custom'))

    q = q.includes(:category).order(triggered_at: :asc)
  end
end
