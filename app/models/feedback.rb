# frozen_string_literal: true

class Feedback < ApplicationRecord

  belongs_to :user
  belongs_to :renewal_detail

  accepts_nested_attributes_for :renewal_detail, update_only: true

end
