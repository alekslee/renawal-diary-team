# frozen_string_literal: true

class UsersCountry < ApplicationRecord
  acts_as_paranoid

  belongs_to :user
  belongs_to :country, counter_cache: true

  after_commit :reset_country_counters

  validates_uniqueness_of :user_id, scope: :country_id

  private

  def reset_country_counters
    Country.reset_counters(country_id, :users_countries)
  end
end
