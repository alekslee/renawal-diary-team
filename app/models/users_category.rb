# frozen_string_literal: true

class UsersCategory < ApplicationRecord
  belongs_to :user
  belongs_to :category, counter_cache: true

  validates_uniqueness_of :category_id, scope: :user_id

end
