# frozen_string_literal: true

class Country < ApplicationRecord
  has_many :users_countries, dependent: :destroy
  has_many :users, through: :users_countries
  accepts_nested_attributes_for :users_countries
  has_many :leaf_categories, -> { leaf }, class_name: 'Category'
  has_many :root_categories, -> { roots }, class_name: 'Category'

  has_many :categories, dependent: :destroy
  has_many :companies, through: :categories

  has_many :user_statistics, as: :statisticable


  validates :en_name, :iso_a2_code, :default_lang_code, :available_lang_codes, presence: true
  validates :en_name, uniqueness: true
  validates :iso_a2_code, uniqueness: true

  alias_attribute :users_count, :users_countries_count
  alias_attribute :code, :iso_a2_code

  attr_accessor :rank

  def self.setup_from_yml!
    Countries::SetupFromYML.run!
  end

  def name
    I18n.t("countries_names.#{iso_a2_code}")
  end

  def self.all_with_rank(prepended_country: nil)
    Countries::WithRank.run!(prepended_country: prepended_country)
  end

  def self.default
    find_by_iso_a2_code(FALLBACK_COUNTRY)
  end

  def currency
    IsoCountryCodes.find(iso_a2_code).currency
  end
end
