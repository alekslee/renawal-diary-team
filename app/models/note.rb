class Note < ApplicationRecord

  belongs_to :user
  belongs_to :category
  has_many_attached :files

  validate :files_or_title

  private

  def files_or_title
    if !files.attached? && title.blank?
      errors.add(:title, 'or attachments cant be blank')
    end
  end

end
