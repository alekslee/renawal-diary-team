# frozen_string_literal: true

class UserStatistic < ApplicationRecord
  acts_as_paranoid

  belongs_to :statisticable, polymorphic: true
end
