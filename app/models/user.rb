# frozen_string_literal: true

class User < ApplicationRecord
  include DeviseTokenAuth::Concerns::User

  PROVIDERS_ATTRIBUTES = {
    'google_oauth2' => Users::Omniauth::AttributesFromGoogle,
    'twitter' => Users::Omniauth::AttributesFromTwitter,
    'linkedin' => Users::Omniauth::AttributesFromLinkedin,
    'facebook' => Users::Omniauth::AttributesFromFacebook
  }.freeze

  has_attached_file :avatar,
    styles: { thumb: '100x100>', medium: '300x300>' },
    default_url: '/images/user-default-avatar.svg'
  validates_attachment_content_type :avatar, content_type: %r{\Aimage/.*\z}

  scope :approved, -> { where.not(id: User.invitation_not_accepted.pluck(:id)) }

  acts_as_paranoid

  # Include default devise modules. Others available are:
  # :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :invitable,
    :confirmable, :lockable, :trackable, :zxcvbnable, :omniauthable, :async,
    omniauth_providers: %i[facebook twitter google_oauth2 linkedin]

  has_many :users_countries, dependent: :destroy
  has_many :countries, through: :users_countries
  has_one :users_country
  has_one :country, through: :users_country
  has_many :country_categories, through: :country, source: :categories
  has_many :country_leaf_categories, through: :country, source: :leaf_categories
  has_many :country_root_categories, through: :country, source: :root_categories
  has_many :personalize_filters, dependent: :destroy
  has_many :renewal_details, dependent: :destroy
  has_many :smart_reminders, dependent: :destroy
  accepts_nested_attributes_for :users_countries

  has_many :users_categories, dependent: :destroy
  has_many :feedbacks, dependent: :destroy
  has_many :categories, through: :users_categories
  has_many :leaf_categories, -> { leaf }, through: :users_categories, source: :category
  has_many :root_categories, -> { roots }, through: :users_categories, source: :category

  accepts_nested_attributes_for :users_categories, allow_destroy: true

  has_many :invitees, class_name: 'User', foreign_key: :invited_by_id
  belongs_to :inviter, class_name: 'User', foreign_key: :invited_by_id, optional: true

  validates :display_name, uniqueness: true
  validates_uniqueness_of :email

  has_many :notes, dependent: :destroy

  reverse_geocoded_by :lat, :lng do |obj, results|
    geo = results.first
    if geo
      obj.city = geo.city
      obj.address = geo.address
      obj.country_code = geo.country_code
      obj.state_district = geo.state_district || geo.county
      obj.state = geo.state || geo.province
      obj.state_code = geo.state_code || geo.province_code

      country = Country.find_by_iso_a2_code(obj.country_code) || Country.default
      if obj.new_record? || obj.country.nil?
        obj.users_countries.build(country_id: country.id)
        I18n.locale = country.default_lang_code
      end
    end
  end
  after_validation :reverse_geocode
  validates_presence_of :lat, :lng

  def email_required?
    false
  end

  def avatar_url_options
    {
      avatar: {
        original_url: avatar.url,
        thumb_url: avatar.url(:thumb),
        medium_url: avatar.url(:medium)
      }
    }
  end

  def default_criteria_questions(category_id)
    pf = personalize_filters.find_by category_id: category_id
    pf&.criteria_questions&.deep_symbolize_keys || {}
  end

  def category_feedbacks(category_id)
    feedbacks.joins(:renewal_detail)
             .where(renewal_details: { category_id: category_id })
  end

  private

  def confirmation_required?
    false
  end
end
