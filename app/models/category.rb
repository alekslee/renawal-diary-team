# frozen_string_literal: true

class Category < ApplicationRecord
  HIDDEN_CATEGORIES_CODES = %i[gyms magazines games].freeze

  default_scope { without_hidden }

  acts_as_nested_set counter_cache: :children_count
  accepts_nested_attributes_for :children

  has_many :users_categories, dependent: :destroy
  has_many :users, through: :users_categories
  has_many :companies, dependent: :destroy
  has_many :renewal_details, dependent: :destroy
  has_many :personalize_filters
  has_many :feedbacks, through: :renewal_details
  has_many :smart_reminders, dependent: :destroy

  scope :leaf, -> { where(children_count: 0) }
  scope :without_hidden, -> { where.not(code: HIDDEN_CATEGORIES_CODES) }

  accepts_nested_attributes_for :users_categories

  belongs_to :country
  validates_presence_of :en_name
  validates_uniqueness_of :en_name, scope: %i[country_id parent_id]
  validates_uniqueness_of :code, scope: %i[country_id parent_id]

  attr_accessor :companies_changed

  def companies_changed?
    @companies_changed
  end

  def users_count
    users_categories_count
  end

  def name
    I18n.t("g_categories_names.#{en_name}") || pretty_en_name
  end

  def pretty_en_name
    en_name.humanize
  end

  def slag
    "#{self.class.slag_prefix}#{code}".dasherize
  end

  def self.slag_prefix
    'best_'
  end

  def self_or_depends_categories
    if (root || self).code == 'subscriptions'
      self
    else
      self_and_ancestors
    end
  end

  def provider_companies
    # (root? ? companies : root.companies).with_type(:provider)
    (root? ? companies : Company.where(category: self_or_depends_categories)).with_type(:provider)
  end

  def broker_companies
    # (root? ? companies : root.companies).with_type(:broker)
    (root? ? companies : Company.where(category: self_or_depends_categories)).with_type(:broker)
  end

  def comparsion_site_companies
    # (root? ? companies : root.companies).with_type(:comparsion_site)
    (root? ? companies : Company.where(category: self_or_depends_categories)).with_type(:comparsion_site)
  end

  def self.cache_exclude_ids

    Category.roots
            .group_by(&:country_id)
            .each do |country_id, categories|
      exlucuded_ids = []

      categories.each do |root_category|
        leaf_categories = root_category.descendants.where(children_count: 0)

        categories_with_companies = if root_category.code == 'subscriptions'
          leaf_categories.joins(:companies).uniq
        else
          leaf_categories.joins("INNER JOIN companies ON (companies.category_id = categories.id OR companies.category_id = '#{root_category.id}')").uniq
        end
        categories_with_companies = categories_with_companies.blank? ? [leaf_categories.first] : categories_with_companies

        exlucuded_ids = exlucuded_ids.concat( leaf_categories.map(&:id) - categories_with_companies.map(&:id) )
      end

      $redis.hset('cache-exclude-category-ids', country_id, exlucuded_ids.uniq.to_json)
    end
  end
end
