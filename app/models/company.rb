class Company < ApplicationRecord
  extend Enumerize

  has_one_attached :avatar

  XLSX_HEADERS = { name: 'name', url: 'url', twitter: 'twitter', code: 'code', alias: 'alias' }.freeze

  attr_accessor :members, :insights, :satisfaction, :rank, :rating, :direct, :with_validation

  belongs_to :category
  has_one :country, through: :category

  has_many :current_renewal_details,
    foreign_key: :current_provider_id, class_name: 'RenewalDetail', dependent: :destroy
  has_many :buying_type_renewal_details,
    foreign_key: :buying_type_company_id, class_name: 'RenewalDetail', dependent: :destroy

  enumerize :company_type, in: %i[provider broker comparison_site comparsion_site other], scope: :with_type
  enumerize :status, in: %i[pending approved], scope: :with_status, default: :pending

  validates_presence_of :company_type, :name, :code
  validates_uniqueness_of :name, :url, scope: :category_id, if: -> { with_validation }

  before_validation :generate_code, on: :create

  def url=(url)
    return url if url == '-'

    uri = URI.parse(url.to_s.delete(' '))
    if uri.scheme.nil? && uri.host.nil? && uri.path.present?
      uri.scheme = 'http'
      uri.host = uri.path[%r{[^\/]+(?=\/)?}]
      uri.path = ''
    end
    super(uri.to_s)
  end

  def self.generate_code(name)
    new(name: name).send(:generate_code)
  end

  def id_token
    JWT.encode({ company_id: id }, 'jwt_super_psw', 'none', { typ: 'JWT' })
  end

  def self.from_id_token(t)
    data = JWT.decode(t, 'jwt_super_psw', false).first || {}
    self.find_by(id: data['company_id'])
  end

  private

  def generate_code
    return if name.nil?

    self.code = name.strip.downcase.gsub(/\s+/, '_')
  end
end
