class SmartReminder < ApplicationRecord
  extend Enumerize

  REMINDERS_INTERVAL = 10.days
  APPROXIMATE_CORRELATION = 10.seconds
  STATUSES_INTERVALS = {
    green: ->(date) { date > (2 * REMINDERS_INTERVAL).from_now },
    amber: ->(date) { date.in?(REMINDERS_INTERVAL.from_now..(2 * REMINDERS_INTERVAL).from_now) },
    red: ->(date) { date.in?(Time.now..REMINDERS_INTERVAL.from_now) }
  }.freeze
  WORKER_DATE_TRIGGERED = {
    green: ->(date) { date - (2 * REMINDERS_INTERVAL) + APPROXIMATE_CORRELATION },
    amber: ->(date) { date - REMINDERS_INTERVAL + APPROXIMATE_CORRELATION },
    red: ->(date) { date + APPROXIMATE_CORRELATION }
  }.freeze

  acts_as_paranoid

  belongs_to :category, required: false
  belongs_to :user
  belongs_to :renewal_detail, required: false

  enumerize :status, in: %i[completed red amber green]
  enumerize :reminder_type, in: %i[select custom]

  validates_presence_of :triggered_at, :status
  validate :triggered_in_the_future, if: :triggered_at_changed?
  validate :renewal_detail_from_category
  validate :should_valid_reminder_type

  before_validation :set_status
  before_validation :set_year
  before_update :set_delayed_job, if: :triggered_at_changed?
  after_commit :start_reminder, on: :create
  after_commit :send_notification_mail, on: :create

  def name
    alias_name.presence || category&.name || ''
  end

  def triggered_at=(value)
    super(value.beginning_of_day + Time.now.hour.hours)
  end

  def set_status
    self.status = STATUSES_INTERVALS.keys.find do |key|
      STATUSES_INTERVALS[key].call(triggered_at)
    end || :completed
  end

  def worker
    return if notification_job_id.nil?

    @worker ||= FindScheduledWorkerByJid.run!(jid: notification_job_id)
  end

  private

  def set_year
    self.year = triggered_at.year
  end

  def triggered_in_the_future
    return if triggered_at > Time.now

    errors.add(:triggered_at, 'should be in the future')
  end

  def should_valid_reminder_type

    if reminder_type == 'custom'
      errors.add(:alias_name, 'Enter your custom name') if alias_name.blank?
    else
      errors.add(:category_id, 'should not be empty') if category_id.blank?
    end
  end

  def renewal_detail_from_category
    return if renewal_detail.nil? || renewal_detail.category_id == category_id

    errors.add(:renewal_detail, 'should be from correct category')
  end

  def set_delayed_job
    worker&.delete
    self.notification_job_id = SmartReminders::RunNotificationWorker.run!(smart_reminder: self)
  end

  def start_reminder
    self.notification_job_id = SmartReminders::RunNotificationWorker.run!(smart_reminder: self)
    save
  end

  def send_notification_mail
    SmartReminderNotificationMailer.with(smart_reminder_id: id).set_up_smart_reminder.deliver_later
  end
end
