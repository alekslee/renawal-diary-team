class RenewalDetail < ApplicationRecord
  extend Enumerize

  LAST_BUY_DATE_VALUES = {
    today: -> { Date.today },
    this_week: -> { Date.today.beginning_of_week },
    this_month: -> { Date.today.beginning_of_month },
    this_quarter: -> { Date.today.beginning_of_quarter },
    this_year: -> { Date.today.beginning_of_year },
    year_or_more: -> { 1.year.ago }
  }.freeze

  serialize :criteria_questions, CriteriaQuestionsSerializer
  serialize :prices_questions, CriteriaQuestionsSerializer

  has_paper_trail only: %i[current_provider_id]

  belongs_to :user
  belongs_to :category
  belongs_to :current_provider, class_name: 'Company', required: false
  belongs_to :buying_type_company, class_name: 'Company', required: false
  has_one :feedback, dependent: :destroy
  has_one :smart_reminder, dependent: :destroy

  enumerize :checked_places, in: %i[one_place two_places three_places_or_more]
  enumerize :buying_type, in: Company.company_type.values, predicates: { prefix: true }

  validate :buying_type_company_type
  validate :category_doesnt_have_children

  validates_presence_of :year
  validates_presence_of :buying_type_company, if: :should_buying_type_company_present?
  validates_presence_of :checked_places, :buying_type, :current_provider, :last_buy_date,
    if: :using_provider?

  accepts_nested_attributes_for :current_provider
  accepts_nested_attributes_for :buying_type_company

  before_validation :set_year

  def buying_type_company_id=(val)
    super(val&.zero? ? nil : val)
  end

  def last_buy
    return nil if last_buy_date.nil?

    LAST_BUY_DATE_VALUES.find do |_key, value|
      last_buy_date >= value.call
    end&.first || :year_or_more
  end

  def last_buy=(str)
    self.last_buy_date = LAST_BUY_DATE_VALUES[str.to_sym].call
  end

  private

  def buying_type_company_type
    return if buying_type_company.nil? || buying_type_company.company_type == buying_type

    errors.add(:buying_type_company, 'company type should be the same with buying type')
  end

  def category_doesnt_have_children
    return if category.children_count.zero?

    errors.add(:renewal_detail, 'can be added only to the last level categories')
  end

  def set_year
    self.year = last_buy_date&.year || Date.today.year
  end

  def should_buying_type_company_present?
    using_provider? && buying_type != 'provider'
  end
end
