# frozen_string_literal: true

class ApplicationCable::Connection < ActionCable::Connection::Base
  identified_by :current_user
  identified_by :current_country

  def connect
    self.current_user = find_verified_user
    self.current_country = find_current_country
  end

  private

  def find_verified_user
    env['warden'].user
  end

  def find_current_country
    country = Country.find_by_iso_a2_code(request.params[:country])
    country || reject_unauthorized_connection
  end
end
