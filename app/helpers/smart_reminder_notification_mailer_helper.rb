# frozen_string_literal: true

module SmartReminderNotificationMailerHelper
  CATEGORY_COLORS = {
    insurance: {
      base: '#09A965',
      dark: '#06844f',
    },
    broadband: {
      base: '#624C9E',
      dark: '#4d3b7b',
    },
    energy: {
      base: '#E07B50',
      dark: '#ae5f3e',
    },
    subscriptions: {
      base: '#2FA3B8',
      dark: '#247f8f',
    },
    business: {
      base: '#335299',
      dark: '#284078',
    },
  }.freeze

  def css_color(name, opts={})
    return CATEGORY_COLORS[name&.to_sym][:dark] if opts[:dark]
    CATEGORY_COLORS[name&.to_sym][:base]
  end

  def asset_host
    ActionMailer::Base.asset_host
  end

end
