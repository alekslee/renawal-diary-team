# frozen_string_literal: true

module CategoriesHelper
  def category_checked?(category_id)
    current_user && current_user.categories.ids.include?(category_id)
  end
end
