# frozen_string_literal: true

module HomeHelper
  def current_country_slider_index
    index = @countries.pluck(:id).index(current_country.id)
    index.positive? ? index - 1 : index
  end
end
