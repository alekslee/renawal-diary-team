class FeedbackController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    if data = params.dig(:attachments, 0)
      type = data[:title]
      message = data[:text]

      SystemMailer
        .send_feedback(type, message)
        .deliver_later
    end

    render json: { status: :ok }
  end

end