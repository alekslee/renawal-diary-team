# frozen_string_literal: true

class InvitationsController < ApplicationController
  def batch_invite
    existing_users = User.where(email: params[:emails])

    if existing_users.present?
      render json: { error: I18n.t('invitations.errors.email_taken') }, status: :unprocessable_entity
    else
      message = params[:message].present? ? params[:message] : nil
      BatchInviteWorker.perform_async(params[:emails], message, current_user&.id)
      render json: {}, status: :ok
    end
  end
end
