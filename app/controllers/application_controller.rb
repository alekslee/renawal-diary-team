# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  include LocalesSupport
  before_action :configure_permitted_parameters, if: :devise_controller?

  protect_from_forgery with: :exception, prepend: true

  private

  def current_user
    @current_user ||= super || warden.authenticate(scope: :user)
  rescue StandardError
    nil
  end

  def after_sign_in_path_for(resource)
    root_path(country: resource.country.code)
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:accept_invitation, keys: %i[display_name lat lng password password_confirmation])
  end
end
