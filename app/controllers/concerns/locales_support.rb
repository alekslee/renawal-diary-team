# frozen_string_literal: true

module LocalesSupport
  extend ActiveSupport::Concern

  attr_reader :current_country

  included do
    before_action :set_locale
    before_action :ensure_path_sets_country

    helper_method :current_country
  end

  private

  def set_locale
    country_code = retrieve_country_code

    if country_code.present? && current_country&.iso_a2_code != country_code
      @current_country = Country.find_by(iso_a2_code: country_code)
    end
    @current_country ||= Country.find_by(iso_a2_code: FALLBACK_COUNTRY)

    I18n.locale = current_country.default_lang_code
  rescue I18n::InvalidLocale
    flash[:info] = 'Unable to recognize your locale, fallback to default'
  end

  def ensure_path_sets_country
    return if correct_path?

    redirect_to("/#{current_country&.iso_a2_code || FALLBACK_COUNTRY}#{request.fullpath}")
  end

  def retrieve_country_code
    params[:country] || \
      (current_user && current_user.countries.first.code) || \
      current_country&.code || \
      request.location.data['country']&.downcase
  end

  def correct_path?
    controller_name == 'omniauth_callbacks' || \
      (controller_name == 'docs' && action_name == 'api') || \
      AVAILABLE_COUNTRIES.any? do |country_code|
        request.path.include?("/#{country_code}/") || request.path.split('/').last == country_code.to_s
      end
  end
end
