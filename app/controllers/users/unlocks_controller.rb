class Users::UnlocksController < Devise::UnlocksController
  def show
    super do |user|
      if user.errors.present?
        @default_props = {
          current_user: {
            resend_unlock_instructions_form_errors: user.errors.full_messages,
            object: { email: user.email }
          }
        }
        flash[:alert] = user.errors.full_messages.to_sentence
      end
    end
  end
end
