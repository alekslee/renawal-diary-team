# frozen_string_literal: true

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  before_action :set_req_params
  before_action :set_auth

  def facebook
    autorize
  end

  def twitter
    autorize
  end

  def google_oauth2
    autorize
  end

  def linkedin
    autorize
  end

  def failure
    redirect_to root_path(country: @req_params[:country])
  end

  private

  def set_req_params
    @req_params = request.env['omniauth.params'].symbolize_keys
  end

  def set_auth
    @auth = request.env['omniauth.auth']
  end

  def autorize
    interactor = Users::Omniauth::Action.run(@req_params.merge(auth: @auth))
    if interactor.valid?
      user = interactor.result
      @req_params[:type] == 'login' ? after_login(user) : after_signup(user)
    else
      flash[:alert] = interactor.errors.full_messages.to_sentence
      @req_params[:type] == 'login' ? after_login_error : after_signup_error
    end
  end

  def after_login(user)
    sign_in_and_redirect user
  end

  def after_login_error
    attributes = User::PROVIDERS_ATTRIBUTES[@auth.provider].run!(auth: @auth)
    uid_attributes = attributes.slice(:google_uid, :facebook_uid, :twitter_uid, :linkedin_uid)
    uid_attributes = "#{uid_attributes.keys[0]}=#{uid_attributes.values[0]}"
    redirect_to "#{root_path(country: @req_params[:country])}?email=#{attributes[:email]}&#{uid_attributes}"
  end

  def after_signup(user)
    redirect_to root_path(generate_sign_up_omniauth_params(user))
  end

  def after_signup_error
    redirect_to new_user_session_path(country: @req_params[:country])
  end

  def generate_sign_up_omniauth_params(user)
    user.as_json(only: %i[email google_uid facebook_uid twitter_uid linkedin_uid]).merge(country: @req_params[:country])
  end
end
