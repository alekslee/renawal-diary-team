class CategoriesController < ApplicationController
  # rubocop:disable Naming/PredicateName
  # Disabling rubocop - because this page should have name vote
  def vote
    @default_props = Categories::VotePage::LoadDefaultProps.run!(
      user: current_user, category_id: params[:id].to_s, controller: self, country: current_country
    )
  end
  # rubocop:enable Naming/PredicateName

  def leaderboard
    @default_props = Categories::LeaderboardPage::LoadDefaultProps.run!(
      user: current_user, category_id: params[:id].to_s, controller: self, country: current_country
    )
  end

  def renewal_details
  end

  def my_notes
    @default_props = Categories::Notes::LoadDefaultProps.run!(
      user: current_user, category_id: params[:id].to_s, controller: self, country: current_country
    )
  end

end
