class ProvidersController < ApplicationController

  def approve
    token = params[:token]

    company = Company.from_id_token(token)
    if company
      company.update(status: :approved)

      status = 'success'
    else
      status = 'error'
      country = Country.find_by(iso_a2_code: params[:country])
      company = country.companies.first
    end

    redirect_to url_for("/#{company.country.iso_a2_code}/categories/#{company.category.slag}/leaderboard?company-approve-status=#{status}")
  end

end
