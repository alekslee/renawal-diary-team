# frozen_string_literal: true

class DocsController < ApplicationController
  before_action :authenticate_user!

  def api
  end
end
