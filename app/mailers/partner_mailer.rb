# frozen_string_literal: true

class PartnerMailer < ApplicationMailer
  def become_a_partner
    @email = params[:email]
    @name = params[:name]
    @body = params[:body]
    mail(to: 'directory@renewaldiary.com', subject: 'Become a partner mail')
  end
end
