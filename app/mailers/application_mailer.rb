# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'info@myrenewaldiary.com'
  layout 'mailer'

  include SmartReminderNotificationMailerHelper

end
