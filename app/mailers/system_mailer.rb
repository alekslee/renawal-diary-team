# frozen_string_literal: true

class SystemMailer < ApplicationMailer

  def company_approve
    @company = params[:company]
    mail(to: 'cormac@renewaldiary.com', subject: 'Company Confirmation Email')
  end

  def send_feedback(type, message)
    @type = type || 'bug'
    @message = message || ''

    mail(to: 'cormac@renewaldiary.com', cc: 'nikolay@renewaldiary.com', subject: "Renewaldiary Feeback: #{@type.upcase}")
  end

end
