class SmartReminderNotificationMailer < ApplicationMailer
  layout 'smart_reminder_notification_mailer'

  def set_up_smart_reminder
    @smart_reminder = SmartReminder.find(params[:smart_reminder_id])
    @root_category = @smart_reminder.category&.root
    mail(to: @smart_reminder.user.email, subject: "‘ #{@smart_reminder.name.to_s.capitalize} ’ Reminder status : #{@smart_reminder.status.capitalize} : Congrats your reminder is set!")
  end

  def green_to_amber
    @smart_reminder = SmartReminder.find(params[:smart_reminder_id])
    @root_category = @smart_reminder.category&.root
    days_left = @smart_reminder.triggered_at.yday - Time.now.yday
    mail(to: @smart_reminder.user.email, subject: "‘ #{@smart_reminder.name.to_s.capitalize} ’ Reminder status : #{@smart_reminder.status.capitalize} : (#{days_left} days left to shop around and save money)")
  end

  def amber_to_red
    @smart_reminder = SmartReminder.find(params[:smart_reminder_id])
    @root_category = @smart_reminder.category&.root
    days_left = @smart_reminder.triggered_at.yday - Time.now.yday
    mail(to: @smart_reminder.user.email, subject: "‘ #{@smart_reminder.name.to_s.capitalize} ’ Reminder status : #{@smart_reminder.status.capitalize} : (#{days_left} days left to shop around and save money)")
  end

  def date_becomes
    @smart_reminder = SmartReminder.find(params[:smart_reminder_id])
    @root_category = @smart_reminder.category&.root
    days_left = @smart_reminder.triggered_at.yday - Time.now.yday
    mail(to: @smart_reminder.user.email, subject: "‘ #{@smart_reminder.name.to_s.capitalize} ’ Reminder status : #{@smart_reminder.status.capitalize} : (#{days_left} days left to shop around and save money)")
  end
end
