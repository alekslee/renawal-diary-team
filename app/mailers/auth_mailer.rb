# frozen_string_literal: true

class AuthMailer < Devise::Mailer
  include DefaultUrlOptions
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`

  helper :application
  helper :smart_reminder_notification_mailer

  default template_path: 'users/mailer'

  def confirmation_instructions(record, token, opts = {})
    # headers['mj-templateid'] = 'rubyPR_Test_ID_1469790724'
    super
  end

  def invitation_instructions(record, token, options, *args)
    @message = options[:message]
    super
  end

  def last_chance_confirmation_instructions(user_id, token)
    @user = User.find_by(id: user_id)
    @token = token
    mail(to: @user.email, subject: t('devise.mailer.confirmation_instructions.subject'))
  end
end
