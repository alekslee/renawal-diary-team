source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

gem 'dotenv-rails', require: 'dotenv/rails-now'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.1'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sassc-rails', '~> 1.3.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '>= 4.0.x'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'enumerize'
gem 'hamlit'
gem 'hamlit-rails'
gem 'jbuilder', '~> 2.5'
gem 'oj'
gem 'paper_trail'
gem 'will_paginate'

# For implement interaction and service patterns
gem 'active_interaction', '~> 3.6'

gem 'acts_as_paranoid', '~> 0.6.0'
gem 'devise-async'
gem 'devise_invitable', '~> 1.7.0'
gem 'devise_token_auth'
gem 'devise_zxcvbn'
gem 'friendly_id', '~> 5.2.0'
gem 'geocoder'

gem 'mailjet'
gem 'rack-cors', require: 'rack/cors'

gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'omniauth-linkedin'
gem 'omniauth-twitter'

gem 'awesome_nested_set'
gem 'axlsx'
gem "aws-sdk-s3", require: false
gem 'google-cloud-translate'
gem 'google_drive'
gem 'graphql', '~> 1.9.7'
gem 'image_size'
gem 'iso_country_codes'
gem 'city-state'

gem 'link_thumbnailer', git: 'https://github.com/Warrior109/link_thumbnailer.git', ref: '8ba72dc'
gem 'paperclip', '~> 6.0.0'
gem 'react_on_rails', '11.1.4' # Use the exact gem version to match npm version
gem 'redis-rails'
gem 'roo'
gem 'sidekiq'
gem 'sidekiq-failures'
gem 'sidekiq-scheduler'
gem 'zip-zip'
# Use Redis adapter to run Action Cable in production
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  # gem 'pry-byebug'
  gem 'pry-rails'

  gem 'brakeman', require: false
  gem 'rubocop-rails_config'

  gem 'chromedriver-helper'
  gem 'rspec-rails', '~> 3.8'
  gem 'selenium-webdriver'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  gem 'bullet'
  gem 'foreman'
  gem 'graphiql-rails'
  gem 'graphql-rails_logger'
  gem 'letter_opener'
  gem 'rubocop-rspec'
  gem 'capistrano', require: false
  gem 'capistrano-rvm', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma', require: false
  gem 'capistrano-yarn', require: false
  gem 'capistrano-webpacker-precompile', require: false
  gem 'capistrano-nginx', require: false
  gem 'capistrano-sidekiq', github: 'seuros/capistrano-sidekiq'
  gem 'sshkit-sudo'
end

gem 'awesome_print'

group :test do
  gem 'cucumber'
  gem 'capybara'
  gem 'capybara-webkit', github: 'thoughtbot/capybara-webkit', branch: 'master'
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'fakeredis', require: 'fakeredis/rspec'
  gem 'launchy'
  gem 'poltergeist'
  gem 'shoulda-matchers', git: 'https://github.com/thoughtbot/shoulda-matchers.git', tag: 'v4.0.0.rc1'
  gem 'simplecov'
  gem 'webmock'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

gem 'graphql-batch'

gem 'mini_racer', platforms: :ruby
