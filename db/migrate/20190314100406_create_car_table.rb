# frozen_string_literal: true

class DummyCar < ApplicationRecord
  self.table_name = 'cars'
end

class CreateCarTable < ActiveRecord::Migration[5.2]
  PATH_TO_FILE = Rails.root.join('config', 'car_data.sql')
  MAKE_HEADER_KEY = 'model_make_display'
  MODEL_HEADER_KEY = 'model_name'
  TRIM_HEADER_KEY = 'model_trim'
  YEAR_HEADER_KEY = 'model_year'

  def up
    create_table :cars do |t|
      t.string :make
      t.string :model
      t.string :trim
      t.integer :year

      t.timestamps null: false
    end
    csv = RenewalDetails::Criterias::ParseCarDataFile.run!
    DummyCar.transaction do
      csv.each do |row|
        DummyCar.create(
          make: row[MAKE_HEADER_KEY],
          model: row[MODEL_HEADER_KEY],
          trim: row[TRIM_HEADER_KEY],
          year: row[YEAR_HEADER_KEY]
        )
      end
    end
  end

  def down
    drop_table :cars
  end
end
