class AddRatesCommentsToFeedbacks < ActiveRecord::Migration[5.2]
  def change
    add_column :feedbacks, :current_provider_rate_comment, :text
    add_column :feedbacks, :buying_type_company_rate_comment, :text
  end
end
