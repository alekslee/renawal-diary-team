class AddCategoryToSmartReminders < ActiveRecord::Migration[5.2]
  def change
    add_reference :smart_reminders, :category, foreign_key: true
  end
end
