class AddCriteriaQuestionsToRenewalDetails < ActiveRecord::Migration[5.2]
  def change
    add_column :renewal_details, :criteria_questions, :jsonb
  end
end
