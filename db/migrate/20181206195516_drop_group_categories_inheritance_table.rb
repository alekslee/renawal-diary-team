# frozen_string_literal: true

class DropGroupCategoriesInheritanceTable < ActiveRecord::Migration[5.2]
  def change
    drop_table :group_categories_inheritances
  end
end
