# frozen_string_literal: true

class AddInvitationsCountToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :invitations_count, :integer, default: 0, index: true
    add_index :users, :invited_by_id
  end
end
