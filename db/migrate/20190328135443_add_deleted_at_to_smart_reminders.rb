class AddDeletedAtToSmartReminders < ActiveRecord::Migration[5.2]
  def change
    add_column :smart_reminders, :deleted_at, :datetime
  end
end
