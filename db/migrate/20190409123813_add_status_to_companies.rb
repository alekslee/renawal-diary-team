class CompanyDummy < ApplicationRecord
  self.table_name = 'companies'
end

class AddStatusToCompanies < ActiveRecord::Migration[5.2]
  def up
    add_column :companies, :status, :string
    CompanyDummy.update_all(status: :approved)
  end

  def down
    remove_column :companies, :status
  end
end
