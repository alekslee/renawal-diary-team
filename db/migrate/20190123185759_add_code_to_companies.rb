class AddCodeToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :code, :string
  end
end
