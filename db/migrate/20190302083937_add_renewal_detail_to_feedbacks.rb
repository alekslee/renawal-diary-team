class AddRenewalDetailToFeedbacks < ActiveRecord::Migration[5.2]
  def up
    add_reference :feedbacks, :renewal_detail, foreign_key: true
    execute <<-SQL
      UPDATE feedbacks SET
      renewal_detail_id = (
        SELECT renewal_details.id
        FROM renewal_details
        WHERE renewal_details.category_id = feedbacks.category_id
        AND renewal_details.user_id = feedbacks.user_id
      )
    SQL
    remove_reference :feedbacks, :category, foreign_key: true
  end

  def down
    add_reference :feedbacks, :category, foreign_key: true
    execute <<-SQL
      UPDATE feedbacks SET
      category_id = (
        SELECT renewal_details.category_id
        FROM renewal_details
        WHERE renewal_details.id = feedbacks.renewal_detail_id
      )
    SQL
    remove_reference :feedbacks, :renewal_detail, foreign_key: true
  end
end
