# frozen_string_literal: true

class UsersUsernameRemoveUniqueIndexing < ActiveRecord::Migration[5.2]
  def change
    remove_index :users, :username # unique
    add_index :users, :username
  end
end
