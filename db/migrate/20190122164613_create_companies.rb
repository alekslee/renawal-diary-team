class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :url
      t.string :twitter
      t.string :name
      t.string :company_type

      t.timestamps
    end
  end
end
