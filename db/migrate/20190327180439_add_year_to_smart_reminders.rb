class AddYearToSmartReminders < ActiveRecord::Migration[5.2]
  def change
    add_column :smart_reminders, :year, :integer
  end
end
