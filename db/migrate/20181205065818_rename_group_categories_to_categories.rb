# frozen_string_literal: true

class RenameGroupCategoriesToCategories < ActiveRecord::Migration[5.2]
  def change
    rename_table :group_categories, :categories
  end
end
