class AddPriceableToPrices < ActiveRecord::Migration[5.2]
  def change
    add_reference :prices, :priceable, polymorphic: true
  end
end
