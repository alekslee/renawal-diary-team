class LastBuyDateRemoveTimeRange < ActiveRecord::Migration[5.2]
  def change
    change_last_buy_date_from_time_range(:feedbacks)
    change_last_buy_date_from_time_range(:renewal_details)
  end

  private

  def change_last_buy_date_from_time_range(table_name)
    rename_column table_name, :last_buy_start_date, :last_buy_date
    remove_column table_name, :last_buy_end_date
  end
end
