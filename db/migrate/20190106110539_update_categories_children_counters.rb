# frozen_string_literal: true

class GenericCategory < ApplicationRecord
  self.table_name = 'categories'
  acts_as_nested_set counter_cache: :children_count
end

class UpdateCategoriesChildrenCounters < ActiveRecord::Migration[5.2]
  def up
    GenericCategory.all.each do |cat|
      GenericCategory.reset_counters(cat.id, :children_count)
    end
  end

  def down
    GenericCategory.all.update_all(children_count: 0)
  end
end
