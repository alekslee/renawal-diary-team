class AddUsingProviderToRenewalDetails < ActiveRecord::Migration[5.2]
  def change
    add_column :renewal_details, :using_provider, :boolean
  end
end
