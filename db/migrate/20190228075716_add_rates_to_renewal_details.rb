class AddRatesToRenewalDetails < ActiveRecord::Migration[5.2]
  def change
    add_column :renewal_details, :current_provider_rate, :integer
    add_column :renewal_details, :buying_type_company_rate, :integer
    add_column :renewal_details, :current_provider_rate_comment, :text
    add_column :renewal_details, :buying_type_company_rate_comment, :text
  end
end
