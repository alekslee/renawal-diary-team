class RemovePricesModel < ActiveRecord::Migration[5.2]
  def up
    remove_reference :prices, :priceable
    drop_table :prices
  end

  def down
    create_table :prices do |t|
      t.string :currency
      t.integer :cents
      t.string :name

      t.timestamps
    end
    add_reference :prices, :priceable, polymorphic: true
  end
end
