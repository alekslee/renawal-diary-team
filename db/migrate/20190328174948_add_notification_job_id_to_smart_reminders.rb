class AddNotificationJobIdToSmartReminders < ActiveRecord::Migration[5.2]
  def change
    add_column :smart_reminders, :notification_job_id, :string
  end
end
