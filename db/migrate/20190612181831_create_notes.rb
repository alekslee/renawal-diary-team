class CreateNotes < ActiveRecord::Migration[5.2]
  def change
    create_table :notes do |t|
      t.string :title
      t.text :body

      t.integer :category_id, index: true
      t.integer :user_id, index: true

      t.timestamps
    end
  end
end
