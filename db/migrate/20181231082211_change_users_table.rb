class ChangeUsersTable < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :latitude, :decimal, precision: 10, scale: 6
    remove_column :users, :longitude, :decimal, precision: 10, scale: 6
    add_column :users, :tokens, :json
    add_column :users, :provider, :string
    rename_column :users, :uuid, :uid
  end
end
