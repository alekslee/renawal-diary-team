# frozen_string_literal: true

class RemoveFieldsRelatedToGroups < ActiveRecord::Migration[5.2]
  def change
    remove_column :categories, :groups_slugs, :jsonb
    remove_column :countries, :country_group_connections_count, :bigint
    drop_table :country_group_connections
    drop_table :group_category_group_connections
    drop_table :group_memberships
    drop_table :groups
  end
end
