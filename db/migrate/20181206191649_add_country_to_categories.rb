# frozen_string_literal: true

class AddCountryToCategories < ActiveRecord::Migration[5.2]
  def change
    add_reference :categories, :country, foreign_key: true
  end
end
