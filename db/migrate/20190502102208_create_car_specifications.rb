class CreateCarSpecifications < ActiveRecord::Migration[5.2]
  def change
    create_table :car_specifications do |t|
      t.string :name
      t.string :value
      t.string :trim
      t.string :unit
      t.integer :uid_car_specification
      t.integer :uid_car_specification_value
      t.integer :uid_car_trim

      t.timestamps
    end
  end
end
