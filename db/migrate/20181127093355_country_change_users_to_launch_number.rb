# frozen_string_literal: true

class CountryChangeUsersToLaunchNumber < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:countries, :users_to_launch, 10_000)
  end
end
