class RemoveRenewalDetailsFieldsFromFeedbacks < ActiveRecord::Migration[5.2]
  def up
    remove_column :feedbacks, :checked_places
    remove_column :feedbacks, :buying_type
    remove_column :feedbacks, :last_buy_date
    remove_column :feedbacks, :current_provider_rate
    remove_column :feedbacks, :current_provider_rate_comment
    remove_column :feedbacks, :buying_type_company_rate
    remove_column :feedbacks, :buying_type_company_rate_comment
    remove_column :feedbacks, :criteria_questions
    remove_column :feedbacks, :price_questions

    remove_foreign_key :feedbacks, column: :current_provider_id
    remove_reference :feedbacks, :current_provider, index: true

    remove_foreign_key :feedbacks, column: :buying_type_company_id
    remove_reference :feedbacks, :buying_type_company, index: true
  end

  def down
    add_column :feedbacks, :checked_places, :string
    add_column :feedbacks, :buying_type, :string
    add_column :feedbacks, :last_buy_date, :date
    add_column :feedbacks, :current_provider_rate, :integer
    add_column :feedbacks, :current_provider_rate_comment, :text
    add_column :feedbacks, :buying_type_company_rate, :integer
    add_column :feedbacks, :buying_type_company_rate_comment, :text
    add_column :feedbacks, :criteria_questions, :jsonb
    add_column :feedbacks, :price_questions, :jsonb

    add_reference :feedbacks, :current_provider, index: true
    add_foreign_key :feedbacks, :companies, column: :current_provider_id

    add_reference :feedbacks, :buying_type_company, index: true
    add_foreign_key :feedbacks, :companies, column: :buying_type_company_id
  end
end
