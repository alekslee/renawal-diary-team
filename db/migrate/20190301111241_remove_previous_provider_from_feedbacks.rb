class RemovePreviousProviderFromFeedbacks < ActiveRecord::Migration[5.2]
  def up
    remove_foreign_key :feedbacks, column: :previous_provider_id
    remove_reference :feedbacks, :previous_provider, index: true
  end

  def down
    add_reference :feedbacks, :previous_provider, index: true
    add_foreign_key :feedbacks, :companies, column: :previous_provider_id
  end
end
