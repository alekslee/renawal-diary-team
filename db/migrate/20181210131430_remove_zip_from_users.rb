# frozen_string_literal: true

class RemoveZipFromUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :zip, :string
    remove_column :users, :zip_code_name, :string
  end
end
