class AddLastByDateToFeedbacks < ActiveRecord::Migration[5.2]
  def up
    add_column :feedbacks, :last_buy_date, :date
    execute <<-SQL
      UPDATE feedbacks SET
      last_buy_date =
      CASE WHEN (last_buy = 'today') THEN (created_at)
           WHEN (last_buy = 'this_week') THEN (date_trunc('week', created_at::timestamp))
           WHEN (last_buy = 'this_month') THEN (date_trunc('month', created_at::timestamp))
           ELSE (created_at::date - interval '1 year')
      END;
    SQL
    remove_column :feedbacks, :last_buy
  end

  def down
    add_column :feedbacks, :last_buy, :string
    execute <<-SQL
      UPDATE feedbacks SET
      last_buy =
      CASE WHEN (last_buy_date >= created_at::date AND last_buy_date < (created_at::date + '1 day'::interval))
             THEN ('today')
           WHEN (last_buy_date >= date_trunc('week', created_at::timestamp)
                  AND last_buy_date < (date_trunc('week', created_at::timestamp) + '1 day'::interval))
             THEN ('this_week')
           WHEN (last_buy_date >= date_trunc('month', created_at::timestamp)
                  AND last_buy_date < (date_trunc('month', created_at::timestamp) + '1 day'::interval))
             THEN ('this_month')
           ELSE ('other')
      END;
    SQL
    remove_column :feedbacks, :last_buy_date
  end
end
