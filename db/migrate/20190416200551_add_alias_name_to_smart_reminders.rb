class AddAliasNameToSmartReminders < ActiveRecord::Migration[5.2]

  def change
    add_column :smart_reminders, :alias_name, :string
  end

end
