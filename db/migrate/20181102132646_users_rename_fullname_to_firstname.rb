# frozen_string_literal: true

class UsersRenameFullnameToFirstname < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :full_name, :first_name
  end
end
