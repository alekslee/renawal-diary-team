# frozen_string_literal: true

class AddCodeToCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :code, :string
  end
end
