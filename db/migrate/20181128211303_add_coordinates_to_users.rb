# frozen_string_literal: true

class AddCoordinatesToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :lat, :float, limit: 24
    add_column :users, :lng, :float, limit: 24
  end
end
