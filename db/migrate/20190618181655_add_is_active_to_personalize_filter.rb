class AddIsActiveToPersonalizeFilter < ActiveRecord::Migration[5.2]
  def change
    add_column :personalize_filters, :is_active, :boolean, default: true
  end
end
