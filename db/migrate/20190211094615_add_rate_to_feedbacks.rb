class AddRateToFeedbacks < ActiveRecord::Migration[5.2]
  def change
    add_column :feedbacks, :current_provider_rate, :integer
    add_column :feedbacks, :buying_type_company_rate, :integer
  end
end
