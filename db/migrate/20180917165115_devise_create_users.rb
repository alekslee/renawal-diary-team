# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[5.2]
  # rubocop:disable Metrics/MethodLength, Metrics/AbcSize, Metrics/BlockLength
  def change
    enable_extension 'uuid-ossp'

    create_table :users do |t|
      ## UID
      t.uuid :uuid, default: 'uuid_generate_v4()'

      ## Database authenticatable
      t.string  :username,           null: false, default: ''
      t.string  :full_name,          null: false, default: ''
      t.string  :mobile_number,      null: false, default: ''
      t.string  :email,              null: false, default: ''
      t.integer :bonus_points,       null: false, default: 0
      t.string  :encrypted_password, null: true,  default: ''

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      # Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.inet     :current_sign_in_ip
      t.inet     :last_sign_in_ip

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      t.string   :unlock_token # Only if unlock strategy is :email or :both
      t.datetime :locked_at

      t.datetime :deleted_at, default: nil

      ## Invitable
      t.string   :invitation_token
      t.datetime :invitation_created_at
      t.datetime :invitation_sent_at
      t.datetime :invitation_accepted_at
      t.integer  :invitation_limit
      t.integer  :invited_by_id
      t.string   :invited_by_type

      ## Geocoder
      t.decimal :latitude, precision: 10, scale: 6
      t.decimal :longitude, precision: 10, scale: 6
      t.string :country_code
      t.string :state
      t.string :state_code
      t.string :state_district
      t.string :city
      t.string :address
      t.string :zip

      t.timestamps null: false
    end

    add_index :users, :username,             unique: true
    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
    add_index :users, :confirmation_token,   unique: true
    add_index :users, :unlock_token,         unique: true
    add_index :users, :invitation_token,     unique: true
    add_index :users, :uuid,                 unique: true
    add_index :users, :latitude
    add_index :users, :longitude

    add_index :users, %i[username deleted_at], unique: true
    add_index :users, %i[email deleted_at], unique: true
    add_index :users, %i[reset_password_token deleted_at], unique: true
    add_index :users, :deleted_at, where: 'deleted_at IS NULL'
  end
  # rubocop:enable Metrics/MethodLength, Metrics/AbcSize, Metrics/BlockLength
end
