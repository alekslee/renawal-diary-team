class AddRelationsToRenewalDetails < ActiveRecord::Migration[5.2]
  def change
    add_reference :renewal_details, :user, foreign_key: true
    add_reference :renewal_details, :category, foreign_key: true

    add_reference :renewal_details, :current_provider, index: true
    add_foreign_key :renewal_details, :companies, column: :current_provider_id

    add_reference :renewal_details, :previous_provider, index: true
    add_foreign_key :renewal_details, :companies, column: :previous_provider_id

    add_reference :renewal_details, :buying_type_company, index: true
    add_foreign_key :renewal_details, :companies, column: :buying_type_company_id
  end
end
