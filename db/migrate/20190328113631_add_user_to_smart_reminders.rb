class AddUserToSmartReminders < ActiveRecord::Migration[5.2]
  def change
    add_reference :smart_reminders, :user, foreign_key: true
  end
end
