class AddReminderType < ActiveRecord::Migration[5.2]

  def change
    add_column :smart_reminders, :reminder_type, :string
  end

end
