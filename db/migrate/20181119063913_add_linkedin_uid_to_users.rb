# frozen_string_literal: true

class AddLinkedinUidToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :linkedin_uid, :string
  end
end
