class AddSheetIndexToCompany < ActiveRecord::Migration[5.2]

  def change
    add_column :companies, :sheet_index, :integer
  end

end
