# frozen_string_literal: true

class AddZipCodeNameToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :zip_code_name, :string
  end
end
