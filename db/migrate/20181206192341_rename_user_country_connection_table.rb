# frozen_string_literal: true

class RenameUserCountryConnectionTable < ActiveRecord::Migration[5.2]
  def change
    rename_table :user_country_connections, :users_countries
    rename_column :countries, :user_country_connections_count, :users_countries_count
  end
end
