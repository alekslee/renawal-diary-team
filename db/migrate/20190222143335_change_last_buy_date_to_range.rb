class ChangeLastBuyDateToRange < ActiveRecord::Migration[5.2]
  def up
    up_last_buy_date_to_time_range(:feedbacks)
    up_last_buy_date_to_time_range(:renewal_details)
  end

  def down
    down_last_buy_date_to_time_range(:feedbacks)
    down_last_buy_date_to_time_range(:renewal_details)
  end

  private

  def up_last_buy_date_to_time_range(table_name)
    rename_column table_name, :last_buy_date, :last_buy_start_date
    add_column table_name, :last_buy_end_date, :date
    execute <<-SQL
      UPDATE feedbacks SET
      last_buy_end_date = created_at
    SQL
  end

  def down_last_buy_date_to_time_range(table_name)
    rename_column table_name, :last_buy_start_date, :last_buy_date
    remove_column table_name, :last_buy_end_date
  end
end
