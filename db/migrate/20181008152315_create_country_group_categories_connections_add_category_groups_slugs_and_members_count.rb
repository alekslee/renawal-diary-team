# frozen_string_literal: true

class CreateCountryGroupCategoriesConnectionsAddCategoryGroupsSlugsAndMembersCount < ActiveRecord::Migration[5.2]
  def change
    add_column :group_categories, :groups_slugs, :jsonb, default: []
    add_column :group_categories, :members_count_per_country, :jsonb, default: {}
    add_column :group_categories, :countries_codes, :jsonb, null: false, default: []

    add_index  :group_categories, :countries_codes, using: :gin # , opclass: :gin
  end
end
