# frozen_string_literal: true

class AddIndexesToSocialUids < ActiveRecord::Migration[5.2]
  def change
    add_index :users, :facebook_uid, unique: true
    add_index :users, :twitter_uid,  unique: true
    add_index :users, :google_uid,   unique: true
    add_index :users, :linkedin_uid, unique: true
  end
end
