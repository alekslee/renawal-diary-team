class RenameCommunityNameToDisplayName < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :community_name, :display_name
  end
end
