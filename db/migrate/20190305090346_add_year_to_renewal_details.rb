class AddYearToRenewalDetails < ActiveRecord::Migration[5.2]
  def up
    add_column :renewal_details, :year, :integer
    execute <<-SQL
      UPDATE renewal_details SET
      year = EXTRACT(YEAR FROM last_buy_date::timestamp)
    SQL
  end

  def down
    remove_column :renewal_details, :year
  end
end
