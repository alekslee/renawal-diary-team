class AddVehicleTypeToCars < ActiveRecord::Migration[5.2]
  def change
    add_column :cars, :car_type, :integer
    add_index :cars, :car_type
  end
end
