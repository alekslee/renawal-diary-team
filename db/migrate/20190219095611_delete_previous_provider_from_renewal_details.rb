class DeletePreviousProviderFromRenewalDetails < ActiveRecord::Migration[5.2]
  def up
    remove_foreign_key :renewal_details, column: :previous_provider_id
    remove_reference :renewal_details, :previous_provider, index: true
  end

  def down
    add_reference :renewal_details, :previous_provider, index: true
    add_foreign_key :renewal_details, :companies, column: :previous_provider_id
  end
end
