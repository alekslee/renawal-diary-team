class AddRenewalDetailToSmartReminders < ActiveRecord::Migration[5.2]
  def change
    add_reference :smart_reminders, :renewal_detail, foreign_key: true
  end
end
