class AddInsightsCommentToFeedbacks < ActiveRecord::Migration[5.2]
  def change
    add_column :feedbacks, :insights_comment, :text
  end
end
