class AddCounterCacheToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :current_feedbacks_count, :integer, default: 0
    add_column :companies, :buying_type_feedbacks_count, :integer, default: 0
  end
end
