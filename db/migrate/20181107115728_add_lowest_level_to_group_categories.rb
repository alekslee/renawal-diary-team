# frozen_string_literal: true

class AddLowestLevelToGroupCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :group_categories, :lowest_level, :boolean, default: false, null: false
  end
end
