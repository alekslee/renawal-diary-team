# frozen_string_literal: true

class CreateCountriesGroupsCategoriesGroupMemberships < ActiveRecord::Migration[5.2]
  # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
  def change
    create_table :countries do |t|
      t.string     :en_name,                         null: false
      t.string     :iso_a2_code,                     null: false
      t.string     :default_lang_code,               null: false
      t.jsonb      :available_lang_codes,            null: false, default: []
      t.integer    :users_to_launch,                 default: 100_000
      t.bigint     :user_country_connections_count,  default: 0
      t.bigint     :country_group_connections_count, default: 0
      t.timestamps null: false
      # HABTM groups
    end

    add_index :countries, :en_name,     unique: true
    add_index :countries, :iso_a2_code, unique: true

    create_table :groups do |t|
      t.string     :name,                    null: false
      t.jsonb      :criteria_questions,      null: false, default: {}
      t.bigint     :group_memberships_count, default: 0
      t.string     :slug,                    null: false
      t.datetime   :deleted_at,              default: nil
      # HABTM group_categories
      t.timestamps null: false
      # HABTM countries
    end

    add_index :groups, :name,       unique: true
    add_index :groups, :slug,       unique: true
    add_index :groups, :deleted_at, where: 'deleted_at IS NULL'

    create_table :group_memberships do |t|
      t.belongs_to :user,       null: false, index: true
      t.belongs_to :group,      null: false, index: true
      # users_counter
      t.jsonb      :criteria,   null: false, default: {}
      t.datetime   :deleted_at, default: nil
      t.timestamps null: false
    end

    add_index :group_memberships, :deleted_at, where: 'deleted_at IS NULL'

    create_table :group_categories do |t| # serve to all countries
      t.string  :en_name, null: false # use as key to get a translations for current_user locale
      t.integer :level,   null: false, default: 1
      # so 1lvl are not expected to have groups directly, instead through :lower_group_categories
      # HABTM groups
      # has_many child_category_groups
      # HABTM :parent_group_categories - lvl + 1
      # HABTM :child_group_categories  - lvl - 1
      t.timestamps null: false
    end

    # has_many through instead of has_and_belongs_to_many in order to use dependent: :destroy
    create_table :group_categories_inheritances do |t|
      t.belongs_to :parent_group_category, null: false, index: true
      t.belongs_to :child_group_category,  null: false, index: true
      t.timestamps null: false
    end

    # has_many through instead of has_and_belongs_to_many in order to use dependent: :destroy
    create_table :group_category_group_connections do |t|
      t.belongs_to :group_category, null: false, index: true
      t.belongs_to :group,          null: false, index: true
      t.timestamps null: false
    end

    # has_many through instead of has_and_belongs_to_many in order to use dependent: :destroy
    create_table :country_group_connections do |t|
      t.belongs_to :country,    null: false, index: true
      t.belongs_to :group,      null: false, index: true
      t.datetime   :deleted_at, default: nil
      t.timestamps null: false
    end

    add_index :country_group_connections, :deleted_at, where: 'deleted_at IS NULL'

    # separated table for user statistics
    create_table :user_statistics do |t|
      t.bigint     :statisticable_id # references polymorphic: true generates too long index name
      t.string     :statisticable_type
      t.bigint     :members_number, default: 0
      t.datetime   :deleted_at,     default: nil
      t.timestamps null: false
    end

    add_index :user_statistics, %i[statisticable_id statisticable_type],
      name: 'index_u_stats_by_statisticable_id_and_statisticable_type'
    add_index :user_statistics, :deleted_at, where: 'deleted_at IS NULL'

    create_table :user_country_connections do |t|
      t.belongs_to :user,          null: false, index: true
      t.belongs_to :country,       null: false, index: true
      t.boolean    :primary,       default: true
      t.string     :language_code
      t.datetime   :deleted_at, default: nil
      t.timestamps null: false
    end

    add_index :user_country_connections, :deleted_at, where: 'deleted_at IS NULL'
    # we may define a uniq index
    # add_index :users_countries, :user, unique: true, where: 'primary'
  end
  # rubocop:enable Metrics/MethodLength, Metrics/AbcSize
end
