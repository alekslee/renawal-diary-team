class AddPricesQuestionsToRenewalDetails < ActiveRecord::Migration[5.2]
  def change
    add_column :renewal_details, :prices_questions, :jsonb
  end
end
