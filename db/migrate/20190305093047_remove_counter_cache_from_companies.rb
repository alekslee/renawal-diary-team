class RemoveCounterCacheFromCompanies < ActiveRecord::Migration[5.2]
  def change
    remove_column :companies, :current_feedbacks_count, :integer, default: 0
    remove_column :companies, :buying_type_feedbacks_count, :integer, default: 0
  end
end
