# frozen_string_literal: true

class AddOauthFieldsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :facebook_uid, :string
    add_column :users, :twitter_uid, :string
    add_column :users, :google_uid, :string
  end
end
