class AddReferencesToFeedbacks < ActiveRecord::Migration[5.2]
  def change
    add_reference :feedbacks, :user, foreign_key: true
    add_reference :feedbacks, :category, foreign_key: true

    add_reference :feedbacks, :current_provider, index: true
    add_foreign_key :feedbacks, :companies, column: :current_provider_id

    add_reference :feedbacks, :previous_provider, index: true
    add_foreign_key :feedbacks, :companies, column: :previous_provider_id

    add_reference :feedbacks, :buying_type_company, index: true
    add_foreign_key :feedbacks, :companies, column: :buying_type_company_id
  end
end
