# frozen_string_literal: true

class AddUsersCategoriesCouterToCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :users_categories_count, :integer, default: 0
  end
end
