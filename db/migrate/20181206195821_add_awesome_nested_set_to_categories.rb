# frozen_string_literal: true

class AddAwesomeNestedSetToCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :parent_id, :integer, index: true
    add_column :categories, :lft, :integer, index: true
    add_column :categories, :rgt, :integer, index: true

    add_column :categories, :depth, :integer, default: 0
    add_column :categories, :children_count, :integer, default: 0

    remove_column :categories, :level, :integer
    remove_column :categories, :members_count_per_country, :jsonb
    remove_column :categories, :countries_codes, :jsonb
    remove_column :categories, :lowest_level, :boolean
  end
end
