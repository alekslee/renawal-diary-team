# frozen_string_literal: true

class ChangeUsernameToCommunityName < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :username, :community_name
  end
end
