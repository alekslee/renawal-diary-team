class DeteleUsersToLaunchColumnFromCountries < ActiveRecord::Migration[5.2]
  def change
    remove_column :countries, :users_to_launch, :integer
  end
end
