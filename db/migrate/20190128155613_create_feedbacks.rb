class CreateFeedbacks < ActiveRecord::Migration[5.2]
  def change
    create_table :feedbacks do |t|
      t.string :last_buy
      t.string :checked_places
      t.string :buying_type

      t.timestamps
    end
  end
end
