class AddCriteriaAndPriceQuestionsToFeedbacks < ActiveRecord::Migration[5.2]
  def change
    add_column :feedbacks, :criteria_questions, :jsonb
    add_column :feedbacks, :price_questions, :jsonb
  end
end
