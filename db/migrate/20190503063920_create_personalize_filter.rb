class CreatePersonalizeFilter < ActiveRecord::Migration[5.2]
  def change
    create_table :personalize_filters do |t|
      t.jsonb :criteria_questions
      t.bigint :user_id, index: true
      t.bigint :category_id, index: true

      t.timestamps null: false
    end
  end
end
