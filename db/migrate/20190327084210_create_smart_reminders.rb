class CreateSmartReminders < ActiveRecord::Migration[5.2]
  def change
    create_table :smart_reminders do |t|
      t.datetime :triggered_at
      t.string :status

      t.timestamps
    end
  end
end
