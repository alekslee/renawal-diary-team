class AddColumnToRenevalDetail < ActiveRecord::Migration[5.2]

  def change
    add_column :renewal_details, :price_rate, :float
    add_column :renewal_details, :service_rate, :float
    add_column :renewal_details, :claim_rate, :float
    add_column :renewal_details, :claim_with_bussiness, :boolean
  end

end
