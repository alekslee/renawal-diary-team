# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_25_002205) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "car_specifications", force: :cascade do |t|
    t.string "name"
    t.string "value"
    t.string "trim"
    t.string "unit"
    t.integer "uid_car_specification"
    t.integer "uid_car_specification_value"
    t.integer "uid_car_trim"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cars", force: :cascade do |t|
    t.string "make"
    t.string "model"
    t.string "trim"
    t.integer "year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "car_type"
    t.index ["car_type"], name: "index_cars_on_car_type"
  end

  create_table "categories", force: :cascade do |t|
    t.string "en_name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "country_id"
    t.integer "parent_id"
    t.integer "lft"
    t.integer "rgt"
    t.integer "depth", default: 0
    t.integer "children_count", default: 0
    t.integer "users_categories_count", default: 0
    t.string "code"
    t.index ["country_id"], name: "index_categories_on_country_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "url"
    t.string "twitter"
    t.string "name"
    t.string "company_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_id"
    t.string "code"
    t.string "status"
    t.integer "sheet_index"
    t.index ["category_id"], name: "index_companies_on_category_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "en_name", null: false
    t.string "iso_a2_code", null: false
    t.string "default_lang_code", null: false
    t.jsonb "available_lang_codes", default: [], null: false
    t.bigint "users_countries_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["en_name"], name: "index_countries_on_en_name", unique: true
    t.index ["iso_a2_code"], name: "index_countries_on_iso_a2_code", unique: true
  end

  create_table "feedbacks", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.text "insights_comment"
    t.bigint "renewal_detail_id"
    t.index ["renewal_detail_id"], name: "index_feedbacks_on_renewal_detail_id"
    t.index ["user_id"], name: "index_feedbacks_on_user_id"
  end

  create_table "mobiles", force: :cascade do |t|
    t.string "make"
    t.string "model"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notes", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.integer "category_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_notes_on_category_id"
    t.index ["user_id"], name: "index_notes_on_user_id"
  end

  create_table "personalize_filters", force: :cascade do |t|
    t.jsonb "criteria_questions"
    t.bigint "user_id"
    t.bigint "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_active", default: true
    t.index ["category_id"], name: "index_personalize_filters_on_category_id"
    t.index ["user_id"], name: "index_personalize_filters_on_user_id"
  end

  create_table "renewal_details", force: :cascade do |t|
    t.string "checked_places"
    t.string "buying_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.bigint "category_id"
    t.bigint "current_provider_id"
    t.bigint "buying_type_company_id"
    t.date "last_buy_date"
    t.boolean "using_provider"
    t.jsonb "criteria_questions"
    t.jsonb "prices_questions"
    t.integer "current_provider_rate"
    t.integer "buying_type_company_rate"
    t.text "current_provider_rate_comment"
    t.text "buying_type_company_rate_comment"
    t.integer "year"
    t.float "price_rate"
    t.float "service_rate"
    t.float "claim_rate"
    t.boolean "claim_with_bussiness"
    t.index ["buying_type_company_id"], name: "index_renewal_details_on_buying_type_company_id"
    t.index ["category_id"], name: "index_renewal_details_on_category_id"
    t.index ["current_provider_id"], name: "index_renewal_details_on_current_provider_id"
    t.index ["user_id"], name: "index_renewal_details_on_user_id"
  end

  create_table "smart_reminders", force: :cascade do |t|
    t.datetime "triggered_at"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_id"
    t.bigint "renewal_detail_id"
    t.integer "year"
    t.bigint "user_id"
    t.datetime "deleted_at"
    t.string "notification_job_id"
    t.string "alias_name"
    t.string "reminder_type"
    t.index ["category_id"], name: "index_smart_reminders_on_category_id"
    t.index ["renewal_detail_id"], name: "index_smart_reminders_on_renewal_detail_id"
    t.index ["user_id"], name: "index_smart_reminders_on_user_id"
  end

  create_table "user_statistics", force: :cascade do |t|
    t.bigint "statisticable_id"
    t.string "statisticable_type"
    t.bigint "members_number", default: 0
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deleted_at"], name: "index_user_statistics_on_deleted_at", where: "(deleted_at IS NULL)"
    t.index ["statisticable_id", "statisticable_type"], name: "index_u_stats_by_statisticable_id_and_statisticable_type"
  end

  create_table "users", force: :cascade do |t|
    t.uuid "uid", default: -> { "uuid_generate_v4()" }
    t.string "display_name", default: "", null: false
    t.string "mobile_number", default: "", null: false
    t.string "email", default: "", null: false
    t.integer "bonus_points", default: 0, null: false
    t.string "encrypted_password", default: ""
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "deleted_at"
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.integer "invited_by_id"
    t.string "invited_by_type"
    t.string "country_code"
    t.string "state"
    t.string "state_code"
    t.string "state_district"
    t.string "city"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "facebook_uid"
    t.string "twitter_uid"
    t.string "google_uid"
    t.integer "invitations_count", default: 0
    t.string "linkedin_uid"
    t.float "lat"
    t.float "lng"
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.json "tokens"
    t.string "provider"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["deleted_at"], name: "index_users_on_deleted_at", where: "(deleted_at IS NULL)"
    t.index ["display_name", "deleted_at"], name: "index_users_on_display_name_and_deleted_at", unique: true
    t.index ["display_name"], name: "index_users_on_display_name"
    t.index ["email", "deleted_at"], name: "index_users_on_email_and_deleted_at", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["facebook_uid"], name: "index_users_on_facebook_uid", unique: true
    t.index ["google_uid"], name: "index_users_on_google_uid", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["linkedin_uid"], name: "index_users_on_linkedin_uid", unique: true
    t.index ["reset_password_token", "deleted_at"], name: "index_users_on_reset_password_token_and_deleted_at", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["twitter_uid"], name: "index_users_on_twitter_uid", unique: true
    t.index ["uid"], name: "index_users_on_uid", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  create_table "users_categories", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_users_categories_on_category_id"
    t.index ["user_id"], name: "index_users_categories_on_user_id"
  end

  create_table "users_countries", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "country_id", null: false
    t.boolean "primary", default: true
    t.string "language_code"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_id"], name: "index_users_countries_on_country_id"
    t.index ["deleted_at"], name: "index_users_countries_on_deleted_at", where: "(deleted_at IS NULL)"
    t.index ["user_id"], name: "index_users_countries_on_user_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "categories", "countries"
  add_foreign_key "companies", "categories"
  add_foreign_key "feedbacks", "renewal_details"
  add_foreign_key "feedbacks", "users"
  add_foreign_key "renewal_details", "categories"
  add_foreign_key "renewal_details", "companies", column: "buying_type_company_id"
  add_foreign_key "renewal_details", "companies", column: "current_provider_id"
  add_foreign_key "renewal_details", "users"
  add_foreign_key "smart_reminders", "categories"
  add_foreign_key "smart_reminders", "renewal_details"
  add_foreign_key "smart_reminders", "users"
  add_foreign_key "users_categories", "categories"
  add_foreign_key "users_categories", "users"
end
