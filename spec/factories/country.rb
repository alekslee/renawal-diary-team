# frozen_string_literal: true

FactoryBot.define do
  factory :country do
    sequence :en_name do |n|
      "#{Faker::Nation.nationality}#{n}"
    end
    sequence :iso_a2_code do |n|
      "#{Faker::Lorem.characters(1)}#{n}"
    end
    default_lang_code { 'en' }
    available_lang_codes do
      [Faker::Lorem.characters(2), Faker::Lorem.characters(2), Faker::Lorem.characters(2)]
    end
  end
end
