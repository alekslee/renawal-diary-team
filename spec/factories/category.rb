# frozen_string_literal: true

FactoryBot.define do
  factory :category do
    code { Faker::Lorem.characters(5) }
    en_name { Faker::Hobbit.location }
    country

    factory :leaf_category do
      parent_id { create(:category) }
    end
  end
end
