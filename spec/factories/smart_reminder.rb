# frozen_string_literal: true

FactoryBot.define do
  factory :smart_reminder do
    user
    category
    alias_name { Faker::Internet.user_name }
    reminder_type { SmartReminder.reminder_type.values.sample }
    triggered_at { Time.zone.today }
  end
end
