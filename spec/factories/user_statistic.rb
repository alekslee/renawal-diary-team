# frozen_string_literal: true

FactoryBot.define do
  factory :user_statistic do
    statisticable { user }
  end
end
