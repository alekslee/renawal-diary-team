# frozen_string_literal: true

FactoryBot.define do
  factory :feedback do
    category
    user
  end
end
