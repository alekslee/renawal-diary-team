# frozen_string_literal: true

FactoryBot.define do
  factory :users_category do
    user
    category
  end
end
