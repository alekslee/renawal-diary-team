# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    transient do
      categories_count { 0 }
    end

    display_name { Faker::Name.name_with_middle.underscore.tr(' ', '_') }
    mobile_number { Faker::PhoneNumber.cell_phone }
    email { Faker::Internet.email }
    uid { SecureRandom.uuid }
    lat { 40.7143528 }
    lng { -74.0059731 }
    password { Faker::Internet.password(10, 20, true, true) }
    password_confirmation { password }

    after :create do |user, evaluator|
      create_list(:category, evaluator.categories_count, users: [user])
    end
  end
end
