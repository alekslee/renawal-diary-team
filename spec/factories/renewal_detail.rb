# frozen_string_literal: true

FactoryBot.define do
  factory :renewal_detail do
    using_provider { true }
    buying_type { RenewalDetail.buying_type.values.sample }
    last_buy { LastBuyable::POSSIBLE_VALUES.sample }
    checked_places { RenewalDetail.checked_places.values.sample }
    current_provider { create(:company, company_type: :provider) }
    current_provider_rate { Faker::Number.between(1, 10) }
    buying_type_company_rate { Faker::Number.between(1, 10) }
    category
    user

    before :create do |renewal_detail|
      if renewal_detail.buying_type_company.nil? && renewal_detail.buying_type != :n_a
        renewal_detail.buying_type_company = create(:company, company_type: renewal_detail.buying_type)
      end
    end
  end
end
