# frozen_string_literal: true

FactoryBot.define do
  factory :company do
    transient do
      country { nil }
      buying_type_feedback_options { nil }
      buying_type_feedbacks_count { 0 }
    end

    name { Faker::FunnyName.name }
    url { Faker::Internet.url }
    twitter { Faker::Internet.url('twitter.com') }
    company_type { Company.company_type.values.sample }
    category

    after :create do |company, elevator|
      company.category.update(country: elevator.country) if elevator.country

      if elevator.buying_type_feedback_options
        company.buying_type_feedbacks << build(
          :feedback, elevator.buying_type_feedback_options.merge(buying_type: company.company_type)
        )
      end

      create_list(
        :feedback,
        elevator.buying_type_feedbacks_count,
        buying_type_company: company, buying_type: company.company_type
      )
    end
  end
end
