# frozen_string_literal: true

module InteractionHelper
  def stub_valid_interaction_classes(*klasses)
    klasses.each do |klass_obj|
      if klass_obj.is_a? Hash
        klass_obj.each do |klass, result|
          stub_valid_interaction_class(klass, result)
        end
      else
        stub_valid_interaction_class(klass_obj)
      end
    end
  end

  def stub_valid_interaction_class(interactor_class, result = nil)
    interactor = stub_valid_interactor_object(interactor_class, result)

    allow(interactor_class).to receive(:run).and_return(interactor)
  end

  def stub_valid_interactor_object(interactor_class, result = nil)
    interactor = instance_double(interactor_class)
    allow(interactor).to receive(:valid?).and_return(true)
    allow(interactor).to receive(:invalid?).and_return(false)
    allow(interactor).to receive(:result).and_return(result)
    interactor
  end

  # rubocop:disable RSpec/AnyInstance
  def stub_interactor_execute(interactor_class, result: nil, calls_original_before_stub: 0)
    calls_counter = 0
    allow_any_instance_of(interactor_class).to receive(:execute).and_wrap_original do |method, *args|
      calls_counter += 1
      if calls_counter <= calls_original_before_stub
        method.call(*args)
      else
        result
      end
    end
  end
  # rubocop:enable RSpec/AnyInstance
end

RSpec.configure do |config|
  config.include InteractionHelper, type: :interaction
end

RSpec::Matchers.define :run_interactor do |interactor_class|
  match do |interactor_proc|
    stub_valid_interaction_class(interactor_class)

    if @with_valid_inputs
      stub_interactor_execute(interactor_class, result: @result, calls_original_before_stub: @with_recursion ? 1 : 0)
      allow(interactor_class).to receive(:run).and_wrap_original do |m, inputs|
        @interactor_obj = m.call(inputs)
      end
    end
    interactor_proc.call

    matcher = retrieve_matcher
    expect(interactor_class).to(matcher) && (@interactor_obj.nil? || @interactor_obj.valid?)
  rescue RSpec::Expectations::ExpectationNotMetError => e
    @failure_message = e.message
    false
  end

  chain :and_return do |result|
    @result = result
  end

  chain :with_recursion do
    @with_recursion = true
  end

  chain :exactly do |times|
    @times = times
  end

  chain :times do
  end

  chain :once do
    @times = 1
  end

  chain :twice do
    @times = 2
  end

  chain :with do |params|
    @params = params
  end

  chain :with_valid_inputs do
    @with_valid_inputs = true
  end

  failure_message do |_interactor_proc|
    @failure_message || super() + failure_reason
  end

  supports_block_expectations

  private

  def retrieve_matcher
    matcher = have_received(:run)
    matcher = matcher.exactly(@times).times if @times
    matcher = matcher.with(@params) if @params
    matcher
  end

  def failure_reason
    return '' if @interactor_obj.nil? || @interactor_obj.valid?

    error_messages = @interactor_obj.errors.full_messages.join(";\n")
    ", but receive this errors: \n#{error_messages}"
  end
end
