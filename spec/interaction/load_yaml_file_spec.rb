# frozen_string_literal: true

RSpec.describe LoadYamlFile do
  describe '#execute' do
    it 'deeps symbolize keys' do
      allow(YAML).to receive(:load_file).and_return('key1' => 'value1', 'key2' => { 'key3' => 'value3' })
      result = described_class.run!(path: 'test')
      expect(result).to include(:key1, key2: { key3: 'value3' })
    end

    it 'rescues Errno::ENOENT error' do
      allow(YAML).to receive(:load_file).and_raise(Errno::ENOENT, 'Test message')
      result = described_class.run(path: 'test')
      expect(result).not_to be_valid
      expect(result.errors.messages[:load_yaml_file]).to include(a_string_matching('Test message'))
    end

    it 'rescues Psych::Exception error' do
      allow(YAML).to receive(:load_file).and_raise(Psych::Exception, 'Test message')
      result = described_class.run(path: 'test')
      expect(result).not_to be_valid
      expect(result.errors.messages[:load_yaml_file]).to include(a_string_matching('Test message'))
    end
  end
end
