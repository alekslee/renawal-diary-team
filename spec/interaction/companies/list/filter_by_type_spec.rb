# frozen_string_literal: true

RSpec.describe Companies::List::FilterByType, type: :interaction do
  describe '#execute' do
    it 'returns relation object' do
      inputs = { companies: Company.none, company_type: :provider }
      expect(described_class.run(inputs).result).to be_kind_of ActiveRecord::Relation
    end

    it 'returns companies with correct company type' do
      companies = create_list(:company, rand(1..3), company_type: :provider)
      inputs = { companies: Company.all, company_type: :provider }
      expect(described_class.run(inputs).result.to_a).to match_array companies
    end

    it 'doesn\'t return companies with incorrect company type' do
      companies = create_list(:company, rand(1..2), company_type: :provider)
      _companies_not_included = create_list(:company, rand(1..2), company_type: :broker)
      inputs = { companies: Company.all, company_type: :provider }
      expect(described_class.run(inputs).result.to_a).to match_array companies
    end
  end
end
