# frozen_string_literal: true

RSpec.describe Companies::List::Paginate, type: :interaction do
  describe '#validation' do
    it 'adds error if page less than 1' do
      interaction = described_class.new(companies: Company.none, page: 0)
      expect(interaction).not_to be_valid
    end
  end

  describe '#execute' do
    it 'returns companies per page' do
      companies = create_list(:company, 5)
      result = described_class.run(companies: Company.where(id: companies.map(&:id)), page: 2, per_page: 2).result
      expect(result).to eq companies[2..3]
    end

    it 'returns companies array with rank' do
      companies = create_list(:company, 5)
      result = described_class.run(companies: Company.where(id: companies.map(&:id)), page: 2, per_page: 2).result
      expect(result.map(&:rank)).to eq [3, 4]
    end
  end

  describe '#retrieve_offset' do
    it 'returns correct offset for first page' do
      interaction = described_class.new(companies: Company.none, page: 2, per_page: 10)
      expect(interaction.send(:retrieve_offset)).to eq 10
    end
  end

  describe '#retrieve_limit' do
    it 'returns correct limit' do
      interaction = described_class.new(companies: Company.none, per_page: 10)
      offset = rand(1..100)
      expect(interaction.send(:retrieve_limit, offset)).to eq offset + 10
    end
  end

  describe '#add_rank' do
    it 'adds rank to companies' do
      companies = create_list(:company, 5)
      interaction = described_class.new(companies: Company.none)
      result = interaction.send(:add_rank, companies, 10)

      expect(result.map(&:rank)).to eq [11, 12, 13, 14, 15]
    end

    it 'returns empty array if companies doesn\'t present' do
      interaction = described_class.new(companies: Company.none)
      result = interaction.send(:add_rank, nil, 10)

      expect(result).to eq []
    end
  end
end
