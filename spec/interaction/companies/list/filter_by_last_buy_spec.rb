# frozen_string_literal: true

RSpec.describe Companies::List::FilterByLastBuy, type: :interaction do
  describe '#execute' do
    let(:inputs) { { companies: Company.all.joins(:buying_type_feedbacks), last_buy: :this_week } }

    it 'returns relation object' do
      expect(described_class.run(inputs).result).to be_kind_of ActiveRecord::Relation
    end

    it 'returns companies with correct company type' do
      companies = create_list(:company, rand(1..3), buying_type_feedback_options: { last_buy: :today })
      expect(described_class.run(inputs).result.to_a).to match_array companies
    end

    it 'doesn\'t return companies with incorrect company type' do
      companies = create_list(:company, rand(1..2), buying_type_feedback_options: { last_buy: :today })
      _companies_not_included = create_list(:company, rand(1..2), buying_type_feedback_options: { last_buy: :other })
      expect(described_class.run(inputs).result.to_a).to match_array companies
    end
  end
end
