# frozen_string_literal: true

RSpec.describe Companies::List::FilterByLeafCategory, type: :interaction do
  describe '#execute' do
    let(:category) { create(:category) }

    it 'returns relation object' do
      inputs = { companies: Company.all.joins(:buying_type_feedbacks), category_id: 0 }
      expect(described_class.run(inputs).result).to be_kind_of ActiveRecord::Relation
    end

    it 'returns companies with correct leaf category' do
      companies = create_list(:company, rand(1..3), buying_type_feedback_options: { category_id: category.id })
      inputs = { companies: Company.all.joins(:buying_type_feedbacks), category_id: category.id }
      expect(described_class.run(inputs).result.to_a).to match_array companies
    end

    it 'doesn\'t return companies with incorrect company type' do
      category2 = create(:category)
      companies = create_list(:company, rand(1..2), buying_type_feedback_options: { category_id: category.id })
      _companies_not_included = create_list(
        :company, rand(1..2), buying_type_feedback_options: { category_id: category2.id }
      )
      inputs = { companies: Company.all.joins(:buying_type_feedbacks), category_id: category.id }
      expect(described_class.run(inputs).result.to_a).to match_array companies
    end
  end
end
