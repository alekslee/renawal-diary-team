# frozen_string_literal: true

RSpec.describe Companies::List::RetrieveTotalCount, type: :interaction do
  describe '#execute' do
    it 'returns companies count' do
      create_list(:company, 2)
      expect(described_class.run(companies: Company.all).result).to eq 2
    end

    it 'counts only uniq companies' do
      create_list(:company, 2, buying_type_feedbacks_count: 2)
      inputs = { companies: Company.all.left_joins(:buying_type_feedbacks) }
      expect(described_class.run(inputs).result).to eq Company.all.count
    end
  end
end
