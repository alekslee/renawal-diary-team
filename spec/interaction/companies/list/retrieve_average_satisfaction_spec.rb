# frozen_string_literal: true

RSpec.describe Companies::List::RetrieveAverageSatisfaction, type: :interaction do
  describe '#execute' do
    it 'returns hash with average rate' do
      companies = create_list(:company, 3, buying_type_feedback_options: { buying_type_company_rate: 4 })
      result = described_class.run(companies: retrieve_companies_relation(companies)).result
      expect(result).to eq(companies[0].id => 4, companies[1].id => 4, companies[2].id => 4)
    end

    it 'returns hash with average rate when company_type is provider' do
      companies = create_list(:company, 3, buying_type_feedback_options: { current_provider_rate: 4 })
      result = described_class.run(companies: retrieve_companies_relation(companies), company_type: :provider).result
      expect(result).to eq(companies[0].id => 4, companies[1].id => 4, companies[2].id => 4)
    end
  end

  describe '#retrieve_field_rate' do
    it 'returns current_provider_rate when company_type is not provider' do
      interaction = described_class.new(companies: Company.none, company_type: :provider)
      result = interaction.send(:retrieve_field_rate)
      expect(result).to eq :current_provider_rate
    end

    it 'returns buying_type_company_rate when company_type is provider' do
      interaction = described_class.new(companies: Company.none, company_type: :broker)
      result = interaction.send(:retrieve_field_rate)
      expect(result).to eq :buying_type_company_rate
    end
  end

  def retrieve_companies_relation(companies)
    Company.joins(:buying_type_feedbacks).where(id: companies.map(&:id))
  end
end
