# frozen_string_literal: true

RSpec.describe Companies::List::FilterByCheckedPlaces, type: :interaction do
  describe '#execute' do
    it 'returns relation object' do
      inputs = { companies: Company.all.joins(:buying_type_feedbacks), checked_places: :one_place }
      expect(described_class.run(inputs).result).to be_kind_of ActiveRecord::Relation
    end

    it 'returns companies with correct checked_places' do
      companies = create_list(:company, rand(1..3), buying_type_feedback_options: { checked_places: :one_place })
      inputs = { companies: Company.all.joins(:buying_type_feedbacks), checked_places: :one_place }
      expect(described_class.run(inputs).result.to_a).to match_array companies
    end

    it 'doesn\'t return companies with incorrect company type' do
      companies = create_list(:company, rand(1..2), buying_type_feedback_options: { checked_places: :one_place })
      _companies_not_included = create_list(
        :company, rand(1..2), buying_type_feedback_options: { checked_places: :two_places }
      )
      inputs = { companies: Company.all.joins(:buying_type_feedbacks), checked_places: :one_place }
      expect(described_class.run(inputs).result.to_a).to match_array companies
    end
  end

  describe '#retrieve_checked_places_clause' do
    it 'returns array with two_places and three places or more with two_places_or_more filter' do
      interaction = described_class.new(companies: Company.none, checked_places: :two_places_or_more)
      expect(interaction.send(:retrieve_checked_places_clause)).to eq %i[two_places three_places_or_more]
    end

    it 'returns checked_places filter if checked_places doesn\'t equal to two_places_or_more filter' do
      interaction = described_class.new(companies: Company.none, checked_places: :one_place)
      expect(interaction.send(:retrieve_checked_places_clause)).to eq :one_place
    end
  end
end
