# frozen_string_literal: true

RSpec.describe Companies::List::FilterByCountries, type: :interaction do
  describe '#execute' do
    it 'doesn\'t valid if country code is not allowed' do
      inputs = { companies: Company.none, country_code: :wrong }
      interaction = described_class.run(inputs)
      expect(interaction).not_to be_valid
    end

    it 'valids if country code is allowed' do
      inputs = { companies: Company.none, country_code: :ie }
      interaction = described_class.run(inputs)
      expect(interaction).to be_valid
    end

    it 'returns relation object' do
      inputs = { companies: Company.none, country_code: :ie }
      result = described_class.run(inputs).result
      expect(result).to be_kind_of ActiveRecord::Relation
    end

    it 'returns companies for current country' do
      country = create(:country, iso_a2_code: :ua)
      category = create(:category, country: country)
      companies = create_list(:company, rand(1..3), category: category)
      inputs = { companies: Company.all, country_code: country.code }
      result = described_class.run(inputs).result
      expect(result.to_a).to match_array companies
    end

    it 'doesn\'t return companies for not current country', :aggregate_failures do
      country = create(:country, iso_a2_code: :ua)
      country2 = create(:country, iso_a2_code: :us)

      category = create(:category, country: country)
      category2 = create(:category, country: country2)

      companies_included = create_list(:company, rand(1..5), category: category)
      _companies_not_included = create_list(:company, rand(1..5), category: category2)

      inputs = { companies: Company.all, country_code: country.code }
      result = described_class.run(inputs).result
      expect(result.to_a).to match_array companies_included
    end
  end
end
