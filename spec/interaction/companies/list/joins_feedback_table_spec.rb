# frozen_string_literal: true

RSpec.describe Companies::List::JoinsFeedbackTable, type: :interaction do
  describe '#execute' do
    it 'left_joins feedbacks table' do
      inputs = { companies: Company.none, company_type: attributes_for(:company)[:company_type] }
      interaction = described_class.run(inputs)
      expect(interaction.result.left_outer_joins_values).to match_array [interaction.send(:retrieve_joins_table_name)]
    end

    it 'returns relation object' do
      inputs = { companies: Company.none, company_type: attributes_for(:company)[:company_type] }
      interaction = described_class.run(inputs)
      expect(interaction.result).to be_kind_of ActiveRecord::Relation
    end
  end

  describe '#retrieve_joins_table_name' do
    it 'returns :current_feedbacks if company type is provider' do
      inputs = { companies: Company.none, company_type: :provider }
      interaction = described_class.new(inputs)
      expect(interaction.send(:retrieve_joins_table_name)).to eq :current_feedbacks
    end

    it 'returns :buying_type_companies if company type is company type is not provider' do
      inputs = { companies: Company.none, company_type: :not_provider }
      interaction = described_class.new(inputs)
      expect(interaction.send(:retrieve_joins_table_name)).to eq :buying_type_feedbacks
    end
  end
end
