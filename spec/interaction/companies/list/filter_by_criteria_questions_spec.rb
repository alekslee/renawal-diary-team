# frozen_string_literal: true

RSpec.describe Companies::List::FilterByCriteriaQuestions, type: :interaction do
  include_context 'with criteria_questions'

  describe '#execute' do
    subject { described_class.run(inputs).result }

    describe 'returns companies with age' do
      let(:criteria_question) { questions[:age] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with male_femail' do
      let(:criteria_question) { questions[:male_femail] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with location' do
      let(:criteria_question) { questions[:location] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with location when RenewalDetail criteria_question lng is string' do
      let(:criteria_question) { { location: questions[:location] } }
      let(:renewal_details) do
        create(:renewal_detail,
          current_provider: company,
          criteria_questions: { location: {
            lat: questions[:location][:lat],
            lng: questions[:location][:lng].to_s,
            address: questions[:location][:address],
            contryCode: questions[:location][:countryCode]
          } },
          last_buy: :today)
      end

      it { is_expected.to match_array company }
    end

    describe 'returns companies with location when criteria_question lng is string' do
      let(:criteria_question) { { location: questions[:location] } }
      let(:inputs) { { companies: Company.joins(:current_renewal_details), criteria_questions: location_question } }
      let(:location_question) do
        { location: {
          lat: questions[:location][:lat],
            lng: questions[:location][:lng].to_s,
            address: questions[:location][:address],
            contryCode: questions[:location][:countryCode]
        } }
      end

      it { is_expected.to match_array company }
    end

    describe 'returns companies with has_anyone_policy_claim_past_5_years' do
      let(:criteria_question) { questions[:has_anyone_policy_claim_past_5_years] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with energy_type' do
      let(:criteria_question) { questions[:energy_type] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with paying_method' do
      let(:criteria_question) { questions[:paying_method] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with energy_usage' do
      let(:criteria_question) { questions[:energy_usage] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with contract_type' do
      let(:criteria_question) { questions[:contract_type] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with contract_length' do
      let(:criteria_question) { questions[:contract_length] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with energy_supply_region' do
      let(:criteria_question) { questions[:energy_supply_region] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with having_saving_metter' do
      let(:criteria_question) { questions[:having_saving_metter] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with important_to_you' do
      let(:criteria_question) { questions[:important_to_you] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with speed_is' do
      let(:criteria_question) { questions[:speed_is] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with broadband_type' do
      let(:criteria_question) { questions[:broadband_type] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with broadband_package' do
      let(:criteria_question) { questions[:broadband_package] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with most_important_to_you' do
      let(:criteria_question) { questions[:most_important_to_you] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with monthly_usage_allowance' do
      let(:criteria_question) { questions[:monthly_usage_allowance] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with years_driving_experience' do
      let(:criteria_question) { questions[:years_driving_experience] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with years_no_claims_bonus' do
      let(:criteria_question) { questions[:years_no_claims_bonus] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with type_of_cover_required' do
      let(:criteria_question) { questions[:type_of_cover_required] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with no_of_drivers' do
      let(:criteria_question) { questions[:no_of_drivers] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with car_age' do
      let(:criteria_question) { questions[:car_age] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with animal_type' do
      let(:criteria_question) { questions[:animal_type] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with insurance_type' do
      let(:criteria_question) { questions[:insurance_type] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with who_is_traveling' do
      let(:criteria_question) { questions[:who_is_traveling] }

      it { is_expected.to match_array company }
    end

    describe 'returns companies with traveling_place' do
      let(:criteria_question) { questions[:traveling_place] }

      it { is_expected.to match_array company }
    end
  end
end
