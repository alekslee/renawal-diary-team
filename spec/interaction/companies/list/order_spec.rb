# frozen_string_literal: true

RSpec.describe Companies::List::Order, type: :interaction do
  describe '#execute' do
    it 'orders by renewal details rate nps' do
      reneval_detail3 = create(:renewal_detail, current_provider_rate: 7)
      reneval_detail1 = create(:renewal_detail, current_provider_rate: 10)
      reneval_detail2 = create(:renewal_detail, current_provider_rate: 8)

      company1 = reneval_detail1.current_provider
      company2 = reneval_detail2.current_provider
      company3 = reneval_detail3.current_provider

      companies_array = [company1, company2, company3]
      result = described_class.run(
        companies: Company.where(id: companies_array.map(&:id)).left_joins(:current_renewal_details),
        company_type: :provider
      ).result
      expect(result.to_a).to eq companies_array
    end
  end
end
