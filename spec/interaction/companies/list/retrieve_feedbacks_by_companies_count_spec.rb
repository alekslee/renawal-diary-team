# frozen_string_literal: true

RSpec.describe Companies::List::RetrieveFeedbacksByCompaniesCount, type: :interaction do
  describe '#execute' do
    it 'returns correct feedbacks count for companies ids' do
      company = create(:company, buying_type_feedbacks_count: 2)
      result = described_class.run(companies: Company.all.left_joins(:buying_type_feedbacks).group(:id)).result
      expect(result[company.id]).to eq 2
    end
  end
end
