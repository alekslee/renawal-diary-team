# frozen_string_literal: true

RSpec.describe Companies::List::AddDynamicVariables, type: :interaction do
  describe '#execute' do
    it 'adds members to every company element' do
      companies = create_list(:company, 3)
      inputs = {
        companies: companies,
        feedbacks_count: { companies[0].id => 2, companies[1].id => 4, companies[2].id => 8 },
        average_satisfaction: { companies[0].id => 7, companies[1].id => 9, companies[2].id => 8 }
      }
      result = described_class.run(inputs).result
      expect(result.map(&:members)).to eq [14.3, 28.6, 57.1]
    end

    it 'adds insights to every company element' do
      companies = create_list(:company, 3)
      inputs = {
        companies: companies,
        feedbacks_count: { companies[0].id => 2, companies[1].id => 4, companies[2].id => 8 },
        average_satisfaction: { companies[0].id => 7, companies[1].id => 9, companies[2].id => 8 }
      }
      result = described_class.run(inputs).result
      expect(result.map(&:insights)).to eq [2, 4, 8]
    end

    it 'adds satisfaction to every company element' do
      companies = create_list(:company, 3)
      inputs = {
        companies: companies,
        feedbacks_count: { companies[0].id => 2, companies[1].id => 4, companies[2].id => 8 },
        average_satisfaction: { companies[0].id => 7, companies[1].id => 9.444444444445, companies[2].id => nil }
      }
      result = described_class.run(inputs).result
      expect(result.map(&:satisfaction)).to eq [7, 9.4, nil]
    end
  end

  describe '#retrieve_total_feedbacks_count' do
    it 'returns sum of feedbacks_count values' do
      interaction = described_class.new(companies: [], feedbacks_count: { 1 => 2, 2 => 3, 3 => 1 })
      result = interaction.send(:retrieve_total_feedbacks_count)
      expect(result).to eq 6
    end
  end

  describe '#calculate_percent' do
    it 'returns correct percents' do
      company = create(:company)
      interaction = described_class.new(companies: [], feedbacks_count: { company.id => 2 })
      result = interaction.send(:calculate_percent, company, 4)
      expect(result).to eq 50
    end
  end
end
