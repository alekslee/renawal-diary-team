# frozen_string_literal: true

RSpec.describe Companies::List::RetrieveAllCompanies, type: :interaction do
  describe '#execute' do
    let(:inputs) { { user: create(:user), category_id: 0 } }

    it 'returns relation object' do
      expect(interaction_result).to be_kind_of ActiveRecord::Relation
    end

    it 'returns companies inside all same categories within all countries' do
      category = create(:leaf_category)
      second_category = create(:leaf_category, code: category.code)
      company1 = create(:company, category: category)
      company2 = create(:company, category: second_category)
      expect(interaction_result(category: category)).to include(company1, company2)
    end

    it 'doesn\'t return companies inside different category', :aggregate_failures do
      category = create(:leaf_category)
      second_category = create(:leaf_category, code: "#{category.code}dif")
      company1 = create(:company, category: category)
      company2 = create(:company, category: second_category)
      result = interaction_result(category: category)
      expect(result).to include(company1)
      expect(result).not_to include(company2)
    end
  end

  def interaction_result(category: create(:leaf_category))
    stub_valid_interaction_classes(Categories::FindUsersLeafCategory => category)
    described_class.run(inputs).result
  end
end
