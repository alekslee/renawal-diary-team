# frozen_string_literal: true

RSpec.describe Companies::Parser::ParseSpreadsheet, type: :interaction do
  describe '#execute' do
    let(:category) { create(:category) }
    let(:spreadsheet) { stub_google_spreadsheet }
    let(:inputs) { { spreadsheet: spreadsheet, category: category } }

    it 'runs RetrieveTmpSpreadsheetPath interaction' do
      stub_interaction(except_interaction_klass: Companies::Parser::RetrieveTmpSpreadsheetPath)

      expect { described_class.run(inputs) }.to run_interactor(Companies::Parser::RetrieveTmpSpreadsheetPath)
        .with_valid_inputs
    end

    it 'exports spreadsheet to the file' do
      stub_interaction

      described_class.run(inputs)
      expect(spreadsheet).to have_received(:export_as_file).with('test/path.xlsx')
    end

    it 'runs ParseXlsx interactor' do
      stub_interaction(except_interaction_klass: Companies::Parser::ParseXlsx)

      expect { described_class.run(inputs) }.to run_interactor(Companies::Parser::ParseXlsx).with_valid_inputs
    end

    context 'when category companies changed' do
      before { category.companies_changed = true }

      it 'runs RegenerateXlsx::Generate interaction' do
        stub_interaction(except_interaction_klass: Companies::Parser::RegenerateXlsx::Generate)

        expect { described_class.run(inputs) }.to run_interactor(Companies::Parser::RegenerateXlsx::Generate)
          .with_valid_inputs
      end

      it 'runs UpdateSpreadsheetOnGoogle interaction' do
        stub_interaction(except_interaction_klass: Companies::Parser::UpdateSpreadsheetOnGoogle)

        expect { described_class.run(inputs) }.to run_interactor(Companies::Parser::UpdateSpreadsheetOnGoogle)
          .with_valid_inputs
      end

      it 'doesn\'t remove file directly in current interaction' do
        stub_interaction

        described_class.run(inputs)
        expect(FileUtils).not_to have_received(:rm)
      end
    end

    it 'doesn\'t run RegenerateXlsx::Generate interaction' do
      stub_interaction(except_interaction_klass: Companies::Parser::RegenerateXlsx::Generate)

      expect { described_class.run(inputs) }.not_to run_interactor(Companies::Parser::RegenerateXlsx::Generate)
        .with_valid_inputs
    end

    it 'doesn\'t run UpdateSpreadsheetOnGoogle interaction' do
      stub_interaction(except_interaction_klass: Companies::Parser::UpdateSpreadsheetOnGoogle)

      expect { described_class.run(inputs) }.not_to run_interactor(Companies::Parser::UpdateSpreadsheetOnGoogle)
        .with_valid_inputs
    end

    it 'removes file directly in current interaction' do
      stub_interaction

      described_class.run(inputs)
      expect(FileUtils).to have_received(:rm).with('test/path.xlsx')
    end

    def stub_interaction(except_interaction_klass: nil)
      interaction_classes = [
        { Companies::Parser::RetrieveTmpSpreadsheetPath => 'test/path.xlsx' },
        Companies::Parser::ParseXlsx,
        Companies::Parser::RegenerateXlsx::Generate,
        Companies::Parser::UpdateSpreadsheetOnGoogle
      ].reject { |klass| except_interaction_klass == (klass.is_a?(Hash) ? klass.keys[0] : klass) }

      stub_valid_interaction_classes(*interaction_classes)
      stub_roo_spreadsheet
      stub_file_utils
    end

    def stub_roo_spreadsheet(xlsx: stub_xlsx)
      class_double(Roo::Spreadsheet).as_stubbed_const
      allow(Roo::Spreadsheet).to receive(:open).and_return(xlsx)
    end

    def stub_xlsx
      xlsx_stub = instance_double(Roo::Excelx)
      allow(xlsx_stub).to receive(:is_a?).with(Roo::Excelx).and_return(true)
      xlsx_stub
    end

    def stub_file_utils
      class_double(FileUtils).as_stubbed_const
      allow(FileUtils).to receive(:rm)
    end

    # rubocop:disable RSpec/VerifiedDoubles
    def stub_google_spreadsheet
      spreadsheet = double(GoogleDrive::Spreadsheet, id: 'test_id')
      allow(spreadsheet).to receive(:is_a?).with(GoogleDrive::Spreadsheet).and_return(true)
      allow(spreadsheet).to receive(:export_as_file)
      spreadsheet
    end
    # rubocop:enable RSpec/VerifiedDoubles
  end
end
