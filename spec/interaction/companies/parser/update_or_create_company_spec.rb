# frozen_string_literal: true

RSpec.describe Companies::Parser::UpdateOrCreateCompany, type: :interaction do
  let(:company) { create(:company) }
  let(:category) { company.category }

  describe '#execute' do
    context 'when company exists and company attributes doesn\'t changed' do
      let(:inputs) do
        company.as_json(only: %i[name url twitter company_type code category]).symbolize_keys.merge(category: category)
      end

      it 'doesn\'t set companies changed flag to the category' do
        expect { described_class.run(inputs) }.not_to change(category, :companies_changed)
      end

      it 'doesn\'t run companies update interactor' do
        expect { described_class.run(inputs) }.not_to run_interactor(Companies::Update)
      end

      it 'doesn\'t run companies create interactor' do
        expect { described_class.run(inputs) }.not_to run_interactor(Companies::Create)
      end

      it 'returns company' do
        expect(described_class.run(inputs).result).to eq company
      end
    end

    context 'when company exists and company attributes changed' do
      let(:inputs) do
        name = company.name.dup
        name[1] = 'dif'
        company.as_json(only: %i[url twitter company_type code category])
               .symbolize_keys
               .merge(category: category, name: name)
      end

      it 'sets companies changed flag to the category' do
        expect { described_class.run(inputs) }.to change(category, :companies_changed).from(nil).to(true)
      end

      it 'runs companies update interactor' do
        expect { described_class.run(inputs) }.to run_interactor(Companies::Update).with_valid_inputs
      end

      it 'doesn\'t run companies create interactor' do
        stub_valid_interaction_class(Companies::Update)
        expect { described_class.run(inputs) }.not_to run_interactor(Companies::Create)
      end

      it 'returns company' do
        stub_valid_interaction_class(Companies::Update, company)
        expect(described_class.run(inputs).result).to eq company
      end
    end

    context 'when company doesn\'t exists' do
      let(:inputs) do
        name = company.name.dup
        name[1] = 'dif'
        company.as_json(only: %i[url twitter company_type category])
               .symbolize_keys
               .merge(category: category, name: name)
      end

      it 'sets companies changed flag to the category' do
        expect { described_class.run(inputs) }.to change(category, :companies_changed).from(nil).to(true)
      end

      it 'doesn\'t run companies update interactor' do
        stub_valid_interaction_class(Companies::Create)
        expect { described_class.run(inputs) }.not_to run_interactor(Companies::Update)
      end

      it 'runs companies create interactor' do
        expect { described_class.run(inputs) }.to run_interactor(Companies::Create)
      end

      it 'returns created company' do
        stub_valid_interaction_class(Companies::Create, company)
        expect(described_class.run(inputs).result).to eq company
      end
    end
  end

  describe '#retrieve_company' do
    it 'returns nil if code nil' do
      interaction = described_class.new(company.as_json.merge(code: nil, category: category))
      expect(interaction.send(:retrieve_company)).to be nil
    end

    it 'returns nil if company types is different' do
      interaction = described_class.new(company.as_json.merge(company_type: 'something_wrong', category: category))
      expect(interaction.send(:retrieve_company)).to be nil
    end

    it 'returns nil if code is different' do
      interaction = described_class.new(company.as_json.merge(code: 'something_wrong', category: category))
      expect(interaction.send(:retrieve_company)).to be nil
    end

    it 'returns company if code and company type is correct' do
      interaction = described_class.new(company.as_json.merge(category: category))
      expect(interaction.send(:retrieve_company)).to be_truthy
    end
  end

  describe '#company_attributes_doesnt_change' do
    it 'returns false if have different attributes' do
      name = company.name.dup
      name[1] = 'dif'
      interaction = described_class.new(company.as_json.merge(name: name, category: category))
      expect(interaction.send(:company_attributes_doesnt_change?, company)).to be false
    end

    it 'returns true if doesn\'t have different attributes' do
      interaction = described_class.new(company.as_json(include: :category))
      expect(interaction.send(:company_attributes_doesnt_change?, company)).to be true
    end
  end
end
