# frozen_string_literal: true

RSpec.describe Companies::Parser::ParseSheet, type: :interaction do
  let(:category) { create(:category) }
  let(:rows) do
    rows = [%w[first second third]]
    rand(1..10).times do
      rows.push(name: Faker::ChuckNorris.fact, url: Faker::ChuckNorris.fact, twitter: Faker::ChuckNorris.fact)
    end
    rows
  end

  describe '#execute' do
    it 'runs UpdateOrCreateCompany interaction n - 1 times where n is the rows count inside the sheet' do
      inputs = { company_type: 'test', sheet: stub_sheet(rows: rows), category: category }
      expect { described_class.run(inputs) }.to run_interactor(Companies::Parser::UpdateOrCreateCompany)
        .with_valid_inputs
        .exactly(rows.count - 1)
        .times
    end
  end

  describe '#retrieve_parse_options' do
    it 'returns headers only if they present in the headers row' do
      stub_xlsx_headers(headers: { first: 'first', second: 'second', apple: 'apple' })
      sheet = stub_sheet(rows: [%w[first second third]])

      result = described_class.new(company_type: 'tst', sheet: sheet, category: category).send(:retrieve_parse_options)
      expect(result).to eq(first: 'first', second: 'second')
    end
  end

  def stub_sheet(rows: [])
    sheet = instance_double(Roo::Excelx)
    allow(sheet).to receive(:is_a?).with(Roo::Excelx).and_return(true)
    allow(sheet).to receive(:each_with_index) do |_opts, &block|
      rows.each_with_index(&block)
    end
    allow(sheet).to receive(:row).with(1).and_return(rows[0])
    sheet
  end

  def stub_xlsx_headers(headers: [])
    stub_const('Company::XLSX_HEADERS', headers)
  end
end
