# frozen_string_literal: true

RSpec.describe Companies::Parser::ParseXlsx, type: :interaction do
  describe '#execute' do
    let(:category) { create(:category) }
    let(:pages) do
      pages = {}
      rand(1..10).times { pages.merge(stub_page) }
      pages
    end

    it 'runs ParseSheet interaction n times where n is count of xlsx pages' do
      inputs = { xlsx: stub_xlsx(pages: pages), category: category }
      expect { described_class.run(inputs) }.to run_interactor(Companies::Parser::ParseSheet)
        .with_valid_inputs
        .exactly(pages.count)
        .times
    end

    def stub_xlsx(pages: [])
      xlsx = instance_double(Roo::Excelx)
      allow(xlsx).to receive(:is_a?).with(Roo::Excelx).and_return(true)
      allow(xlsx).to receive(:each_with_pagename) do |&block|
        pages.each(&block)
      end
      xlsx
    end

    def stub_page(name: Faker::Color.color_name, sheet: stub_xlsx)
      { name => sheet }
    end
  end
end
