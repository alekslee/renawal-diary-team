# frozen_string_literal: true

RSpec.describe Companies::Parser::Synchronize, type: :interaction do
  describe '#execute' do
    it 'runs parse root folder interactor' do
      stub_google_drive_session
      expect { described_class.run }.to run_interactor(Companies::Parser::ParseRootFolder)
    end

    def stub_google_drive_session
      folder_stub = instance_double(GoogleDrive::Collection)

      session_stub = instance_double('GoogleDrive::Session')
      allow(session_stub).to receive(:folder_by_id).and_return(folder_stub)

      allow(GoogleDrive::Session).to receive(:from_config).and_return(session_stub)
      folder_stub
    end
  end
end
