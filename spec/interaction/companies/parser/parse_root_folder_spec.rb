# frozen_string_literal: true

RSpec.describe Companies::Parser::ParseRootFolder, type: :interaction do
  describe '#execute' do
    it 'runs ParseCountryFolder interaction if finds country' do
      country = create(:country)
      country_folder = stub_folder(name: country.iso_a2_code)
      inputs = { folder: stub_folder(subfolders: [country_folder]) }
      expect { described_class.run(inputs) }.to run_interactor(Companies::Parser::ParseCountryFolder).with_valid_inputs
    end

    it 'adds error if country doesn\'t found' do
      country_folder = stub_folder(name: 'test')
      inputs = { folder: stub_folder(subfolders: [country_folder]) }
      expect(described_class.run(inputs)).not_to be_valid
    end

    def stub_folder(subfolders: [], name: '')
      folder_stub = instance_double(GoogleDrive::Collection)
      allow(folder_stub).to receive(:is_a?).with(GoogleDrive::Collection).and_return(true)
      allow(folder_stub).to receive(:subfolders).and_return(subfolders)
      allow(folder_stub).to receive(:name).and_return(name)
      folder_stub
    end
  end
end
