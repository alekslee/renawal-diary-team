# frozen_string_literal: true

RSpec.describe Companies::Parser::ParseCountryFolder, type: :interaction do
  describe '#execute' do
    it 'runs ParseSpreadsheet interaction if finds category' do
      category = create(:category)
      spreadsheet = stub_spreadsheet(name: category.code)
      inputs = { country: category.country, country_folder: stub_folder(spreadsheets: [spreadsheet]) }

      expect { described_class.run(inputs) }.to run_interactor(Companies::Parser::ParseSpreadsheet).with_valid_inputs
    end

    it 'adds error if country doesn\'t found' do
      spreadsheet = stub_spreadsheet(name: 'test')
      inputs = { country: create(:country), folder: stub_folder(spreadsheets: [spreadsheet]) }

      expect(described_class.run(inputs)).not_to be_valid
    end

    def stub_folder(spreadsheets: [])
      folder_stub = instance_double(GoogleDrive::Collection)
      allow(folder_stub).to receive(:is_a?).with(GoogleDrive::Collection).and_return(true)
      allow(folder_stub).to receive(:spreadsheets).and_return(spreadsheets)
      folder_stub
    end

    def stub_spreadsheet(name: '')
      spreadsheet_stub = instance_double(GoogleDrive::Spreadsheet)
      allow(spreadsheet_stub).to receive(:is_a?).with(GoogleDrive::Spreadsheet).and_return(true)
      allow(spreadsheet_stub).to receive(:name).and_return(name)
      spreadsheet_stub
    end
  end
end
