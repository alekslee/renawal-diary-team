# frozen_string_literal: true

RSpec.describe Companies::Parser::RetrieveTmpSpreadsheetPath, type: :interaction do
  describe '#execute' do
    let(:tmp_dirname) { Companies::Parser::RetrieveTmpSpreadsheetPath::TMP_DIRNAME }

    context 'when directory exists' do
      before { stub_check_directory(exists: true) }

      it 'doesn\'t create new directory' do
        stub_mkdir
        described_class.run(spreadsheet_id: 'test')
        expect(FileUtils).not_to have_received(:mkdir_p).with(tmp_dirname)
      end

      it 'generates correct path' do
        expect(described_class.run(spreadsheet_id: 'test').result).to eq("#{tmp_dirname}/test.xlsx")
      end
    end

    context 'when directory doesn\'t exists' do
      before { stub_check_directory(exists: false) }

      it 'creates new directory' do
        stub_mkdir
        described_class.run(spreadsheet_id: 'test')
        expect(FileUtils).to have_received(:mkdir_p).with(tmp_dirname)
      end

      it 'generates correct path' do
        stub_mkdir
        expect(described_class.run(spreadsheet_id: 'test').result).to eq("#{tmp_dirname}/test.xlsx")
      end
    end

    # Should use partial double because ths libs used by rspec
    def stub_mkdir
      allow(FileUtils).to receive(:mkdir_p)
    end

    # Should use partial double because ths libs used by rspec
    def stub_check_directory(exists:)
      allow(File).to receive(:directory?).and_return(exists)
    end
  end
end
