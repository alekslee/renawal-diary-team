# frozen_string_literal: true

RSpec.describe Companies::Parser::RegenerateXlsx::AddWorksheet, type: :interaction do
  describe '#execute' do
    around do |example|
      Axlsx::Package.new do |package|
        @package = package
        example.run
      end
    end

    it 'sets name to the sheet' do
      stub_valid_interaction_class(Companies::Parser::RegenerateXlsx::FillSheet)
      described_class.run(package: @package, company_type: 'broker', companies: [])
      name = @package.workbook.worksheets.first.name
      expect(name).to eq 'broker'
    end

    it 'adds headers first row' do
      stub_valid_interaction_class(Companies::Parser::RegenerateXlsx::FillSheet)
      stub_xlsx_headers(headers: { test: 'test', data: 'data', row: 'row' })

      described_class.run(package: @package, company_type: 'broker', companies: [])
      first_row = @package.workbook.worksheets.first.rows.first.to_ary.map(&:value)
      expect(first_row).to match_array %w[test data row]
    end

    it 'runs FillSheet interactor' do
      expect { described_class.run(package: @package, company_type: 'broker', companies: []) }
        .to run_interactor(Companies::Parser::RegenerateXlsx::FillSheet).with_valid_inputs
    end

    def stub_xlsx_headers(headers: [])
      stub_const('Company::XLSX_HEADERS', headers)
    end
  end
end
