# frozen_string_literal: true

RSpec.describe Companies::Parser::RegenerateXlsx::Generate, type: :interaction do
  describe '#execute' do
    let(:category) { create(:category) }

    it 'runs fill package interaction' do
      package = stub_axlsx_package_instance
      stub_axlsx_package_class(package: package)

      expect { described_class.run(path: 'test', category: category) }
        .to run_interactor(Companies::Parser::RegenerateXlsx::FillPackage).with_valid_inputs
    end

    it 'saves package to file' do
      package = stub_axlsx_package_instance
      stub_axlsx_package_class(package: package)

      described_class.run(path: 'test', category: category)
      expect(package).to have_received(:serialize).with('test')
    end

    def stub_axlsx_package_class(package:)
      allow(Axlsx::Package).to receive(:new) do |&block|
        block.call(package)
      end
    end

    def stub_axlsx_package_instance
      package = instance_double(Axlsx::Package)
      allow(package).to receive(:is_a?).with(Axlsx::Package).and_return(true)
      allow(package).to receive(:serialize)
      package
    end
  end
end
