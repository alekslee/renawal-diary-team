# frozen_string_literal: true

RSpec.describe Companies::Parser::RegenerateXlsx::FillSheet, type: :interaction do
  describe '#execute' do
    around do |example|
      Axlsx::Package.new do |package|
        package.workbook.add_worksheet do |sheet|
          @sheet = sheet
          example.run
        end
      end
    end

    it 'adds n rows where n is a companies count' do
      companies = [create(:company), create(:company)]

      described_class.run(sheet: @sheet, companies: companies)
      expect(@sheet.rows.count).to eq 2
    end

    it 'adds rows with correct format' do
      company = create(:company)
      companies = [company]

      described_class.run(sheet: @sheet, companies: companies)
      values = @sheet.rows.first.to_ary.map(&:value)
      expect(values).to match_array [company.name, company.url, company.twitter, company.code]
    end
  end
end
