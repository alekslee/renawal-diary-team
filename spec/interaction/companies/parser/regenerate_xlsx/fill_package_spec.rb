# frozen_string_literal: true

RSpec.describe Companies::Parser::RegenerateXlsx::FillPackage, type: :interaction do
  let(:category) { create(:category) }

  describe '#execute' do
    it 'runs AddWorksheet interactot n times where n is a count of different company types' do
      create(:company, category: category, company_type: 'broker')
      create(:company, category: category, company_type: 'provider')

      expect { described_class.run(category: category.reload, package: stub_package) }
        .to run_interactor(Companies::Parser::RegenerateXlsx::AddWorksheet).with_valid_inputs.twice
    end
  end

  def stub_package
    package = instance_double(Axlsx::Package)
    allow(package).to receive(:is_a?).with(Axlsx::Package).and_return(true)
    package
  end
end
