# frozen_string_literal: true

RSpec.describe Companies::Parser::UpdateSpreadsheetOnGoogle, type: :interaction do
  describe '#execute' do
    before do
      stub_thread
      stub_file_utils_rm
    end

    it 'updates google spreadsheet' do
      spreadsheet = stub_spreadsheet
      described_class.run(path: 'test_path', spreadsheet: spreadsheet)
      expect(spreadsheet).to have_received(:update_from_file).with('test_path')
    end

    it 'removes files' do
      described_class.run(path: 'test_path', spreadsheet: stub_spreadsheet)
      expect(FileUtils).to have_received(:rm).with('test_path')
    end

    it 'runs code inside thread' do
      described_class.run(path: 'test_path', spreadsheet: stub_spreadsheet)
      expect(Thread).to have_received(:new)
    end

    def stub_spreadsheet
      spreadsheet = instance_double(GoogleDrive::Spreadsheet)
      allow(spreadsheet).to receive(:is_a?).with(GoogleDrive::Spreadsheet).and_return(true)
      allow(spreadsheet).to receive(:update_from_file)
      spreadsheet
    end

    def stub_file_utils_rm
      allow(FileUtils).to receive(:rm)
    end

    def stub_thread
      allow(Thread).to receive(:new) do |&block|
        block.call
      end
    end
  end
end
