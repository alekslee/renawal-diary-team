# frozen_string_literal: true

RSpec.describe Companies::GenerateNestedAttributes, type: :interaction do
  describe '#execute' do
    let(:category) { create(:leaf_category) }

    it 'returns empty hash if attributes empty' do
      result = described_class.run(category: category, attributes: {}).result
      expect(result).to eq({})
    end

    it 'merges root category id to the attributes elements' do
      attrs = { attrs: { name: 'test', company_type: 'test2' } }
      result = described_class.run(category: category, attributes: attrs).result
      expect(result).to eq attrs.deep_merge(attrs: { category_id: category.root.id }).with_indifferent_access
    end
  end
end
