# frozen_string_literal: true

RSpec.describe Companies::List, type: :interaction do
  describe '#execute' do
    let(:inputs) do
      {
        category_id: 0,
        page: 1,
        company_type: :test,
        last_buy: :test,
        checked_places: :test,
        country_code: :test,
        user: create(:user)
      }
    end

    it 'returns array with two elements' do
      stub_interaction
      result = described_class.run(inputs).result
      expect(result.length).to eq 2
    end

    it 'returns array with companies as first element' do
      companies = create_list(:company, rand(2..10))
      stub_interaction(companies: Company.all)
      result = described_class.run(inputs).result[0]
      expect(result).to match_array companies
    end

    it 'returns array with total_count as second element' do
      total_count = rand(1..100)
      stub_interaction(total_count: total_count)
      result = described_class.run(inputs).result[1]
      expect(result).to eq total_count
    end
  end

  def stub_interaction(companies: Company.none, total_count: 0, feedbacks_count: {}, average_satisfaction: {})
    companies_array = companies.to_a

    stub_valid_interaction_classes(
      Companies::List::RetrieveAllCompanies => companies,
      Companies::List::JoinsFeedbackTable => companies,
      Companies::List::FilterByCountries => companies,
      Companies::List::FilterByType => companies,
      Companies::List::FilterByCheckedPlaces => companies,
      Companies::List::FilterByLastBuy => companies,
      Companies::List::RetrieveTotalCount => total_count,
      Companies::List::RetrieveAverageSatisfaction => average_satisfaction,
      Companies::List::Order => companies,
      Companies::List::RetrieveFeedbacksByCompaniesCount => feedbacks_count,
      Companies::List::Paginate => companies_array,
      Companies::List::AddDynamicVariables => companies_array
    )
  end
end
