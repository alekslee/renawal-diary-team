# frozen_string_literal: true

RSpec.describe Companies::Update, type: :interaction do
  describe '#execute' do
    let(:company) { create(:company) }

    it 'updates name, url and twitter fields' do
      inputs = { company: company, name: "#{company.name}df", url: "#{company.url}df", twitter: "#{company.twitter}df" }
      expect { described_class.run(inputs) }.to change(company, :name)
        .and change(company, :url)
        .and change(company, :twitter)
    end

    it 'doesn\'t update nil attributes', :aggregate_failures do
      inputs = { company: company }
      expect { described_class.run(inputs) }.not_to(change { company.as_json(only: %i[name url twitter]) })
    end

    it 'adds errors if send invalid inputs' do
      inputs = { company: company, name: '' }
      expect(described_class.run(inputs)).not_to be_valid
    end
  end
end
