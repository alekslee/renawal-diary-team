# frozen_string_literal: true

RSpec.describe Companies::Create, type: :interaction do
  describe '#execute' do
    let(:category) { create(:category) }
    let(:company_attributes) { attributes_for(:company).merge(category: category) }

    it 'creates instance' do
      expect(described_class.run(company_attributes).result.id).to be_truthy
    end

    it 'creates with name, url, twitter and compny_type' do
      result = described_class.run(company_attributes).result.as_json(only: company_attributes.keys)
      expect(result).to eq company_attributes.except(:category).stringify_keys
    end

    it 'adds errors if send invalid inputs' do
      inputs = company_attributes.merge(name: '')
      expect(described_class.run(inputs)).not_to be_valid
    end
  end
end
