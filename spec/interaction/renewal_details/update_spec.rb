# frozen_string_literal: true

RSpec.describe RenewalDetails::Update, type: :interaction do
  let(:user) { create(:user) }
  let(:category) { create(:leaf_category, country: user.country) }
  let(:renewal_detail) { create(:renewal_detail, user: user, category: category) }
  let(:inputs) do
    attributes_for(:renewal_detail).merge(user: user, renewal_detail_id: renewal_detail.id, buying_type: nil)
  end

  describe '#execute' do
    context 'when using_provider is true' do
      it 'valid interaction' do
        stub_valid_interaction_classes(RenewalDetails::Find => renewal_detail, Companies::GenerateNestedAttributes => {})
        expect(described_class.run(inputs)).to be_valid
      end

      it 'updates renewal_detail' do
        stub_valid_interaction_classes(
          RenewalDetails::Find => renewal_detail,
          Companies::GenerateNestedAttributes => {}
        )
        result = described_class.run(inputs).result
        expect(result.id).to be_truthy
      end

      it 'updates companies with attributes', :aggregate_failures do
        stub_valid_interaction_classes(
          RenewalDetails::Find => renewal_detail,
          Companies::GenerateNestedAttributes => {
            current_provider_attributes: attributes_for(:company, category_id: category.root.id),
            buying_type_company_attributes: attributes_for(
              :company, company_type: inputs[:buying_type], category_id: category.root.id
            )
          }
        )
        result = described_class.run(inputs).result.reload
        expect(result.current_provider.id).to be_truthy
        expect(result.buying_type_company.id).to be_truthy
      end

      it 'adds errors if attributes doesn\'t valid' do
        stub_valid_interaction_classes(
          RenewalDetails::Find => renewal_detail,
          Companies::GenerateNestedAttributes => {
            current_provider_attributes: attributes_for(:company, category_id: category.root.id).except(:name)
          }
        )
        expect(described_class.run(inputs)).not_to be_valid
      end
    end

    context 'when using_provider is false' do
      let(:inputs) { { user: user, renewal_detail_id: renewal_detail.id, using_provider: false } }

      it 'updates renewal_detail' do
        stub_valid_interaction_classes(
          RenewalDetails::Find => renewal_detail,
          RenewalDetails::Update::ResetRenewalDetail => renewal_detail
        )
        result = described_class.run(inputs).result
        expect(result.id).to be_truthy
      end
    end
  end

  describe '#companies_attributes' do
    it 'returns only attributes inputs' do
      attrs = { current_provider_attributes: {}, buying_type_company_attributes: {} }
      result = described_class.new(inputs.merge(attrs)).send(:companies_attributes)
      expect(result).to eq attrs
    end

    it 'removes nill attributes' do
      attrs = {
        current_provider_attributes: {}, buying_type_company_attributes: nil
      }
      result = described_class.new(inputs.merge(attrs)).send(:companies_attributes)
      expect(result).to eq(current_provider_attributes: {})
    end
  end
end
