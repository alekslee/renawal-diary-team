# frozen_string_literal: true

RSpec.describe RenewalDetails::Find, type: :interaction do
  describe '#execute' do
    let(:user) { create(:user) }

    it 'valid interaction when renewal detail exists' do
      renewal_detail = create(:renewal_detail, user_id: user.id)
      interaction = described_class.run(renewal_detail_id: renewal_detail.id, user: user)
      expect(interaction).to be_valid
    end

    it 'returns renewal detail when renewal detail exists' do
      renewal_detail = create(:renewal_detail, user_id: user.id)
      result = described_class.run(renewal_detail_id: renewal_detail.id, user: user).result
      expect(result).to be_truthy
    end

    it 'doesn\'t valid interaction when renewal details belongs to another user' do
      second_user = create(:user)
      renewal_detail = create(:renewal_detail, user_id: second_user.id)
      interaction = described_class.run(renewal_detail_id: renewal_detail.id, user: user)
      expect(interaction).not_to be_valid
    end

    it 'doesn\'t valid interaction when renewal details doesn\'t exists' do
      interaction = described_class.run(renewal_detail_id: 0, user: user)
      expect(interaction).not_to be_valid
    end
  end
end
