# frozen_string_literal: true

RSpec.describe Categories::FindUsersLeafCategory, type: :interaction do
  describe '#execute' do
    let(:category) { create(:category) }

    it 'returns category when category exists' do
      user = create(:user, country: category.country)
      result = described_class.run(user: user, category_id: category.id).result
      expect(result).to eq category
    end

    it 'adds errors if category exists but with wrong user' do
      user = create(:user)
      interactor = described_class.run(user: user, category_id: category.id)
      expect(interactor).not_to be_valid
    end

    it 'adds errors if category doesn\'t exists' do
      user = create(:user)
      interactor = described_class.run(user: user, category_id: 0)
      expect(interactor).not_to be_valid
    end
  end
end
