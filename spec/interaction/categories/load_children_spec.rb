# frozen_string_literal: true

RSpec.describe Categories::LoadChildren, type: :interaction do
  let(:category) { create(:category) }
  let(:child_category) { create(:category, parent: category) }

  describe '#execute' do
    it 'creates categories in the loop' do
      inputs = { category: category, children: { cat1: {}, cat2: {} } }
      expect { described_class.run(inputs) }.to run_interactor(Categories::CreateChild).twice
    end

    it 'runs create category child unless category present' do
      inputs = { category: category, children: { cat: { name: 'name' } } }

      expect { described_class.run(inputs) }.to run_interactor(Categories::CreateChild).with_valid_inputs
    end

    it 'does not to create category child if category child present' do
      inputs = { category: category, children: { child_category.code.to_sym => {} } }

      expect { described_class.run(inputs) }.not_to run_interactor(Categories::CreateChild)
    end

    it 'toes load children if categories present' do
      inputs = { category: category, children: { child_category.code.to_sym => { categories: { cat2: {} } } } }

      # twice - because this interaction runs in recursion
      expect { described_class.run(inputs) }.to run_interactor(Categories::LoadChildren).with_valid_inputs.twice
                                                                                        .with_recursion
    end

    it 'does not to load children if categories empty' do
      inputs = { category: category, children: { child_category.code.to_sym => { categories: {} } } }

      # twice - because this interaction runs in recursion
      expect { described_class.run(inputs) }.to run_interactor(Categories::LoadChildren).once.with_recursion
    end
  end
end
