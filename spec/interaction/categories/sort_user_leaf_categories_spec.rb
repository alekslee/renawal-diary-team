# frozen_string_literal: true

RSpec.describe Categories::SortUserLeafCategories, type: :interaction do
  describe '#execute' do
    context 'when user has categories' do
      let(:user) { create(:user, categories_count: 1) }
      let(:category) { create(:category) }

      it 'sortes categories by selected' do
        all_categories = user.categories + [category]
        result = described_class.run(categories: all_categories.shuffle, user: user).result
        expect(result).to eq all_categories
      end
    end

    context 'when hasn\'t categories' do
      let(:user) { create(:user) }
      let(:categories) { create_list(:category, 2) }

      it 'sortes categories by selected' do
        result = described_class.run(categories: categories, user: user).result
        expect(result).to eq categories
      end
    end
  end
end
