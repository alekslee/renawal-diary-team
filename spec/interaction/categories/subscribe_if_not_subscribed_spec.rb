# frozen_string_literal: true

RSpec.describe Categories::SubscribeIfNotSubscribed, type: :interaction do
  let(:users_category) { create(:users_category) }

  describe '#execute' do
    it 'doesn\'t create users category if its already exists' do
      inputs = { user: users_category.user, category_id: users_category.category_id }
      expect { described_class.run(inputs) }.not_to change(UsersCategory, :count)
    end

    it 'creates new users category if doesn\'t exist' do
      category = create(:category)
      inputs = { user: users_category.user, category_id: category.id }
      expect { described_class.run(inputs) }.to change(UsersCategory, :count).by 1
    end
  end
end
