# frozen_string_literal: true

RSpec.describe Categories::Create, type: :interaction do
  let(:category) { create(:category) }
  let(:inputs) { attributes_for(:category).except(:country).merge(category: category) }

  describe '#execute' do
    it 'returns created category' do
      expect(described_class.run(inputs).result).to eq category.children.find_by_code(inputs[:code])
    end

    it 'adds errors if can\'t create category' do
      create(:category, inputs.except(:category).merge(parent: category))
      expect(described_class.run(inputs)).not_to be_valid
    end
  end
end
