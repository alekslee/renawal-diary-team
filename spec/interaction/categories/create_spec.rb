# frozen_string_literal: true

RSpec.describe Categories::Create, type: :interaction do
  let(:inputs) { attributes_for(:category) }

  describe '#execute' do
    it 'returns created category' do
      expect(described_class.run(inputs).result).to eq Category.find_by_code(inputs[:code])
    end

    it 'adds errors if can\'t create category' do
      create(:category, inputs)
      expect(described_class.run(inputs)).not_to be_valid
    end
  end
end
