# frozen_string_literal: true

RSpec.describe SmartReminders::Create, type: :interaction do
  describe '#execute' do
    subject { described_class.run(inputs).result.category }

    let(:country) { create(:country) }
    let(:user) { create(:user, country: country) }
    let(:category) { create(:category, country: country) }
    let!(:default_category) { create(:category, country: country, code: 'other') }
    let(:category_id) { category.id }
    let(:inputs) do
      {
        user: user,
        category_id: category_id,
        alias_name: Faker::Internet.user_name,
        reminder_type: 'custom',
        triggered_at: Time.zone.today
      }
    end

    describe 'creates with category' do
      it { is_expected.to match category }
    end

    describe 'creates without category' do
      let(:category_id) { nil }

      it { is_expected.to match default_category }
    end
  end
end
