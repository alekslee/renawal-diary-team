# frozen_string_literal: true

RSpec.describe Feedbacks::Create, type: :interaction do
  let(:user) { create(:user) }
  let(:category) { create(:leaf_category, country: user.country) }
  let(:inputs) { attributes_for(:feedback).merge(user: user, category_id: category.id) }

  describe '#execute' do
    it 'creates feedback' do
      stub_valid_interaction_classes(
        Categories::SubscribeIfNotSubscribed,
        Categories::FindUsersLeafCategory,
        Companies::GenerateNestedAttributes => {
          current_provider_attributes: attributes_for(:company, category_id: category.root.id),
          buying_type_company_attributes: attributes_for(
            :company, company_type: inputs[:buying_type], category_id: category.root.id
          )
        }
      )
      result = described_class.run(inputs).result
      expect(result.id).to be_truthy
    end

    it 'creates companies with attributes', :aggregate_failures do
      stub_valid_interaction_classes(
        Categories::SubscribeIfNotSubscribed,
        Categories::FindUsersLeafCategory,
        Companies::GenerateNestedAttributes => {
          current_provider_attributes: attributes_for(:company, category_id: category.root.id),
          buying_type_company_attributes: attributes_for(
            :company, company_type: inputs[:buying_type], category_id: category.root.id
          )
        }
      )
      result = described_class.run(inputs).result
      expect(result.current_provider.id).to be_truthy
      expect(result.buying_type_company.id).to be_truthy
    end

    it 'adds errors if attributes doesn\'t valid' do
      stub_valid_interaction_classes(
        Categories::SubscribeIfNotSubscribed,
        Categories::FindUsersLeafCategory,
        Companies::GenerateNestedAttributes => {
          current_provider_attributes: attributes_for(:company, category_id: category.root.id).except(:name)
        }
      )
      expect(described_class.run(inputs)).not_to be_valid
    end
  end

  describe '#companies_attributes' do
    it 'returns only attributes inputs' do
      attrs = { current_provider_attributes: {}, buying_type_company_attributes: {} }
      result = described_class.new(inputs.merge(attrs)).send(:companies_attributes)
      expect(result).to eq attrs
    end

    it 'removes nill attributes' do
      attrs = {
        current_provider_attributes: {}, buying_type_company_attributes: nil
      }
      result = described_class.new(inputs.merge(attrs)).send(:companies_attributes)
      expect(result).to eq(current_provider_attributes: {})
    end
  end
end
