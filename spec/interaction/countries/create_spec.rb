# frozen_string_literal: true

RSpec.describe Countries::Create, type: :interaction do
  let(:inputs) { attributes_for(:country) }

  describe '#execute' do
    it 'returns created country' do
      expect(described_class.run(inputs).result).to eq Country.find_by_iso_a2_code(inputs[:iso_a2_code])
    end

    it 'adds errors if can\'t create country' do
      Country.create(inputs)
      expect(described_class.run(inputs)).not_to be_valid
    end
  end
end
