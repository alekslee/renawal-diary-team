# frozen_string_literal: true

RSpec.describe Countries::SetupFromYML, type: :interaction do
  let(:country) { create(:country) }

  describe '#execute' do
    it 'creates countries in the loop' do
      stub_valid_interaction_class(LoadYamlFile, countries: { test1: {}, test2: {} })

      expect { described_class.run }.to run_interactor(Countries::Create).twice
    end

    it 'runs create country unless country present' do
      stub_valid_interaction_class(LoadYamlFile, countries: { ua: attributes_for(:country).except(:iso_a2_code) })

      expect { described_class.run }.to run_interactor(Countries::Create).with_valid_inputs
    end

    it 'does not to create country if country present' do
      stub_valid_interaction_class(LoadYamlFile, countries: { country.iso_a2_code.to_sym => {} })

      expect { described_class.run }.not_to run_interactor(Countries::Create)
    end

    it 'toes load categories if categories present' do
      stub_valid_interaction_class(LoadYamlFile, countries: { country.iso_a2_code.to_sym => {
                                     categories: { cat: {} }
                                   } })

      expect { described_class.run }.to run_interactor(Countries::LoadCategories).with_valid_inputs
    end

    it 'does not to load categories if categories empty' do
      stub_valid_interaction_class(LoadYamlFile, countries: { country.iso_a2_code.to_sym => {} })

      expect { described_class.run }.not_to run_interactor(Countries::LoadCategories)
    end
  end
end
