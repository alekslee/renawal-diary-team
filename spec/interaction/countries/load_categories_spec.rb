# frozen_string_literal: true

RSpec.describe Countries::LoadCategories, type: :interaction do
  let(:country) { create(:country) }
  let(:category) { create(:category) }

  describe '#execute' do
    it 'creates categories in the loop' do
      inputs = { country: country, categories: { cat1: {}, cat2: {} } }
      expect { described_class.run(inputs) }.to run_interactor(Categories::Create).twice
    end

    it 'runs create category unless category present' do
      inputs = { country: country, categories: { cat: { name: 'name' } } }

      expect { described_class.run(inputs) }.to run_interactor(Categories::Create).with_valid_inputs
    end

    it 'does not to create category if category present' do
      inputs = { country: category.country, categories: { category.code.to_sym => {} } }

      expect { described_class.run(inputs) }.not_to run_interactor(Categories::Create)
    end

    it 'toes load children if categories present' do
      inputs = { country: category.country, categories: { category.code.to_sym => { categories: { cat2: {} } } } }

      expect { described_class.run(inputs) }.to run_interactor(Categories::LoadChildren).with_valid_inputs
    end

    it 'does not to load children if categories empty' do
      inputs = { country: category.country, categories: { category.code.to_sym => { categories: {} } } }

      expect { described_class.run(inputs) }.not_to run_interactor(Categories::LoadChildren)
    end
  end
end
