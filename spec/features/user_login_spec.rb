# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Login', type: :feature, js: true do
  let!(:user) { create(:user) }

  before do
    page.driver.browser.ignore_ssl_errors
    current_window.resize_to(600, 600)
  end

  scenario 'user logged in successfully' do
    sleep 10
    visit '/us'
    sleep 20
    $stdout << "\nPage: #{page.text}\n"
    find('button.navbar-toggler').click
    find('a.nav-link', text: 'Login').click
    fill_in 'email', with: user.email
    fill_in 'password', with: user.password
    click_button 'Login'
    sleep 2

    expect(current_path).to eq '/us/categories/best-car-insurance/leaderboard'
    expect(page).not_to have_content('Login')
  end
end