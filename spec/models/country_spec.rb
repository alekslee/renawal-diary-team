# frozen_string_literal: true

require 'rails_helper'

describe Country do
  describe 'validations' do
    subject { create(:country) }

    it { is_expected.to validate_presence_of(:en_name) }
    it { is_expected.to validate_presence_of(:iso_a2_code) }
    it { is_expected.to validate_presence_of(:default_lang_code) }
    it { is_expected.to validate_presence_of(:available_lang_codes) }
    it { is_expected.to validate_uniqueness_of(:en_name) }
    it { is_expected.to validate_uniqueness_of(:iso_a2_code) }
  end

  describe 'associations' do
    it { is_expected.to have_many(:user_statistics) }
  end

  describe 'methods' do
    let(:country) { create(:country) }

    it 'name' do
      allow(I18n).to receive(:t).with("countries_names.#{country.iso_a2_code}")
      country.name
      expect(I18n).to have_received(:t).with("countries_names.#{country.iso_a2_code}")
    end
  end
end
