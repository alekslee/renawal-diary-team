# frozen_string_literal: true

require 'rails_helper'

describe UserStatistic do
  context 'with associations' do
    it { is_expected.to belong_to(:statisticable) }
  end
end
