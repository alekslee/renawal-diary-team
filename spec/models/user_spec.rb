# frozen_string_literal: true

require 'rails_helper'

describe User do
  context 'with validations' do
    subject { create(:user) }

    it { is_expected.to validate_uniqueness_of(:display_name) }
    # TODO: check validation
    # it { is_expected.to validate_uniqueness_of(:email).ignoring_case_sensitivity }
  end

  context 'with associations' do
    it { is_expected.to have_many(:invitees).class_name('User').with_foreign_key(:invited_by_id) }
    it { is_expected.to belong_to(:inviter).class_name('User').with_foreign_key(:invited_by_id).optional }
  end
end
