# frozen_string_literal: true

require 'rails_helper'

describe Category do
  context 'with validations' do
    it { is_expected.to validate_presence_of(:en_name) }
  end
end
