# frozen_string_literal: true

require 'rails_helper'

describe UsersCountry do
  describe 'associations' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:country).counter_cache(true) }
  end
end
