shared_context 'with criteria_questions' do
  let(:companies) { create_list(:company, 5, company_type: :provider) }
  let(:company) { companies.last }
  let(:inputs) { { companies: Company.joins(:current_renewal_details), criteria_questions: criteria_question } }
  let(:criteria_question) { '' }

  let(:renewal_details) do
    create(:renewal_detail,
      current_provider: company,
      criteria_questions: criteria_question,
      last_buy: :today)
  end

  let(:questions) do
    {
      age: { age: Faker::Number.between(20, 30) },
      male_femail: { male_femail: Faker::Lorem.word },
      location: {
        lat:  Faker::Address.latitude,
        lng: Faker::Address.longitude,
        address: Faker::Address.full_address,
        countryCode: Faker::Address.country_code
      },
      has_anyone_policy_claim_past_5_years: {
        has_anyone_policy_claim_past_5_year: Faker::Boolean.boolean
      },
      energy_type: { energy_type: Faker::Lorem.word },
      paying_method: { paying_method: Faker::Lorem.word },
      energy_usage: { energy_usage: Faker::Lorem.word },
      contract_type: { contract_type: Faker::Lorem.word },
      contract_length: { contract_length: Faker::Lorem.word },
      energy_supply_region: { energy_supply_region: Faker::Lorem.word },
      having_saving_metter: { having_saving_metter: Faker::Boolean.boolean },
      important_to_you: { important_to_you: Faker::Lorem.word },
      speed_is: { speed_is: Faker::Lorem.word },
      broadband_type: { broadband_type: Faker::Lorem.word },
      broadband_package: { broadband_package: Faker::Lorem.word },
      most_important_to_you: { most_important_to_you: Faker::Lorem.word },
      monthly_usage_allowance: { monthly_usage_allowance: Faker::Lorem.word },
      years_driving_experience: { years_driving_experience: Faker::Number.between(1, 20) },
      years_no_claims_bonus: { years_no_claims_bonus: Faker::Number.between(1, 5) },
      type_of_cover_required: { type_of_cover_required: Faker::Lorem.word },
      no_of_drivers: { no_of_drivers: Faker::Number.between(1, 5) },
      car_age: { car_age: Faker::Number.between(1, 5) },
      animal_type: { animal_type: Faker::Lorem.word },
      insurance_type: { insurance_type: Faker::Lorem.word },
      who_is_traveling: { who_is_traveling: Faker::Lorem.word },
      traveling_place: { traveling_place: Faker::Lorem.word }
    }
  end

  before do
    renewal_details
    companies.each do |company|
      create(:renewal_detail,
        current_provider: company,
        criteria_questions: { "#{Faker::Lorem.word}": Faker::Lorem.word },
        last_buy: :today)
    end
  end
end
