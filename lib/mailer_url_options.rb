# frozen_string_literal: true

MAILER_URL_OPTIONS = { host: ENV['RAILS_HOST_URL'], port: ENV['RAILS_HOST_PORT'] }.freeze
