class UserMailerPreview < ActionMailer::Preview

  def confirmation_instructions
    AuthMailer.confirmation_instructions(user, 'token')
  end

  def invitation_instructions
    AuthMailer.invitation_instructions(user, 'token', {})
  end

  def last_chance_confirmation_instructions
    AuthMailer.last_chance_confirmation_instructions(user.id, 'token')
  end

  def become_a_partner
    PartnerMailer.with({ email: 'email@email.com', name: 'name', body: 'body' }).become_a_partner
  end

  private

  def user
    params[:id] ? User.find(params[:id]) : User.first
  end

end