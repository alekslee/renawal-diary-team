class SmartReminderNotificationMailerPreview < ActionMailer::Preview

  def set_up_smart_reminder
    SmartReminderNotificationMailer.with(smart_reminder_id: smart_reminder.id).set_up_smart_reminder
  end

  def amber_to_red
    params[:status] = 'red'
    SmartReminderNotificationMailer.with(smart_reminder_id: smart_reminder.id).amber_to_red
  end

  def date_becomes
    SmartReminderNotificationMailer.with(smart_reminder_id: smart_reminder.id).date_becomes
  end

  def green_to_amber
    params[:status] = 'green'
    SmartReminderNotificationMailer.with(smart_reminder_id: smart_reminder.id).green_to_amber
  end

  private

  def smart_reminder
    smrts = SmartReminder
      .joins(:category)
      .joins('INNER JOIN categories as root ON root.lft <= categories.lft AND root.rgt >= categories.rgt')

    smrts = smrts.find(params[:id]) if params[:id]

    case params[:code].to_i
    when 1
      smrts = smrts.where(root: { code: 'insurance' } )
    when 2
      smrts = smrts.where(root: { code: 'broadband' } )
    when 3
      smrts = smrts.where(root: { code: 'energy' } )
    when 4
      smrts = smrts.where(root: { code: 'subscriptions' } )
    when 5
      smrts = smrts.where(root: { code: 'business' } )
    end

    if params[:status]
      smrts = smrts.where(status: params[:status])
    end

    smrts.last || SmartReminder.last
  end

end