class SystemMailerPreview < ActionMailer::Preview

  def company_approve
    company = Company.where(status: 'pending').last
    SystemMailer.with({ company: company }).company_approve
  end

  def send_feedback
    type = params[:type] || 'bug'
    SystemMailer.send_feedback(type, 'test message')
  end

end