# frozen_string_literal: true

module DefaultUrlOptions
  def default_url_options
    MAILER_URL_OPTIONS
  end
end
