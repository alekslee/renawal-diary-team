# frozen_string_literal: true

namespace :server do
  desc 'run puma server in the production mode'
  task :production do
    sh %(NODE_ENV=production yarn install)
    # clober only if we has release and versions
    # sh %(NODE_ENV=production bundle exec rails webpacker:clobber)
    sh %(NODE_ENV=production bin/webpack --color --progress)
    sh %(RAILS_SERVE_STATIC_FILES=true puma -e production -d -b unix:///home/coder/renawal-diary-team/puma.sock)
  end

  desc 'demonize puma server in the production mode'
  task :restart do
    sh %(NODE_ENV=production yarn install)
    sh %(NODE_ENV=production bin/webpack --color --progress)
    sh %(kill -9 $(pgrep -f puma))
    sh %(RAILS_SERVE_STATIC_FILES=true puma -e production -d -b unix:///home/coder/renawal-diary-team/puma.sock)
  end

end
