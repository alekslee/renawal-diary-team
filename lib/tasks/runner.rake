# frozen_string_literal: true
require 'sidekiq/api'

namespace :run do

  task :parse_companies, [:async] => [:environment] do |_, args|

    if args[:async]
      ParseAndSyncCompaniesWorker.perform_async
    else
      Companies::Parser::Synchronize.run!
    end

  end

  task view_folders: :environment do |_, args|
    CONFIG_PATH = Rails.root.join('config', 'google_drive_config.json').freeze
    ROOT_FOLDER_ID = ENV['ROOT_FOLDER_ID']
    session = GoogleDrive::Session.from_config(CONFIG_PATH.to_s)
    folder = session.folder_by_id(ROOT_FOLDER_ID)

    puts "#{folder.name} (folder)".red
    folder.subfolders.each do |country_folder|
      puts "--#{country_folder.name} (folder)".blue

      country_folder.subfolders.each do |sub_category_folder|
        puts "----#{sub_category_folder.name} (folder)".green

        sub_category_folder.spreadsheets.each do |sheet|
          puts "------#{sheet.name}.xlsx".yellow
        end
      end

      country_folder.spreadsheets.each do |sheet|
        puts "----#{sheet.name}.xlsx".yellow
      end
    end

    folder.spreadsheets.each do |sheet|
      puts "--#{sheet.name}.xlsx".yellow
    end

  end

  task update_companies_avatars: [:environment] do
    UpdateCompaniesAvatarsWorker.perform_async
  end
end
