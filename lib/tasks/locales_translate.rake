# frozen_string_literal: true

namespace :locales do
  desc 'translate locales'
  task :translate, [:prefix] => [:environment] do |_t, args|
    ::Locales::Translate.run!(prefix: args[:prefix])
  end
end
