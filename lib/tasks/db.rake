# frozen_string_literal: true

namespace :db do

  desc "Dumps the database to db/dump.sql"
  task :dump => :environment do
    command_list = []
    with_config do |app, db, user, password, host|
      command_list << "PGPASSWORD=#{password} pg_dump --host #{host || 'localhost'} --username #{user} --verbose --no-owner --no-acl --format=p #{db} > #{Rails.root}/tmp/#{app}"
      command_list << "7z a -mx9 -y #{Rails.root}/db/#{app}.7z #{Rails.root}/tmp/#{app}"
    end
    execute(command_list)
  end

  desc "Restores the database dump at db/APP_NAME.sql"
  task :restore => :environment do
    command_list = []
    with_config do |app, db, user, password, host|
      command_list << "7z x -y #{Rails.root}/db/#{app}.7z -o#{Rails.root}/tmp"
      command_list << "PGPASSWORD=#{password} psql --host #{host || 'localhost'} --username #{user} --dbname #{db} --file #{Rails.root}/tmp/#{app}"      
    end
    Rake::Task['db:drop'].invoke
    Rake::Task['db:create'].invoke
    execute(command_list)
  end

  private

  def execute(command_list)
    executed_commands = []
    result = nil

    command_list.each do |command|
      stdout, stderr, status = Open3.capture3(command)
      result = status.exitstatus
      executed_commands << [command, stdout, stderr, result]
      break if result != 0
    end

    pp executed_commands
    puts "exited with #{result} exit status."
  end

  def with_config
    yield Rails.application.class.parent_name.underscore,
      ActiveRecord::Base.connection_config[:database],
      ActiveRecord::Base.connection_config[:username],
      ActiveRecord::Base.connection_config[:password],
      ActiveRecord::Base.connection_config[:host]
  end
end