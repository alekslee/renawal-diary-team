# README


This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

### System requirements

`Ruby 2.5.1, PostgreSQL 10.3, NodeJS 10.10.0, Yarn 1.9.4, Phantomjs 2.1.1, Redis 4.0.9`

##### Code analysis:
`rails lint` (runs `rubocop`, `eslint` and `brakeman`)

##### Tests:
`rspec` - rails tests (rspec, capybara)

`yarn test` - js tests (jest, enzyme)

##### Workflow:
* for a new task branch from "development", name schema: `#{task_id}_essential_short_task_descriptor`. You can use "Create branch" button on gitlab issue page for that.
* cover functionality with reasonable pack of autotests
* when development is finished, please make sure all legasy test and linters passed
* rebase on top of "development" branch inside of your branch
* submit MR

##### Notes:
* We're using Webpacker. Sprockets is also there but only for backward capability.
* All JS goes under `app/javascript`.
* All CSS goes under `app/javascript` too.
* CSS libs installed via `yarn` are included in `app/javascripts/packs/application_pack.scss`.
* `app/assets/javascripts` and `app/assets/stylesheets` shouldn't be used.
* Use page-specific CSS files.

##### Snippets:
* `rails s`
* `redis-server`
* `sidekiq -C config/sidekiq.yml`
