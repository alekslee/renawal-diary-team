server '35.227.30.235', user: 'coder', roles: %w[app db web], primary: true
set :rails_env, 'staging'
set :branch, 'staging'
set :deploy_to, '/home/coder/renawal-diary-team'