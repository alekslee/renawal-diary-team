# frozen_string_literal: true

Rails.application.config.i18n.available_locales = Dir['config/locales/*.yml'].map do |name|
  name[/\w{2,}(?=\.yml)/]
end.uniq
