if Rails.env.development?

  GraphQL::RailsLogger::Subscriber.class_eval do
    def start_processing(event)
      return unless logger.info?

      payload = event.payload
      params  = payload[:params].except(*GraphQL::RailsLogger::Subscriber::INTERNAL_PARAMS)
      format  = payload[:format]
      format  = format.to_s.upcase if format.is_a?(Symbol)

      config = GraphQL::RailsLogger.configuration

      info "Processing by #{payload[:controller]}##{payload[:action]} as #{format}"

      if config.white_list.fetch(payload[:controller], []).include?(payload[:action])
        formatter = Rouge::Formatters::Terminal256.new
        query_lexer = Rouge::Lexers::GraphQL.new
        variables_lexer = Rouge::Lexers::Ruby.new

        each_data = -> (data) do
          next if config.skip_introspection_query && data['query'].index(/query IntrospectionQuery/)

          query = data['query'].lines.map { |line| "  #{line}" }.join.chomp # add indentation
          variables = PP.pp(data['variables'] || {}, '')
          info "  Variables: #{formatter.format(variables_lexer.lex(variables))}"
          info formatter.format(query_lexer.lex(query))
        end

        if params.has_key?('queries')
          params['queries'].each_with_index do |subparams, ind|
            info "Query #{ind + 1}"

            ([subparams.slice('query', 'variables')]).each do |data|
              each_data.(data)
            end
          end
        else
          (params['_json'] || [params.slice('query', 'variables')]).each do |data|
            each_data.(data)
          end
        end

      else
        info "  Parameters: #{params.inspect}" unless params.empty?
      end

      rescue
        info "  Parameters: #{params.inspect}" unless params.empty?
    end
  end

end
