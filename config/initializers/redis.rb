# frozen_string_literal: true

require 'sidekiq/web'

if Rails.env.development?
  $redis = Redis.new(host: "localhost", port: 6379, db: 1)
elsif Rails.env.staging?
  $redis = Redis.new(url: ENV['REDIS_URL'])
elsif Rails.env.production?
  $redis = Redis.new(url: ENV['REDIS_URL'])
end


