# frozen_string_literal: true

FALLBACK_COUNTRY = 'ie'
AVAILABLE_COUNTRIES = LoadYamlFile.run!(path: Rails.root.to_s + '/config/country_categories.yml')[:countries].keys
