# frozen_string_literal: true

require 'sidekiq/web'

server_config, client_config = if Rails.env.test?
                                 if defined?(Redis::Connection::Memory)
                                   [
                                     { driver: Redis::Connection::Memory },
                                     { driver: Redis::Connection::Memory }
                                   ]
                                 else
                                   [{}, {}]
                                 end
                               elsif Rails.env.development?
                                 [
                                   { url: 'redis://localhost:6379/0' },
                                   { url: 'redis://localhost:6379/0' }
                                 ]
                               elsif Rails.env.staging?
                                 [
                                   { url: ENV['REDIS_URL']  },
                                   { url: ENV['REDIS_URL']  }
                                 ]
                               elsif Rails.env.production?
                                 [
                                   { url: ENV['REDIS_URL']  },
                                   { url: ENV['REDIS_URL']  }
                                 ]
                               end

Sidekiq.configure_server do |config|
  config.redis = server_config
  # config.error_handlers << Proc.new {|ex,ctx_hash| MyErrorService.notify(ex, ctx_hash) }
end

Sidekiq.configure_client do |config|
  config.redis = client_config
end

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == [ENV['SIDEKIQ_ADMIN_USERNAME'], ENV['SIDEKIQ_ADMIN_PASSWORD']]
end
