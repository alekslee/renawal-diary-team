# frozen_string_literal: true

require 'sidekiq/web'

PROVIDER_REGEXP = /(facebook|google_oauth2|twitter|linkedin)/
# rubocop:disable Metrics/BlockLength
Rails.application.routes.draw do
  mount GraphiQL::Rails::Engine, at: '/graphiql', graphql_path: '/ie/graphql' if Rails.env.development?
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, only: :omniauth_callbacks, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }


  scope '(:country)', country: /#{AVAILABLE_COUNTRIES.join('|')}/ do
    devise_for :users, skip: %i[omniauth_callbacks registrations],
      controllers: { confirmations: 'users/confirmations', unlocks: 'users/unlocks' }

    post '/graphql', to: 'graphql#execute'

    get :approve, controller: :providers, as: :providers_approve
    resources :feedback, only: :create

    devise_scope :user do
      resources :categories, only: [] do
        member do
          get :'have-your-say', controller: :categories, action: :vote, as: :vote_path
          get :renewal_details
          get :leaderboard
          get :my_notes
        end
      end
    end

    scope controller: :home do
    end
    root to: 'home#landing'

    get '*path', controller: 'app', action: :index, constraints: ->(req) { !req.url.match(%r{rails\/active_storage}) }
  end

  mount Sidekiq::Web, at: '/sidekiq'
  mount ActionCable.server => '/:country/cable'
end
# rubocop:enable Metrics/BlockLength
