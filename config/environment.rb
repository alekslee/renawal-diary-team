# frozen_string_literal: true

# Load the Rails application.
require_relative 'application'
require 'mailer_url_options'
require 'default_url_options'

Dir[Rails.root.join('lib/active_storage/**/*.rb')].each { |f| require f }

# Initialize the Rails application.
Rails.application.initialize!
