# config valid for current version and patch releases of Capistrano
lock '~> 3.11.0'

set :application, 'renawal-diary-team'
set :repo_url, 'git@bitbucket.org:Nikolay_Anatolyevych/renawal-diary-team.git'
set :user, 'coder'
set :use_sudo, true
set :pty, false
set :rvm_ruby_version, '2.5.1'
set :log_level, :info
set :linked_files, fetch(:linked_files, []).concat(%w[config/puma.rb])
set :linked_dirs, fetch(:linked_dirs, []).concat(%w[log tmp/pids tmp/cache
                                                    tmp/sockets tmp/themes
                                                    vendor/bundle public/system
                                                    public/uploads public/packs
                                                    client/packs/locales])
set :config_files, %w[config/database.yml config/secrets.yml]
set :puma_conf, '/home/coder/renawal-diary-team/shared/config/puma.rb'
set :nvm_type, :user
set :use_sudo, true
set :nvm_map_bins, %w[node npm yarn]
set :yarn_target_path, -> { release_path.join('client') }
set :yarn_flags, '--production --silent --no-progress'
set :yarn_roles, :all
set :yarn_env_variables, {}

namespace :deploy do
  desc 'Initial locales'
  task :set_react_locales do
    on roles(:web) do
      within release_path.join('client') do
        with rails_env: fetch(:rails_env) do
          rake 'react_on_rails:locale'
        end
      end
    end
  end

  after 'check:linked_files', 'puma:jungle:setup'
  after 'check:linked_files', 'puma:nginx_config'
  after 'puma:smart_restart', 'nginx:restart'
  after 'publishing', 'set_react_locales'
end

after 'deploy:updated', 'webpacker:precompile'
