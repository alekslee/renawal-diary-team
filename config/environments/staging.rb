# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
Rails.application.configure do
  config.webpacker.check_yarn_integrity = false
  config.action_cable.mount_path = '/:country/cable'
  config.action_cable.disable_request_forgery_protection = true
  config.cache_classes = true
  config.eager_load = true
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true
  config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?
  config.assets.compile = false
  config.assets.digest = true
  config.active_storage.service = :local
  config.log_level = :debug
  config.log_tags = [:request_id]
  config.active_job.queue_adapter = :sidekiq

  config.action_mailer.asset_host = ENV['MAILER_ASSET_HOST']
  config.action_mailer.perform_deliveries = true
  config.action_mailer.perform_caching = false
  config.action_mailer.default_url_options = MAILER_URL_OPTIONS
  config.action_mailer.delivery_method = :mailjet
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    address:     'in-v3.mailjet.com',
    port:        587,
    user_name:   Rails.application.secrets.mailjet_smtp_username,
    password:    Rails.application.secrets.mailjet_smtp_password,
    domain:      'myrenewaldiary.com'
  }

  config.i18n.fallbacks = true
  config.active_support.deprecation = :notify
  config.log_formatter = ::Logger::Formatter.new

  if ENV['RAILS_LOG_TO_STDOUT'].present?
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger    = ActiveSupport::TaggedLogging.new(logger)
  end

  config.active_record.dump_schema_after_migration = false
end
# rubocop:enable Metrics/BlockLength
