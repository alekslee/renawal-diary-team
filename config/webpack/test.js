process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const environment = require('./environment');

const config = environment.toWebpackConfig();

const entry = Object.keys(config.entry).reduce((memo, key) => {
  if (['serverRendering', 'bundles-styles', 'bundles', 'application'].includes(key)) {
    memo[key] = config.entry[key];
  }
  return memo;
}, {})
config.entry = entry;


module.exports = config;
