process.env.NODE_ENV = process.env.NODE_ENV || 'dll';

const { join } = require('path');
// const environment = require('./environment');
const { environment } = require('@rails/webpacker');

const defaults = require('lodash/defaultsDeep');
const webpack = require('webpack');
const pkg = require(join(process.cwd(), 'package.json'));
const dllPlugin = require('./dll/config').dllPlugin;

if (!pkg.dllPlugin) { process.exit(0); }


const dllConfig = defaults(pkg.dllPlugin, dllPlugin.defaults);
const outputPath = join(process.cwd(), dllConfig.path);
console.log(environment.module);
console.log('----------------');

const config = environment.toWebpackConfig();

config.devtool = 'eval';
config.entry = dllConfig.dlls ? dllConfig.dlls : dllPlugin.entry(pkg),
config.optimization.minimizer = [];
config.output = {
  filename: '[name].dll.js',
  path: outputPath,
  library: '[name]',
}

config.node = {
  fs: 'empty',
  module: 'empty',
  child_process: 'empty',
  jquery: 'empty',
  'node-gyp': 'empty',
}

// config.resolve = {
//   alias: {
//     jquery: "jquery/src/jquery"
//   },
// }
// config.resolve.extensions.splice(1, 0, ".mjs")
// = config.resolve.extensions.splice(".mjs")

config.plugins = [
  new webpack.DllPlugin({ name: '[name]', path: join(outputPath, '[name].json') })
  // new webpack.ProvidePlugin({
  //   $: 'jquery',
  //   jQuery: 'jquery'
  // }),
]

// module.loaders
config.module.rules = config.module.rules.slice(0, -1).concat({
   test: /\.mjs$/,
   include: /node_modules/,
   type: 'javascript/auto'
})
console.log(config.resolve.extensions);
// console.log(config.module.rules[config.module.rules.length - 1]);
// config.module.rules[config.module.rules.length - 1].use[0].options.babelrc = true
// console.log(config.module.rules[config.module.rules.length - 1].use);
// console.log(config.module.rules[config.module.rules.length - 1].use[]);

module.exports = config;
