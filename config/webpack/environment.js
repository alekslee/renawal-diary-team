const { environment } = require('@rails/webpacker');

environment.loaders.append('yml', {
  test: /\.yml$/,
  use: 'yml-loader',
});

environment.loaders.append('graphql', {
  test: /\.(graphql|gql)$/,
  exclude: /node_modules/,
  use: 'webpack-graphql-loader'
});

module.exports = environment;
