process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');
const smp = new SpeedMeasurePlugin();

const environment = require('./environment');
// console.log(environment.loaders);
const isDevServer = process.argv.find(v => v.includes('webpack-dev-server'));
if (isDevServer) environment.splitChunks();

const config = environment.toWebpackConfig();
// config.devtool = 'none';

const entry = Object.keys(config.entry).reduce((memo, key) => {
  if (['serverRendering', 'bundles-styles', 'bundles', 'application'].includes(key)) {
    memo[key] = config.entry[key];
  }
  return memo;
}, {})
config.entry = entry;

console.log(config.entry);
// console.log(config.plugins);

// "webpack": "^4.37.0",
// "webpack-cli": "^3.3.6",

module.exports = smp.wrap(config);